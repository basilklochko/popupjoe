﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace WebApp.Classes
{
    public static class Enums
    {
        public enum MenuStatus
        {
            [Description("Invisible")]
            Invisible,

            [Description("Read-only")]
            ReadOnly,

            [Description("Editable")]
            Editable,

            [Description("Full control")]
            FullControl
        };

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null && attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }        
    }
}