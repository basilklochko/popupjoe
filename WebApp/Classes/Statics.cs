﻿using System;

namespace WebApp.Classes
{
    public static class Statics
    {
        public static string GetApplicationName()
        {
            return "PopUpJoe Administration";
        }

        public static string GetSuccessWindow(string message)
        {
            return string.Format("$('#successMessage').html(\"{0}\"); $('#alertSuccess').modal('show');", message);
        }

        public static string GetWarningWindow(string message)
        {
            return string.Format("$('#warningMessage').html(\"{0}\"); $('#alertWarning').modal('show');", message);
        }

        public static decimal GetPages(long resultCount)
        {
            return Math.Ceiling((decimal)resultCount / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("PageSize")));
        }
    }
}