﻿using System.Linq;

namespace WebApp.Classes
{
    public class UserRights
    {
        public UserRights(string title)
        { 
            var menu = new MenuCookie().Get();
            var rights = menu.Menu.Where(o => o.Name.Contains(title)).FirstOrDefault().UserRights;

            if (rights == Enums.GetEnumDescription(Enums.MenuStatus.Editable) || rights == Enums.GetEnumDescription(Enums.MenuStatus.FullControl))
            {
                this.Editable = true;
            }

            if (rights == Enums.GetEnumDescription(Enums.MenuStatus.Invisible))
            {
                this.Invisible = true;
            }

            if (rights == Enums.GetEnumDescription(Enums.MenuStatus.FullControl))
            {
                this.FullControl = true;
            }

            if (rights == Enums.GetEnumDescription(Enums.MenuStatus.ReadOnly))
            {
                this.ReadOnly = true;
            }
        }

        public bool FullControl { get; set; }
        public bool ReadOnly { get; set; }
        public bool Editable { get; set; }
        public bool Invisible { get; set; }
    }
}