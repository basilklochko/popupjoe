﻿using System;
using System.Collections.Generic;
using Models.Models;
using WebApp.Services;

namespace WebApp.Classes
{
    [Serializable]
    public class MenuCookie
    {
        public List<MenuItem> Menu { set; get; }

        public MenuCookie Get()
        {
            MenuCookie result = new MenuCookie();

            var user = new UserCookie().Get().User;
            var response = new AccountService().GetMenuOptions(user).UserPermissions.Menu;
            result.Menu = response;
           
            return result;
        }

        //public MenuCookie Get()
        //{
        //    MenuCookie result = new MenuCookie();

        //    try
        //    {
        //        var cookie = (List<MenuItem>)JsonConvert.DeserializeObject(HttpContext.Current.Session["Menu"].ToString(), typeof(List<MenuItem>));

        //        result.Menu = cookie;
        //    }
        //    catch
        //    {
        //        System.Web.HttpContext.Current.Request.Cookies.Clear();
        //        System.Web.HttpContext.Current.Response.Cookies.Clear();
        //        FormsAuthentication.SignOut();

        //        System.Web.HttpContext.Current.Response.Redirect("~/Account/Login");
        //    }

        //    return result;
        //}

        //public void Set(List<MenuItem> menu)
        //{
        //    var q = (from m in menu
        //            select new { ID = m.ID, AppID = m.AppID, ParentID = m.ParentID, Name = m.Name, Path = m.Path, Order = m.Order, UserRights = m.UserRights }).ToList();

        //    HttpContext.Current.Session["Menu"] = JsonConvert.SerializeObject(q);
        //}

        //public MenuCookie Get()
        //{
        //    MenuCookie result = new MenuCookie();

        //    try 
        //    {
        //        var cookie = (List<MenuItem>)JsonConvert.DeserializeObject(System.Web.HttpContext.Current.Request.Cookies["Menu"].Value, typeof(List<MenuItem>));

        //        result.Menu = cookie;
        //    }
        //    catch 
        //    {
        //        System.Web.HttpContext.Current.Request.Cookies.Clear();
        //        System.Web.HttpContext.Current.Response.Cookies.Clear();
        //        FormsAuthentication.SignOut();

        //        //System.Web.HttpContext.Current.Response.Redirect("~/Account/Login");
        //    }

        //    return result;
        //}

        //public void Set(List<MenuItem> menu) 
        //{
        //    var q = (from m in menu
        //             select new { ID = m.ID, AppID = m.AppID, ParentID = m.ParentID, Name = m.Name, Path = m.Path, Order = m.Order, UserRights = m.UserRights }).ToList();

        //    HttpCookie cookie = new HttpCookie("Menu", JsonConvert.SerializeObject(q));
        //    cookie.Expires = DateTime.Today.AddMonths(12);

        //    System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        //}
    }
}