﻿using ServiceAccess;
using Models.Responses;
using Models.Requests;
using Newtonsoft.Json;
using Models.Models;

namespace WebApp.Services
{
    public class CouponService : BaseService
    {
        public CouponService()
        {
            Wrapper = new ServiceWrapper(ServiceName.CouponService);
        }

        public CouponListResponse GetCouponList(Domain domain, int pageIndex, int pageSize, string sortColumn, string sortDirection, bool hideRevoked)
        {
            return (CouponListResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetCouponList, new CouponListRequest(domain.ID, pageIndex, pageSize, sortColumn, sortDirection) { HideRevoked = hideRevoked }), typeof(CouponListResponse));
        }

        public CouponResponse GetCoupon(Coupon coupon)
        {
            return (CouponResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetCoupon, new CouponRequest(coupon)), typeof(CouponResponse));
        }

        public CouponResponse CreateCoupon(Coupon coupon)
        {
            return (CouponResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.CreateCoupon, new CouponRequest(coupon)), typeof(CouponResponse));
        }

        public SuccessFailResponse UpdateCoupon(Coupon coupon)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.UpdateCoupon, new CouponRequest(coupon)), typeof(SuccessFailResponse));
        }

        public SuccessFailResponse ChangeCouponStatus(Coupon coupon)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.ChangeCouponStatus, new CouponRequest(coupon)), typeof(SuccessFailResponse));
        }
    }
}