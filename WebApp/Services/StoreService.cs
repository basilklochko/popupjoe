﻿using ServiceAccess;
using Models.Responses;
using Models.Requests;
using Newtonsoft.Json;
using Models.Models;

namespace WebApp.Services
{
    public class StoreService : BaseService
    {
        public StoreService()
        {
            Wrapper = new ServiceWrapper(ServiceName.StoreService);
        }

        public StoreResponse AddStore(Store store)
        {
            return (StoreResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.AddStore, new StoreRequest(store)), typeof(StoreResponse));
        }

        public StoreResponse GetStore(Store store)
        {
            return (StoreResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetStore, new StoreRequest(store)), typeof(StoreResponse));
        }

        public StoreListResponse GetStoreListByUser(User user, bool hideRevoked = false)
        {
            return (StoreListResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetStoreListByUser, new UserRequest(user) { HideRevoked = hideRevoked }), typeof(StoreListResponse));
        }

        public SuccessFailResponse ChangeStoreStatus(Store store)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.ChangeStoreStatus, new StoreRequest(store)), typeof(SuccessFailResponse));
        }

        public SuccessFailResponse UpdateStore(Store store)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.UpdateStore, new StoreRequest(store)), typeof(SuccessFailResponse));
        }
    }
}