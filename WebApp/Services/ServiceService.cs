﻿using ServiceAccess;
using Models.Responses;
using Models.Requests;
using Newtonsoft.Json;

namespace WebApp.Services
{
    public class ServiceService : BaseService
    {
        public ServiceService()
        {
            Wrapper = new ServiceWrapper(ServiceName.ServiceService);
        }

        public LookUpResponse GetLookUp(string lookUpName)
        {
            return (LookUpResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetLookUp, new LookUpRequest(lookUpName)), typeof(LookUpResponse));
        }
    }
}