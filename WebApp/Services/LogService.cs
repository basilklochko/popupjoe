﻿using System;
using System.Configuration;
using ServiceAccess;

namespace WebApp.Services
{
    public class LogService : BaseService
    {
        public LogService()
        {
            Wrapper = new ServiceWrapper(ServiceName.LogService);
        }

        public void WriteEntry(string server, string message, string user)
        {
            if (Boolean.Parse(ConfigurationManager.AppSettings.Get("IsLoggingEnabled")))
            {
                Wrapper.HttpPostAsync(MethodName.WriteEntry,
                    "server", server, "message", message, "user", user);
            }
        }
    }
}