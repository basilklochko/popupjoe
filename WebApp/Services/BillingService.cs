﻿using ServiceAccess;
using Models.Responses;
using Models.Requests;
using Newtonsoft.Json;
using Models.Models;

namespace WebApp.Services
{
    public class BillingService : BaseService
    {
        public BillingService()
        {
            Wrapper = new ServiceWrapper(ServiceName.BillingService);
        }

        public BSettingListResponse GetBSettingList(Store store, int pageIndex, int pageSize, string sortColumn, string sortDirection, bool hideRevoked)
        {
            return (BSettingListResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetBSettingList, new BSettingListRequest(store.ID, pageIndex, pageSize, sortColumn, sortDirection) { HideRevoked = hideRevoked }), typeof(BSettingListResponse));
        }

        public BSettingResponse GetBSetting(BSetting bSetting)
        {
            return (BSettingResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetBSetting, new BSettingRequest(bSetting)), typeof(BSettingResponse));
        }

        public BSettingResponse CreateBSetting(BSetting bSetting)
        {
            return (BSettingResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.CreateBSetting, new BSettingRequest(bSetting)), typeof(BSettingResponse));
        }

        public SuccessFailResponse UpdateBSetting(BSetting bSetting)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.UpdateBSetting, new BSettingRequest(bSetting)), typeof(SuccessFailResponse));
        }

        public SuccessFailResponse ChangeBSettingStatus(BSetting bSetting)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.ChangeBSettingStatus, new BSettingRequest(bSetting)), typeof(SuccessFailResponse));
        }
    }
}