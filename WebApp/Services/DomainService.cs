﻿using ServiceAccess;
using Models.Responses;
using Models.Requests;
using Newtonsoft.Json;
using Models.Models;

namespace WebApp.Services
{
    public class DomainService : BaseService
    {
        public DomainService()
        {
            Wrapper = new ServiceWrapper(ServiceName.DomainService);
        }

        public DomainListResponse GetStoreIPList(Store store, int pageIndex, int pageSize, string sortColumn, string sortDirection, bool hideRevoked = false)
        {
            return (DomainListResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetStoreIPList, new DomainListRequest(store.ID, pageIndex, pageSize, sortColumn, sortDirection) { HideRevoked = hideRevoked }), typeof(DomainListResponse));
        }

        public DomainResponse GetDomain(Domain domain)
        {
            return (DomainResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetDomain, new DomainRequest(domain)), typeof(DomainResponse));
        }

        public DomainResponse CreateDomain(Domain domain)
        {
            return (DomainResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.CreateDomain, new DomainRequest(domain)), typeof(DomainResponse));
        }

        public SuccessFailResponse UpdateDomain(Domain domain)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.UpdateDomain, new DomainRequest(domain)), typeof(SuccessFailResponse));
        }

        public SuccessFailResponse ChangeDomainStatus(Domain domain)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.ChangeDomainStatus, new DomainRequest(domain)), typeof(SuccessFailResponse));
        }
    }
}