﻿using ServiceAccess;
using Models.Responses;
using Models.Requests;
using Newtonsoft.Json;
using Models.Models;

namespace WebApp.Services
{
    public class InvoiceService : BaseService
    {
        public InvoiceService()
        {
            Wrapper = new ServiceWrapper(ServiceName.InvoiceService);
        }

        public InvoiceListResponse GetUnInvoicedList(User user, int pageIndex, int pageSize, string sortColumn, string sortDirection, bool hideRevoked = false)
        {
            return (InvoiceListResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetUnInvoicedList, new InvoiceListRequest(user, pageIndex, pageSize, sortColumn, sortDirection) { HideRevoked = hideRevoked }), typeof(InvoiceListResponse));
        }

        public InvoiceListResponse GetInvoicedList(User user, int pageIndex, int pageSize, string sortColumn, string sortDirection)
        {
            return (InvoiceListResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetInvoicedList, new InvoiceListRequest(user, pageIndex, pageSize, sortColumn, sortDirection)), typeof(InvoiceListResponse));
        }

        public InvoiceListResponse GetInvoiceDetails(User user, Invoice invoice)
        {
            return (InvoiceListResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetInvoiceDetails, new InvoiceRequest((long)user.UserID, invoice)), typeof(InvoiceListResponse));
        }

        public ChargeListResponse GetChargeDetails(User user, Invoice invoice)
        {
            return (ChargeListResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetChargeDetails, new InvoiceRequest((long)user.UserID, invoice)), typeof(ChargeListResponse));
        }

        public InvoiceListResponse CreateInvoices(User user, bool close)
        {
            return (InvoiceListResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.CreateInvoices, new InvoiceListRequest() { User = user, InvoiceExisted = close }), typeof(InvoiceListResponse));
        }

        public SuccessFailResponse VoidInvoice(User user, Invoice invoice)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.VoidInvoice, new InvoiceRequest((long)user.UserID, invoice)), typeof(SuccessFailResponse));
        }
    }
}