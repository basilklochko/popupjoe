﻿using ServiceAccess;
using Newtonsoft.Json;
using Models.Models;
using Models.Responses;
using Models.Requests;

namespace WebApp.Services
{
    public class AccountService : BaseService
    {
        public AccountService()
        {
            Wrapper = new ServiceWrapper(ServiceName.AccountService);
        }

        public UserResponse ValidateUser(Login login)
        {
            return (UserResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.ValidateUser, new LoginRequest(login)), typeof(UserResponse));
        }

        public SuccessFailResponse ValidateSecureAnswer(PasswordReminder reminder)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.ValidateSecureAnswer, new PasswordReminderRequest(reminder)), typeof(SuccessFailResponse));
        }

        public UserMenuResponse GetMenuOptions(User user)
        {
            return (UserMenuResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetMenuOptions, new UserRequest(user)), typeof(UserMenuResponse));
        }

        public SuccessFailResponse ChangePassword(ChangePassword changePassword)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.ChangePassword, new ChangePasswordRequest(changePassword)), typeof(SuccessFailResponse));
        }

        public UserResponse CreateUser(User user)
        {
            return (UserResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.CreateUser, new UserRequest(user)), typeof(UserResponse));
        }

        public SuccessFailResponse UpdateUser(User user)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.UpdateUser, new UserRequest(user)), typeof(SuccessFailResponse));
        }

        public UserListResponse GetUserListByStore(User user, int pageIndex, int pageSize, string sortColumn, string sortDirection, bool hideRevoked)        
        {
            return (UserListResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.GetUserListByStore, new UserListRequest(user.StoreID, pageIndex, pageSize, sortColumn, sortDirection) { HideRevoked = hideRevoked }), typeof(UserListResponse));
        }

        public SuccessFailResponse UpdateMenuOptions(User user, MenuItemList list)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.UpdateMenuOptions, new MenuItemRequest() { UserUpdated = user.UserID, Menu = list }), typeof(SuccessFailResponse));
        }

        public SuccessFailResponse ChangeUserStatus(User user)
        {
            return (SuccessFailResponse)JsonConvert.DeserializeObject(Wrapper.HttpPost(MethodName.ChangeUserStatus, new UserRequest(user)), typeof(SuccessFailResponse));
        }
    }
}