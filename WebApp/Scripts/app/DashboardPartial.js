﻿google.load('visualization', '1', { packages: ['corechart'] });
google.load('visualization', '1', { packages: ['table'] });

$(document).ready(function () {
    dashboard.Init();
    dashboard.Render();
});

var dashboard = {
    sel: {
        storeSelect: '.storeSelect',
        domainSelect: '#domainSelect',
        start: '.start',
        end: '.end',
        chart: 'button#chart',
        table: 'button#table',
        csv: '#csv',
        warningMessage: '#warningMessage',
        alertWarning: '#alertWarning',
        close: '.close'
    },
    Init: function () {
        if ($(dashboard.sel.storeSelect).text() == 'PopUpJoe') {
            $(dashboard.sel.domainSelect).attr('disabled', 'disabled');
        }
        else {
            $(dashboard.sel.domainSelect).removeAttr('disabled');
        }

        if ($(dashboard.sel.start).val() == '') {
            $(dashboard.sel.start).val('1/1/' + new Date().getFullYear());
        }

        $(dashboard.sel.start).attr('readonly', 'readonly');
        $(dashboard.sel.start).css('cursor', 'pointer');
        $(dashboard.sel.start).addClass('input-small');

        $(dashboard.sel.start).datepicker({
            showAnim: 'slideDown',
            dateFormat: 'mm/dd/yy'
        });

        if ($(dashboard.sel.end).val() == '') {
            $(dashboard.sel.end).val(new Date().getMonth() + 1 + '/' + new Date().getDate() + '/' + new Date().getFullYear());
        }

        $(dashboard.sel.end).attr('readonly', 'readonly');
        $(dashboard.sel.end).css('cursor', 'pointer');
        $(dashboard.sel.end).addClass('input-small');

        $(dashboard.sel.end).datepicker({
            showAnim: 'slideDown',
            dateFormat: 'mm/dd/yy'
        });

        $(dashboard.sel.chart).bind('click', function () {
            $(dashboard.sel.table).removeClass('active');
            $(dashboard.sel.chart).addClass('active');
            dashboard.Render();
        });
        $(dashboard.sel.table).bind('click', function () {
            $(dashboard.sel.chart).removeClass('active');
            $(dashboard.sel.table).addClass('active');
            dashboard.Render();
        });
        $(dashboard.sel.csv).bind('click', function () {
            dashboard.ExportCSV();
        });
    },
    CheckDates: function () {
        if (new Date($(dashboard.sel.end).val()) < new Date($(dashboard.sel.start).val())) {
            $(dashboard.sel.warningMessage).html('Please select valid date range!');
            $(dashboard.sel.alertWarning).removeClass('hide');

            $(dashboard.sel.warningMessage).parent().find(dashboard.sel.close).unbind();
            $(dashboard.sel.warningMessage).parent().find(dashboard.sel.close).bind('click', function (event) {
                $(dashboard.sel.warningMessage).html('');
                $(dashboard.sel.alertWarning).addClass('hide');
            });

            layout.Loaded();

            return false;
        }

        return true;
    },
    Render: function () {
        if (dashboard.CheckDates()) {
            gchart.Dashboard.Render();
        }
    },
    ExportCSV: function () {
        if (dashboard.CheckDates()) {
            gchart.Dashboard.ExportCSV();
        }
    }
};