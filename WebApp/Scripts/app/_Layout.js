﻿$(document).ready(function () {
    layout.Init();
});

var layout = {
    sel: {
        dropdownToggle: '.dropdown-toggle',
        button: 'button',
        form: 'form',
        a: 'a',
    },
    Init: function () {
        $(layout.sel.dropdownToggle).dropdown();

        $(layout.sel.a).bind('click', function (e) {
            if ($(e.target).attr('href') != '' && $(e.target).attr('href') != '#' && $(e.target).attr('href') != undefined) {
                layout.Loading();
            }
        });

        $(layout.sel.button).bind('click', function (e) {
            if (!$(e.target).hasClass('close')) {
                if ($(layout.sel.form).valid()) {
                    layout.Loading();
                }
            }
        });
    },
    Loading: function () {
        $.blockUI({ 
            message: "<div id='facebook'><div id='block_1' class='facebook_block'></div><div id='block_2' class='facebook_block'></div><div id='block_3' class='facebook_block'></div><h3>Please wait...</h3></div>",
            css: {            
                border: 'none',
                padding: '15px',
                height: '80px',
                top:  ($(window).height() - 400) /2 + 'px', 
                left: ($(window).width() - 400) /2 + 'px', 
                width: '200px',
                backgroundColor: '#fff',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                //color: '#fff'
        }
        });
    },
    Loaded: function () {
        $.unblockUI();
    }
};