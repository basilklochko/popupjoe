﻿$(document).ready(function () {
    login.Init();
});

var login = {
    sel: {

    },
    Init: function () {
        login.InitCache();
        layout.Loaded();
    },
    InitCache: function () {
        $.ajax({
            async: true,
            cache: false,
            type: "POST",
            url: Routes.InitCache
        });        
    }
};