﻿var domainPartial = {
    sel: {
        activate: '#activate',
        revoke: '#revoke',
        domainId: '#ID'
    },
    Init: function () {
        $(domainPartial.sel.activate).live('click', function () {
            window.location.href = Routes.ActivateDomain + $(domainPartial.sel.domainId).val();
        });

        $(domainPartial.sel.revoke).live('click', function () {
            window.location.href = Routes.RevokeDomain + $(domainPartial.sel.domainId).val();
        });

        layout.Loaded();
    }
};