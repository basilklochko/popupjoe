﻿var couponPartial = {
    sel: {
        activate: '#activate',
        revoke: '#revoke',
        couponId: '#ID'
    },
    Init: function () {
        $(couponPartial.sel.activate).live('click', function () {
            window.location.href = Routes.ActivateCoupon + $(couponPartial.sel.couponId).val();
        });
        $(couponPartial.sel.revoke).live('click', function () {
            window.location.href = Routes.RevokeCoupon + $(couponPartial.sel.couponId).val();
        });

        layout.Loaded();
    }
};