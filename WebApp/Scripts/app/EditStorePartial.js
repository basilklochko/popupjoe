﻿$(document).ready(function () {
    storePartial.Init();
});

var storePartial = {
    sel: {
        expiration: "#ContractExpirationDate",
        activate: '#activate',
        revoke: '#revoke',
        countrySelect: '#countrySelect',
        stateSelect: '#stateSelect',
        storeId: '#ID',
        jsStateList: '#jsStateList',
        selected: '#selected'
    },
    Init: function () {
        storePartial.RepopulateStateList();

        $(storePartial.sel.expiration).attr('readonly', 'readonly');
        $(storePartial.sel.expiration).css('cursor', 'pointer');
        $(storePartial.sel.expiration).addClass('input-small');

        $(storePartial.sel.expiration).datepicker({
            showAnim: 'slideDown',
            dateFormat: 'mm/dd/yy'
        });

        $(storePartial.sel.activate).bind('click', function () {
            window.location.href = Routes.ActivateStore + $(storePartial.sel.storeId).val();
        });
        $(storePartial.sel.revoke).bind('click', function () {
            window.location.href = Routes.RevokeStore + $(storePartial.sel.storeId).val();
        });

        $(storePartial.sel.countrySelect).change(function () {
            storePartial.RepopulateStateList();
        });

        layout.Loaded();
    },
    RepopulateStateList: function () {
        if ($(storePartial.sel.jsStateList).length > 0) {
            var allStates = JSON.parse($(storePartial.sel.jsStateList).val());

            $(storePartial.sel.stateSelect).find('option').remove();

            $.each(allStates, function (index, value) {
                if (value.Col1 == $(storePartial.sel.countrySelect).val()) {
                    var selected = value.Value == $(storePartial.sel.selected).val() ? "selected" : "";
                    $(storePartial.sel.stateSelect).append($("<option " + selected + " />").val(value.Value).text(value.Value + " - " + value.Description));
                }
            });
        }
    }
};
