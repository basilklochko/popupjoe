﻿var billingPartial = {
    sel: {
        activate: '#activate',
        revoke: '#revoke',
        billingId: '#ID'
    },
    Init: function () {
        $(billingPartial.sel.activate).live('click', function () {
            window.location.href = Routes.ActivateBilling + $(billingPartial.sel.billingId).val();
        });

        $(billingPartial.sel.revoke).live('click', function () {
            window.location.href = Routes.RevokeBilling + $(billingPartial.sel.billingId).val();
        });

        layout.Loaded();
    }
};