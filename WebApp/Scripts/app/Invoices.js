﻿$(document).ready(function () {
    invoices.Init();
    //domainPartial.Init();
});

var invoices = {
    sel: {
        uninvoiced: '.uninvoiced',
        invoiced: '.invoiced',
        modalLink: '.modalLink',
        viewInvoice: '#viewInvoice',
        viewCharge: '#viewCharge',
        tabContent: '.tab-content',
        modalBody: '.modal-body',
        table: '.table-striped',
        invoiceNumber: '#invoiceNumber',
        close: ".close",
        ppjStoreID: '#ppjStoreID',
        storeID: '#storeID',
        userID: '#userID',
        charge: '#charge',
        voider: '#void',
        csv: "#csv",
        downloadCSV: '#downloadCSV',
        btnClose: '#btnClose',
        divRevoked: '#divRevoked',
        hideRevoked: '#hideRevoked',
        pdf: '.pdf',
        trError: 'tr[error=error]',
        td: 'td',
        bill: '#bill',
        billClose: '#billClose',
        trMatched: 'tr[matched=matched]'
    },
    Init: function () {
        $('.fade').removeClass('fade');

        $(invoices.sel.hideRevoked).live('click', function () {
            layout.Loading();
            document.location = Routes.Invoices + "?hideRevoked=" + $(invoices.sel.hideRevoked).is(':checked');
        });

        $(invoices.sel.close).live('click', function () {
            layout.Loaded();
        });

        $(invoices.sel.uninvoiced).live('click', function () {
            $(invoices.sel.divRevoked).show();
            layout.Loaded();
        });

        $(invoices.sel.invoiced).live('click', function () {
            $(invoices.sel.divRevoked).hide();
            layout.Loaded();
        });

        $(invoices.sel.btnClose).live('click', function () {
            layout.Loaded();
        });

        $(invoices.sel.modalLink).live('click', function () {
            invoices.ShowInvoiceDetails($(this));
        });

        $(invoices.sel.pdf).bind('click', function () {
            invoices.GeneratePDF($(this));
        });

        $(invoices.sel.csv).bind('click', function () {
            invoices.ExportCSV();
        });

        $(invoices.sel.charge).live('click', function () {
            invoices.ShowChargeDetails($(this));
        });

        $(invoices.sel.voider).live('click', function () {
            invoices.VoidInvoice();
        });

        $(invoices.sel.trError).children(invoices.sel.td).css('background-color', 'silver');
        $(invoices.sel.trError).children(invoices.sel.td).css('color', 'white');

        $(invoices.sel.trMatched).children(invoices.sel.td).css('background-color', 'yellow');
        $(invoices.sel.trMatched).children(invoices.sel.td).css('color', 'white');

        var url = window.location.href;

        if (url.indexOf('ubPageIndex') > -1) {
            if (!$(invoices.sel.uninvoiced).parent().hasClass('active')) {
                $(invoices.sel.uninvoiced).click();
            }
        }
        else if (url.indexOf('bPageIndex') > -1) {
            if (!$(invoices.sel.invoiced).parent().hasClass('active')) {
                $(invoices.sel.invoiced).click();
            }
        }

        $(invoices.sel.bill).bind('click', function () {
            window.location.href = Routes.BillInvoice + "?close=false";
        });

        $(invoices.sel.billClose).bind('click', function () {
            window.location.href = Routes.BillInvoice + "?close=true";
        });

        layout.Loaded();
    },
    ShowInvoiceDetails: function (link) {
        $(invoices.sel.viewInvoice).modal({
            keyboard: false
        })
        $(invoices.sel.viewInvoice).modal('show');

        $(invoices.sel.viewInvoice).find(invoices.sel.invoiceNumber).text(link.attr("invoiceid"));

        var url = Routes.InvoiceDetails + link.attr("invoiceid");

        layout.Loading();

        $.get(url, function (response) {
            $(invoices.sel.viewInvoice).find(invoices.sel.modalBody).html(response);

            $(invoices.sel.viewInvoice).find(invoices.sel.table).fixheadertable({
                height: 200
            });

            layout.Loaded();
        });
    },
    ShowChargeDetails: function (link) {
        $(invoices.sel.viewCharge).modal({
            keyboard: false
        });
        $(invoices.sel.viewCharge).modal('show');

        $(invoices.sel.viewCharge).find(invoices.sel.invoiceNumber).text($(invoices.sel.viewInvoice).find(invoices.sel.invoiceNumber).text());

        var url = Routes.ChargeDetails + $(invoices.sel.viewCharge).find(invoices.sel.invoiceNumber).text();

        layout.Loading();

        $.get(url, function (response) {
            $(invoices.sel.viewCharge).find(invoices.sel.modalBody).html(response);

            $(invoices.sel.viewCharge).find(invoices.sel.table).fixheadertable({
                height: 175,
                width: 2000
            });

            layout.Loaded();
        });
    },
    VoidInvoice: function () {
        var url = Routes.VoidInvoice + $(invoices.sel.viewInvoice).find(invoices.sel.invoiceNumber).text();

        layout.Loading();

        $.get(url, function (response) {
            $(invoices.sel.viewInvoice).modal('hide');
            layout.Loaded();

            response;
        });
    },
    ExportCSV: function () {
        var id = $(invoices.sel.viewInvoice).find(invoices.sel.invoiceNumber).text();

        document.location = Routes.InvoiceDetailsCSV + "?id=" + id + "&userID=" + $(invoices.sel.userID).val();

        layout.Loaded();
    },
    GeneratePDF: function (link) {
        document.location = Routes.InvoicePDF + "?id=" + link.attr('invoiceid') + "&ppjStoreID=" + $(invoices.sel.ppjStoreID).val() + "&storeID=" + link.attr('storeid') + "&userID=" + $(invoices.sel.userID).val();

        layout.Loaded();
    }
};