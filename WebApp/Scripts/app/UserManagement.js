﻿$(document).ready(function () {
    user.Init();
    userPartial.Init();
});

var user = {
    sel: {
        grid: '.grid',
        createUser: '#createUser',
        divRevoked: '#divRevoked',
        hideRevoked: '#hideRevoked'
    },
    Init: function () {
        $(user.sel.hideRevoked).live('click', function () {
            layout.Loading();
            document.location = Routes.UserManagement + "?hideRevoked=" + $(user.sel.hideRevoked).is(':checked');
        });

        $(user.sel.grid).bind('click', function () {
            $(user.sel.createUser).css('display', 'block');
            layout.Loaded();
        });

        $(user.sel.createUser).bind('click', function () {
            $(user.sel.createUser).css('display', 'none');
            layout.Loaded();
        });

        layout.Loaded();
    }
};