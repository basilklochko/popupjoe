﻿$(document).ready(function () {
    coupon.Init();
    couponPartial.Init();
});

var coupon = {
    sel: {
        grid: '.grid',
        createCoupon: '#createCoupon',
        domainSelect: '#domainSelect',
        select: 'select',
        domainId: '#domainId',
        divRevoked: '#divRevoked',
        hideRevoked: '#hideRevoked'
    },
    Init: function () {
        $(coupon.sel.hideRevoked).live('click', function () {
            layout.Loading();
            document.location = Routes.CouponSettings + "?hideRevoked=" + $(coupon.sel.hideRevoked).is(':checked');
        });

        if ($(coupon.sel.domainId).val() != '') {
            $(coupon.sel.domainSelect).val($(coupon.sel.domainId).val());
        }

        $(coupon.sel.grid).bind('click', function () {
            $(coupon.sel.createCoupon).css('display', 'block');
            layout.Loaded();
        });

        $(coupon.sel.createCoupon).bind('click', function () {
            $(coupon.sel.createCoupon).css('display', 'none');
            layout.Loaded();
        });

        $(coupon.sel.domainSelect).bind('change', function () {
            window.location.href = Routes.CouponSettings + $(coupon.sel.domainSelect).val();
        });

        $(coupon.sel.select).bind('change', function (e) {
            layout.Loading();
        });

        layout.Loaded();
    }
};