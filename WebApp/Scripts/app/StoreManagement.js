﻿$(document).ready(function () {
    store.Init();
});

var store = {
    sel: {
        tree: "#tree",
        link: ":link[data-ajax-update]",
        divTreeBtns: '#divTreeBtns',
        addStore: '#addStore',
        routeId: '#routeId',
        hideRevoked: '#hideRevoked'
    },
    Init: function () {
        $(store.sel.tree).treeview({
            animated: 'fast'
        });

        $(store.sel.link).each(function (index) {
            $(this).bind('click', function () {
                $(this).css('color', 'black');
                var selIndex = index;

                $(store.sel.divTreeBtns).css('display', 'block');

                $(store.sel.link).each(function (index) {
                    if (index != selIndex) {
                        $(this).css('color', '');
                    }
                });
            });
        });

        $(store.sel.hideRevoked).live('click', function () {
            layout.Loading();
            document.location = Routes.StoreManagement + "?hideRevoked=" + $(store.sel.hideRevoked).is(':checked');
        });

        $(store.sel.link).each(function (index) {
            if ($(this).attr("href").indexOf("EditStore/" + $(store.sel.routeId).val()) > 0) {
                $(this).trigger('click');
            }
        });

        $(store.sel.addStore).bind('click', function () {
            var parentId = 0;

            $(store.sel.link).each(function (index) {
                if ($(this).css('color') == 'rgb(0, 0, 0)') {
                    parentId = $(this).attr("href").substring($(this).attr("href").lastIndexOf("/") + 1);
                }
            });

            window.location.href = Routes.AddStore + parentId;
        });

        layout.Loaded();
    }
};