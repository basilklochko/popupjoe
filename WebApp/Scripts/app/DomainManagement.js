﻿$(document).ready(function () {
    domain.Init();
    domainPartial.Init();
});

var domain = {
    sel: {
        grid: '.grid',
        createDomain: '#createDomain',
        hideRevoked: '#hideRevoked'
    },
    Init: function () {
        $(domain.sel.hideRevoked).live('click', function () {
            layout.Loading();
            document.location = Routes.DomainManagement + "?hideRevoked=" + $(domain.sel.hideRevoked).is(':checked');
        });

        $(domain.sel.grid).bind('click', function () {
            $(domain.sel.createDomain).css('display', 'block');
            layout.Loaded();
        });

        $(domain.sel.createDomain).bind('click', function () {
            $(domain.sel.createDomain).css('display', 'none');
            layout.Loaded();
        });

        layout.Loaded();
    }
};