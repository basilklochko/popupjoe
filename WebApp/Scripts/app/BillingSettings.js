﻿$(document).ready(function () {
    billing.Init();
    billingPartial.Init();
});

var billing = {
    sel: {
        grid: '.grid',
        createBSetting: '#createBSetting',
        divRevoked: '#divRevoked',
        hideRevoked: '#hideRevoked'
    },
    Init: function () {
        $(billing.sel.hideRevoked).live('click', function () {
            layout.Loading();
            document.location = Routes.BillingSettings + "?hideRevoked=" + $(billing.sel.hideRevoked).is(':checked');
        });

        $(billing.sel.grid).bind('click', function () {
            $(billing.sel.createBSetting).css('display', 'block');
            layout.Loaded();
        });

        $(billing.sel.createBSetting).bind('click', function () {
            $(billing.sel.createBSetting).css('display', 'none');
            layout.Loaded();
        });

        layout.Loaded();
    }
};