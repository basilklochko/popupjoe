﻿var userPartial = {
    sel: {
        activate: '#activate',
        revoke: '#revoke',
        userid: '#User_UserID'
    },
    Init: function () {
        $(userPartial.sel.activate).live('click', function () {
            window.location.href = Routes.ActivateAccount + $(userPartial.sel.userid).val();
        });

        $(userPartial.sel.revoke).live('click', function () {
            window.location.href = Routes.RevokeAccount + $(userPartial.sel.userid).val();
        });

        layout.Loaded();
    }
};