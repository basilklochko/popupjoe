﻿using System.Web.Mvc;

namespace WebApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return RedirectToAction("Index");
        }

        public PartialViewResult DashboardPartial()
        {
            return PartialView();
        }
    }
}
