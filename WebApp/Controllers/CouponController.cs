﻿using System;
using System.Configuration;
using System.Web.Mvc;
using Models.Models;
using WebApp.Services;
using WebApp.Classes;

namespace WebApp.Controllers
{
    [Authorize]
    public class CouponController : Controller
    {
        public ActionResult CouponSettings(long? id, int pageIndex = 1, string sortColumn = "ID", string sortDirection = "DESC", bool hideRevoked = false)
        {
            if (id == null)
            {
                var user = new UserCookie().Get().User;

                var domain = new DomainService().GetStoreIPList(new Store() { ID = user.StoreID }, 1, 1, "Name", "ASC").DomainList.List;

                if (domain.Count > 0)
                {
                    id = domain[0].ID;
                }
                else
                {
                    return RedirectToAction("DomainManagement", "Domain");
                }
            }

            ViewBag.DomainID = id;

            var model = new CouponService().GetCouponList(new Domain() { ID = (long)id }, pageIndex, Convert.ToInt32(ConfigurationManager.AppSettings.Get("PageSize")), sortColumn, sortDirection, hideRevoked).CouponList;

            ViewBag.hideRevoked = hideRevoked;

            return View(model);
        }

        public ActionResult ChangeDomain(long id)
        {
            return RedirectToAction("CouponSettings", new { id = id });
        }

        public PartialViewResult CreateCoupon(long id)
        {
            return PartialView("EditCouponPartial", new Coupon() { ID = 0, DomainID = id, ExpirationDate = DateTime.Now });
        }

        [HttpPost]
        public ActionResult CreateCoupon(Coupon model)
        {
            if (ModelState.IsValid)
            {
                var cookie = new UserCookie().Get().User;
                model.UserUpdated = cookie.UserID;

                var response = new CouponService().CreateCoupon(model);

                if (response.ErrorNumber > 0)
                {
                    return JavaScript(Statics.GetWarningWindow(response.ErrorMessage));
                }
            }
            else
            {
                return JavaScript(Statics.GetWarningWindow("The form you're trying to submit containts validation errors!"));
            }

            return JavaScript(String.Format("window.location.href='{0}{1}'", this.HttpContext.Request.ApplicationPath, "/Coupon/CouponSettings"));
        }

        public PartialViewResult EditViewCoupon(long? id)
        {
            var model = new Coupon();

            if (id != null && id > 0)
            {
                model = new CouponService().GetCoupon(new Coupon() { ID = (long)id }).Coupon;
            }

            return PartialView("EditCouponPartial", model);
        }

        [HttpPost]
        public ActionResult EditCoupon(Coupon model)
        {
            if (ModelState.IsValid)
            {
                var cookie = new UserCookie().Get().User;
                model.UserUpdated = cookie.UserID;

                var response = new CouponService().UpdateCoupon(model);

                if (response.ErrorNumber > 0)
                {
                    return JavaScript(Statics.GetWarningWindow(response.ErrorMessage));
                }
            }
            else
            {
                return JavaScript(Statics.GetWarningWindow("The form you're trying to submit containts validation errors!"));
            }

            return JavaScript(String.Format("window.location.href='{0}{1}'", this.HttpContext.Request.ApplicationPath, "/Coupon/CouponSettings"));
        }

        public ActionResult Revoke(long id)
        {
            var cookie = new UserCookie().Get().User;
            var response = new CouponService().ChangeCouponStatus(new Coupon() { ID = id, UserUpdated = cookie.UserID, RevokeStatus = true });

            return RedirectToAction("CouponSettings");
        }

        public ActionResult Activate(long id)
        {
            var cookie = new UserCookie().Get().User;
            var response = new CouponService().ChangeCouponStatus(new Coupon() { ID = id, UserUpdated = cookie.UserID, RevokeStatus = false });

            return RedirectToAction("CouponSettings");
        }
    }
}
