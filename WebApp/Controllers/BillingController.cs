﻿using System;
using System.Web.Mvc;
using WebApp.Classes;
using WebApp.Services;
using Models.Models;
using System.Configuration;

namespace WebApp.Controllers
{
    [Authorize]
    public class BillingController : Controller
    {
        public ActionResult BillingSettings(int pageIndex = 1, string sortColumn = "BillingPeriodName", string sortDirection = "DESC", bool hideRevoked = false)
        {
            var user = new UserCookie().Get().User;
            var response = new BillingService().GetBSettingList(new Store() { ID = user.StoreID }, pageIndex, Convert.ToInt32(ConfigurationManager.AppSettings.Get("PageSize")), sortColumn, sortDirection, hideRevoked);

            if (response.ErrorNumber > 0)
            {
                return RedirectToAction("StoreManagement", "Store");
            }

            ViewBag.hideRevoked = hideRevoked;

            return View(response.BSettingList);
        }

        public PartialViewResult CreateBSetting()
        {
            return PartialView("EditBSettingPartial", new BSetting() { ID = 0, Date = DateTime.Now, LastBilledDate = DateTime.Now });
        }

        [HttpPost]
        public ActionResult CreateBSetting(BSetting model)
        {
            if (ModelState.IsValid)
            {
                var cookie = new UserCookie().Get().User;
                model.UserUpdated = cookie.UserID;
                model.StoreID = cookie.StoreID;

                var response = new BillingService().CreateBSetting(model);

                if (response.ErrorNumber > 0)
                {
                    return JavaScript(Statics.GetWarningWindow(response.ErrorMessage));
                }
            }
            else
            {
                return JavaScript(Statics.GetWarningWindow("The form you're trying to submit containts validation errors!"));
            }

            return JavaScript(String.Format("window.location.href='{0}{1}'", this.HttpContext.Request.ApplicationPath, "/Billing/BillingSettings"));
        }

        public PartialViewResult EditViewBSetting(long? id)
        {
            var model = new BSetting();

            if (id != null && id > 0)
            {
                model = new BillingService().GetBSetting(new BSetting() { ID = (long)id }).BSetting;
            }

            return PartialView("EditBSettingPartial", model);
        }

        [HttpPost]
        public ActionResult EditBSetting(BSetting model)
        {
            if (ModelState.IsValid)
            {
                var cookie = new UserCookie().Get().User;
                model.UserUpdated = cookie.UserID;

                var response = new BillingService().UpdateBSetting(model);

                if (response.ErrorNumber > 0)
                {
                    return JavaScript(Statics.GetWarningWindow(response.ErrorMessage));
                }
            }
            else
            {
                return JavaScript(Statics.GetWarningWindow("The form you're trying to submit containts validation errors!"));
            }

            return JavaScript(String.Format("window.location.href='{0}{1}'", this.HttpContext.Request.ApplicationPath, "/Billing/BillingSettings"));
        }

        public ActionResult Revoke(long id)
        {
            var cookie = new UserCookie().Get().User;
            var response = new BillingService().ChangeBSettingStatus(new BSetting() { ID = id, UserUpdated = cookie.UserID, RevokeStatus = true });

            return RedirectToAction("BillingSettings");
        }

        public ActionResult Activate(long id)
        {
            var cookie = new UserCookie().Get().User;
            var response = new BillingService().ChangeBSettingStatus(new BSetting() { ID = id, UserUpdated = cookie.UserID, RevokeStatus = false });

            return RedirectToAction("BillingSettings");
        }
    }
}
