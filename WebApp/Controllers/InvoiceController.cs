﻿using System.Web.Mvc;
using WebApp.Services;
using WebApp.Classes;
using Models.Models;
using System;
using System.Configuration;
using Models.Responses;

namespace WebApp.Controllers
{
    [Authorize]
    public class InvoiceController : Controller
    {
        public ActionResult Invoices(int ubPageIndex = 1, int bPageIndex = 1, string sortColumn = "", string sortDirection = "", bool hideRevoked = false, bool? close = null)
        {
            ViewBag.user = new UserCookie().Get().User;

            var createdInvoices = new InvoiceListResponse();

            if (close.HasValue) 
            {
                createdInvoices = new InvoiceService().CreateInvoices((User)ViewBag.user, close.Value);
            }

            ViewBag.createdInvoices = createdInvoices;

            ViewBag.ubPageIndex = ubPageIndex;
            ViewBag.bPageIndex = bPageIndex;
            ViewBag.sortColumn = sortColumn;
            ViewBag.sortDirection = sortDirection;
            ViewBag.hideRevoked = hideRevoked;

            return View();
        }

        public PartialViewResult UnInvoicedPartial(UserRights rights, User user, int pageIndex, string sortColumn, string sortDirection, bool hideRevoked)
        {
            var response = new InvoiceService().GetUnInvoicedList(user, pageIndex, Convert.ToInt32(ConfigurationManager.AppSettings.Get("PageSize")), sortColumn, sortDirection, hideRevoked);

            ViewBag.rights = rights;

            return PartialView(response.InvoiceList);
        }

        public PartialViewResult InvoicedPartial(UserRights rights, User user, int pageIndex, string sortColumn, string sortDirection, InvoiceListResponse createdInvoices = null)
        {
            var response = new InvoiceService().GetInvoicedList(user, pageIndex, Convert.ToInt32(ConfigurationManager.AppSettings.Get("PageSize")), sortColumn, sortDirection);

            ViewBag.rights = rights;
            ViewBag.userID = user.UserID;
            ViewBag.ppjStoreID = user.ppjStoreID;
            ViewBag.createdInvoices = createdInvoices;

            return PartialView(response.InvoiceList);
        }

        public PartialViewResult InvoiceDetails(long id)
        {
            var user = new UserCookie().Get().User;
            var response = new InvoiceService().GetInvoiceDetails(user, new Invoice() { ID = id });

            return PartialView(response.InvoiceList);
        }

        public PartialViewResult ChargeDetails(long id)
        {
            var user = new UserCookie().Get().User;
            var response = new InvoiceService().GetChargeDetails(user, new Invoice() { ID = id });

            return PartialView(response.ChargeList);
        }

        public ActionResult CreateInvoices(bool close) 
        {
            return RedirectToAction("Invoices", new { bPageIndex = 1, close = close });
        }

        public ActionResult VoidInvoice(long id)
        {
            string result = string.Empty;

            var user = new UserCookie().Get().User;
            var response = new InvoiceService().VoidInvoice(user, new Invoice() { ID = id });

            if (response.IsSuccessful)
            {
                result = Statics.GetSuccessWindow("The invoice has been successfully voided.");
            }
            else
            {
                result = Statics.GetWarningWindow(response.ErrorMessage);
            }

            return JavaScript(result);
        }
    }
}
