﻿using System.Web.Mvc;
using WebApp.Services;
using WebApp.Classes;
using Models.Models;
using System.Configuration;
using System;
using System.Web.UI;

namespace WebApp.Controllers
{
    [Authorize]
    public class DomainController : Controller
    {
        public ActionResult DomainManagement(int pageIndex = 1, string sortColumn = "IPAddress", string sortDirection = "DESC", bool hideRevoked = false)
        {
            var user = new UserCookie().Get().User;
            var response = new DomainService().GetStoreIPList(new Store() { ID = user.StoreID }, pageIndex, Convert.ToInt32(ConfigurationManager.AppSettings.Get("PageSize")), sortColumn, sortDirection, hideRevoked);

            if (response.ErrorNumber > 0)
            {
                return RedirectToAction("StoreManagement", "Store");
            }

            ViewBag.hideRevoked = hideRevoked;

            return View(response.DomainList);
        }

        public PartialViewResult CreateDomain()
        {
            return PartialView("EditDomainPartial", new Domain() { ID = 0 });
        }

        [HttpPost]
        public ActionResult CreateDomain(Domain model)
        {
            if (ModelState.IsValid)
            {
                var cookie = new UserCookie().Get().User;
                model.UserUpdated = cookie.UserID;
                model.StoreID = cookie.StoreID;

                var response = new DomainService().CreateDomain(model);

                if (response.ErrorNumber > 0)
                {
                    return JavaScript(Statics.GetWarningWindow(response.ErrorMessage));
                }
            }
            else
            {
                return JavaScript(Statics.GetWarningWindow("The form you're trying to submit containts validation errors!"));
            }

            return JavaScript(String.Format("window.location.href='{0}{1}'", this.HttpContext.Request.ApplicationPath, "/Domain/DomainManagement"));
        }

        public PartialViewResult EditViewDomain(long? id)
        {
            var model = new Domain();

            if (id != null && id > 0)
            {
                model = new DomainService().GetDomain(new Domain() { ID = (long)id }).Domain;
            }

            return PartialView("EditDomainPartial", model);
        }

        [HttpPost]
        public ActionResult EditDomain(Domain model)
        {
            if (ModelState.IsValid)
            {
                var cookie = new UserCookie().Get().User;
                model.UserUpdated = cookie.UserID;

                var response = new DomainService().UpdateDomain(model);

                if (response.ErrorNumber > 0)
                {
                    return JavaScript(Statics.GetWarningWindow(response.ErrorMessage));
                }
            }
            else
            {
                return JavaScript(Statics.GetWarningWindow("The form you're trying to submit containts validation errors!"));
            }

            return JavaScript(String.Format("window.location.href='{0}{1}'", this.HttpContext.Request.ApplicationPath, "/Domain/DomainManagement"));
        }

        public ActionResult Revoke(long id)
        {
            var cookie = new UserCookie().Get().User;
            var response = new DomainService().ChangeDomainStatus(new Domain() { ID = id, UserUpdated = cookie.UserID, RevokeStatus = true });

            return RedirectToAction("DomainManagement");
        }

        public ActionResult Activate(long id)
        {
            var cookie = new UserCookie().Get().User;
            var response = new DomainService().ChangeDomainStatus(new Domain() { ID = id, UserUpdated = cookie.UserID, RevokeStatus = false });

            return RedirectToAction("DomainManagement");
        }
    }
}
