﻿using System;
using System.Web;
using System.Web.Mvc;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            if (Session["LastError"] != null)
            {
                Exception ex = (Exception)Session["LastError"];

                new LogService().WriteEntry(System.Environment.MachineName, ex.Message, HttpContext.User.Identity.Name);

                return View(ex);
            }
            else
            {
                return View();
            }
        }
    }
}
