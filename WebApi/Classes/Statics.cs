﻿using System;
using System.Collections.Generic;
using Models.Responses;
using System.Data.SqlClient;
using DataAccess;

namespace WebApi.Classes
{
    public static class Statics
    {
        public static void AddDebugParameters(List<SqlParameter> parameters)
        {
            parameters.Add(new SqlParameter("iRetCode", 0) { Direction = System.Data.ParameterDirection.Output });
            parameters.Add(new SqlParameter("szMsgTier1", string.Empty) { Direction = System.Data.ParameterDirection.Output, Size = 2048 });
            parameters.Add(new SqlParameter("szMsgTier2", string.Empty) { Direction = System.Data.ParameterDirection.Output, Size = 2048 });
        }

        public static void FetchDebugParameters(BaseResponse response, Result result)
        {
            response.ErrorNumber = Convert.ToInt32(result.Parameters["iRetCode"].Value);
            response.ErrorMessage = result.Parameters["szMsgTier2"].Value.ToString();
        } 
    }
}