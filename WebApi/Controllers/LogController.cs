﻿using System.Collections.Generic;
using System.Web.Http;
using DataAccess;
using System.Data.SqlClient;

namespace WebApi.Controllers
{
    public class LogController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        public string Index()
        {
            return "Log Controller";
        }

        [HttpPost]
        public void WriteEntry(string server, string message, int? user)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("szWEBServer", server));
            parameters.Add(new SqlParameter("szErrorMessage", message));
            parameters.Add(new SqlParameter("iUserCreated", user ?? 0));

            var db = new DataSource().Execute("uspRecordWEBAudit", parameters);
        }
    }
}
