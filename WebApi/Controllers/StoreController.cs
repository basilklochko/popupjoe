﻿using System.Web.Http;
using System;
using Models.Responses;
using Models.Requests;
using System.Collections.Generic;
using System.Data.SqlClient;
using DataAccess;
using Models.Models;
using System.Data;
using WebApi.Classes;

namespace WebApi.Controllers
{
    public class StoreController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        public string Index()
        {
            return "Store Controller";
        }

        [HttpPost]
        public StoreResponse GetStore(StoreRequest request)
        {
            StoreResponse response = new StoreResponse() { Store = new Store() };

            try 
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreID", request.Store.ID));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().ExecuteDataSet("svc.uspGetStoreInfo", parameters);

                if (result.DataSet != null && result.DataSet.Tables.Count > 0)
                {
                    response.Store.ID = Convert.ToInt64(result.DataSet.Tables[0].Rows[0]["StoreID"]);
                    response.Store.ParentID = Convert.ToInt64(result.DataSet.Tables[0].Rows[0]["ParentStoreID"]);
                    response.Store.Address1 = result.DataSet.Tables[0].Rows[0]["Address1"].ToString();
                    response.Store.Address2 = result.DataSet.Tables[0].Rows[0]["Address2"].ToString();
                    response.Store.City = result.DataSet.Tables[0].Rows[0]["City"].ToString();
                    response.Store.CountryCode = result.DataSet.Tables[0].Rows[0]["CountryCode"].ToString();
                    response.Store.eCommerceName = result.DataSet.Tables[0].Rows[0]["eCommerceName"].ToString();
                    response.Store.eCommerceType = Convert.ToInt32(result.DataSet.Tables[0].Rows[0]["eCommerceType"]);
                    response.Store.Name = result.DataSet.Tables[0].Rows[0]["StoreName"].ToString();
                    response.Store.PostalCode = result.DataSet.Tables[0].Rows[0]["PostalCode"].ToString();
                    response.Store.PostalName = result.DataSet.Tables[0].Rows[0]["PostalName"].ToString();
                    response.Store.StateProvince = result.DataSet.Tables[0].Rows[0]["StateProvince"].ToString();
                    response.Store.RevokeStatus = Convert.ToBoolean(result.DataSet.Tables[0].Rows[0]["RevokeStatus"]);
                    response.Store.SessionExpiration = Convert.ToInt32(result.DataSet.Tables[0].Rows[0]["SessionExpiration"]);
                    response.Store.ContractExpirationDate = Convert.ToDateTime(result.DataSet.Tables[0].Rows[0]["ContractExpirationDate"]);
                    response.Store.CustomerProfileID = result.DataSet.Tables[0].Rows[0]["CustomerProfileID"].ToString();
                }

                Statics.FetchDebugParameters((BaseResponse)response, result);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public StoreListResponse GetStoreListByUser(UserRequest request)
        {
            StoreListResponse response = new StoreListResponse() { StoreList = new StoreList() { List = new List<Store>() } };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biUserID", request.User.UserID));
                parameters.Add(new SqlParameter("bHideRevoked", request.HideRevoked));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().ExecuteDataSet("svc.uspGetUsersStoreList", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (result.DataSet != null && result.DataSet.Tables.Count > 0)
                {
                    foreach (DataRow row in result.DataSet.Tables[0].Rows)
                    {
                        response.StoreList.List.Add(new Store()
                            {
                                ID = Convert.ToInt64(row["StoreID"]),
                                Name = row["StoreName"].ToString(),
                                RNumber = row["RNumber"].ToString(),
                                ParentID = Convert.ToInt64(row["ParentStoreID"]),
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public StoreResponse AddStore(StoreRequest request)
        {
            StoreResponse response = new StoreResponse() { Store = request.Store };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("ieCommerceType", 1));
                parameters.Add(new SqlParameter("szStoreName", "New Store"));
                parameters.Add(new SqlParameter("szPostalName", "New Store"));
                parameters.Add(new SqlParameter("szAddress1", ""));
                parameters.Add(new SqlParameter("szAddress2", ""));
                parameters.Add(new SqlParameter("szCity", ""));
                parameters.Add(new SqlParameter("szStateProvince", ""));
                parameters.Add(new SqlParameter("szPostalCode", ""));
                parameters.Add(new SqlParameter("szCountryCode", ""));
                parameters.Add(new SqlParameter("biParentStoreID", request.Store.ParentID));
                parameters.Add(new SqlParameter("biStoreID", request.Store.ParentID) { Direction = ParameterDirection.InputOutput });

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspStoreInsert", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                response.Store.ID = Convert.ToInt64(result.Parameters["biStoreID"].Value);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public SuccessFailResponse UpdateStore(StoreRequest request)
        {
            SuccessFailResponse response = new SuccessFailResponse();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreID", request.Store.ID));
                parameters.Add(new SqlParameter("ieCommerceType", request.Store.eCommerceType));
                parameters.Add(new SqlParameter("szStoreName", request.Store.Name));
                parameters.Add(new SqlParameter("szPostalName", request.Store.PostalName));
                parameters.Add(new SqlParameter("szAddress1", request.Store.Address1));
                parameters.Add(new SqlParameter("szAddress2", request.Store.Address2));
                parameters.Add(new SqlParameter("szCity", request.Store.City));
                parameters.Add(new SqlParameter("szStateProvince", request.Store.StateProvince));
                parameters.Add(new SqlParameter("szPostalCode", request.Store.PostalCode));
                parameters.Add(new SqlParameter("szCountryCode", request.Store.CountryCode));
                parameters.Add(new SqlParameter("biParentStoreID", request.Store.ParentID));
                parameters.Add(new SqlParameter("iSessionExpiration", request.Store.SessionExpiration));
                parameters.Add(new SqlParameter("dtContractExpirationDate", request.Store.ContractExpirationDate));
                parameters.Add(new SqlParameter("biUpdUser", request.Store.UserUpdated));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspStoreUpdate", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (response.ErrorNumber == 0)
                {
                    response.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public SuccessFailResponse ChangeStoreStatus(StoreRequest request)
        {
            SuccessFailResponse response = new SuccessFailResponse();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreID", request.Store.ID));
                parameters.Add(new SqlParameter("bRevokeStatus", request.Store.RevokeStatus));
                parameters.Add(new SqlParameter("biUpdUser", request.Store.UserUpdated));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspStoreChangeStatus", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (response.ErrorNumber == 0)
                {
                    response.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }
    }       
}
