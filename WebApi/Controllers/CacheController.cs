﻿using System.Web.Http;
using DataAccess;

namespace WebApi.Controllers
{
    public class CacheController : ApiController
    {
        [HttpPost]
        public void InitCache()
        {
            CacheDb.GetDependencies();
        }
    }
}
