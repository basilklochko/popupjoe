﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Models.Responses;
using Models.Requests;
using Models.Models;
using System.Data.SqlClient;
using System.Data;
using WebApi.Classes;
using DataAccess;

namespace WebApi.Controllers
{
    public class CouponController : ApiController
    {
        [HttpPost]
        public CouponListResponse GetCouponList(CouponListRequest request)
        {
            CouponListResponse response = new CouponListResponse() { CouponList = new CouponList() { List = new List<Coupon>() } };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreIPListID", request.DomainID));
                parameters.Add(new SqlParameter("iPageIndex", request.PageIndex));
                parameters.Add(new SqlParameter("iPageSize", request.PageSize));
                parameters.Add(new SqlParameter("szSortColumn", request.SortColumn));
                parameters.Add(new SqlParameter("szSortDirection", request.SortDirection));
                parameters.Add(new SqlParameter("bHideRevoked", request.HideRevoked));
                parameters.Add(new SqlParameter("biResultCount", 0) { Direction = ParameterDirection.Output });

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().ExecuteDataSet("svc.uspGetCouponList", parameters);

                if (result.DataSet != null && result.DataSet.Tables.Count > 0 && result.DataSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in result.DataSet.Tables[0].Rows)
                    {
                        response.CouponList.List.Add(new Coupon()
                        {
                            ID = Convert.ToInt64(row["CouponID"]),
                            DomainID = Convert.ToInt64(row["StoreIPListID"]),
                            StoreID = Convert.ToInt64(row["StoreID"]),
                            eCommCouponID = row["eCommCouponID"].ToString(),
                            Description = row["CouponDescription"].ToString(),
                            Value = Convert.ToDecimal(row["CouponValue"]),
                            MinOrderAmount = Convert.ToDecimal(row["MinOrderAmt"]),
                            MaxOrderAmount = Convert.ToDecimal(row["MaxOrderAmt"]),
                            TypeID = Convert.ToInt32(row["CouponType"]),
                            Type = row["CouponTypeName"].ToString(),
                            Message = row["MsgToPopUp"].ToString(),
                            ExpirationDate = (row["ExpirationDate"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["ExpirationDate"])),
                            RevokeStatus = Convert.ToBoolean(row["RevokeStatus"]),
                            DateRevoked = (row["DateRevoked"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["DateRevoked"])),
                            DateCreated = Convert.ToDateTime(row["DateCreated"]),
                            DateUpdated = (row["DateUpdated"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["DateUpdated"]))
                        });
                    }

                    response.CouponList.ResultCount = Convert.ToInt64(result.Parameters["biResultCount"].Value);
                }

                response.CouponList.PageIndex = request.PageIndex;
                response.CouponList.PageSize = request.PageSize;
                response.CouponList.SortColumn = request.SortColumn;
                response.CouponList.SortDirection = request.SortDirection;

                Statics.FetchDebugParameters((BaseResponse)response, result);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public CouponResponse CreateCoupon(CouponRequest request)
        {
            CouponResponse response = new CouponResponse() { Coupon = request.Coupon };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreIPListID", request.Coupon.DomainID));
                parameters.Add(new SqlParameter("szeCommCouponID", request.Coupon.eCommCouponID));
                parameters.Add(new SqlParameter("szCouponDescription", request.Coupon.Description));
                parameters.Add(new SqlParameter("nCouponValue", request.Coupon.Value));
                parameters.Add(new SqlParameter("nMinOrderAmt", request.Coupon.MinOrderAmount));
                parameters.Add(new SqlParameter("nMaxOrderAmt", request.Coupon.MaxOrderAmount));
                parameters.Add(new SqlParameter("iCouponType", request.Coupon.TypeID));
                parameters.Add(new SqlParameter("szMsgToPopUp", request.Coupon.Message));
                parameters.Add(new SqlParameter("dtExpirationDate", request.Coupon.ExpirationDate));
                parameters.Add(new SqlParameter("bLimitedNumber", request.Coupon.LimitedNumber));
                parameters.Add(new SqlParameter("iUses", request.Coupon.Uses));
                parameters.Add(new SqlParameter("biUpdUser", request.Coupon.UserUpdated));
                parameters.Add(new SqlParameter("@biCouponID", request.Coupon.ID) { Direction = ParameterDirection.InputOutput });

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspCouponInsert", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                response.Coupon.ID = Convert.ToInt64(result.Parameters["@biCouponID"].Value);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public CouponResponse GetCoupon(CouponRequest request)
        {
            CouponResponse response = new CouponResponse() { Coupon = request.Coupon };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biCouponID", request.Coupon.ID));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().ExecuteDataSet("svc.uspGetCouponInfo", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (result.DataSet != null && result.DataSet.Tables.Count > 0 && result.DataSet.Tables[0].Rows.Count > 0)
                {
                    response.Coupon.DomainID = Convert.ToInt64(result.DataSet.Tables[0].Rows[0]["StoreIPListID"]);
                    response.Coupon.eCommCouponID = result.DataSet.Tables[0].Rows[0]["eCommCouponID"].ToString();
                    response.Coupon.Description = result.DataSet.Tables[0].Rows[0]["CouponDescription"].ToString();
                    response.Coupon.Value = Convert.ToDecimal(result.DataSet.Tables[0].Rows[0]["CouponValue"]);
                    response.Coupon.MinOrderAmount = Convert.ToDecimal(result.DataSet.Tables[0].Rows[0]["MinOrderAmt"]);
                    response.Coupon.MaxOrderAmount = Convert.ToDecimal(result.DataSet.Tables[0].Rows[0]["MaxOrderAmt"]);
                    response.Coupon.TypeID = Convert.ToInt32(result.DataSet.Tables[0].Rows[0]["CouponType"]);
                    response.Coupon.Type = result.DataSet.Tables[0].Rows[0]["CouponTypeName"].ToString();
                    response.Coupon.Message = result.DataSet.Tables[0].Rows[0]["MsgToPopUp"].ToString();
                    response.Coupon.ExpirationDate = (result.DataSet.Tables[0].Rows[0]["ExpirationDate"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(result.DataSet.Tables[0].Rows[0]["ExpirationDate"]));
                    response.Coupon.LimitedNumber = Convert.ToBoolean(result.DataSet.Tables[0].Rows[0]["LimitedNumber"]);
                    response.Coupon.Uses = Convert.ToInt32(result.DataSet.Tables[0].Rows[0]["Uses"]);
                    response.Coupon.RevokeStatus = Convert.ToBoolean(result.DataSet.Tables[0].Rows[0]["RevokeStatus"]);
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public SuccessFailResponse UpdateCoupon(CouponRequest request)
        {
            SuccessFailResponse response = new SuccessFailResponse();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biCouponID", request.Coupon.ID));
                parameters.Add(new SqlParameter("szeCommCouponID", request.Coupon.eCommCouponID));
                parameters.Add(new SqlParameter("szCouponDescription", request.Coupon.Description));
                parameters.Add(new SqlParameter("nCouponValue", request.Coupon.Value));
                parameters.Add(new SqlParameter("nMinOrderAmt", request.Coupon.MinOrderAmount));
                parameters.Add(new SqlParameter("nMaxOrderAmt", request.Coupon.MaxOrderAmount));
                parameters.Add(new SqlParameter("iCouponType", request.Coupon.TypeID));
                parameters.Add(new SqlParameter("szMsgToPopUp", request.Coupon.Message));
                parameters.Add(new SqlParameter("dtExpirationDate", request.Coupon.ExpirationDate));
                parameters.Add(new SqlParameter("bLimitedNumber", request.Coupon.LimitedNumber));
                parameters.Add(new SqlParameter("iUses", request.Coupon.Uses));
                parameters.Add(new SqlParameter("biUpdUser", request.Coupon.UserUpdated));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspCouponUpdate", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (response.ErrorNumber == 0)
                {
                    response.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public SuccessFailResponse ChangeCouponStatus(CouponRequest request)
        {
            SuccessFailResponse response = new SuccessFailResponse();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biCouponID", request.Coupon.ID));
                parameters.Add(new SqlParameter("bRevokeStatus", request.Coupon.RevokeStatus));
                parameters.Add(new SqlParameter("biUpdUser", request.Coupon.UserUpdated));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspCouponChangeStatus", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (response.ErrorNumber == 0)
                {
                    response.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }
    }
}
