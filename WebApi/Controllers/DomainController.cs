﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Models.Responses;
using Models.Requests;
using Models.Models;
using System.Data.SqlClient;
using System.Data;
using WebApi.Classes;
using DataAccess;

namespace WebApi.Controllers
{
    public class DomainController : ApiController
    {
        [HttpPost]
        public DomainListResponse GetStoreIPList(DomainListRequest request)
        {
            DomainListResponse response = new DomainListResponse() { DomainList = new DomainList() { List = new List<Domain>() } };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreID", request.StoreID));
                parameters.Add(new SqlParameter("iPageIndex", request.PageIndex));
                parameters.Add(new SqlParameter("iPageSize", request.PageSize));
                parameters.Add(new SqlParameter("szSortColumn", request.SortColumn));
                parameters.Add(new SqlParameter("szSortDirection", request.SortDirection));
                parameters.Add(new SqlParameter("bHideRevoked", request.HideRevoked));
                parameters.Add(new SqlParameter("biResultCount", 0) { Direction = ParameterDirection.Output });

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().ExecuteDataSet("svc.uspGetStoreIPList", parameters);

                if (result.DataSet != null && result.DataSet.Tables.Count > 0 && result.DataSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in result.DataSet.Tables[0].Rows)
                    {
                        response.DomainList.List.Add(new Domain()
                        {
                            ID = Convert.ToInt64(row["StoreIPListID"]),
                            IPAddress = row["IPAddress"].ToString(),
                            Name = row["DomainName"].ToString(),
                            TimerValue1 = Convert.ToInt32(row["TimerValue1"]),
                            TimerValue2 = Convert.ToInt32(row["TimerValue2"]),
                            RevokeStatus = Convert.ToBoolean(row["RevokeStatus"]),
                            DateRevoked = (row["DateRevoked"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["DateRevoked"])),
                            DateUpdated = (row["DateUpdated"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["DateUpdated"]))
                        });
                    }

                    response.DomainList.ResultCount = Convert.ToInt64(result.Parameters["biResultCount"].Value);
                }

                response.DomainList.PageIndex = request.PageIndex;
                response.DomainList.PageSize = request.PageSize;
                response.DomainList.SortColumn = request.SortColumn;
                response.DomainList.SortDirection = request.SortDirection;

                Statics.FetchDebugParameters((BaseResponse)response, result);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public DomainResponse GetDomain(DomainRequest request)
        {
            DomainResponse response = new DomainResponse() { Domain = request.Domain };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreIPListID", request.Domain.ID));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().ExecuteDataSet("svc.uspGetStoreIPListInfo", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (result.DataSet != null && result.DataSet.Tables.Count > 0 && result.DataSet.Tables[0].Rows.Count > 0)
                {
                    response.Domain.StoreID = Convert.ToInt64(result.DataSet.Tables[0].Rows[0]["StoreID"]);
                    response.Domain.IPAddress = result.DataSet.Tables[0].Rows[0]["IPAddress"].ToString();
                    response.Domain.Name = result.DataSet.Tables[0].Rows[0]["DomainName"].ToString();
                    response.Domain.TimerValue1 = Convert.ToInt32(result.DataSet.Tables[0].Rows[0]["TimerValue1"]);
                    response.Domain.TimerValue2 = Convert.ToInt32(result.DataSet.Tables[0].Rows[0]["TimerValue2"]);
                    response.Domain.RevokeStatus = Convert.ToBoolean(result.DataSet.Tables[0].Rows[0]["RevokeStatus"]);
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public DomainResponse CreateDomain(DomainRequest request)
        {
            DomainResponse response = new DomainResponse() { Domain = request.Domain };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreID", request.Domain.StoreID));
                parameters.Add(new SqlParameter("szIPAddress", request.Domain.IPAddress));
                parameters.Add(new SqlParameter("szDomainName", request.Domain.Name));
                parameters.Add(new SqlParameter("iTimerValue1", request.Domain.TimerValue1));
                parameters.Add(new SqlParameter("iTimerValue2", request.Domain.TimerValue2));
                parameters.Add(new SqlParameter("biUpdUser", request.Domain.UserUpdated));
                parameters.Add(new SqlParameter("biStoreIPListID", request.Domain.ID) { Direction = ParameterDirection.InputOutput });

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspStoreIPListInsert", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);
               
                response.Domain.ID = Convert.ToInt64(result.Parameters["biStoreIPListID"].Value);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public SuccessFailResponse UpdateDomain(DomainRequest request)
        {
            SuccessFailResponse response = new SuccessFailResponse();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreIPListID", request.Domain.ID));
                parameters.Add(new SqlParameter("szIPAddress", request.Domain.IPAddress));
                parameters.Add(new SqlParameter("szDomainName", request.Domain.Name));
                parameters.Add(new SqlParameter("iTimerValue1", request.Domain.TimerValue1));
                parameters.Add(new SqlParameter("iTimerValue2", request.Domain.TimerValue2));
                parameters.Add(new SqlParameter("biUpdUser", request.Domain.UserUpdated));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspStoreIPListUpdate", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (response.ErrorNumber == 0)
                {
                    response.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public SuccessFailResponse ChangeDomainStatus(DomainRequest request)
        {
            SuccessFailResponse response = new SuccessFailResponse();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreIPListID", request.Domain.ID));
                parameters.Add(new SqlParameter("bRevokeStatus", request.Domain.RevokeStatus));
                parameters.Add(new SqlParameter("biUpdUser", request.Domain.UserUpdated));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspStoreIPListChangeStatus", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (response.ErrorNumber == 0)
                {
                    response.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }
    }
}
