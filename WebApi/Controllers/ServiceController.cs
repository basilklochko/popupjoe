﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Models.Responses;
using Models.Requests;
using System.Data.SqlClient;
using WebApi.Classes;
using DataAccess;
using Models.Models;
using System.Data;

namespace WebApi.Controllers
{
    public class ServiceController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        public string Index()
        {
            return "Service Controller";
        }

        [HttpPost]
        public LookUpResponse GetLookUp(LookUpRequest request)
        {
            LookUpResponse response = new LookUpResponse(new List<LookUp>());

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("szLookUpName", request.LookUpName));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().ExecuteDataSet("svc.uspGetLookUpData", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (result.DataSet != null && result.DataSet.Tables.Count > 0 && result.DataSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in result.DataSet.Tables[0].Rows)
                    {
                        response.LookUpList.Add(new LookUp()
                            {
                                Key = Convert.ToInt32(row["LookUpSubType"]),
                                Value = row["LookUpvalue"].ToString(),
                                Description = row["LookUpDescription"].ToString(),
                                Col1 = row["col1"].ToString()
                            });
                    }
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }
    }
}
