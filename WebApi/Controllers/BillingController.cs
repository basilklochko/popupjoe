﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Models.Responses;
using Models.Requests;
using Models.Models;
using System.Data.SqlClient;
using DataAccess;
using WebApi.Classes;
using System.Data;

namespace WebApi.Controllers
{
    public class BillingController : ApiController
    {
        [HttpPost]
        public BSettingListResponse GetBSettingList(BSettingListRequest request)
        {
            BSettingListResponse response = new BSettingListResponse() { BSettingList = new BSettingList() { List = new List<BSetting>() } };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreID", request.StoreID));
                parameters.Add(new SqlParameter("iPageIndex", request.PageIndex));
                parameters.Add(new SqlParameter("iPageSize", request.PageSize));
                parameters.Add(new SqlParameter("szSortColumn", request.SortColumn));
                parameters.Add(new SqlParameter("szSortDirection", request.SortDirection));
                parameters.Add(new SqlParameter("bHideRevoked", request.HideRevoked));
                parameters.Add(new SqlParameter("biResultCount", 0) { Direction = ParameterDirection.Output });

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().ExecuteDataSet("svc.uspGetBSettingList", parameters);

                if (result.DataSet != null && result.DataSet.Tables.Count > 0 && result.DataSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in result.DataSet.Tables[0].Rows)
                    {
                        response.BSettingList.List.Add(new BSetting()
                        {
                            ID = Convert.ToInt64(row["BSettingID"]),
                            PeriodID = Convert.ToInt64(row["BillingPeriod"]),
                            Period = row["BillngPeriodName"].ToString(),
                            TypeID = Convert.ToInt32(row["BillingType"]),
                            Type = row["BillngTypeName"].ToString(),
                            Unit = Convert.ToInt32(row["BillingUnit"]),
                            Rate = Convert.ToDecimal(row["BillingRate"]),
                            Date=Convert.ToDateTime(row["BillingDate"]),
                            LastBilledDate = (row["LastBilledDate"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["LastBilledDate"])),
                            RevokeStatus = Convert.ToBoolean(row["RevokeStatus"]),
                            DateRevoked = (row["DateRevoked"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["DateRevoked"])),
                            DateCreated = Convert.ToDateTime(row["DateCreated"]),
                            DateUpdated = (row["DateUpdated"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row["DateUpdated"]))
                        });
                    }

                    response.BSettingList.ResultCount = Convert.ToInt64(result.Parameters["biResultCount"].Value);
                }

                response.BSettingList.PageIndex = request.PageIndex;
                response.BSettingList.PageSize = request.PageSize;
                response.BSettingList.SortColumn = request.SortColumn;
                response.BSettingList.SortDirection = request.SortDirection;

                Statics.FetchDebugParameters((BaseResponse)response, result);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public BSettingResponse GetBSetting(BSettingRequest request)
        {
            BSettingResponse response = new BSettingResponse() { BSetting = request.BSetting };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biBSettingID", request.BSetting.ID));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().ExecuteDataSet("svc.uspGetBSettingInfo", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (result.DataSet != null && result.DataSet.Tables.Count > 0 && result.DataSet.Tables[0].Rows.Count > 0)
                {
                    response.BSetting.StoreID = Convert.ToInt64(result.DataSet.Tables[0].Rows[0]["StoreID"]);
                    response.BSetting.PeriodID = Convert.ToInt64(result.DataSet.Tables[0].Rows[0]["BillingPeriod"]);
                    response.BSetting.Period = result.DataSet.Tables[0].Rows[0]["BillngPeriodName"].ToString();
                    response.BSetting.TypeID = Convert.ToInt32(result.DataSet.Tables[0].Rows[0]["BillingType"]);
                    response.BSetting.Type = result.DataSet.Tables[0].Rows[0]["BillngTypeName"].ToString();
                    response.BSetting.Unit = Convert.ToInt32(result.DataSet.Tables[0].Rows[0]["BillingUnit"]);
                    response.BSetting.Rate = Convert.ToDecimal(result.DataSet.Tables[0].Rows[0]["BillingRate"]);
                    response.BSetting.Date = Convert.ToDateTime(result.DataSet.Tables[0].Rows[0]["BillingDate"]);
                    response.BSetting.LastBilledDate = (result.DataSet.Tables[0].Rows[0]["LastBilledDate"] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(result.DataSet.Tables[0].Rows[0]["LastBilledDate"]));
                    response.BSetting.CN = result.DataSet.Tables[0].Rows[0]["cn"].ToString();
                    response.BSetting.CE = result.DataSet.Tables[0].Rows[0]["ce"].ToString();
                    response.BSetting.CustomerPaymentProfileID = result.DataSet.Tables[0].Rows[0]["CustomerPaymentProfileID"].ToString();
                    response.BSetting.RevokeStatus = Convert.ToBoolean(result.DataSet.Tables[0].Rows[0]["RevokeStatus"]);
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public BSettingResponse CreateBSetting(BSettingRequest request)
        {
            BSettingResponse response = new BSettingResponse() { BSetting = request.BSetting };

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biStoreID", request.BSetting.StoreID));
                parameters.Add(new SqlParameter("biBillingPeriod", request.BSetting.PeriodID));
                parameters.Add(new SqlParameter("iBillingType", request.BSetting.TypeID));
                parameters.Add(new SqlParameter("iBillingUnit", request.BSetting.Unit));
                parameters.Add(new SqlParameter("nBillingRate", request.BSetting.Rate));
                parameters.Add(new SqlParameter("dtBillingDate", request.BSetting.Date));
                parameters.Add(new SqlParameter("biUpdUser", request.BSetting.UserUpdated));
                parameters.Add(new SqlParameter("biBSettingID", request.BSetting.ID) { Direction = ParameterDirection.InputOutput });

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspBSettingInsert", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                response.BSetting.ID = Convert.ToInt64(result.Parameters["biBSettingID"].Value);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public SuccessFailResponse UpdateBSetting(BSettingRequest request)
        {
            SuccessFailResponse response = new SuccessFailResponse();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biBSettingID", request.BSetting.ID));
                parameters.Add(new SqlParameter("biBillingPeriod", request.BSetting.PeriodID));
                parameters.Add(new SqlParameter("iBillingType", request.BSetting.TypeID));
                parameters.Add(new SqlParameter("iBillingUnit", request.BSetting.Unit));
                parameters.Add(new SqlParameter("nBillingRate", request.BSetting.Rate));
                parameters.Add(new SqlParameter("dtBillingDate", request.BSetting.Date));
                parameters.Add(new SqlParameter("cn", request.BSetting.CN));
                parameters.Add(new SqlParameter("ce", request.BSetting.CE));
                parameters.Add(new SqlParameter("biUpdUser", request.BSetting.UserUpdated));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspBSettingUpdate", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (response.ErrorNumber == 0)
                {
                    response.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }

        [HttpPost]
        public SuccessFailResponse ChangeBSettingStatus(BSettingRequest request)
        {
            SuccessFailResponse response = new SuccessFailResponse();

            try
            {
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("biBSettingID", request.BSetting.ID));
                parameters.Add(new SqlParameter("bRevokeStatus", request.BSetting.RevokeStatus));
                parameters.Add(new SqlParameter("biUpdUser", request.BSetting.UserUpdated));

                Statics.AddDebugParameters(parameters);

                var result = new DataSource().Execute("svc.uspBSettingChangeStatus", parameters);

                Statics.FetchDebugParameters((BaseResponse)response, result);

                if (response.ErrorNumber == 0)
                {
                    response.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }

            return response;
        }
    }
}
