﻿using System;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.Configuration;
using Models.Responses;

namespace ServiceAccess
{
    public static class ServiceName
    {
        public const string AccountService = "AccountService";
        public const string LogService = "LogService";
        public const string StoreService = "StoreService";
        public const string DomainService = "DomainService";
        public const string CouponService = "CouponService";
        public const string BillingService = "BillingService";
        public const string ReportService = "ReportService";
        public const string ServiceService = "ServiceService";
        public const string InvoiceService = "InvoiceService";
    }

    public static class MethodName
    {
        public const string ValidateUser = "ValidateUser";
        public const string ValidateSecureAnswer = "ValidateSecureAnswer";
        public const string GetMenuOptions = "GetMenuOptions";
        public const string ChangePassword = "ChangePassword";
        public const string ChangeUserStatus = "ChangeUserStatus";
        public const string CreateUser = "CreateUser";
        public const string UpdateUser = "UpdateUser";
        public const string UpdateMenuOptions = "UpdateMenuOptions";

        public const string GetStore = "GetStore";
        public const string AddStore = "AddStore";
        public const string UpdateStore = "UpdateStore";
        public const string ChangeStoreStatus = "ChangeStoreStatus";
        public const string GetStoreListByUser = "GetStoreListByUser";
        public const string GetUserListByStore="GetUserListByStore";

        public const string GetStoreIPList = "GetStoreIPList";
        public const string GetDomain = "GetDomain";
        public const string CreateDomain = "CreateDomain";
        public const string UpdateDomain = "UpdateDomain";
        public const string ChangeDomainStatus = "ChangeDomainStatus";

        public const string GetUnInvoicedList = "GetUnInvoicedList";
        public const string GetInvoicedList = "GetInvoicedList";
        public const string GetInvoiceDetails = "GetInvoiceDetails";
        public const string GetChargeDetails = "GetChargeDetails";
        public const string CreateInvoices = "CreateInvoices";
        public const string VoidInvoice = "VoidInvoice";

        public const string GetCouponList = "GetCouponList";
        public const string GetCoupon = "GetCoupon";
        public const string CreateCoupon = "CreateCoupon";
        public const string UpdateCoupon = "UpdateCoupon";
        public const string ChangeCouponStatus = "ChangeCouponStatus";

        public const string GetBSettingList = "GetBSettingList";
        public const string GetBSetting = "GetBSetting";
        public const string CreateBSetting = "CreateBSetting";
        public const string UpdateBSetting = "UpdateBSetting";
        public const string ChangeBSettingStatus = "ChangeBSettingStatus";

        public const string GetUserStoreStatistics = "GetUserStoreStatistics";

        public const string GetLookUp = "GetLookUp";

        public const string WriteEntry = "WriteEntry";
    }

    public class ServiceWrapper
    {
        string endPoint = string.Empty;

        public ServiceWrapper(string serviceName)
        {
            endPoint = ConfigurationManager.AppSettings.Get(serviceName);
        }

        public string HttpPost(string action, object parameter)
        {
            string result = string.Empty;
            string url = String.Format("{0}{1}", endPoint, action);

            using (WebClient webClient = new WebClient())
            {
                PopulateHeaders(webClient, action);
                webClient.Headers.Add("Content-Type", "application/json");
                result = webClient.UploadString(url, "POST", JsonConvert.SerializeObject(parameter));
            }

            Check4Exception(result);

            return result;
        }

        public void HttpPostAsync(string action, params object[] parameters)
        {
            string url = String.Format("{0}{1}?{2}", endPoint, action, PopulateQueryString(parameters));

            using (WebClient webClient = new WebClient())
            {
                PopulateHeaders(webClient, action);
                webClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                webClient.UploadStringAsync(new Uri(url), "POST", string.Empty);
            }
        }

        private void Check4Exception(string result)
        {
            BaseResponse response = (BaseResponse)JsonConvert.DeserializeObject(result, typeof(BaseResponse));

            if (response.Exception != null)
            {
                throw response.Exception;
            }
        }

        private void PopulateHeaders(WebClient webClient, string action) 
        {
            webClient.Headers.Add("action", Crypto.Cryptor.Encrypt(action, ConfigurationManager.AppSettings.Get("EncryptPassword")));
        }

        private string PopulateQueryString(params object[] parameters)
        {
            StringBuilder queryString = new StringBuilder();

            int i = 1;
            foreach (var param in parameters)
            {
                if (i % 2 == 0)
                {
                    queryString.AppendFormat("{0}&", param);
                }
                else
                {
                    queryString.AppendFormat("{0}=", param);
                }

                i++;
            }

            return queryString.ToString();
        }
    }    
}