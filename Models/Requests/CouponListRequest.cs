﻿namespace Models.Requests
{
    public class CouponListRequest : ListRequest
    {
        public CouponListRequest(long domainID, int pageIndex, int pageSize, string sortColumn, string sortDirection)
        {
            this.DomainID = domainID;
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
            this.SortColumn = sortColumn;
            this.SortDirection = sortDirection;
        }

        public long DomainID { get; set; }

        public bool HideRevoked { get; set; }
    }
}