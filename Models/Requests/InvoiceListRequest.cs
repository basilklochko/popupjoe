﻿using Models.Models;
namespace Models.Requests
{
    public class InvoiceListRequest : ListRequest
    {
        public InvoiceListRequest(User user, int pageIndex, int pageSize, string sortColumn, string sortDirection)
        {
            this.User = user;
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
            this.SortColumn = sortColumn;
            this.SortDirection = sortDirection;
        }

        public InvoiceListRequest()
        {
            
        }

        public User User { get; set; }

        public bool HideRevoked { get; set; }

        public bool InvoiceExisted { get; set; }
    }
}