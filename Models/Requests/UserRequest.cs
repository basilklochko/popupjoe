﻿using Models.Models;

namespace Models.Requests
{
    public class UserRequest : BaseRequest
    {
        public UserRequest(User user) 
        {
            this.User = user;
        }

        public User User { get; set; }

        public bool HideRevoked { get; set; }
    }
}