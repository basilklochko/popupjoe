﻿using System;

namespace Models.Requests
{
    public class DashboardSetRequest 
    {
        public long UserID { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public int? GroupTypeID { get; set; }
        public long? StoreID { get; set; }
        public long? DomainID { get; set; }
        public bool? HideRevoked { get; set; }         
    }
}