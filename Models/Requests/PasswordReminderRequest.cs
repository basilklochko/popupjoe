﻿using Models.Models;

namespace Models.Requests
{
    public class PasswordReminderRequest : BaseRequest
    {
        public PasswordReminderRequest(PasswordReminder passwordReminder)
        {
            this.PasswordReminder = passwordReminder;
        }

        public PasswordReminder PasswordReminder { get; set; }
    }
}