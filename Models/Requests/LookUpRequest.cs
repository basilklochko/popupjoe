﻿namespace Models.Requests
{
    public class LookUpRequest : BaseRequest
    {
        public LookUpRequest(string lookUpName)
        {
            this.LookUpName = lookUpName;
        }

        public string LookUpName { get; set; }
    }
}