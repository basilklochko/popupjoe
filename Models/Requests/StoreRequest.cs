﻿using Models.Models;

namespace Models.Requests
{
    public class StoreRequest : BaseRequest
    {
        public StoreRequest(Store store) 
        {
            this.Store = store;
        }

        public Store Store { get; set; }
    }
}