﻿using Models.Models;

namespace Models.Requests
{
    public class CouponRequest : BaseRequest
    {
        public CouponRequest(Coupon coupon) 
        {
            this.Coupon = coupon;
        }

        public Coupon Coupon { get; set; }
    }
}