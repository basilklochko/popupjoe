﻿using System;
using Models.Models;

namespace Models.Requests
{
    [Serializable]
    public class LoginRequest : BaseRequest
    {
        public LoginRequest(Login login) 
        {
            this.Login = login;
        }
        
        public Login Login { get; set; }
    }
}