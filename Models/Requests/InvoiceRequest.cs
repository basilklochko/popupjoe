﻿using Models.Models;

namespace Models.Requests
{
    public class InvoiceRequest : BaseRequest
    {
        public InvoiceRequest(long userID, Invoice invoice) 
        {
            this.Invoice = invoice;
            this.UserID = userID;
        }

        public Invoice Invoice { get; set; }

        public long UserID { get; set; }
    }
}