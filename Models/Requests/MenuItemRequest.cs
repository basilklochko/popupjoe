﻿using Models.Models;

namespace Models.Requests
{
    public class MenuItemRequest : BaseRequest
    {
        public MenuItemList Menu { get; set; }

        public long? UserUpdated { get; set; }
    }
}