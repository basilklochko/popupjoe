﻿using Models.Models;

namespace Models.Requests
{
    public class ChangePasswordRequest : BaseRequest
    {
        public ChangePasswordRequest(ChangePassword changePassword) 
        {
            this.ChangePassword = changePassword;
        }

        public ChangePassword ChangePassword { get; set; }
    }
}