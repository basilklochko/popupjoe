﻿using Models.Models;

namespace Models.Requests
{
    public class BSettingRequest : BaseRequest
    {
        public BSettingRequest(BSetting bSetting) 
        {
            this.BSetting = bSetting;
        }

        public BSetting BSetting { get; set; }
    }
}