﻿
namespace Models.Requests
{
    public class BSettingListRequest : ListRequest
    {
        public BSettingListRequest(long storeID, int pageIndex, int pageSize, string sortColumn, string sortDirection)
        {
            this.StoreID = storeID;
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
            this.SortColumn = sortColumn;
            this.SortDirection = sortDirection;
        }

        public long StoreID { get; set; }

        public bool HideRevoked { get; set; }
    }
}