﻿using Models.Models;

namespace Models.Requests
{
    public class DomainRequest : BaseRequest
    {
        public DomainRequest(Domain domain) 
        {
            this.Domain = domain;
        }

        public Domain Domain { get; set; }
    }
}