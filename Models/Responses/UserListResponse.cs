﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class UserListResponse : BaseResponse
    {
        public UserList UserList { get; set; }
    }
}