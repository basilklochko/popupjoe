﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class DomainResponse : BaseResponse
    {
        public Domain Domain { get; set; }
    }
}