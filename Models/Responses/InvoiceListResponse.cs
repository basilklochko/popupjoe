﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class InvoiceListResponse : BaseResponse
    {
        public InvoiceList InvoiceList { get; set; }
    }
}