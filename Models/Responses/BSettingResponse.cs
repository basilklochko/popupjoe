﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class BSettingResponse : BaseResponse
    {
        public BSetting BSetting { get; set; }
    }
}