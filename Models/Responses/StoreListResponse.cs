﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class StoreListResponse : BaseResponse
    {
        public StoreList StoreList { get; set; }
    }
}