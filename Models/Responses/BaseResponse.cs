﻿using System;

namespace Models.Responses
{
    public class BaseResponse
    {
        public BaseResponse()
        {
            ErrorNumber = 0;
            ErrorMessage = string.Empty;
        }

        public int ErrorNumber { get; set; }
        public string ErrorMessage { get; set; }

        public Exception Exception { get; set; }
    }
}