﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class ChargeListResponse : BaseResponse
    {
        public ChargeList ChargeList { get; set; }
    }
}