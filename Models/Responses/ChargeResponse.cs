﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class ChargeResponse : BaseResponse
    {
        public Charge Charge { get; set; }
    }
}