﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class StoreResponse : BaseResponse
    {
        public Store Store { get; set; }
    }
}