﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Models.Models;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class DashboardSetResponse : BaseResponse
    {
        public List<BarLineChartSet> BarLineChartSetList { get; set; }
        public List<PieChartSet> PieChartSetList { get; set; }

        public string GraphTitle { get; set; }
        public string GraphType { get; set; }
    }
}