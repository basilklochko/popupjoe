﻿using System;
using Newtonsoft.Json;
using Models.Models;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class UserMenuResponse : BaseResponse
    {
        public UserPermissions UserPermissions { get; set; }
    }
}