﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class InvoiceResponse : BaseResponse
    {
        public Invoice Invoice { get; set; }
    }
}