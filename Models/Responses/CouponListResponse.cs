﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class CouponListResponse : BaseResponse
    {
        public CouponList CouponList { get; set; }
    }
}