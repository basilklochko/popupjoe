﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class DomainListResponse : BaseResponse
    {
        public DomainList DomainList { get; set; }
    }
}