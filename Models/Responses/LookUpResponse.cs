﻿using System.Collections.Generic;
using Models.Models;

namespace Models.Responses
{
    public class LookUpResponse : BaseResponse
    {
        public LookUpResponse(List<LookUp> lookUpList)
        {
            this.LookUpList = lookUpList;
        }

        public List<LookUp> LookUpList { set; get; }
    }
}