﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class BSettingListResponse : BaseResponse
    {
        public BSettingList BSettingList { get; set; }
    }
}