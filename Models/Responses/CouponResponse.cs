﻿using System;
using Models.Models;
using Newtonsoft.Json;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class CouponResponse : BaseResponse
    {
        public Coupon Coupon { get; set; }
    }
}