﻿using Newtonsoft.Json;
using System;

namespace Models.Responses
{
    [JsonObject(MemberSerialization.OptOut)]
    [Serializable]
    public class SuccessFailResponse : BaseResponse
    {
        public bool IsSuccessful { get; set; }
    }
}