﻿using System.ComponentModel.DataAnnotations;
using System;

namespace Models.Models
{
    public class User
    {
        public long? UserID { get; set; }

        public long ppjStoreID { get; set; }

        [Required]
        [Display(Name = "Store Name")]
        public long StoreID { get; set; }

        [Required]        
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Email Address")]
        [Email]
        [DataType(DataType.EmailAddress)]
        public string UserEmail { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name="Security Question")]
        public string SecureQuestion { get; set; }

        [Display(Name = "Security Answer")]
        public string SecureAnswer { get; set; }

        [Display(Name = "Last Login")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DateLastLogin { get; set; }

        [Required]
        [Display(Name = "Alert Recipient")]
        public bool EmailAlertRecipient { get; set; }

        [Required]
        [Display(Name = "Billing Report Recipient")]
        public bool BillingReportRecipient { get; set; }

        public bool ResetPassword { get; set; }

        public bool RevokeStatus { get; set; }

        [Display(Name = "Revoked")]
        public DateTime? DateRevoked { get; set; }

        [Display(Name = "Created")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Created By")]
        public long UserCreated { get; set; }

        [Display(Name = "Updated")]
        public DateTime? DateUpdated { get; set; }

        [Display(Name = "Updated By")]
        public long? UserUpdated { get; set; }
    }
}