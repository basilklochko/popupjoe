﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.Models
{
    public class BSetting
    {
        public long ID { get; set; }

        public long StoreID { get; set; }

        [Required]
        [Display(Name = "Period")]
        public long PeriodID { get; set; }

        [Display(Name = "Period")]
        public string Period { get; set; }

        [Required]
        [Display(Name = "Type")]
        public int TypeID { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Required]
        public int Unit { get; set; }

        [Required]
        public decimal Rate { get; set; }

        [Display(Name = "Card Number")]
        public string CN { get; set; }

        [Display(Name = "Card Expiration")]
        [RegularExpression(@"20[0-4]\d-(0[1-9]|1[0-2])")]
        public string CE { get; set; }

        [Display(Name = "Payment Profile ID")]
        public string CustomerPaymentProfileID { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Billing Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Date { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Last Billed Date")]
        public DateTime? LastBilledDate { get; set; }

        public bool RevokeStatus { get; set; }

        [Display(Name = "Revoked")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DateRevoked { get; set; }

        [Display(Name = "Created")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Created By")]
        public long UserCreated { get; set; }

        [Display(Name = "Updated")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DateUpdated { get; set; }

        [Display(Name = "Updated By")]
        public long? UserUpdated { get; set; }
    }
}