﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace Models.Models
{
    public class Store
    {
        public long ID { get; set; }

        [Required]
        [Display(Name = "eCommerce")]
        public int eCommerceType { get; set; }

        [Display(Name="eCommerce")]
        public string eCommerceName { get; set; }

        [Required]
        [Display(Name = "Store Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Postal Name")]
        public string PostalName { get; set; }

        [Required]
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        [Display(Name = "State/Province")]
        public string StateProvince { get; set; }

        [Required]
        [Display(Name = "Zip/Postal Code")]
        public string PostalCode { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string CountryCode { get; set; }

        [Required]
        [Display(Name = "Session Expiration")]
        public int SessionExpiration { get; set; }

        [Required]
        [Display(Name = "Expiration Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ContractExpirationDate { get; set; }

        [Display(Name = "Updated By")]
        public long? UserUpdated { get; set; }

        public bool RevokeStatus { get; set; }

        public string CustomerProfileID { get; set; }

        public string RNumber { get; set; }
        public long ParentID { get; set; }

        public int Order
        {
            get
            {
                int result = 0;

                if (!string.IsNullOrEmpty(RNumber))
                {
                    string[] orders = this.RNumber.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                    if (orders.Length > 0)
                    {
                        result = Convert.ToInt32(orders[orders.Length - 1]);
                    }
                }

                return result;
            }
        }

        public List<Store> Children(List<Store> list)
        {
            return (from s in list
                    where s.ParentID.Equals(this.ID)
                    select s).OrderBy(o => o.Order).ToList();
        }
    }
}