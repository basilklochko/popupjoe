﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Models.Models
{
    public class ChangePassword
    {
        public long UserID { get; set; }

        [Required]
        [Display(Name = "Old Password")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required]
        [Display(Name = "Confirm Password")]
        [Compare("NewPassword")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Security Question")]
        public string SecureQuestion { get; set; }

        [Required]
        [Display(Name = "Security Answer")]
        public string SecureAnswer { get; set; }
    }
}