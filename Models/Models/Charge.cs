﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.Models
{
    public class Charge
    {
        public long TransactionID { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        public string Number { get; set; }

        public string Description { get; set; }

        public decimal Amount { get; set; }

        public string CustomerProfileID { get; set; }

        public string CustomerPaymentProfileID { get; set; }

        public string TransID { get; set; }

        public string TranType { get; set; }

        public string retCode { get; set; }

        public string ErrMsg { get; set; }

        public string CPReturnCode { get; set; }

        public string CPPReturnCode { get; set; }

        public string TranReturnCode { get; set; }
    }
}