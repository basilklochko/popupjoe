﻿using System.Collections.Generic;

namespace Models.Models
{
    public class InvoiceList : BaseList
    {
        public List<Invoice> List { get; set; }
    }
}