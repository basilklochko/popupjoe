﻿namespace Models.Models
{
    public class BarLineChartSet
    {
        public long StoreID { get; set; }
        public string YAxis { get; set; }
        public long DomainID { get; set; }
        public long NumberSessions { get; set; }
        public long NumberPopUps { get; set; }
        public long NumberOrdsWithPopUp { get; set; }
        public long NumberOrdsNoPopUp { get; set; }
        public string XAxisValue { get; set; }
        public string XAxis { get; set; }
        public int Year { get; set; }
    }
}