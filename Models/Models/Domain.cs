﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.Models
{
    public class Domain
    {
        public long ID { get; set; }

        public long StoreID { get; set; }

        [Required]
        [Display(Name = "IP Address")]
        public string IPAddress { get; set; }

        [Required]
        [Display(Name = "Domain Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Timer1")]
        public int TimerValue1 { get; set; }

        [Required]
        [Display(Name = "Timer2")]
        public int TimerValue2 { get; set; }

        public bool RevokeStatus { get; set; }

        [Display(Name = "Revoked")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DateRevoked { get; set; }

        [Display(Name = "Created")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Created By")]
        public long UserCreated { get; set; }

        [Display(Name = "Updated")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DateUpdated { get; set; }

        [Display(Name = "Updated By")]
        public long? UserUpdated { get; set; }
    }
}