﻿using System.Collections.Generic;
using System.Linq;

namespace Models.Models
{
    public class StoreList : BaseList
    {
        public List<Store> List { get; set; }

        public List<Store> Root
        {
            get
            {
                return (from r in this.List
                        join a in this.List on r.ParentID equals a.ID into j
                        from a in j.DefaultIfEmpty()
                        where a == null
                        select r).ToList();
            }
        }

    }
}