﻿using System.Collections.Generic;

namespace Models.Models
{
    public class DomainList : BaseList
    {
        public List<Domain> List { get; set; }
    }
}