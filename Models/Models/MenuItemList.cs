﻿using System.Collections.Generic;

namespace Models.Models
{
    public class MenuItemList
    {
        public List<MenuItem> List { get; set; }
    }
}