﻿using System;

namespace Models.Models
{
    public class MenuItem
    {
        public int ID { get; set; }
        public int AppID { get; set; }
        public int ParentID { get; set; }
        public string Name { get; set; }
        public string UserRights { get; set; }
        public string Path { get; set; }
        public string RNumber { get; set; }
        public int Status { get; set; }

        public int Order
        {
            get 
            {
                int result = 0;

                if (!string.IsNullOrEmpty(RNumber))
                {
                    string[] orders = this.RNumber.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);

                    if (orders.Length > 0)
                    {
                        result = Convert.ToInt32(orders[orders.Length - 1]);
                    }
                }

                return result;
            }
        }
    }
}