﻿using System.Collections.Generic;

namespace Models.Models
{
    public class ChargeList : BaseList
    {
        public List<Charge> List { get; set; }
    }
}