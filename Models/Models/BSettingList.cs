﻿using System.Collections.Generic;

namespace Models.Models
{
    public class BSettingList : BaseList
    {
        public List<BSetting> List { get; set; }
    }
}