﻿using System;

namespace Models.Models
{
    public class Invoice
    {
        public long ID { get; set; }

        public long StoreID { get; set; }

        public string StoreName { get; set; }

        public string DomainName { get; set; }

        public DateTime SessionDate { get; set; }

        public bool PopUpFlag { get; set; }

        public bool OrderSubmitFlag { get; set; }

        public decimal PopUpOrderTotal { get; set; }

        public decimal SubmitOrderTotal { get; set; }

        public string BillingPeriodName { get; set; }

        public string BillingTypeName { get; set; }

        public long BillingUnit { get; set; }

        public decimal BillingRate { get; set; }

        public long Year { get; set; }

        public int NumberOfPeriod { get; set; }

        public string NameOfUnit { get; set; }

        public decimal NumberOfUnits { get; set; }

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        public decimal Total { get; set; }

        public DateTime BillingDate { get; set; }

        public bool BSettError { get; set; }

        public string Status { get; set; }
    }
}