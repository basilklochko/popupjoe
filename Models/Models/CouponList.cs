﻿using System.Collections.Generic;

namespace Models.Models
{
    public class CouponList : BaseList
    {
        public List<Coupon> List { get; set; }
    }
}