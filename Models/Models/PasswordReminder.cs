﻿using System.ComponentModel.DataAnnotations;

namespace Models.Models
{
    public class PasswordReminder
    {
        [Required]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string UserEmail { get; set; }

        [Required]
        [Display(Name = "Password Reminder Question")]
        public string SecureQuestion { get; set; }

        [Required]
        [Display(Name = "Password Reminder Answer")]
        public string SecureAnswer { get; set; }
    }
}