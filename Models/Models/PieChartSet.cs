﻿namespace Models.Models
{
    public class PieChartSet
    {
        public long StoreID { get; set; }
        public string YAxis { get; set; }
        public long DomainID { get; set; }
        public string Coupon { get; set; }
        public long NumberSessions { get; set; }
        public long NumberPopUps { get; set; }
        public long NumberOrdersWithPopUps { get; set; }
    }
}