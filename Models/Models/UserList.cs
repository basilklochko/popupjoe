﻿using System.Collections.Generic;

namespace Models.Models
{
    public class UserList : BaseList
    {
        public List<User> List { get; set; }
    }
}