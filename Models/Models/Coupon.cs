﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.Models
{
    public class Coupon
    {
        public long ID { get; set; }

        public long DomainID { get; set; }

        public long StoreID { get; set; }

        public string eCommCouponID { get; set; }

        [Required]
        [Display(Name = "Coupon Description")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Coupon Value")]
        public decimal Value { get; set; }

        [Required]
        [Display(Name = "Minimum Order Amount")]
        public decimal MinOrderAmount { get; set; }

        [Required]
        [Display(Name = "Maximum Order Amount")]
        public decimal MaxOrderAmount { get; set; }

        [Required]
        [Display(Name = "Coupon Type")]
        public int TypeID { get; set; }

        [Display(Name = "Coupon Type")]
        public string Type { get; set; }

        [Required]
        [Display(Name = "Popup Message")]
        public string Message { get; set; }

        [Display(Name = "Expiration Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ExpirationDate { get; set; }

        [Required]
        [Display(Name = "Limited Number")]
        public bool LimitedNumber { get; set; }

        [Required]
        [Display(Name = "Uses")]
        public int Uses { get; set; }

        public bool RevokeStatus { get; set; }

        [Display(Name = "Revoked")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DateRevoked { get; set; }

        [Display(Name = "Created")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Created By")]
        public long UserCreated { get; set; }

        [Display(Name = "Updated")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? DateUpdated { get; set; }

        [Display(Name = "Updated By")]
        public long? UserUpdated { get; set; }
    }
}