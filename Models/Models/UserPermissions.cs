﻿using System.Collections.Generic;

namespace Models.Models
{
    public class UserPermissions
    {
        public User User { get; set; }

        public List<MenuItem> Menu { get; set; }
    }
}