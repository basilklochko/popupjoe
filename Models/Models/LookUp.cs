﻿
namespace Models.Models
{
    public class LookUp
    {
        public int Key { get; set; }
        public string Value { get; set; }

        public string Description { get; set; }
        public string Col1 { get; set; }
    }
}