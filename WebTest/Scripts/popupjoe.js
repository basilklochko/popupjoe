var $j = jQuery.noConflict();

// Init
$j(document).ready(function () {
    $j.getJSON(urls.ipUrl, function (data) {
        constants.ip = data.ip;
    });

    fn.VerifyCoupon();
});

//////////////////////////////////////////////////////////
// Main functionality
//////////////////////////////////////////////////////////
// Declarations
var urls = {
    requestUrl: "/WebApi/PopUpJoe/ProcessRequest",
    ipUrl: "http://jsonip.appspot.com?callback=?"
};

var constants = {
    debug: false,
    main: "popupjoeMain",
    total: "popupjoeTotal",
    items: "popupjoeItems",
    ip: ""
};

var storeSettings = {
    StoreName: "Etronics",
    Domain: "etronics.com",
    StoreGUID: "E6A0A863-D681-4C78-90EB-CD0AB8A4246D"
};

//loadScript(paths.jquery, loadJqueryComplete);

//////////////////////////////////////////////////////////
// Functions
var fn = {
    Bind: function (target, type) {
        var items = null;

        if (type == "update") {
            $j("#" + target).bind("click", function () {
                var sessionID = '';
                var c = fn.GetCookie(constants.main);
                var cookie = fn.GetCookieObject(c);

                if (cookie != null) {
                    sessionID = cookie.PopUpJoeSessionID;
                }

                fn.DeleteCookies(constants.main);
                fn.DeleteCookies(constants.total);
                fn.DeleteCookies(constants.items);

                items = GetItems();

                fn.SetCookie(constants.items, JSON.stringify(items));
                fn.SetCookie(constants.total, fn.GetTotal(items));

                if (constants.debug) {
                    console.log('request 1');
                }

                fn.Ajax("GET", urls.requestUrl, { WSRequest: fn.CreateRequest(items, sessionID, 1, null) }, fn.SetCoupon);
            });
        }

        if (type == "checkout") {
            $j("#" + target).bind("click", function () {
                var cookies = fn.GetCookies(constants.main);
                var itemList = fn.GetCookie(constants.items);

                if (itemList != null) {
                    items = fn.GetCookieObject(itemList);
                }

                if (cookies.length > 0) {
                    var cookie = fn.GetCookieObject(cookies[0]);

                    if (cookie != null && items != null && cookie.ResponseType == 2) {
                        if (constants.debug) {
                            console.log('request 3');
                        }

                        fn.Ajax("GET", urls.requestUrl, { WSRequest: fn.CreateRequest(items, cookie.PopUpJoeSessionID, 3, cookie.Coupon.Value) }, fn.CloseSession);
                    }
                }
            });
        }
    },
    // Creates request with all required fields
    CreateRequest: function (items, sessionID, requestType, usedCoupon) {
        var server = {
            StoreName: storeSettings.StoreName,
            Domain: storeSettings.Domain
        };

        var parameters = new Object();

        if (sessionID == "") {
            parameters.StoreGUID = storeSettings.StoreGUID;
        }
        else {
            parameters.PopUpJoeSessionID = sessionID;
        }

        var customer = {
            IPAddress: constants.ip,
            LoginName: ""
        };

        var result = new Object();

        if (requestType == 1) {
            result = {
                eCommerceServer: server,
                LogParameters: parameters,
                Customer: customer,
                RequestType: requestType,
                OrderTotal: fn.GetTotal(items),
                Items: items
            };
        }

        if (requestType == 2) {
            result = {
                eCommerceServer: server,
                LogParameters: parameters,
                Customer: customer,
                RequestType: requestType,
                OrderTotal: fn.GetCookie(constants.total),
                UsedCoupon: usedCoupon
            };
        }

        if (requestType == 3) {
            result = {
                eCommerceServer: server,
                LogParameters: parameters,
                Customer: customer,
                RequestType: requestType,
                OrderTotal: fn.GetCookie(constants.total),
                Items: items,
                UsedCoupon: usedCoupon
            };
        }

        return result;
    },
    // Sets up a cookie with values obtained from server
    SetCoupon: function (data) {
        if (constants.debug) {
            console.log('response 1');
        }

        if (data != null && data != undefined) {
            var response = data["n:WSResponse"];

            if (response.Timer.ActivateTimer == "Y") {
                fn.SetCookie(constants.main + response.PopUpJoeSessionID, JSON.stringify(response));
            }

            fn.VerifyCoupon();
        }
    },
    // Verifies cookies and sets up timeout for popup
    VerifyCoupon: function () {
        var cookies = fn.GetCookies(constants.main);

        if (cookies.length > 0) {
            var cookie = fn.GetCookieObject(cookies[0]);

            if (cookie != null && cookie.Timer.ActivateTimer == "Y") {
                setTimeout(function () { fn.BeforePopup(cookie) }, cookie.Timer.TimerDelay * 1000 * 60);
            }
        }
    },
    // Calls the service checks if coupon is still valid
    BeforePopup: function (cookie) {
        if (constants.debug) {
            console.log('request 2');
        }

        fn.Ajax("GET", urls.requestUrl, { WSRequest: fn.CreateRequest(null, cookie.PopUpJoeSessionID, 2, cookie.Coupon.Value) }, fn.ShowPopup);
    },
    // Shows Popup
    ShowPopup: function (data) {
        if (constants.debug) {
            console.log('response 2');
        }

        if (data != null && data != undefined) {
            var response = data["n:WSResponse"];

            if (response.Result.ResultType == "Success") {
                var cookies = fn.GetCookies(constants.main);

                if (cookies.length > 0) {
                    var cookie = fn.GetCookieObject(cookies[0]);

                    // Displays popup
                    alert(cookie.Coupon.Message);

                    fn.DeleteCookies(constants.main);

                    response.Coupon.Value = cookie.Coupon.Value;

                    fn.SetCookie(constants.main + response.PopUpJoeSessionID, JSON.stringify(response), cookie.Timer.ShowPopUpDuration);
                }
            }
        }
    },
    // Closes PopuUpJoe session
    CloseSession: function (data) {
        if (constants.debug) {
            console.log('response 3');
        }

        if (data != null && data != undefined) {
            //var response = data["n:WSResponse"];

            fn.DeleteCookies(constants.main);
            fn.DeleteCookies(constants.total);
            fn.DeleteCookies(constants.items);
        }
    },
    // Calculates order total amount
    GetTotal: function (items) {
        var total = 0;

        for (var i = 0; i < items.Item.length; i++) {
            total += items.Item[i].Price * items.Item[i].Quantity;
        }

        return total;
    },
    // Executes an ajax call to server
    Ajax: function (type, url, parameter, callback) {
        $j.ajax({
            traditional: true,
            type: type,
            url: url,
            data: { parameter: JSON.stringify(parameter) },
            success: callback,
            contentType: "application/json",
            dataType: "json"
        });
    },
    // Sets cookie
    SetCookie: function (c_name, value, minutes) {
        var c_value = null;

        if (minutes != undefined) {
            var now = new Date();
            now.setMinutes(Number(now.getMinutes()) + Number(minutes));

            c_value = escape(value) + ((now == null) ? ";path=/" : ";path=/; expires=" + now.toUTCString());
        }
        else {
            c_value = escape(value) + ";path=/";
        }

        document.cookie = c_name + "=" + c_value;
    },
    // Gets all cookies by prefix
    GetCookies: function (prefix) {
        var result = [];
        var i, x, y, ARRcookies = document.cookie.split(";");

        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);

            x = x.replace(/^\s+|\s+$j/g, "");

            if (fn.Left(x, prefix.length) == prefix) {
                result.push(unescape(y));
            }
        }

        return result;
    },
    // Deletes all cookies by prefix
    DeleteCookies: function (prefix) {
        var cookies = fn.GetCookies(prefix);

        if (cookies.length > 0) {
            for (var i = 0; i < cookies.length; i++) {
                document.cookie = encodeURIComponent(prefix + fn.GetCookieObject(cookies[i]).PopUpJoeSessionID) + "=deleted; expires=" + new Date(0).toUTCString();
            }
        }
    },
    // Deletes a cookie by name
    DeleteCookie: function (name) {
        fn.SetCookie(name, '', -1);
    },
    // Gets a cookie
    GetCookie: function (c_name) {
        var i, x, y, ARRcookies = document.cookie.split(";");

        for (i = 0; i < ARRcookies.length; i++) {
            x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
            y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);

            x = x.replace(/^\s+|\s+$j/g, "");

            if (x == c_name) {
                return unescape(y);
            }
        }

        return '';
    },
    // Deserializes a cookie into object
    GetCookieObject: function (cookie) {
        if (cookie != '') {
            return eval('(' + cookie + ')');
        }
        else {
            return null;
        }
    },
    Left: function (str, n) {
        if (n <= 0) {
            return "";
        }
        else if (n > String(str).length) {
            return str;
        }
        else {
            return String(str).substring(0, n);
        }
    },
    Right: function (str, n) {
        if (n <= 0) {
            return "";
        }
        else if (n > String(str).length) {
            return str;
        }
        else {
            var iLen = String(str).length;
            return String(str).substring(iLen, iLen - n);
        }
    }
};
//////////////////////////////////////////////////////////

/*
json2.js
2012-10-08

Public Domain.

NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

See http://www.JSON.org/js.html


This code should be minified before deployment.
See http://javascript.crockford.com/jsmin.html

USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
NOT CONTROL.


This file creates a global JSON object containing two methods: stringify
and parse.

JSON.stringify(value, replacer, space)
value       any JavaScript value, usually an object or array.

replacer    an optional parameter that determines how object
values are stringified for objects. It can be a
function or an array of strings.

space       an optional parameter that specifies the indentation
of nested structures. If it is omitted, the text will
be packed without extra whitespace. If it is a number,
it will specify the number of spaces to indent at each
level. If it is a string (such as '\t' or '&nbsp;'),
it contains the characters used to indent at each level.

This method produces a JSON text from a JavaScript value.

When an object value is found, if the object contains a toJSON
method, its toJSON method will be called and the result will be
stringified. A toJSON method does not serialize: it returns the
value represented by the name/value pair that should be serialized,
or undefined if nothing should be serialized. The toJSON method
will be passed the key associated with the value, and this will be
bound to the value

For example, this would serialize Dates as ISO strings.

Date.prototype.toJSON = function (key) {
function f(n) {
// Format integers to have at least two digits.
return n < 10 ? '0' + n : n;
}

return this.getUTCFullYear()   + '-' +
f(this.getUTCMonth() + 1) + '-' +
f(this.getUTCDate())      + 'T' +
f(this.getUTCHours())     + ':' +
f(this.getUTCMinutes())   + ':' +
f(this.getUTCSeconds())   + 'Z';
};

You can provide an optional replacer method. It will be passed the
key and value of each member, with this bound to the containing
object. The value that is returned from your method will be
serialized. If your method returns undefined, then the member will
be excluded from the serialization.

If the replacer parameter is an array of strings, then it will be
used to select the members to be serialized. It filters the results
such that only members with keys listed in the replacer array are
stringified.

Values that do not have JSON representations, such as undefined or
functions, will not be serialized. Such values in objects will be
dropped; in arrays they will be replaced with null. You can use
a replacer function to replace those with JSON values.
JSON.stringify(undefined) returns undefined.

The optional space parameter produces a stringification of the
value that is filled with line breaks and indentation to make it
easier to read.

If the space parameter is a non-empty string, then that string will
be used for indentation. If the space parameter is a number, then
the indentation will be that many spaces.

Example:

text = JSON.stringify(['e', {pluribus: 'unum'}]);
// text is '["e",{"pluribus":"unum"}]'


text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
// text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

text = JSON.stringify([new Date()], function (key, value) {
return this[key] instanceof Date ?
'Date(' + this[key] + ')' : value;
});
// text is '["Date(---current time---)"]'


JSON.parse(text, reviver)
This method parses a JSON text to produce an object or array.
It can throw a SyntaxError exception.

The optional reviver parameter is a function that can filter and
transform the results. It receives each of the keys and values,
and its return value is used instead of the original value.
If it returns what it received, then the structure is not modified.
If it returns undefined then the member is deleted.

Example:

// Parse the text. Values that look like ISO date strings will
// be converted to Date objects.

myData = JSON.parse(text, function (key, value) {
var a;
if (typeof value === 'string') {
a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$j/.exec(value);
if (a) {
return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
+a[5], +a[6]));
}
}
return value;
});

myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
var d;
if (typeof value === 'string' &&
value.slice(0, 5) === 'Date(' &&
value.slice(-1) === ')') {
d = new Date(value.slice(5, -1));
if (d) {
return d;
}
}
return value;
});


This is a reference implementation. You are free to copy, modify, or
redistribute.
*/

/*jslint evil: true, regexp: true */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
lastIndex, length, parse, prototype, push, replace, slice, stringify,
test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

if (typeof JSON !== 'object') {
    JSON = {};
}

(function () {
    'use strict';

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return isFinite(this.valueOf())
                ? this.getUTCFullYear() + '-' +
                    f(this.getUTCMonth() + 1) + '-' +
                    f(this.getUTCDate()) + 'T' +
                    f(this.getUTCHours()) + ':' +
                    f(this.getUTCMinutes()) + ':' +
                    f(this.getUTCSeconds()) + 'Z'
                : null;
        };

        String.prototype.toJSON =
            Number.prototype.toJSON =
            Boolean.prototype.toJSON = function (key) {
                return this.valueOf();
            };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"': '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

        // If the string contains no control characters, no quote characters, and no
        // backslash characters, then we can safely slap some quotes around it.
        // Otherwise we must also replace the offending characters with safe escape
        // sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string'
                ? c
                : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

        // Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

        // If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

        // If we were called with a replacer function, then call the replacer to
        // obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

        // What happens next depends on the value's type.

        switch (typeof value) {
            case 'string':
                return quote(value);

            case 'number':

                // JSON numbers must be finite. Encode non-finite numbers as null.

                return isFinite(value) ? String(value) : 'null';

            case 'boolean':
            case 'null':

                // If the value is a boolean or null, convert it to a string. Note:
                // typeof null does not produce 'null'. The case is included here in
                // the remote chance that this gets fixed someday.

                return String(value);

                // If the type is 'object', we might be dealing with an object or an array or
                // null.

            case 'object':

                // Due to a specification blunder in ECMAScript, typeof null is 'object',
                // so watch out for that case.

                if (!value) {
                    return 'null';
                }

                // Make an array to hold the partial results of stringifying this object value.

                gap += indent;
                partial = [];

                // Is the value an array?

                if (Object.prototype.toString.apply(value) === '[object Array]') {

                    // The value is an array. Stringify every element. Use null as a placeholder
                    // for non-JSON values.

                    length = value.length;
                    for (i = 0; i < length; i += 1) {
                        partial[i] = str(i, value) || 'null';
                    }

                    // Join all of the elements together, separated with commas, and wrap them in
                    // brackets.

                    v = partial.length === 0
                    ? '[]'
                    : gap
                    ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']'
                    : '[' + partial.join(',') + ']';
                    gap = mind;
                    return v;
                }

                // If the replacer is an array, use it to select the members to be stringified.

                if (rep && typeof rep === 'object') {
                    length = rep.length;
                    for (i = 0; i < length; i += 1) {
                        if (typeof rep[i] === 'string') {
                            k = rep[i];
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                } else {

                    // Otherwise, iterate through all of the keys in the object.

                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                }

                // Join all of the member texts together, separated with commas,
                // and wrap them in braces.

                v = partial.length === 0
                ? '{}'
                : gap
                ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}'
                : '{' + partial.join(',') + '}';
                gap = mind;
                return v;
        }
    }

    // If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {

            // The stringify method takes a value and an optional replacer, and an optional
            // space parameter, and returns a JSON text. The replacer can be a function
            // that can replace values, or an array of strings that will select the keys.
            // A default replacer method can be provided. Use of the space parameter can
            // produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

            // If the space parameter is a number, make an indent string containing that
            // many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

                // If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

            // If there is a replacer, it must be a function or an array.
            // Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

            // Make a fake root object containing our value under the key of ''.
            // Return the result of stringifying the value.

            return str('', { '': value });
        };
    }


    // If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {

            // The parse method takes a text and an optional reviver function, and returns
            // a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

                // The walk method is used to recursively walk the resulting structure so
                // that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


            // Parsing happens in four stages. In the first stage, we replace certain
            // Unicode characters with escape sequences. JavaScript handles many characters
            // incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

            // In the second stage, we run the text against regular expressions that look
            // for non-JSON patterns. We are especially concerned with '()' and 'new'
            // because they can cause invocation, and '=' because it can cause mutation.
            // But just to be safe, we want to reject all unexpected forms.

            // We split the second stage into 4 regexp operations in order to work around
            // crippling inefficiencies in IE's and Safari's regexp engines. First we
            // replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
            // replace all simple value tokens with ']' characters. Third, we delete all
            // open brackets that follow a colon or comma or that begin the text. Finally,
            // we look to see that the remaining characters are only whitespace or ']' or
            // ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$j/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

                // In the third stage we use the eval function to compile the text into a
                // JavaScript structure. The '{' operator is subject to a syntactic ambiguity
                // in JavaScript: it can begin a block or an object literal. We wrap the text
                // in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

                // In the optional fourth stage, we recursively walk the new structure, passing
                // each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function'
                    ? walk({ '': j }, '')
                    : j;
            }

            // If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
} ());