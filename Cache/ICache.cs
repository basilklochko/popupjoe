﻿using System.Collections.Generic;
using System.Data.SqlClient;
namespace Cache
{
    public interface ICache
    {
        bool Enabled { get; }
        object Cache { get; }
        void Insert(string key, object value);
        void Delete(string key);
        object Fetch(string key);
        List<string> GetAllKeys();
        void Reset();
        string GenCacheKey(string command, List<SqlParameter> parameters);
    }
}
