﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Cache
{
    public class Cacher : ICache
    {
        private ICache _cache = null;

        public Cacher(ICache cache)
        {
            _cache = cache;
        }

        public bool Enabled
        {
            get { return _cache.Enabled; }
        }

        public object Cache
        {
            get { return _cache; }
        }

        public void Insert(string key, object value)
        {
            _cache.Insert(key, value);
        }

        public void Delete(string key)
        {
            _cache.Delete(key);
        }

        public object Fetch(string key)
        {
            return _cache.Fetch(key);
        }

        public List<string> GetAllKeys()
        {
            return _cache.GetAllKeys();
        }

        public void Reset()
        {
            _cache.Reset();
        }

        public string GenCacheKey(string command, List<SqlParameter> parameters)
        {
            return _cache.GenCacheKey(command, parameters);
        }
    }
}
