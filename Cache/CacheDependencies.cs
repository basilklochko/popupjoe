﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cache
{
    public class CacheDependencies
    {
        public int ID { get; set; }
        public string Parent { get; set; }
        public List<string> Dependencies { get; set; }
    }
}
