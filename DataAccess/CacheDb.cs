﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cache;
using System.Data;

namespace DataAccess
{
    public static class CacheDb
    {
        public static List<CacheDependencies> GetDependencies() 
        {
            List<CacheDependencies> list = null;

            var cache = new Cacher(new HttpCache());
            var cacheObj = cache.Fetch("deps");

            if (cacheObj == null)
            {
                var result = new DataSource().ExecuteDataSet("svc.uspGetDependenciesList", null);

                if (result.DataSet != null && result.DataSet.Tables.Count > 0)
                {
                    list = new List<CacheDependencies>();

                    foreach (DataRow row in result.DataSet.Tables[0].Rows)
                    {
                        list.Add(new CacheDependencies()
                            {
                                ID = Convert.ToInt32(row["DependenciesID"]),
                                Parent = row["ParentSP"].ToString(),
                                Dependencies = row["Dependencies"].ToString().Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries).ToList()
                            });
                    }

                    cache.Insert("deps", list);
                }
            }
            else
            { 
                list = cacheObj as List<CacheDependencies>;
            }

            return list;
        }
    }
}
