
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblStatusLog]') AND type in (N'U'))
DROP TABLE [dbo].[tblStatusLog]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblStatusLog](
	[StatusLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[SourceTable] [varchar] (128) not NULL,
	[SourceTableID] [bigint] not null,
	[OldStatus] [bit] not null,
	[OldDateRevoked] [datetime] null,
	[NewStatus] [bit] not null,
	[NewDateRevoked] [datetime] null,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
 CONSTRAINT [PK_tblStatusLog] PRIMARY KEY CLUSTERED 
(
	[StatusLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblStatusLog] ADD  DEFAULT ((0)) FOR [OldStatus]
GO
ALTER TABLE [dbo].[tblStatusLog] ADD  DEFAULT ((0)) FOR [NewStatus]
GO
ALTER TABLE [dbo].[tblStatusLog] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[tblStatusLog] ADD  DEFAULT ((0)) FOR [UserCreated]
GO


