--!!!!
-- CHANGE FILENAME values for data and log files before running script!!!!



USE [master]
GO
/****** Object:  Database [ppjdb]    Script Date: 08/05/2012 11:38:39 ******/
CREATE DATABASE [ppjdb] ON  PRIMARY
( NAME = N'ppjdb', FILENAME = N'c:\SQL_Data\ppjdb.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
LOG ON
( NAME = N'ppjdb_log', FILENAME = N'C:\SQL_Data\ppjdb.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ppjdb] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ppjdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ppjdb] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [ppjdb] SET ANSI_NULLS OFF
GO
ALTER DATABASE [ppjdb] SET ANSI_PADDING OFF
GO
ALTER DATABASE [ppjdb] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [ppjdb] SET ARITHABORT OFF
GO
ALTER DATABASE [ppjdb] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [ppjdb] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [ppjdb] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [ppjdb] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [ppjdb] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [ppjdb] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [ppjdb] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [ppjdb] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [ppjdb] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [ppjdb] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [ppjdb] SET  DISABLE_BROKER
GO
ALTER DATABASE [ppjdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [ppjdb] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [ppjdb] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [ppjdb] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [ppjdb] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [ppjdb] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [ppjdb] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [ppjdb] SET  READ_WRITE
GO
ALTER DATABASE [ppjdb] SET RECOVERY SIMPLE
GO
ALTER DATABASE [ppjdb] SET  MULTI_USER
GO
ALTER DATABASE [ppjdb] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [ppjdb] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'ppjdb', N'ON'
GO
USE [ppjdb]
GO
/****** Object:  Table [dbo].[tblStore]    Script Date: 08/05/2012 11:38:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStore](
[StoreID] [bigint] IDENTITY(1,1) NOT NULL,
[eCommerceType] [int] NOT NULL,
[StoreName] [nvarchar](255) NOT NULL,
[PostalName] [nvarchar](255) NOT NULL,
[Address1] [nvarchar](255) NOT NULL,
[Address2] [nvarchar](255) NULL,
[City] [nvarchar](100) NOT NULL,
[StateProvince] [nvarchar](64) NOT NULL,
[PostalCode] [nvarchar](64) NOT NULL,
[CountryCode] [varchar](3) NOT NULL,
[StoreGUID] [uniqueidentifier] NOT NULL,
[RevokeStatus] [bit] NOT NULL,
[DateRevoked] [datetime] NULL,
[ParentStoreID] [int] NULL,
[StoreNode] [hierarchyid] NOT NULL,
[SessionExpiration] [int] NOT NULL,
[ContractExpirationDate] [datetime] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblStore] PRIMARY KEY CLUSTERED
(
[StoreID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblSQLAudit]    Script Date: 08/05/2012 11:38:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSQLAudit](
[SQLAuditID] [bigint] IDENTITY(1,1) NOT NULL,
[ProcessID] [bigint] NOT NULL,
[ErrorNumber] [int] NULL,
[ErrorSeverity] [int] NULL,
[ErrorState] [int] NULL,
[ErrorProcedure] [varchar](512) NULL,
[ErrorLine] [int] NULL,
[ErrorMessage] [varchar](4000) NULL,
[Parameters] [varchar](1024) NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
CONSTRAINT [PK_tblSQLAudit_ProcessID_SQLAuditID] PRIMARY KEY CLUSTERED
(
[ProcessID] ASC,
[SQLAuditID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblMsgOutput]    Script Date: 08/05/2012 11:38:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMsgOutput](
[MsgOutputID] [bigint] IDENTITY(1,1) NOT NULL,
[SPName] [varchar](64) NOT NULL,
[RetCode] [int] NOT NULL,
[MsgTier1] [nvarchar](2048) NULL,
[UIErrCode] [int] NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblMsgOutput] PRIMARY KEY CLUSTERED
(
[MsgOutputID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblLookUp]    Script Date: 08/05/2012 11:38:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLookUp](
[LookUpID] [int] IDENTITY(1,1) NOT NULL,
[LookUpShortName] [varchar](64) NOT NULL,
[LookUpDescription] [varchar](255) NULL,
[LookUpSubType] [int] NOT NULL,
[LookUpSubID] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblLookUp] PRIMARY KEY CLUSTERED
(
[LookUpID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblUIMessage]    Script Date: 08/05/2012 11:38:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUIMessage](
[UIMessageID] [int] IDENTITY(1,1) NOT NULL,
[UIErrCode] [int] NOT NULL,
[MsgTier2] [varchar](1024) NULL,
[MsgTier3] [varchar](1024) NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblUIMessage] PRIMARY KEY CLUSTERED
(
[UIMessageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY],
CONSTRAINT [UC_tblUIMessage_UIErrCode] UNIQUE NONCLUSTERED
(
[UIErrCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblActiveSession]    Script Date: 08/05/2012 11:38:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSessionList](
[SessionID] [bigint] IDENTITY(1,1) NOT NULL,
[StoreIPListID] [bigint] NOT NULL,
[PopUpJoeSessionID] [nvarchar](256) NOT NULL,
[Expiration] [datetime] NOT NULL,
[OrderSubmitFlag] [bit] NOT NULL,
[DateOrderSubmit] [datetime] NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblSessionList] PRIMARY KEY CLUSTERED
(
[SessionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblWEBAudit]    Script Date: 08/05/2012 11:38:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblWEBAudit](
[WEBAuditID] [bigint] IDENTITY(1,1) NOT NULL,
[WEBServer] [nvarchar](512) NULL,
[ErrSource] [varchar](512) NULL,
[ErrorNumber] [int] NULL,
[ErrorLine] [int] NULL,
[ErrorMessage] [varchar](4000) NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
CONSTRAINT [PK_tblWEBAudit] PRIMARY KEY CLUSTERED
(
[WEBAuditID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[uspRecordWEBAudit]    Script Date: 08/05/2012 11:38:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRecordWEBAudit]
@szWEBServer [nvarchar](512),
@szErrSource [varchar] (512) = NULL,
@iErrorNumber [int] = 0,
@iErrorLine [int] = 0,
@szErrorMessage [varchar] (4000),
@iUserCreated [int] = 0
AS
BEGIN
SET NOCOUNT ON
INSERT INTO tblWEBAudit
([WEBServer],
[ErrSource],
[ErrorNumber],
[ErrorLine],
[ErrorMessage],
[DateCreated],
[UserCreated]
)
VALUES
(@szWEBServer,
@szErrSource,
@iErrorNumber,
@iErrorLine,
@szErrorMessage,
GETDATE(),
@iUserCreated)
END
GO
/****** Object:  StoredProcedure [dbo].[uspRecordSQLAudit]    Script Date: 08/05/2012 11:38:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRecordSQLAudit]
@biProcessID bigint
,@iErrorNumber int
,@iErrorSeverity int
,@iErrorState int
,@szErrorProcedure varchar(512)
,@iErrorLine int
,@szErrorMessage varchar(4000)
,@szParameters varchar(1024) = null
,@iInserted [int] = 0

AS
BEGIN
SET NOCOUNT ON
INSERT INTO tblSQLAudit
(ProcessID
,ErrorNumber
,ErrorSeverity
,ErrorState
,ErrorProcedure
,ErrorLine
,ErrorMessage
,[Parameters]
,DateCreated
,UserCreated
)
VALUES
(@biProcessID
,@iErrorNumber
,@iErrorSeverity
,@iErrorState
,@szErrorProcedure
,@iErrorLine
,@szErrorMessage
,@szParameters
,GETDATE()
,@iInserted)
END
GO
/****** Object:  Table [dbo].[tblWSLog]    Script Date: 08/05/2012 11:38:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblWSLog](
[WSLogID] [bigint] IDENTITY(1,1) NOT NULL,
[StoreID] [bigint] NULL,
[StoreIPListID] [bigint] NULL,
[PopUpJoeSessionID] [nvarchar](256) NOT NULL,
[OrderTotal] [decimal](7, 2) NOT NULL,
[ActivateTimer] [char](1) NOT NULL,
[TimerValue] [int] NOT NULL,
[CouponID] [bigint] NULL,
[PopUpFlag] [bit] NOT NULL,
[DatePopUp] [datetime] NULL,
[OrderSubmitFlag] [bit] NOT NULL,
[DateOrderSubmit] [datetime] NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblWSLog] PRIMARY KEY CLUSTERED
(
[WSLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblUser]    Script Date: 08/05/2012 11:38:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUser](
[UserID] [bigint] IDENTITY(1,1) NOT NULL,
[StoreID] [bigint] NOT NULL,
[FirstName] [nvarchar](128) NOT NULL,
[LastName] [nvarchar](128) NOT NULL,
[UserEmail] [nvarchar](512) NOT NULL,
[Password] [nvarchar](128) NOT NULL,
[SecureQuestion] [nvarchar](512) NOT NULL,
[SecureAnswer] [nvarchar](512) NOT NULL,
[DateLastLogin] [datetime] NULL,
[EmailAlertRecipient] [bit] NOT NULL,
[BillingReportRecipient] [bit] NOT NULL,
[ResetPassword] [bit] NOT NULL,
[RevokeStatus] [bit] NOT NULL,
[DateRevoked] [datetime] NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblUser] PRIMARY KEY CLUSTERED
(
[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblStoreIPList]    Script Date: 08/05/2012 11:38:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStoreIPList](
[StoreIPListID] [bigint] IDENTITY(1,1) NOT NULL,
[StoreID] [bigint] NOT NULL,
[IPAddress] [varchar](64) NOT NULL,
[DomainName] [nvarchar](128) NOT NULL,
[TimerValue1] [int] NOT NULL,
[TimerValue2] [int] NOT NULL,
[RevokeStatus] [bit] NOT NULL,
[DateRevoked] [datetime] NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblStoreIPList] PRIMARY KEY CLUSTERED
(
[StoreIPListID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblBilling]    Script Date: 08/05/2012 11:38:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBilling](
[BillingID] [bigint] IDENTITY(1,1) NOT NULL,
[StoreID] [bigint] NOT NULL,
[StoreIPListID] [bigint] NULL,
[BillingPeriod] [bigint] NOT NULL,
[BillingType] [int] NOT NULL,
[BillingUnit] [int] NOT NULL,
[BillingRate] [decimal](7, 4) NOT NULL,
[DateFrom] [datetime] NOT NULL,
[DateTo] [datetime] NOT NULL,
[BilledDate] [datetime] NOT NULL,
[NumberOfUnits] [decimal](12, 2) NOT NULL,
[Total] [decimal](12, 2) NOT NULL,
[Mode] [bit] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblBilling] PRIMARY KEY CLUSTERED
(
[BillingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCoupon]    Script Date: 08/05/2012 11:38:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCoupon](
[CouponID] [bigint] IDENTITY(1,1) NOT NULL,
[StoreIPListID] [bigint] NOT NULL,
[eCommCouponID] [nvarchar](64) NOT NULL,
[CouponDescription] [nvarchar](512) NULL,
[CouponValue] [decimal](8, 3) NULL,
[MinOrderAmt] [decimal](7, 2) NOT NULL,
[MaxOrderAmt] [decimal](7, 2) NOT NULL,
[CouponType] [int] NOT NULL,
[MsgToPopUp] [nvarchar](max) NOT NULL,
[ExpirationDate] [datetime] NULL,
[LimitedNumber] [bit] NOT NULL,
[Uses] [int] null,
[RevokeStatus] [bit] NOT NULL,
[DateRevoked] [datetime] NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblCoupon] PRIMARY KEY CLUSTERED
(
[CouponID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBSetting]    Script Date: 08/05/2012 11:38:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBSetting](
[BSettingID] [bigint] IDENTITY(1,1) NOT NULL,
[StoreID] [bigint] NOT NULL,
[BillingPeriod] [bigint] NOT NULL,
[BillingType] [int] NOT NULL,
[BillingUnit] [int] NOT NULL,
[BillingRate] [decimal](7, 4) NOT NULL,
[BillingDate] [datetime] NOT NULL,
[LastBilledDate] [datetime] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblBSetting] PRIMARY KEY CLUSTERED
(
[BSettingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBillingEmailLog]    Script Date: 08/05/2012 11:38:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBillingEmailLog](
[BillingEmailLogID] [bigint] IDENTITY(1,1) NOT NULL,
[BillingID] [bigint] NOT NULL,
[UserID] [bigint] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
[DateUpdated] [datetime] NULL,
[UserUpdated] [bigint] NULL,
CONSTRAINT [PK_tblBillingEmailLog] PRIMARY KEY CLUSTERED
(
[BillingEmailLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblShoppingList]    Script Date: 08/05/2012 11:38:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblShoppingList](
[ShoppingListID] [bigint] IDENTITY(1,1) NOT NULL,
[WSLogID] [bigint] NOT NULL,
[SKU] [nvarchar](64) NULL,
[ProductName] [nvarchar](255) NOT NULL,
[ItemCategory] [nvarchar](255) NULL,
[Quantity] [decimal](12, 4) NOT NULL,
[Price] [decimal](7, 2) NOT NULL,
[CouponID] [bigint] NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [bigint] NOT NULL,
CONSTRAINT [PK_tblShoppingList] PRIMARY KEY CLUSTERED
(
[ShoppingListID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__tblStore__StoreG__7F60ED59]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblStore] ADD  DEFAULT (newid()) FOR [StoreGUID]
GO
/****** Object:  Default [DF__tblStore__Revoke__00551192]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblStore] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblStore__Sessio__014935CB]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblStore] ADD  DEFAULT ((43200)) FOR [SessionExpiration]
GO
/****** Object:  Default [DF__tblStore__DateCr__023D5A04]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblStore] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblStore__UserCr__03317E3D]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblStore] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF_tblSQLAudit_DateCreated]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblSQLAudit] ADD  CONSTRAINT [DF_tblSQLAudit_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblSQLAudit_UserCreated]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblSQLAudit] ADD  CONSTRAINT [DF_tblSQLAudit_UserCreated]  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF_tblMsgOutput_DateCreated]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblMsgOutput] ADD  CONSTRAINT [DF_tblMsgOutput_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblMsgOutput_UserCreated]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblMsgOutput] ADD  CONSTRAINT [DF_tblMsgOutput_UserCreated]  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblLookUp__LookU__34C8D9D1]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblLookUp] ADD  DEFAULT ((0)) FOR [LookUpSubID]
GO
/****** Object:  Default [DF__tblLookUp__DateC__35BCFE0A]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblLookUp] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblLookUp__UserC__36B12243]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblLookUp] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF_tblUIMessage_DateCreated]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblUIMessage] ADD  CONSTRAINT [DF_tblUIMessage_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblUIMessage_UserCreated]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblUIMessage] ADD  CONSTRAINT [DF_tblUIMessage_UserCreated]  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblActive__DateC__1B0907CE]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblSessionList] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblActive__UserC__1BFD2C07]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblSessionList] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblActive__UserC__1BFD2C07]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblSessionList] ADD  DEFAULT ((0)) FOR [OrderSubmitFlag]
GO
/****** Object:  Default [DF_tblWEBAudit_DateCreated]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblWEBAudit] ADD  CONSTRAINT [DF_tblWEBAudit_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblWEBAudit_UserCreated]    Script Date: 08/05/2012 11:38:41 ******/
ALTER TABLE [dbo].[tblWEBAudit] ADD  CONSTRAINT [DF_tblWEBAudit_UserCreated]  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblWSLog__OrderT__267ABA7A]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ((0)) FOR [OrderTotal]
GO
/****** Object:  Default [DF__tblWSLog__Activa__276EDEB3]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ('N') FOR [ActivateTimer]
GO
/****** Object:  Default [DF__tblWSLog__TimerV__286302EC]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ((0)) FOR [TimerValue]
GO
/****** Object:  Default [DF__tblWSLog__PopUpF__29572725]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ((0)) FOR [PopUpFlag]
GO
/****** Object:  Default [DF__tblWSLog__OrderS__2A4B4B5E]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ((0)) FOR [OrderSubmitFlag]
GO
/****** Object:  Default [DF__tblWSLog__DateCr__2B3F6F97]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblWSLog__UserCr__2C3393D0]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblUser__EmailAl__0DAF0CB0]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT ((0)) FOR [EmailAlertRecipient]
GO
/****** Object:  Default [DF__tblUser__Billing__0EA330E9]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT ((0)) FOR [BillingReportRecipient]
GO
/****** Object:  Default [DF__tblUser__RevokeS__0F975522]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT ((0)) FOR [ResetPassword]
GO
/****** Object:  Default [DF__tblUser__RevokeS__0F975522]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblUser__DateCre__108B795B]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblUser__UserCre__117F9D94]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblStoreI__Timer__060DEAE8]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblStoreIPList] ADD  DEFAULT ((300)) FOR [TimerValue1]
GO
/****** Object:  Default [DF__tblStoreI__Timer__07020F21]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblStoreIPList] ADD  DEFAULT ((300)) FOR [TimerValue2]
GO
/****** Object:  Default [DF__tblStoreI__Revok__07F6335A]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblStoreIPList] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblStoreI__DateC__08EA5793]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblStoreIPList] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblStoreI__UserC__09DE7BCC]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblStoreIPList] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblBillin__Billi__398D8EEE]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT ((0)) FOR [BillingUnit]
GO
/****** Object:  Default [DF__tblBillin__Billi__3A81B327]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT ((1)) FOR [BillingRate]
GO
/****** Object:  Default [DF__tblBillin__Bille__3B75D760]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT (getdate()) FOR [BilledDate]
GO
/****** Object:  Default [DF__tblBilling__Mode__3C69FB99]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT ((0)) FOR [Mode]
GO
/****** Object:  Default [DF__tblBillin__DateC__3D5E1FD2]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblBillin__UserC__3E52440B]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblCoupon__Revok__15502E78]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblCoupon] ADD  DEFAULT ((0)) FOR [LimitedNumber]
GO
/****** Object:  Default [DF__tblCoupon__Revok__15502E78]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblCoupon] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblCoupon__DateC__164452B1]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblCoupon] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblCoupon__UserC__173876EA]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblCoupon] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblBSetti__Billi__1ED998B2]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBSetting] ADD  DEFAULT ((0)) FOR [BillingUnit]
GO
/****** Object:  Default [DF__tblBSetti__Billi__1FCDBCEB]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBSetting] ADD  DEFAULT ((1)) FOR [BillingRate]
GO
/****** Object:  Default [DF__tblBSetti__LastB__20C1E124]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBSetting] ADD  DEFAULT (getdate()) FOR [LastBilledDate]
GO
/****** Object:  Default [DF__tblBSetti__DateC__21B6055D]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBSetting] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblBSetti__UserC__22AA2996]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBSetting] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblBillin__DateC__4222D4EF]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBillingEmailLog] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblBillin__UserC__4316F928]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBillingEmailLog] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblShoppi__DateC__300424B4]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblShoppingList] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblShoppi__UserC__30F848ED]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblShoppingList] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  ForeignKey [FK_tblWSLog_StoreID]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblWSLog]  WITH CHECK ADD  CONSTRAINT [FK_tblWSLog_StoreID] FOREIGN KEY([StoreID])
REFERENCES [dbo].[tblStore] ([StoreID])
GO
ALTER TABLE [dbo].[tblWSLog] CHECK CONSTRAINT [FK_tblWSLog_StoreID]
GO
/****** Object:  ForeignKey [FK_tblUser_StoreID]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblUser]  WITH CHECK ADD  CONSTRAINT [FK_tblUser_StoreID] FOREIGN KEY([StoreID])
REFERENCES [dbo].[tblStore] ([StoreID])
GO
ALTER TABLE [dbo].[tblUser] CHECK CONSTRAINT [FK_tblUser_StoreID]
GO
/****** Object:  ForeignKey [FK_tblStoreIPList_StoreID]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblStoreIPList]  WITH CHECK ADD  CONSTRAINT [FK_tblStoreIPList_StoreID] FOREIGN KEY([StoreID])
REFERENCES [dbo].[tblStore] ([StoreID])
GO
ALTER TABLE [dbo].[tblStoreIPList] CHECK CONSTRAINT [FK_tblStoreIPList_StoreID]
GO
/****** Object:  ForeignKey [FK_tblBilling_StoreID]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBilling]  WITH CHECK ADD  CONSTRAINT [FK_tblBilling_StoreID] FOREIGN KEY([StoreID])
REFERENCES [dbo].[tblStore] ([StoreID])
GO
ALTER TABLE [dbo].[tblBilling] CHECK CONSTRAINT [FK_tblBilling_StoreID]
GO
/****** Object:  ForeignKey [FK_tblCoupon_StoreID]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tblCoupon_StoreIPListID] FOREIGN KEY([StoreIPListID])
REFERENCES [dbo].[tblStoreIPList] ([StoreIPListID])
GO
ALTER TABLE [dbo].[tblCoupon] CHECK CONSTRAINT [FK_tblCoupon_StoreIPListID]
GO
/****** Object:  ForeignKey [FK_tblBSetting_StoreID]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBSetting]  WITH CHECK ADD  CONSTRAINT [FK_tblBSetting_StoreID] FOREIGN KEY([StoreID])
REFERENCES [dbo].[tblStore] ([StoreID])
GO
ALTER TABLE [dbo].[tblBSetting] CHECK CONSTRAINT [FK_tblBSetting_StoreID]
GO
/****** Object:  ForeignKey [FK_tblBillingEmailLog_BillingID]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBillingEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_tblBillingEmailLog_BillingID] FOREIGN KEY([BillingID])
REFERENCES [dbo].[tblBilling] ([BillingID])
GO
ALTER TABLE [dbo].[tblBillingEmailLog] CHECK CONSTRAINT [FK_tblBillingEmailLog_BillingID]
GO
/****** Object:  ForeignKey [FK_tblBillingEmailLog_UserID]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblBillingEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_tblBillingEmailLog_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[tblUser] ([UserID])
GO
ALTER TABLE [dbo].[tblBillingEmailLog] CHECK CONSTRAINT [FK_tblBillingEmailLog_UserID]
GO
/****** Object:  ForeignKey [FK_tblShoppingList_WSLogID]    Script Date: 08/05/2012 11:38:45 ******/
ALTER TABLE [dbo].[tblShoppingList]  WITH CHECK ADD  CONSTRAINT [FK_tblShoppingList_WSLogID] FOREIGN KEY([WSLogID])
REFERENCES [dbo].[tblWSLog] ([WSLogID])
GO
ALTER TABLE [dbo].[tblShoppingList] CHECK CONSTRAINT [FK_tblShoppingList_WSLogID]
GO

USE [ppjdb]
GO

/****** Object:  View [dbo].[vwGetNewID]    Script Date: 08/05/2012 17:05:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwGetNewID] AS SELECT NewId() AS [NewID]
GO
USE [ppjdb]
GO

/****** Object:  UserDefinedFunction [dbo].[fnGeneratePassword]    Script Date: 08/05/2012 17:06:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[fnGeneratePassword] (
@pLength int = 8, --default to 8 characters
@charSet int = 0 -- 1 is alphanumeric + special characters,
-- 0 is alphanumeric
)
RETURNS nvarchar(128)
AS
BEGIN

IF @pLength < 8 SET @pLength = 8 -- set minimum length
	ELSE IF @pLength > 128 SET @pLength = 128 -- set maximum length
 
	DECLARE
		@password nvarchar(128),
		@string varchar(72), --52 possible letters + 10 possible numbers + up to 20 possible extras
		@numbers varchar(10),
		@extra varchar(20),
		@stringlen tinyint,
		@index tinyint,
		@pCount int,
		@Return varchar(50)
		
	SET @pCount = 1 --generate 1 passwords unless otherwise specified
 
	--table variable to hold password list
	--DECLARE @PassList TABLE (
	--	[password] varchar(50)
	--)
	 
	-- eliminate 0, 1, I, l, O to make the password more readable
	SET @string = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz' -- option @charset = 0
	SET @numbers = '123456789'
	SET @extra = '^_!@#$&?-' -- special characters
	 
	SET @string =
		CASE @charset
			WHEN 1 THEN @string + @numbers + @extra
			ELSE @string + @numbers
		END
	 
	SET @stringlen = len(@string)
	 
	WHILE (@pCount > 0)
	BEGIN
		SET @password = ''
		DECLARE @i int; SET @i = @pLength
		WHILE (@i > 0)
		BEGIN
			declare @x varchar(50)
			SELECT @x = [NewId] FROM dbo.vwGetNewID
			SET @index = (ABS(CHECKSUM(@x)) % @stringlen) + 1 --or rand()
			SET @password = @password + SUBSTRING(@string, @index, 1)
			SET @i -= 1 --SET @pLength = @pLength - 1
		END
		SET @pCount -= 1
	END
	SELECT @Return = @password
	RETURN @Return
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblEmailLog](
	[EmailLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[Source] [varchar](100) NOT NULL,
	[EmailSubject] [nvarchar](512) NOT NULL,
	[EmailDetails] [nvarchar](max) NULL,
	[RecipientList] [nvarchar](max) NOT NULL,
	[DateSend] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,	
 CONSTRAINT [PK_tblEmailLog] PRIMARY KEY CLUSTERED 
(
	[EmailLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tblEmailLog] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[tblEmailLog] ADD  DEFAULT (0) FOR [UserCreated]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vwLookUp] 
AS 
with t0 as (
select LookUpSubType as LookUpID,LookUpShortName as LookUpName
	from tblLookUp (nolock) 
	where LookUpSubID = 0
)
select t0.*,
t.LookUpShortName,t.LookUpSubType
	from tblLookUp t (nolock) 
	inner join t0 on t0.LookUpID = t.LookUpSubID
	where t.LookUpSubID <> 0
--	order by t0.LookUpName,t.LookUpSubType
GO

-----------------------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspSendEmail] 
	@szSource [varchar](100) = NULL,
	@szEmailSubject [nvarchar](512),
	@szEmailDetails [nvarchar](max),
	@szRecipientListAddresses [nvarchar](max),
	@szSQLQueryString [nvarchar] (max),	/* query string to email */
	@iUserID [int] = 0,
	@iQResultWidth [int] = 125,			/* for query result */
	@szEmailFormat [varchar] (10) = 'Text', /* type of email body Text/HTML*/
	@iRetCode [int] OUTPUT
AS
/*
	@iRetCode :
		0 sucessful;
		-1 No Recipient List is empty
		-2 Database profile record not found
		-3 Database profile is empty
		-4 Failed to send message inside of call to sp_send_dbmail
*/
BEGIN
	SET NOCOUNT ON

	Declare @iMessageKey [int],
		 @szMsg [varchar] (max),
		 @iMailResult [int],
		 @szDBMailProfileName [nvarchar] (255),
		 @szCurrentDBName [nvarchar] (128),
		 @bAttachResult [bit],
		 @szTmpSource [varchar] (100)

	select @iRetCode = 0
	
	--Clean up parameters
	select	@szEmailSubject		= coalesce(@szEmailSubject,''),
			@szEmailDetails	= coalesce(@szEmailDetails, ''),
			@szSQLQueryString = coalesce(@szSQLQueryString,''),
			@szRecipientListAddresses = coalesce(@szRecipientListAddresses,''),
			@szTmpSource = 'uspSendEmail',
			@iUserID = coalesce(@iUserID,0)
	
	--Log and fail if no addresses stored for the recipient key list
	if len(rtrim(@szRecipientListAddresses)) = 0
		begin
			--select @szMsg = 'No addresses stored for recipient list key [' + @pRecipientListKey
			--		+ '].  The intended message follows.'
			--		+ char(10) + char(13) + 'Subj: ' + rtrim(@szEmailSubject)
			--		+ char(10) + char(13) + 'Message: ' + rtrim(@szEmailDetails)

			--insert into dbo.tblEventLog (BatchKey, ProcessKey, EventMsg, EventDetails, EventSource)
			--values (@pBatchKey,@pProcessKey,'No addresses stored for recipient list key [' + @pRecipientListKey
			--		+ '].',@szMsg,@szTmpSource)

			Select @iRetCode = -1
			Return
		end

	--Retrieve DB mail profile name
	select @szDBMailProfileName = LookUpShortName
		from [dbo].[vwLookUp] (nolock)
		where LookUpName = 'DBMail' and LookupSubType = 1
	
	--Log and fail if no match to recipient key list
	if @@rowcount = 0
		begin
			--select @szMsg = 'No DBMail profile record found when '
			--		+ 'attempting to send the following message.'
			--		+ char(10) + char(13) + 'Subj: ' + rtrim(@szEmailSubject)
			--		+ char(10) + char(13) + 'Message: ' + rtrim(@szEmailDetails)

			--insert into dbo.tblEventLog (BatchKey, ProcessKey, EventMsg, EventDetails, EventSource)
			--values (@pBatchKey,@pProcessKey,'No tblSettings record found when '
			--		+ 'attempting to send the following message.',@szMsg,@szTmpSource)

			Select @iRetCode = -2
			Return
		end

	--Log and fail if no profile name is stored
	if len(rtrim(@szDBMailProfileName)) = 0
		begin
			--select @szMsg = 'DBMail profile was empty when '
			--		+ 'attempting to send the following message.'
			--		+ char(10) + char(13) + 'Subj: ' + rtrim(@szEmailSubject)
			--		+ char(10) + char(13) + 'Message: ' + rtrim(@szEmailDetails)

			--insert into dbo.tblEventLog (BatchKey, ProcessKey, EventMsg, EventDetails, EventSource)
			--values (@pBatchKey,@pProcessKey,'tblSettings.DBMailProfileName was empty when '
			--		+ 'attempting to send the following message.',@szMsg,@szTmpSource)

			Select @iRetCode = -3
			Return
		end

	--All validations passed
	select @bAttachResult = case when len(@szSQLQueryString) = 0 then 0 else 1 end;
	select @szCurrentDBName = DB_NAME(dbid) from master.dbo.sysprocesses WHERE spid = @@SPID;

	--Add entry to messages table
	insert into [dbo].[tblEmailLog](
		[Source],
		[EmailSubject],
		[EmailDetails],
		[RecipientList],
		[DateCreated],
		[UserCreated])	
	values (@szSource,
		@szEmailSubject,
		@szEmailDetails,
		@szRecipientListAddresses,
		GETDATE(),
		@iUserID)

	--Remember new key
	Select @iMessageKey = scope_identity()

	--Physically send the message
	execute @iMailResult = msdb.dbo.sp_send_dbmail 
			@Profile_Name = @szDBMailProfileName, 
			@recipients = @szRecipientListAddresses, 
			@Subject = @szEmailSubject,
			@execute_query_database = @szCurrentDBName,
			@Body = @szEmailDetails,
			@body_format = @szEmailFormat,
			@query = @szSQLQueryString,
			@query_result_width = @iQResultWidth,
			@query_result_header = 1,
			@attach_query_result_as_file = @bAttachResult;

	--Only update the sent date if the message appears to have been sent
	if @iMailResult = 0
	begin
		Update dbo.tblEmailLog set [DateSend] = GetDate() where EmailLogID = @iMessageKey
		Select @iRetCode = 0
	end
	else
	begin
		Select @iRetCode = -4
		Return
	end

	Return
END
GO
------------------------------------------------------------------------------------------------

/* CREATE A NEW ROLE */
CREATE ROLE db_executor
GO
/* GRANT EXECUTE TO THE ROLE */
GRANT EXECUTE TO db_executor
GO