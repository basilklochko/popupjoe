

USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspStoreChangeStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspStoreChangeStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Reset store's RevokeStatus
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/13/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @biStoreID is NULL or zero
	@iRetCode = -3, Store not found
	@iRetCode = -4, Store already has requested status
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/13/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------
	declare
		@biStoreID [bigint]
		,@bRevokeStatus [bit]
		,@biUpdUser [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biStoreID = 2
		,@bRevokeStatus = 0

	exec [svc].[uspStoreChangeStatus]
		@biStoreID
		,@bRevokeStatus
		,@biUpdUser
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		
select * from tblStore where userId = 1
*/

CREATE PROCEDURE [svc].[uspStoreChangeStatus]
	@biStoreID [bigint]
	,@bRevokeStatus [bit] = 1
	,@biUpdUser [bigint] = null
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspStoreChangeStatus')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

declare @bStoreRevokeStatus [bit],
	@bTmpRevokeStatus [bit]	,
	@dtTmpDateRevoked [datetime]

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bRevokeStatus = coalesce(@bRevokeStatus,0)
	,@biUpdUser = coalesce(@biUpdUser,0)
	
-- Validate parameters
if coalesce(@biStoreID,'')=0
-- Parameter is NULL or zero
	set @iRetCode = -2
 begin
	select 
		@dtTmpDateRevoked = s.DateRevoked,
		@bTmpRevokeStatus = s.RevokeStatus
		from dbo.tblStore s (nolock)
		where StoreID = @biStoreID
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		set @iRetCode = -3
	 end
	else if @bTmpRevokeStatus=@bRevokeStatus
	 begin
-- user has requested status	 
		select @iRetCode = -4,
			@szErrSubst='RevokeStatus='+CAST(@bRevokeStatus as [varchar] (10))			
	 end
	else
	 begin
-- update Store

		-- do we have outer transaction?
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspStoreChangeStatus;
		else
		  BEGIN TRANSACTION;

		update dbo.tblStore set [RevokeStatus] = @bRevokeStatus,
			DateRevoked = case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			DateUpdated = GETDATE(),
			UserUpdated = @biUpdUser
			where StoreID = @biStoreID

		insert into dbo.tblStatusLog (SourceTable,
			SourceTableID,
			OldStatus,
			OldDateRevoked,
			NewStatus,
			NewDateRevoked,
			DateCreated,
			UserCreated)
		values ('tblStore',
			@biStoreID,
			@bTmpRevokeStatus,
			@dtTmpDateRevoked,
			@bRevokeStatus,
			case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			GETDATE(),
			@biUpdUser)
			
		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;
		  set @TranCounter = NULL
		 end 
	 end 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspStoreChangeStatus;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

if exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspStoreChangeStatus')
delete from dbo.tblMsgOutput where SPName = 'uspStoreChangeStatus'
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspStoreChangeStatus',-1, 'SQL update error',207
union all
select 'uspStoreChangeStatus',-2, 'Parameter @biStoreID is NULL or zero',207
union all
select 'uspStoreChangeStatus',-3, 'Store {0} not found in dbo.tblStore table',207
union all
select 'uspStoreChangeStatus',-4, 'Store already has requested status',206
go
