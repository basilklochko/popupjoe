insert into [dbo].[tblLookUp](
	[LookUpShortName], 
	[LookUpSubType])
select 'DBMail',1
union all
select 'CouponType',2
union all
select 'BillingPeriod',3
union all
select 'BillingType',4
union all
select 'eCommerceType',5

insert into [dbo].[tblLookUp](
	[LookUpShortName], 
	[LookUpSubType],
	[LookUpSubID])
select 'ppj',1,1
--union all
--select 'StoreAdmin',2,1
--union all
--select 'CouponAdmin',3,1
--union all
--select 'ReportAdmin',4,1

union all
select 'Fixed Amount',1,2
union all
select '% of OrderTotal',2,2
union all
select 'Free Shipping',3,2

union all
select 'Quarterly',1,3
union all
select 'Monthly',2,3
union all
select 'Bi-Weekly',3,3
union all
select 'Weekly',4,3

union all
select 'Fixed Amount per BullingPeriod',1,4
union all
select 'Fixed Amount per PopUp',2,4
union all
select 'Fixed Amount per Submit',3,4
union all
select '% OrderTotal on PopUp',4,4
union all
select '% OrderTotal on Submit',5,4

union all
select 'Magento',1,5
--union all
--select 'ZenCart',2,5
--union all
--select 'VirtueMart',3,5

GO