
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspUserChangePassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspUserChangePassword]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Reset user's password on new or generated randomly value
	 and send email, if password was generated
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/08/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @biUserID is NULL or zero
	@iRetCode = -3, Some of Password parameters is NULL or empty
	@iRetCode = -4, User not found
	@iRetCode = -5, User has revoke status
	@iRetCode = -6, Store has revoke status
	@iRetCode = -7, parameter @szPasswordOld does not match
	@iRetCode = -8, Email template not found
	@iRetCode = -9, error in [dbo].[uspSendEmail]	
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/08/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------
select * from tblUser
	declare
		@biUserID [bigint]
		,@szPasswordOld [nvarchar] (128)
		,@szPassword [nvarchar] (128)
		,@szSecureQuestion [nvarchar] (512)
		,@szSecureAnswer [nvarchar] (512)
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biUserID = 1
		,@szPasswordOld = 'test'
		,@szPassword = 'test1'		
		,@szSecureQuestion ='test'
		,@szSecureAnswer ='test'

	exec [svc].[uspUserChangePassword]
		@biUserID
		,@szPasswordOld
		,@szPassword
		,@szSecureQuestion
		,@szSecureAnswer
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  


	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspUserChangePassword]
	@biUserID [bigint]
	,@szPasswordOld [nvarchar] (128) = NULL
	,@szPassword [nvarchar] (128) = NULL
	,@szSecureQuestion [nvarchar] (512) = NULL
	,@szSecureAnswer [nvarchar] (512) = NULL		
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserChangePassword')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

declare @bRevokeStatus [bit],
	@szTmpPassword [nvarchar] (128),
	@biTmpUserID [bigint],
	@szTmpSecureQuestion [nvarchar] (512),
	@bStoreRevokeStatus [bit],
	@bResetPassword [bit],
	@szUserEmail [nvarchar] (512)	

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bRevokeStatus = 1
	,@szTmpPassword = ''
	,@biTmpUserID = 0
--	,@szSecureQuestion = ''
	,@bStoreRevokeStatus= 1

-- Validate parameters
if coalesce(@biUserID,'')=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else if coalesce(@szPassword,'')<>'' and (coalesce(@szPasswordOld,'')='' or coalesce(@szSecureQuestion,'')='' or coalesce(@szSecureAnswer,'')='')
 begin
	set @iRetCode = -3
  end
else 	
 begin
	select 
		@bRevokeStatus = u.RevokeStatus,
		@szTmpPassword = u.[Password],
		@szTmpSecureQuestion = u.SecureQuestion,
		@bStoreRevokeStatus = s.RevokeStatus,
		@bResetPassword = ResetPassword,
		@szUserEmail = UserEmail
		from dbo.tblUser u (nolock) 
		left join dbo.tblStore s (nolock) on s.StoreID = u.StoreID
		where UserID = @biUserID
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		set @iRetCode = -4
	 end
	else if coalesce(@bRevokeStatus,0)=1
	 begin
-- user has Revoked status	 
		set @iRetCode = -5
	 end
	else if coalesce(@bStoreRevokeStatus,0)=1
	 begin
-- user has Revoked status	 
		set @iRetCode = -6
	 end
	else if (coalesce(@szPasswordOld,'')<>'' and @szTmpPassword <> @szPasswordOld COLLATE SQL_Latin1_General_CP1_CS_AS)
	 begin
-- password does not match	 
		select @iRetCode = -7
	 end
	else
	 begin
-- update password 
		if coalesce(@szPassword,'')=''
		 begin
-- generate password
			select @szPassword = dbo.fnGeneratePassword(8,0)
				,@szSecureQuestion = 'Randomly generated password'
				,@szSecureAnswer = @szPassword
				,@bResetPassword = 1
		 end
		else
		 begin
			set @bResetPassword = 0
		 end 

		-- do we have outer transaction?
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspUserChangePassword;
		else
		  BEGIN TRANSACTION;

		update dbo.tblUser set [Password] = @szPassword,
			SecureQuestion = @szSecureQuestion,
			SecureAnswer = @szSecureAnswer,
			ResetPassword = @bResetPassword,
			DateUpdated = GETDATE()
			where UserID = @biUserID

		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;
		  set @TranCounter = NULL
		 end 

		if @bResetPassword = 1
		 begin
-- send email
			declare @szEmailTemplate [varchar] (max),
				@szSource [varchar](100),
				@szEmailSubject [nvarchar](512),
				@szEmailDetails [nvarchar](max),
				@szRecipientListAddresses [nvarchar](max),
				@szSQLQueryString [nvarchar] (max),
				@iTmpUserID [int],
				@iQResultWidth [int],
				@szEmailFormat [varchar] (10),
				@iTmpRetCode [int],
				@bRecordOnly [bit],
				@szDateTime [varchar] (30)

			declare @szYear [varchar] (4),
				@szDate1 [varchar] (30),
				@szDate2 [varchar] (30),
				@iPos [int]
			
			select @szEmailTemplate = [Template],
				@szEmailSubject = [Subject],
				@szEmailFormat = [EmailFormat] 
				from dbo.tblEmailTemplate (nolock) 
				where TemplateName = 'ChangePassword'
				and RevokeStatus = 0

			if coalesce(@szEmailTemplate,'')=''
			 begin
	-- email template not found
				select @iRetCode = -8		 
			 end
			else
			 begin
					
				select @szYear = CAST(year(getDate()) as [varchar] (4)),
					@szDate1 = convert(varchar, getdate(), 100),
					@iPos = charindex(@szYear,@szDate1)+5,
					@szDate2 = CONVERT(varchar,getdate(),101)+' at '+
					ltrim(SUBSTRING(@szDate1,@iPos,len(@szDate1)-@iPos+1))		 
					
				select @szEmailTemplate = replace(replace(@szEmailTemplate,'[pwd]',@szTmpPassword),'[dt]',@szDate2)

				select @szEmailDetails = @szEmailTemplate,
					@szSource = @szProcessName,
					@szRecipientListAddresses = @szUserEmail

				exec [dbo].[uspSendEmail]
					@szSource,
					@szEmailSubject,
					@szEmailDetails,
					@szRecipientListAddresses,
					@szSQLQueryString,
					@iTmpUserID,
					@iQResultWidth,
					@szEmailFormat,
					@iTmpRetCode output
				
				if @iTmpRetCode <> 0
				 begin
					select @iRetCode = -9
				 end
			 end	
		 end
	 end 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspUserChangePassword;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biTmpUserID

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
