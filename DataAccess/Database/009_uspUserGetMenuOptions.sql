USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspUserGetMenuOptions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspUserGetMenuOptions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Return user's info and user's menu options
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/08/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @biUserID is NULL or zero
	@iRetCode = -3, User not found

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/08/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@biUserID [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biUserID = 1

	exec [svc].[uspUserGetMenuOptions]
		@biUserID
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspUserGetMenuOptions]
	@biUserID [bigint]
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserGetMenuOptions')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------


select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''

declare @tblUser table (UserID [bigint] not null,
	FirstName [nvarchar] (128) not null,
	LastName [nvarchar] (128) not null,
	UserEmail [nvarchar] (512) not null,
	SecureQuestion [nvarchar] (512) not null,
	DateLastLogin [datetime] null,
	EmailAlertRecipient [bit] null,
	BillingReportRecipient [bit] null,
	ResetPassword [bit] not null,
	RevokeStatus [bit] null,
	DateRevoked [datetime] null,
	DateCreated [datetime] not null,
	DateUpdated [datetime] null,
	StoreID [bigint] not null default (0),
	StoreName [nvarchar] (512) not null default (''))

-- Validate parameters
if coalesce(@biUserID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else 	
 begin
	insert into @tblUser (
		UserID,
		FirstName,
		LastName,
		UserEmail,
		SecureQuestion,
		DateLastLogin,
		EmailAlertRecipient,
		BillingReportRecipient,
		ResetPassword,
		RevokeStatus,
		DateRevoked,
		DateCreated,
		DateUpdated,
		StoreID,
		StoreName)
	select 
		u.UserID,
		u.FirstName,
		u.LastName,
		u.UserEmail,
		u.SecureQuestion,
		u.DateLastLogin,
		u.EmailAlertRecipient,
		u.BillingReportRecipient,
		u.ResetPassword,
		u.RevokeStatus,
		u.DateRevoked,
		u.DateCreated,
		u.DateUpdated,
		s.StoreID,
		s.StoreName
		from dbo.tblUser u (nolock) 
		left join dbo.tblStore s (nolock) on s.StoreID = u.StoreID
		where UserID = @biUserID
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		select @iRetCode = -3,
			@szErrSubst = CAST(@biUserID as [varchar] (10))
	 end
 end

if @iRetCode = 0
 begin
	
	select * from @tblUser

	;with t0 as (
	select am.AppMenuID,
		 am.MenuName,
		 am.ParentID,
		 am.PageLocation,
		 um.UserMenuID,
		 um.[Status],
		 vl.LookUpShortName,
		 um.RevokeStatus,
		 um.DateRevoked,
		 um.DateCreated,
		 um.DateUpdated
		from dbo.tblUserMenu um (nolock) 
		inner join dbo.tblAppMenu am (nolock) on um.AppMenuID = am.AppMenuID
		inner join dbo.vwLookUp vl (nolock) on vl.LookUpName = 'MenuStatus'
			and vl.LookUpSubType = um.[Status]
		where um.UserID = @biUserID
--		and um.RevokeStatus = 0
		and am.RevokeStatus = 0
--		and vl.LookUpShortName <> 'Invisible'
	), tt as (
	select AppMenuID,
			MenuName,
			ParentID,
			PageLocation,
			UserMenuID,
		    [Status],
			LookUpShortName as UserRights,
		    RevokeStatus,
		    DateRevoked,
		    DateCreated,
		    DateUpdated,
--			ROW_NUMBER() OVER(ORDER BY AppMenuID) as RNumber
			cast('/'+CAST(t0.AppMenuID as [varchar] (10))+'/' as [varchar] (128)) as RNumber
			from t0 where ParentID =0
		union all 
		select t.AppMenuID,
			t.MenuName,
			t.ParentID,
			t.PageLocation,
			t.UserMenuID,
		    t.[Status],
			t.LookUpShortName as UserRights,
		    t.RevokeStatus,
		    t.DateRevoked,
		    t.DateCreated,
		    t.DateUpdated,
			cast(tt.RNumber+CAST(t.AppMenuID as [varchar] (10))+'/' as [varchar] (128)) RNumber
--			ROW_NUMBER() OVER(ORDER BY t.AppMenuID) as RNumber
			from t0 t 
			inner join tt on tt.AppMenuID=t.ParentID
			where t.ParentID <>0
		)
	select * from tt order by RNumber	
		
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspUserGetMenuOptions;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUserID

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
