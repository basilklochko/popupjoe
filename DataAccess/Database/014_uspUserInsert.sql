
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspUserInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspUserInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Insert dbo.tblUser record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/12/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreID is null or zero
	@iRetCode = -3, Parameter @szFirstName is null or empty
	@iRetCode = -4, Parameter @szLastName is null or empty
	@iRetCode = -5, Parameter @szUserEmail is null or empty
	@iRetCode = -6, Store not found
	@iRetCode = -7, Store has "revoke" status
	@iRetCode = -8, New user's email already in use
	@iRetCode = -9, Error in svc.uspUserChangePassword SP

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/12/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@biUserID [bigint]	
		,@biStoreID [bigint]
		,@szFirstName [nvarchar] (128)
		,@szLastName [nvarchar] (128)
		,@szUserEmail [nvarchar] (512)
		,@bEmailAlertRecipient [bit]
		,@bBillingReportRecipient [bit]
		,@biUpdUser [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biStoreID = 1
		,@szFirstName = 'Sergey'
		,@szLastName = 'Morozov'
		,@szUserEmail = 'smorozov@ppj.com'

	exec [svc].[uspUserInsert]
		@biStoreID
		,@szFirstName
		,@szLastName
		,@szUserEmail
		,@bEmailAlertRecipient
		,@bBillingReportRecipient
		,@biUpdUser
		,@biUserID output
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

select * from tblUser
select * from tblEmailLog (nolock)

*/

CREATE PROCEDURE [svc].[uspUserInsert]
	@biStoreID [bigint]
	,@szFirstName [nvarchar] (128)
	,@szLastName [nvarchar] (128)
	,@szUserEmail [nvarchar] (512)
	,@bEmailAlertRecipient [bit] = 0
	,@bBillingReportRecipient [bit] = 0
	,@biUpdUser [bigint] = null
   	,@biUserID [bigint]	output
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserInsert')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bEmailAlertRecipient = coalesce(@bEmailAlertRecipient,0)
	,@bBillingReportRecipient = coalesce(@bBillingReportRecipient,0)
	,@biUpdUser = coalesce(@biUpdUser,0)

declare @bTmpRevokeStatus [bit]
	,@szTmpUserEmail [nvarchar] (512)
	,@szTmpAnswer [nvarchar] (512)

-- Validate parameters
if	coalesce(@biStoreID,0)=0
	set @iRetCode = -2
else if	coalesce(@szFirstName,'')=''
	set @iRetCode = -3
else if	coalesce(@szLastName,'')=''
	set @iRetCode = -4
else if	coalesce(@szUserEmail,'')=''
	set @iRetCode = -5
else if	exists (select 1 from dbo.tblUser (nolock) where UserEmail = @szUserEmail)
	set @iRetCode = -8
else
 begin
 
-- validate Store parameters	
	select @bTmpRevokeStatus = RevokeStatus 
		from dbo.tblStore s (nolock)
		where StoreID = @biStoreID
		
	if @@ROWCOUNT = 0	
	 begin
-- store not found
		select @iRetCode = -6,
			@szErrSubst = CAST(@biStoreID as [varchar] (10))
	 end
	else if coalesce(@bTmpRevokeStatus,0)=1
	 begin
-- store has "revoke" status
		select @iRetCode = -7,
			@szErrSubst = CAST(@biStoreID as [varchar] (10))
	 end
 end

if @iRetCode = 0
 begin
 
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspUserInsert;
		else
		  BEGIN TRANSACTION;

		insert into dbo.tblUser (
			StoreID,
			FirstName,
			LastName,
			UserEmail,
			EmailAlertRecipient,
			BillingReportRecipient,
			DateCreated,
			UserCreated,
			[Password],
			SecureQuestion,
			SecureAnswer)
		values (	
			@biStoreID,
			@szFirstName,
			@szLastName,
			@szUserEmail,
			@bEmailAlertRecipient,
			@bBillingReportRecipient,
			GETDATE(),
			@biUpdUser,
			'','','')

		set @biUserID = SCOPE_IDENTITY()
		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;		
		  set @TranCounter = NULL
		 end 

		-- generate password and set email to user
		declare @iTmpRetCode [int]
			,@szTmpMsgTier1 [nvarchar] (2048)
			,@szTmpMsgTier2 [nvarchar] (1024)
		
		exec [svc].[uspUserChangePassword] 
				@biUserID
				,NULL
				,NULL
				,NULL
				,NULL		
				,@iTmpRetCode output
				,@szTmpMsgTier1 output   
				,@szTmpMsgTier2 output 

		if @iTmpRetCode <> 0
		 begin
			set @iRetCode = -9
		 end
		else
		 begin
		 -- insert data into tblUserMenu and return record set
			if not exists (select 1 from dbo.tblUserMenu (nolock) where UserID = @biUserID)
			insert into dbo.tblUserMenu (
				UserID
				,AppMenuID
				,[Status]
				,[DateCreated]
				,[UserCreated]
				,[RevokeStatus]
				,[DateRevoked])
			select @biUserID
				,AppMenuID
				,DefaultStatus
				,GETDATE()
				,@biUpdUser
				,[RevokeStatus]
				,[DateRevoked]	
			from dbo.tblAppMenu	(nolock)
				order by AppMenuID
							
		-- return user's menu set for edit on UI
			;with t0 as (
			select um.UserMenuID,
				 am.MenuName,
				 am.ParentID,
				 um.[Status],
				 vl.LookUpShortName
				from dbo.tblUserMenu um (nolock) 
				inner join dbo.tblAppMenu am (nolock) on um.AppMenuID = am.AppMenuID
				inner join dbo.vwLookUp vl (nolock) on vl.LookUpName = 'MenuStatus'
					and vl.LookUpSubType = um.[Status]
				where um.UserID = @biUserID
				and um.RevokeStatus = 0
				and am.RevokeStatus = 0
--				and vl.LookUpShortName <> 'Invisible'
			)
			select UserMenuID,
				MenuName,
				LookUpShortName as UserRights,
				ParentID,
				[Status]
			from t0 order by UserMenuID
			
		 end
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspUserInsert;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

--select * from dbo.tblUIMessage

if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 206)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 206,
	'Error happened during update, please contact with PPJ administrator.',
	'Error happened during update, please contact with PPJ administrator.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 205)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 205,
	'Error happened during insert, please contact with PPJ administrator.',
	'Error happened during insert, please contact with PPJ administrator.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 211)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 211,
	'First name is empty.',
	'First name is empty.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 212)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 212,
	'Last name is empty.',
	'Last name is empty.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 213)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 213,
	'Email is empty.',
	'Email is empty.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 214)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 214,
	'Store has "revoke" status.',
	'Store has "revoke" status.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 215)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 215,
	'User has "revoke" status.',
	'User has "revoke" status.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 216)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 216,
	'Answer does not match with recorded value.',
	'Answer does not match with recorded value.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 217)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 217,
	'New email value {0} already in use.',
	'New email value {0} already in use.'
go

-- delete from dbo.tblMsgOutput where SPName = 'uspUserInsert'
if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspUserInsert')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspUserInsert',-1, 'SQL update error',205
union all
select 'uspUserInsert',-2, 'Parameter @biStoreID is null or zero',205
union all
select 'uspUserInsert',-3, 'Parameter @szFirstName is null or empty',211
union all
select 'uspUserInsert',-4, 'Parameter @szLastName is null or empty',212
union all
select 'uspUserInsert',-5, 'Parameter @szUserEmail is null or empty',213
union all
select 'uspUserInsert',-6, 'Store not found',205
union all
select 'uspUserInsert',-7, 'Store has "revoke" status',214
union all
select 'uspUserInsert',-8, 'New user''s email already in use',217
union all
select 'uspUserInsert',-9, 'Error in svc.uspUserChangePassword SP',205
go

