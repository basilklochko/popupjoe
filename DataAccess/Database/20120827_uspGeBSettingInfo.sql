
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspGeBSettingInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspGeBSettingInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Returns Billing Setting data from dbo.tblBSetting table base 
	on value of @biBSettingID parameter
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/27/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biBSettingID is NULL or zero
	@iRetCode = -3, Billing Setting record for BSettingID={0} not found in dbo.tblBSetting table
	@iRetCode = -4, Store for StoreID={0} has "revoke" status

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/27/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

select * from dbo.tblBSetting

	declare @biBSettingID [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biBSettingID = 1

	exec [svc].[uspGeBSettingInfo]
		@biBSettingID
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspGeBSettingInfo]
	@biBSettingID [bigint]
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspGeBSettingInfo')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''

declare @bTmpStoreRevokeStatus [bit]
	,@biStoreID [bigint]

declare @tbl table (
	[BSettingID] [bigint] NOT NULL,
	[StoreID] [bigint] NOT NULL,
	[BillingPeriod] [bigint] NOT NULL,
	[BillngPeriodName] [varchar] (64) null,
	[BillingType] [int] NOT NULL,
	[BillngTypeName] [varchar] (64) null,
	[BillingUnit] [int] NOT NULL,
	[BillingRate] [decimal](7, 4) NOT NULL,
	[BillingDate] [datetime] NOT NULL,
	[LastBilledDate] [datetime] NOT NULL,
	[RevokeStatus] [bit] NOT NULL,
	[DateRevoked] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NULL)

-- Validate parameters
if coalesce(@biBSettingID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else 	
 begin
 
	insert into @tbl (
		[BSettingID],
		[StoreID],
		[BillingPeriod],
		[BillngPeriodName],
		[BillingType],
		[BillngTypeName],
		[BillingUnit],
		[BillingRate],
		[BillingDate],
		[LastBilledDate],
		[RevokeStatus],
		[DateRevoked],
		[DateCreated],
		[DateUpdated])
	select 
		[BSettingID],
		[StoreID],
		[BillingPeriod],
		bp.LookUpShortName as [BillngPeriodName],
		[BillingType],
		bt.LookUpShortName as [BillngTypeName],
		[BillingUnit],
		[BillingRate],
		[BillingDate],
		[LastBilledDate],
		[RevokeStatus],
		[DateRevoked],
		[DateCreated],
		[DateUpdated]
		from dbo.tblBSetting bs (nolock) 
		left join dbo.vwLookUp bp (nolock) on bp.LookUpName = 'BillingPeriod' 
			and bp.LookUpSubType = bs.BillingPeriod
		left join dbo.vwLookUp bt (nolock) on bt.LookUpName = 'BillingType' 
			and bt.LookUpSubType = bs.BillingType
		where bs.BSettingID = @biBSettingID
	
	if @@ROWCOUNT = 0 
		select @iRetCode = -3
			,@szErrSubst = CAST(@biBSettingID as [varchar] (30))
	else 
	 begin
		select @bTmpStoreRevokeStatus = s.RevokeStatus 
			from dbo.tblStore s (nolock) 
			inner join @tbl t on t.StoreID = s.StoreID

		-- Store has "revoke" status	
		if coalesce(@bTmpStoreRevokeStatus,0)=1
			select @iRetCode = -4
				,@szErrSubst = CAST(@biBSettingID as [varchar] (30))
			
	 end
 end

if @iRetCode = 0
 begin
 
	select 
		[BSettingID],
		[StoreID],
		[BillingPeriod],
		[BillngPeriodName],
		[BillingType],
		[BillngTypeName],
		[BillingUnit],
		[BillingRate],
		[BillingDate],
		[LastBilledDate],
		[RevokeStatus],
		[DateRevoked],
		[DateCreated],
		[DateUpdated]
		from @tbl 
	
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspGeBSettingInfo;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = 0

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspGeBSettingInfo')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspGeBSettingInfo',-1, 'SQL update error',207
union all
select 'uspGeBSettingInfo',-2, 'Parameter @biBSettingID is NULL or zero',207
union all
select 'uspGeBSettingInfo',-3, 'No record found in dbo.tblBSetting for BSettingID={0}',207
union all
select 'uspGeBSettingInfo',-4, 'Store with StoreID={0} has "revoke" status',207
go