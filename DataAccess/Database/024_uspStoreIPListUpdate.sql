
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspStoreIPListUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspStoreIPListUpdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Insert dbo.tblStore record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/14/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreIPListID is null or zero
	@iRetCode = -3, Parameter @szIPAddress is null or empty
	@iRetCode = -4, Parameter @szDomainName is null or empty
	@iRetCode = -5, Parameter @iTimerValue1 is null, zero or negative
	@iRetCode = -6, Parameter @iTimerValue2 is null, zero or negative
	@iRetCode = -7, Record with @biParentStoreID not found in dbo.tblStoreIPList table
	@iRetCode = -8, Record in dbo.tblStoreIPList has "revoke" status
	@iRetCode = -9, Record with @biStoreID {0} not found in dbo.tblStore table
	@iRetCode = -10, Store has "revoked" status
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/14/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
   		@biStoreIPListID [bigint]
		,@szIPAddress [varchar] (64)
		,@szDomainName [nvarchar] (128)
		,@iTimerValue1 [int]
		,@iTimerValue2 [int]
		,@biUpdUser [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select
   		@biStoreIPListID = 1
		,@szIPAddress =''
		,@szDomainName = 'etronics.com'
		,@iTimerValue1 = 180
		,@iTimerValue2 = 300


	exec [svc].[uspStoreIPListUpdate]
   		@biStoreIPListID
		,@szIPAddress
		,@szDomainName
		,@iTimerValue1
		,@iTimerValue2
		,@biUpdUser
   		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspStoreIPListUpdate]
   	@biStoreIPListID [bigint]
	,@szIPAddress [varchar] (64)
	,@szDomainName [nvarchar] (128)
	,@iTimerValue1 [int]
	,@iTimerValue2 [int]
	,@biUpdUser [bigint] = null
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspStoreIPListUpdate')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)

declare @bRevokeStatus [bit]
	,@biStoreID [bigint]
	,@bStoreRevokeStatus [bit]


-- Validate parameters
if	coalesce(@biStoreIPListID,0)<=0
	set @iRetCode = -2
else if	coalesce(@szIPAddress,'')=''
	set @iRetCode = -3
else if	coalesce(@szDomainName,'')=''
	set @iRetCode = -4
else if	coalesce(@iTimerValue1,0)<=0
	set @iRetCode = -5
else if	coalesce(@iTimerValue2,0)<=0
	set @iRetCode = -6
else
 begin
	select @biStoreID = StoreID,
		@bRevokeStatus = RevokeStatus
		from dbo.tblStoreIPList (nolock) 
		where StoreIPListID = @biStoreIPListID
	
	if @@ROWCOUNT = 0
	 select @iRetCode = -7
	 	,@szErrSubst = CAST(@biStoreIPListID as [varchar] (30))
	else if coalesce(@bRevokeStatus,0)=1
	 select @iRetCode = -8
	 	,@szErrSubst = CAST(@biStoreIPListID as [varchar] (30))
	else
	 begin
	 
		select @bStoreRevokeStatus = RevokeStatus
			from dbo.tblStore (nolock) 
			where StoreID = @biStoreID
	 
		if @@ROWCOUNT = 0
		 select @iRetCode = -9
		 	,@szErrSubst = CAST(@biStoreID as [varchar] (30))
		else if coalesce(@bStoreRevokeStatus,0)=1
		 select @iRetCode = -10
		 	,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 
	 end 
 end

if @iRetCode = 0
 begin
	 
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	  SAVE TRANSACTION uspStoreIPListUpdate;
	else
	  BEGIN TRANSACTION;

	update dbo.tblStoreIPList set
		IPAddress = @szIPAddress
		,DomainName = @szDomainName
		,TimerValue1 = @iTimerValue1
		,TimerValue2 = @iTimerValue2
		,DateUpdated = GETDATE()
		,UserUpdated = @biUpdUser
		where StoreIPListID = @biStoreIPListID 

	-- commit transaction
	if @TranCounter = 0 -- no outer transaction
	 begin
	  COMMIT TRANSACTION;		
	  set @TranCounter = NULL
	 end 
 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspStoreIPListUpdate;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

-- delete from dbo.tblMsgOutput where SPName = 'uspStoreIPListUpdate'
if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspStoreIPListUpdate')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspStoreIPListUpdate',-1, 'SQL update error',205
union all
select 'uspStoreIPListUpdate',-2, 'Parameter @biStoreIPListID is null or zero',206
union all
select 'uspStoreIPListUpdate',-3, 'Parameter @szIPAddress is null or empty',206
union all
select 'uspStoreIPListUpdate',-4, 'Parameter @szDomainName is null or empty',206
union all
select 'uspStoreIPListUpdate',-5, 'Parameter @iTimrValue1 is null, zero or negative',206
union all
select 'uspStoreIPListUpdate',-6, 'Parameter @iTimrValue2 is null, zero or negative',218
union all
select 'uspStoreIPListUpdate',-7, 'Record not found for @biStoreIPListID',206
union all
select 'uspStoreIPListUpdate',-8, 'Record has "revoke" status',206
union all
select 'uspStoreIPListUpdate',-9, 'Record with @bStoreID {0} not found in dbo.tblStore table',206
union all
select 'uspStoreIPListUpdate',-10, 'Store has "revoked" status',214
go
