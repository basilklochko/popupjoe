
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspCouponUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspCouponUpdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Update dbo.tblCoupon record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/25/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL insert error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biCouponID is null or zero
	@iRetCode = -3, Parameters @nCouponValue is null or less/equal zero
	@iRetCode = -4, Parameter @szeCommCouponID is null or empty
	@iRetCode = -5, Parameter @iCouponType not found in Coupon Type LookUp table
	@iRetCode = -6, Both parameters @nMinOrderAmt/@nMaxOrderAmt are null or zero {0}
	@iRetCode = -7, Parameter @nMinOrderAmt is more or equal @nMaxOrderAmt {0}
	@iRetCode = -8, Parameter @iUses is null or less/equal zero
	@iRetCode = -9, Could not find Coupon for parameter @biCouponID={0}
	@iRetCode = -10, Coupon with CouponID={0} has "revoke" status
	@iRetCode = -11, Could not find Store for parameter @biStoreIPListID {0}
	@iRetCode = -12, Store with StoreID={0} has "revoke" status
	@iRetCode = -13, Store Domain record with StoreIPListID={0} has "revoke" status

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/25/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	select * from tblCoupon

	declare
   		@biCouponID [bigint]	
		,@szeCommCouponID [nvarchar] (64)		-- eCommerce System Coupon identificator
		,@szCouponDescription [nvarchar] (512)
		,@nCouponValue [decimal] (8,3)
		,@nMinOrderAmt [decimal] (7,2)
		,@nMaxOrderAmt [decimal] (7,2)
		,@iCouponType [int] 
		,@szMsgToPopUp [nvarchar] (max)
		,@dtExpirationDate [datetime]
		,@bLimitedNumber [bit]
		,@iUses [int]
		,@biUpdUser [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

-- Coupon on 5 dollars for all orders with amount >= 50$ 
--	coupon automatically will be disabled when number of uses exceeded 100 times

	select
		@biCouponID = 1
		,@szeCommCouponID = 'SAVE5DOLLAR'
		,@szCouponDescription = '5$ on orders with total exceed 50$'
		,@nCouponValue = 5.00
		,@nMinOrderAmt = 50.00
--		,@nMaxOrderAmt [decimal] (7,2)
		,@iCouponType = 1 -- Fixed Amount
		,@szMsgToPopUp = 'Save 5 dollars'
--		,@dtExpirationDate [datetime]
		,@bLimitedNumber = 1
		,@iUses = 200
		
	exec [svc].[uspCouponUpdate]
   		@biCouponID
		,@szeCommCouponID
		,@szCouponDescription
		,@nCouponValue
		,@nMinOrderAmt
		,@nMaxOrderAmt
		,@iCouponType
		,@szMsgToPopUp
		,@dtExpirationDate
		,@bLimitedNumber
		,@iUses
		,@biUpdUser
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspCouponUpdate]
   	@biCouponID [bigint]
	,@szeCommCouponID [nvarchar] (64) 
	,@szCouponDescription [nvarchar] (512) = ''
	,@nCouponValue [decimal] (8,3) = 0
	,@nMinOrderAmt [decimal] (7,2) = 0
	,@nMaxOrderAmt [decimal] (7,2) = 0
	,@iCouponType [int] 
	,@szMsgToPopUp [nvarchar] (max) = ''
	,@dtExpirationDate [datetime] = NULL
	,@bLimitedNumber [bit] = 0
	,@iUses [int] = 0
	,@biUpdUser [bigint] = 0
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspCouponUpdate')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)
	,@szeCommCouponID = coalesce(@szeCommCouponID,'')
	,@szCouponDescription = coalesce(@szCouponDescription,'')
	,@nCouponValue = coalesce(@nCouponValue,0)
	,@nMinOrderAmt = coalesce(@nMinOrderAmt,0)
	,@nMaxOrderAmt = coalesce(@nMaxOrderAmt,0)
	,@iCouponType = coalesce(@iCouponType,0)
	,@szMsgToPopUp = coalesce(@szMsgToPopUp,'')
	,@bLimitedNumber = coalesce(@bLimitedNumber,0)
	,@iUses = coalesce(@iUses,0)


declare @bTmpStoreStatus [bit]
	,@bTmpStoreIPStatus [bit]
	,@bTmpCouponStatus [bit]
	,@biStoreID [bigint]
	,@biStoreIPListID [bigint]

-- Validate parameters


if	coalesce(@biCouponID,0)=0
	set @iRetCode = -2
else if @nCouponValue <= 0
	set @iRetCode = -3
else if @szeCommCouponID = ''
	set @iRetCode = -4
else if @iCouponType <= 0 or not exists (select 1 	
			from dbo.vwLookUp vl (nolock) 
			where vl.LookUpSubType = @iCouponType
			and vl.LookUpName = 'CouponType')
	select @iRetCode = -5
		,@szErrSubst = CAST(@iCouponType as [varchar] (10))			
else if @nMinOrderAmt <= 0 and @nMaxOrderAmt <= 0 
	select @iRetCode = -6
else if not (@nMaxOrderAmt <= 0) and not (@nMinOrderAmt <= 0) and @nMinOrderAmt >= @nMaxOrderAmt
	select @iRetCode = -7
		,@szErrSubst = 'MinOrderAmt='+CAST(@nMinOrderAmt as [varchar] (10))
			+'/MaxOrderAmt='+CAST(@nMaxOrderAmt as [varchar] (10))
else if @bLimitedNumber = 1 and @iUses <= 0
	set @iRetCode = -8
else
 begin

	select @biStoreIPListID = StoreIPListID,
		@bTmpCouponStatus = RevokeStatus
		from tblCoupon (nolock) 
		where CouponID = @biCouponID

	if @@ROWCOUNT = 0
		select @iRetCode = -9
			,@szErrSubst = CAST(@biCouponID as [varchar] (30))			
	else if @bTmpCouponStatus = 1
		select @iRetCode = -10
			,@szErrSubst = CAST(@biCouponID as [varchar] (30))			
	else
	 begin

		select @bTmpStoreStatus = s.RevokeStatus 
			,@bTmpStoreIPStatus = sipl.RevokeStatus
			,@biStoreID = s.StoreID
			from dbo.tblStore s (nolock) 
			inner join dbo.tblStoreIPList sipl (nolock) on sipl.StoreID = s.StoreID
				and sipl.StoreIPListID = @biStoreIPListID

		if @@ROWCOUNT = 0 
			select @iRetCode = -11
				,@szErrSubst = CAST(@biStoreIPListID as [varchar] (30))
		else if coalesce(@bTmpStoreStatus,0) = 1
			select @iRetCode = -12
				,@szErrSubst = CAST(@biStoreID as [varchar] (30))
		else if coalesce(@bTmpStoreIPStatus,0) = 1
			select @iRetCode = -13
				,@szErrSubst = CAST(@biStoreIPListID as [varchar] (30))
	 
	 end
 end

if @iRetCode = 0
 begin

		SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspCouponUpdate;
		else
		  BEGIN TRANSACTION;


		
		update dbo.tblCoupon set
			eCommCouponID = @szeCommCouponID,
			CouponDescription = @szCouponDescription,
			CouponValue = @nCouponValue,
			MinOrderAmt = @nMinOrderAmt,
			MaxOrderAmt = @nMaxOrderAmt,
			CouponType = @iCouponType,
			MsgToPopUp = @szMsgToPopUp,
			ExpirationDate = 
			case 
				when @dtExpirationDate is not null and @dtExpirationDate <= GETDATE() then GETDATE()
				else @dtExpirationDate
			 end,	
			LimitedNumber = @bLimitedNumber,
			Uses = @iUses,
			DateUpdated = GETDATE(),
			UserUpdated = @biUpdUser
			where CouponID = @biCouponID

		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;		
		  set @TranCounter = NULL
		 end 
 end
else 
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspCouponUpdate;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

-- delete from dbo.tblMsgOutput where SPName = 'uspCouponUpdate'
if exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspCouponUpdate')
delete dbo.tblMsgOutput where SPName = 'uspCouponUpdate'

if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspCouponUpdate')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspCouponUpdate',-1, 'SQL update error',205
union all
select 'uspCouponUpdate',-2, 'Parameter @biCouponID is null or zero',205
union all
select 'uspCouponUpdate',-3, 'Parameters @nCouponValue is null or  zero',205
union all
select 'uspCouponUpdate',-4, 'Parameter @szeCommCouponID is null or empty',205
union all
select 'uspCouponUpdate',-5, 'Parameter @iCouponType not found in Coupon Type LookUp table',205
union all
select 'uspCouponUpdate',-6, 'Both parameters @nMinOrderAmt/@nMaxOrderAmt are null or less/equal zero {0}',205
union all
select 'uspCouponUpdate',-7, 'Parameter @nMinOrderAmt is more or equal @nMaxOrderAmt {0} ',205
union all
select 'uspCouponUpdate',-8, 'Parameter @iUses is null or less/equal zero',205
union all
select 'uspCouponUpdate',-9, 'Could not find Coupon for parameter @biCouponID={0}',205
union all
select 'uspCouponUpdate',-10, 'Coupon with CouponID={0} has "revoke" status',205
union all
select 'uspCouponUpdate',-11, 'Could not find Store for parameter @biStoreIPListID={0}',205
union all
select 'uspCouponUpdate',-12, 'Store with StoreID={0} has "revoke" status',205
union all
select 'uspCouponUpdate',-13, 'Store Domain record with StoreIPListID={0} has "revoke" status',205
go

