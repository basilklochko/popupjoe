
USE [ppjdb]
GO

alter table dbo.tblAppMenu alter column MenuName [nvarchar] (512) not null
go
;with tt as (
select 1 as AppMenuID,'$|img src=''~/Images/glyphicons/png/glyphicons_041_charts_white.png'' width=''15px'' /|$ Dashboard' as MenuName
union all
select 2,'$|img src=''~/Images/glyphicons/png/glyphicons_202_shopping_cart_white.png'' width=''15px'' /|$ Store Info'
union all
select 3,'$|img src=''~/Images/glyphicons/png/glyphicons_280_settings.png'' width=''15px'' /|$ Domain Management'
union all
select 4,'$|img src=''~/Images/glyphicons/png/glyphicons_037_credit.png'' width=''15px'' /|$ Billing Settings'
union all
select 5,'$|img src=''~/Images/glyphicons/png/glyphicons_066_tags.png'' width=''15px'' /|$ Coupon Settings'
union all
select 6,'$|img src=''~/Images/glyphicons/png/glyphicons_043_group.png'' width=''15px'' /|$ User Management'
union all
select 7,'$|img src=''~/Images/glyphicons/png/glyphicons_029_notes_2.png'' width=''15px'' /|$ Invoices'
union all
select 8,'$|img src=''~/Images/glyphicons/png/glyphicons_040_stats_white.png'' width=''15px'' /|$ PopUpJoe Statistics'
)
update t set MenuName = tt.MenuName
from dbo.tblAppMenu t
inner join tt on t.AppMenuID = tt.AppMenuID
GO

IF NOT EXISTS (select 1 from Information_SCHEMA.columns (nolock)
	where Table_Schema = 'dbo' 
	and Table_name='tblAppMenu' 
	and column_name='DateRevoked')
 begin
  ALTER TABLE [dbo].[tblAppMenu] ADD [DateRevoked] [datetime] null
 end
GO