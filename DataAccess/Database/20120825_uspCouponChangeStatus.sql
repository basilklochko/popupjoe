

USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspCouponChangeStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspCouponChangeStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Reset coupone's RevokeStatus
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/25/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @biCouponID is NULL or zero
	@iRetCode = -3, Coupon not found
	@iRetCode = -4, Coupon already has requested status
	@iRetCode = -5, Could not find Store for parameter @biStoreIPListID {0}
	@iRetCode = -6, Store with StoreID={0} has "revoke" status
	@iRetCode = -7, Store Domain record with StoreIPListID={0} has "revoke" status
	
	
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/25/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------
	declare
		@biCouponID [bigint]
		,@bRevokeStatus [bit]
		,@biUpdUser [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biCouponID = 1
		,@bRevokeStatus = 0

	exec [svc].[uspCouponChangeStatus]
		@biCouponID
		,@bRevokeStatus
		,@biUpdUser
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspCouponChangeStatus]
	@biCouponID [bigint]
	,@bRevokeStatus [bit] = 1
	,@biUpdUser [bigint] = null
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspCouponChangeStatus')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

declare @bStoreRevokeStatus [bit],
	@bStoreIPListRevokeStatus [bit],
	@bTmpRevokeStatus [bit]	,
	@dtTmpDateRevoked [datetime],
	@biStoreIPListID [bigint],
	@biStoreID [bigint]	
	

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bRevokeStatus = coalesce(@bRevokeStatus,1)
	,@biUpdUser = coalesce(@biUpdUser,0)
	
-- Validate parameters
if coalesce(@biCouponID,'')=0
-- Parameter is NULL or zero
	set @iRetCode = -2
 begin
	select 
		@dtTmpDateRevoked = c.DateRevoked,
		@bTmpRevokeStatus = c.RevokeStatus,
		@biStoreIPListID = c.StoreIPListID
		from dbo.tblCoupon c (nolock)
		where CouponID = @biCouponID
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		set @iRetCode = -3
	 end
	else if @bTmpRevokeStatus=@bRevokeStatus
	 begin
-- record has requested status	 
		select @iRetCode = -4,
			@szErrSubst='RevokeStatus='+CAST(@bRevokeStatus as [varchar] (10))			
	 end
	else
	 begin
-- validate Revoke status on Store and StoreIPList levels

		select @bStoreRevokeStatus = s.RevokeStatus 
			,@bStoreIPListRevokeStatus = sipl.RevokeStatus
			,@biStoreID = s.StoreID
			from dbo.tblStore s (nolock) 
			inner join dbo.tblStoreIPList sipl (nolock) on sipl.StoreID = s.StoreID
				and sipl.StoreIPListID = @biStoreIPListID

		if @@ROWCOUNT = 0 
			select @iRetCode = -5
				,@szErrSubst = CAST(@biStoreIPListID as [varchar] (30))
		else if coalesce(@bStoreRevokeStatus,0) = 1
			select @iRetCode = -6
				,@szErrSubst = CAST(@biStoreID as [varchar] (30))
		else if coalesce(@bStoreIPListRevokeStatus,0) = 1
			select @iRetCode = -7
				,@szErrSubst = CAST(@biStoreIPListID as [varchar] (30))

	 end
 end

if @iRetCode = 0
 begin
-- update Coupon Status

		-- do we have outer transaction?
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspCouponChangeStatus;
		else
		  BEGIN TRANSACTION;

		update dbo.tblCoupon set [RevokeStatus] = @bRevokeStatus,
			DateRevoked = case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			DateUpdated = GETDATE(),
			UserUpdated = @biUpdUser
			where CouponID = @biCouponID

		insert into dbo.tblStatusLog (SourceTable,
			SourceTableID,
			OldStatus,
			OldDateRevoked,
			NewStatus,
			NewDateRevoked,
			DateCreated,
			UserCreated)
		values ('tblCoupon',
			@biCouponID,
			@bTmpRevokeStatus,
			@dtTmpDateRevoked,
			@bRevokeStatus,
			case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			GETDATE(),
			@biUpdUser)
			
		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;
		  set @TranCounter = NULL
		 end 
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspCouponChangeStatus;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

if exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspCouponChangeStatus')
delete from dbo.tblMsgOutput where SPName = 'uspCouponChangeStatus'
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspCouponChangeStatus',-1, 'SQL update error',207
union all
select 'uspCouponChangeStatus',-2, 'Parameter @biCouponID is NULL or zero',207
union all
select 'uspCouponChangeStatus',-3, 'Coupon with @biCouponID={0} not found in dbo.tblCoupon table',207
union all
select 'uspCouponChangeStatus',-4, 'Coupon already has requested status',206
union all
select 'uspCouponChangeStatus',-5, 'Could not find Store for parameter @biStoreIPListID',207
union all
select 'uspCouponChangeStatus',-6, 'Store with StoreID={0} has "revoke" status',207
union all
select 'uspCouponChangeStatus',-7, 'Store Domain record with StoreIPListID={0} has "revoke" status',207
go
