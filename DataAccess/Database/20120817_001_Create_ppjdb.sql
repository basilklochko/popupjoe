--!!!!
-- CHANGE FILENAME values for data and log files before running script!!!!


USE [master]
GO
/****** Object:  Database [ppjdb]    Script Date: 08/17/2012 21:14:36 ******/
CREATE DATABASE [ppjdb] ON  PRIMARY 
( NAME = N'ppjdb', FILENAME = N'F:\SQL_Data\ppjdb.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ppjdb_log', FILENAME = N'C:\SQL_Logs\ppjdb.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ppjdb] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ppjdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ppjdb] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [ppjdb] SET ANSI_NULLS OFF
GO
ALTER DATABASE [ppjdb] SET ANSI_PADDING OFF
GO
ALTER DATABASE [ppjdb] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [ppjdb] SET ARITHABORT OFF
GO
ALTER DATABASE [ppjdb] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [ppjdb] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [ppjdb] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [ppjdb] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [ppjdb] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [ppjdb] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [ppjdb] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [ppjdb] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [ppjdb] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [ppjdb] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [ppjdb] SET  DISABLE_BROKER
GO
ALTER DATABASE [ppjdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [ppjdb] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [ppjdb] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [ppjdb] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [ppjdb] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [ppjdb] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [ppjdb] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [ppjdb] SET  READ_WRITE
GO
ALTER DATABASE [ppjdb] SET RECOVERY SIMPLE
GO
ALTER DATABASE [ppjdb] SET  MULTI_USER
GO
ALTER DATABASE [ppjdb] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [ppjdb] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'ppjdb', N'ON'
GO
USE [ppjdb]
GO
/****** Object:  User [PopUpJoeWEBUser]    Script Date: 08/17/2012 21:14:36 ******/
CREATE USER [PopUpJoeWEBUser] FOR LOGIN [PopUpJoeWEBUser] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Role [db_executor]    Script Date: 08/17/2012 21:14:36 ******/
CREATE ROLE [db_executor] AUTHORIZATION [dbo]
GO
/****** Object:  Schema [svc]    Script Date: 08/17/2012 21:14:36 ******/
CREATE SCHEMA [svc] AUTHORIZATION [public]
GO
/****** Object:  Table [dbo].[tblSessionList]    Script Date: 08/17/2012 21:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSessionList](
	[SessionID] [bigint] IDENTITY(1,1) NOT NULL,
	[StoreIPListID] [bigint] NOT NULL,
	[PopUpJoeSessionID] [nvarchar](256) NOT NULL,
	[Expiration] [datetime] NOT NULL,
	[OrderSubmitFlag] [bit] NOT NULL,
	[DateOrderSubmit] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblSessionList] PRIMARY KEY CLUSTERED 
(
	[SessionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblMsgOutput]    Script Date: 08/17/2012 21:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMsgOutput](
	[MsgOutputID] [bigint] IDENTITY(1,1) NOT NULL,
	[SPName] [varchar](64) NOT NULL,
	[RetCode] [int] NOT NULL,
	[MsgTier1] [nvarchar](2048) NULL,
	[UIErrCode] [int] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblMsgOutput] PRIMARY KEY CLUSTERED 
(
	[MsgOutputID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblLookUp]    Script Date: 08/17/2012 21:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblLookUp](
	[LookUpID] [int] IDENTITY(1,1) NOT NULL,
	[LookUpShortName] [varchar](64) NOT NULL,
	[LookUpDescription] [varchar](255) NULL,
	[LookUpSubType] [int] NOT NULL,
	[LookUpSubID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblLookUp] PRIMARY KEY CLUSTERED 
(
	[LookUpID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblEmailTemplate]    Script Date: 08/17/2012 21:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmailTemplate](
	[EmailTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [varchar](64) NOT NULL,
	[Subject] [nvarchar](512) NOT NULL,
	[Template] [varchar](max) NOT NULL,
	[EmailFormat] [varchar](10) NOT NULL,
	[RevokeStatus] [bit] NOT NULL,
	[DateRevoked] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblEmailTemplate] PRIMARY KEY CLUSTERED 
(
	[EmailTemplateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblEmailLog]    Script Date: 08/17/2012 21:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEmailLog](
	[EmailLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[Source] [varchar](100) NOT NULL,
	[EmailSubject] [nvarchar](512) NOT NULL,
	[EmailDetails] [nvarchar](max) NULL,
	[RecipientList] [nvarchar](max) NOT NULL,
	[DateSend] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
 CONSTRAINT [PK_tblEmailLog] PRIMARY KEY CLUSTERED 
(
	[EmailLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblStore]    Script Date: 08/17/2012 21:14:37 ******/
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ARITHABORT ON
GO
CREATE TABLE [dbo].[tblStore](
	[StoreID] [bigint] IDENTITY(1,1) NOT NULL,
	[eCommerceType] [int] NOT NULL,
	[StoreName] [nvarchar](255) NOT NULL,
	[PostalName] [nvarchar](255) NOT NULL,
	[Address1] [nvarchar](255) NOT NULL,
	[Address2] [nvarchar](255) NULL,
	[City] [nvarchar](100) NOT NULL,
	[StateProvince] [nvarchar](64) NOT NULL,
	[PostalCode] [nvarchar](64) NOT NULL,
	[CountryCode] [varchar](3) NOT NULL,
	[StoreGUID] [uniqueidentifier] NOT NULL,
	[RevokeStatus] [bit] NOT NULL,
	[DateRevoked] [datetime] NULL,
	[ParentStoreID] [bigint] NULL,
	[StoreNode] [hierarchyid] NOT NULL,
	[SessionExpiration] [int] NOT NULL,
	[ContractExpirationDate] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
	[StoreLevelID]  AS ([StoreNode].[GetLevel]()),
 CONSTRAINT [PK_tblStore] PRIMARY KEY CLUSTERED 
(
	[StoreID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [UI_tblStore_StoreGUID] ON [dbo].[tblStore] 
(
	[StoreGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
CREATE UNIQUE NONCLUSTERED INDEX [UI_tblStore_StoreLevelID_StoreNode] ON [dbo].[tblStore] 
(
	[StoreLevelID] ASC,
	[StoreNode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblStatusLog]    Script Date: 08/17/2012 21:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStatusLog](
	[StatusLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[SourceTable] [varchar](128) NOT NULL,
	[SourceTableID] [bigint] NOT NULL,
	[OldStatus] [bit] NOT NULL,
	[OldDateRevoked] [datetime] NULL,
	[NewStatus] [bit] NOT NULL,
	[NewDateRevoked] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
 CONSTRAINT [PK_tblStatusLog] PRIMARY KEY CLUSTERED 
(
	[StatusLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblSQLAudit]    Script Date: 08/17/2012 21:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSQLAudit](
	[SQLAuditID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProcessID] [bigint] NOT NULL,
	[ErrorNumber] [int] NULL,
	[ErrorSeverity] [int] NULL,
	[ErrorState] [int] NULL,
	[ErrorProcedure] [varchar](512) NULL,
	[ErrorLine] [int] NULL,
	[ErrorMessage] [varchar](4000) NULL,
	[Parameters] [varchar](1024) NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
 CONSTRAINT [PK_tblSQLAudit_ProcessID_SQLAuditID] PRIMARY KEY CLUSTERED 
(
	[ProcessID] ASC,
	[SQLAuditID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblUIMessage]    Script Date: 08/17/2012 21:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblUIMessage](
	[UIMessageID] [int] IDENTITY(1,1) NOT NULL,
	[UIErrCode] [int] NOT NULL,
	[MsgTier2] [varchar](1024) NULL,
	[MsgTier3] [varchar](1024) NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblUIMessage] PRIMARY KEY CLUSTERED 
(
	[UIMessageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY],
 CONSTRAINT [UC_tblUIMessage_UIErrCode] UNIQUE NONCLUSTERED 
(
	[UIErrCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblWEBAudit]    Script Date: 08/17/2012 21:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblWEBAudit](
	[WEBAuditID] [bigint] IDENTITY(1,1) NOT NULL,
	[WEBServer] [nvarchar](512) NULL,
	[ErrSource] [varchar](512) NULL,
	[ErrorNumber] [int] NULL,
	[ErrorLine] [int] NULL,
	[ErrorMessage] [varchar](4000) NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
 CONSTRAINT [PK_tblWEBAudit] PRIMARY KEY CLUSTERED 
(
	[WEBAuditID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblAppMenu]    Script Date: 08/17/2012 21:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAppMenu](
	[AppMenuID] [int] IDENTITY(1,1) NOT NULL,
	[MenuName] [nvarchar](512) NOT NULL,
	[ParentID] [int] NOT NULL,
	[PageLocation] [varchar](512) NOT NULL,
	[RevokeStatus] [bit] NOT NULL,
	[DefaultStatus] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
	[DateRevoked] [datetime] NULL,
 CONSTRAINT [PK_tblAppMenu] PRIMARY KEY CLUSTERED 
(
	[AppMenuID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vwGetNewID]    Script Date: 08/17/2012 21:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwGetNewID] AS SELECT NewId() AS [NewID]
GO
/****** Object:  View [dbo].[vwLookUp]    Script Date: 08/17/2012 21:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vwLookUp] 
AS 
with t0 as (
select LookUpSubType as LookUpID,LookUpShortName as LookUpName
	from tblLookUp (nolock) 
	where LookUpSubID = 0
)
select t0.*,
t.LookUpShortName,t.LookUpSubType
	from tblLookUp t (nolock) 
	inner join t0 on t0.LookUpID = t.LookUpSubID
	where t.LookUpSubID <> 0
--	order by t0.LookUpName,t.LookUpSubType
GO
/****** Object:  Table [dbo].[tblBilling]    Script Date: 08/17/2012 21:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBilling](
	[BillingID] [bigint] IDENTITY(1,1) NOT NULL,
	[StoreID] [bigint] NOT NULL,
	[StoreIPListID] [bigint] NULL,
	[BillingPeriod] [bigint] NOT NULL,
	[BillingType] [int] NOT NULL,
	[BillingUnit] [int] NOT NULL,
	[BillingRate] [decimal](7, 4) NOT NULL,
	[DateFrom] [datetime] NOT NULL,
	[DateTo] [datetime] NOT NULL,
	[BilledDate] [datetime] NOT NULL,
	[NumberOfUnits] [decimal](12, 2) NOT NULL,
	[Total] [decimal](12, 2) NOT NULL,
	[Mode] [bit] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblBilling] PRIMARY KEY CLUSTERED 
(
	[BillingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblBSetting]    Script Date: 08/17/2012 21:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBSetting](
	[BSettingID] [bigint] IDENTITY(1,1) NOT NULL,
	[StoreID] [bigint] NOT NULL,
	[BillingPeriod] [bigint] NOT NULL,
	[BillingType] [int] NOT NULL,
	[BillingUnit] [int] NOT NULL,
	[BillingRate] [decimal](7, 4) NOT NULL,
	[BillingDate] [datetime] NOT NULL,
	[LastBilledDate] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblBSetting] PRIMARY KEY CLUSTERED 
(
	[BSettingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblWSLog]    Script Date: 08/17/2012 21:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblWSLog](
	[WSLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[StoreID] [bigint] NULL,
	[StoreIPListID] [bigint] NULL,
	[PopUpJoeSessionID] [nvarchar](256) NOT NULL,
	[OrderTotal] [decimal](7, 2) NOT NULL,
	[ActivateTimer] [char](1) NOT NULL,
	[TimerValue] [int] NOT NULL,
	[CouponID] [bigint] NULL,
	[PopUpFlag] [bit] NOT NULL,
	[DatePopUp] [datetime] NULL,
	[OrderSubmitFlag] [bit] NOT NULL,
	[DateOrderSubmit] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblWSLog] PRIMARY KEY CLUSTERED 
(
	[WSLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[uspRecordWEBAudit]    Script Date: 08/17/2012 21:14:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRecordWEBAudit]
	@szWEBServer [nvarchar](512),
	@szErrSource [varchar] (512) = NULL,
	@iErrorNumber [int] = 0,
	@iErrorLine [int] = 0,
	@szErrorMessage [varchar] (4000),
	@iUserCreated [int] = 0
AS
BEGIN
SET NOCOUNT ON
INSERT INTO tblWEBAudit
           ([WEBServer],
			[ErrSource],
			[ErrorNumber],
			[ErrorLine],
			[ErrorMessage],
			[DateCreated],
			[UserCreated]
           )
     VALUES
           (@szWEBServer,
			@szErrSource,
			@iErrorNumber,
			@iErrorLine,
			@szErrorMessage,
			GETDATE(),
			@iUserCreated)
END
GO
/****** Object:  StoredProcedure [dbo].[uspRecordSQLAudit]    Script Date: 08/17/2012 21:14:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspRecordSQLAudit]
	@biProcessID bigint
    ,@iErrorNumber int
    ,@iErrorSeverity int
    ,@iErrorState int
    ,@szErrorProcedure varchar(512)
    ,@iErrorLine int
    ,@szErrorMessage varchar(4000)
    ,@szParameters varchar(1024) = null
    ,@iInserted [int] = 0

AS
BEGIN
SET NOCOUNT ON
INSERT INTO tblSQLAudit
           (ProcessID
           ,ErrorNumber
           ,ErrorSeverity
           ,ErrorState
           ,ErrorProcedure
           ,ErrorLine
           ,ErrorMessage
           ,[Parameters]
           ,DateCreated
           ,UserCreated
           )
     VALUES
           (@biProcessID
           ,@iErrorNumber
           ,@iErrorSeverity
           ,@iErrorState
           ,@szErrorProcedure
           ,@iErrorLine
           ,@szErrorMessage
           ,@szParameters
           ,GETDATE()
           ,@iInserted)
END
GO
/****** Object:  StoredProcedure [dbo].[uspMoveSubTree]    Script Date: 08/17/2012 21:14:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspMoveSubTree]
	@biOldParentID [bigint]
	,@biNewParentID [bigint]
	,@iRetCode [int] output
AS
BEGIN
 begin try
	DECLARE @hiOld hierarchyid
		,@hiNew hierarchyid
			
	SELECT @hiOld = StoreNode 
		FROM dbo.tblStore (nolock) 
		WHERE StoreID = @biOldParentID ;

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	BEGIN TRANSACTION
	
		SELECT @hiNew = StoreNode 
			FROM dbo.tblStore (nolock)
			WHERE StoreID = @biNewParentID ;

		SELECT @hiNew = @hiNew.GetDescendant(max(StoreNode), NULL) 
			FROM dbo.tblStore (nolock) 
			WHERE StoreNode.GetAncestor(1)=@hiNew ;

		UPDATE dbo.tblStore  
			SET StoreNode = StoreNode.GetReparentedValue(@hiOld, @hiNew)
			WHERE StoreNode.IsDescendantOf(@hiOld) = 1 ; 

	COMMIT TRANSACTION
	set @iRetCode = 0
 end try
 begin catch
	set @iRetCode = -1
	ROLLBACK TRAN 
 end catch	
END ;
GO
/****** Object:  UserDefinedFunction [dbo].[fnGeneratePassword]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[fnGeneratePassword] (
	@pLength int = 8, --default to 8 characters
	@charSet int = 0 -- 1 is alphanumeric + special characters,
					 -- 0 is alphanumeric
	)			 
RETURNS nvarchar(128)
AS
BEGIN
	
	IF @pLength < 8 SET @pLength = 8 -- set minimum length
	ELSE IF @pLength > 128 SET @pLength = 128 -- set maximum length
 
	DECLARE
		@password nvarchar(128),
		@string varchar(72), --52 possible letters + 10 possible numbers + up to 20 possible extras
		@numbers varchar(10),
		@extra varchar(20),
		@stringlen tinyint,
		@index tinyint,
		@pCount int,
		@Return varchar(50)
		
	SET @pCount = 1 --generate 1 passwords unless otherwise specified
 
	--table variable to hold password list
	--DECLARE @PassList TABLE (
	--	[password] varchar(50)
	--)
	 
	-- eliminate 0, 1, I, l, O to make the password more readable
	SET @string = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz' -- option @charset = 0
	SET @numbers = '123456789'
	SET @extra = '^_!@#$&?-' -- special characters
	 
	SET @string =
		CASE @charset
			WHEN 1 THEN @string + @numbers + @extra
			ELSE @string + @numbers
		END
	 
	SET @stringlen = len(@string)
	 
	WHILE (@pCount > 0)
	BEGIN
		SET @password = ''
		DECLARE @i int; SET @i = @pLength
		WHILE (@i > 0)
		BEGIN
			declare @x varchar(50)
			SELECT @x = [NewId] FROM dbo.vwGetNewID
			SET @index = (ABS(CHECKSUM(@x)) % @stringlen) + 1 --or rand()
			SET @password = @password + SUBSTRING(@string, @index, 1)
			SET @i -= 1 --SET @pLength = @pLength - 1
		END
		SET @pCount -= 1
	END
	SELECT @Return = @password
	RETURN @Return
END
GO
/****** Object:  Table [dbo].[tblUserMenu]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUserMenu](
	[UserMenuID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [bigint] NOT NULL,
	[AppMenuID] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[RevokeStatus] [bit] NOT NULL,
	[DateRevoked] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblUserMenu] PRIMARY KEY CLUSTERED 
(
	[UserMenuID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUser]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUser](
	[UserID] [bigint] IDENTITY(1,1) NOT NULL,
	[StoreID] [bigint] NOT NULL,
	[FirstName] [nvarchar](128) NOT NULL,
	[LastName] [nvarchar](128) NOT NULL,
	[UserEmail] [nvarchar](512) NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[SecureQuestion] [nvarchar](512) NOT NULL,
	[SecureAnswer] [nvarchar](512) NOT NULL,
	[DateLastLogin] [datetime] NULL,
	[EmailAlertRecipient] [bit] NOT NULL,
	[BillingReportRecipient] [bit] NOT NULL,
	[ResetPassword] [bit] NOT NULL,
	[RevokeStatus] [bit] NOT NULL,
	[DateRevoked] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblUser] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblStoreIPList]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblStoreIPList](
	[StoreIPListID] [bigint] IDENTITY(1,1) NOT NULL,
	[StoreID] [bigint] NOT NULL,
	[IPAddress] [varchar](64) NOT NULL,
	[DomainName] [nvarchar](128) NOT NULL,
	[TimerValue1] [int] NOT NULL,
	[TimerValue2] [int] NOT NULL,
	[RevokeStatus] [bit] NOT NULL,
	[DateRevoked] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblStoreIPList] PRIMARY KEY CLUSTERED 
(
	[StoreIPListID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblShoppingList]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblShoppingList](
	[ShoppingListID] [bigint] IDENTITY(1,1) NOT NULL,
	[WSLogID] [bigint] NOT NULL,
	[SKU] [nvarchar](64) NULL,
	[ProductName] [nvarchar](255) NOT NULL,
	[ItemCategory] [nvarchar](255) NULL,
	[Quantity] [decimal](12, 4) NOT NULL,
	[Price] [decimal](7, 2) NOT NULL,
	[CouponID] [bigint] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
 CONSTRAINT [PK_tblShoppingList] PRIMARY KEY CLUSTERED 
(
	[ShoppingListID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblCoupon]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCoupon](
	[CouponID] [bigint] IDENTITY(1,1) NOT NULL,
	[StoreIPListID] [bigint] NOT NULL,
	[eCommCouponID] [nvarchar](64) NOT NULL,
	[CouponDescription] [nvarchar](512) NULL,
	[CouponValue] [decimal](8, 3) NULL,
	[MinOrderAmt] [decimal](7, 2) NOT NULL,
	[MaxOrderAmt] [decimal](7, 2) NOT NULL,
	[CouponType] [int] NOT NULL,
	[MsgToPopUp] [nvarchar](max) NOT NULL,
	[ExpirationDate] [datetime] NULL,
	[LimitedNumber] [bit] NOT NULL,
	[Uses] [int] NULL,
	[RevokeStatus] [bit] NOT NULL,
	[DateRevoked] [datetime] NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblCoupon] PRIMARY KEY CLUSTERED 
(
	[CouponID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [svc].[uspStoreUpdate]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Insert dbo.tblStore record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/12/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreID is null or zero
	@iRetCode = -3, Parameter @szStoreName is null or empty
	@iRetCode = -4, Parameter @ieCommerceType is null or zero
	@iRetCode = -5, Wrong value of parameter @ieCommerceType
	@iRetCode = -6, Parameter @dtContractExpirationDate is expired
	@iRetCode = -7, Parameter @biParentStoreID is null or zero
	@iRetCode = -8, Record not found for @biParentStoreID
	@iRetCode = -9, Record not found for @biStoreID
	@iRetCode = -10, Store has "revoked" status
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/12/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
   		@biStoreID [bigint]
		,@ieCommerceType [int]
		,@szStoreName [nvarchar] (255)
		,@szPostalName [nvarchar] (255)
		,@szAddress1 [nvarchar] (255)
		,@szAddress2 [nvarchar] (255)
		,@szCity [nvarchar] (100)
		,@szStateProvince [nvarchar] (64)
		,@szPostalCode [nvarchar] (64)
		,@szCountryCode [varchar] (3)
		,@biParentStoreID [bigint]
		,@iSessionExpiration [int]
		,@dtContractExpirationDate [datetime]
		,@biUpdUser [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	--select
 --  		@biStoreID = 7
	--	,@ieCommerceType = 1
	--	,@szStoreName = 'eStoreChildChild1.com'
	--	,@szPostalName ='eStoreChildChild1.com'
	--	,@szCountryCode ='USA'
	--	,@biParentStoreID = 3
	--	,@iSessionExpiration = 43400
	--	,@dtContractExpirationDate = '2013-12-31 19:07:31.807'


	--select
 --  		@biStoreID = 5
	--	,@ieCommerceType = 1
	--	,@szStoreName = 'eStoreChild2.com'
	--	,@szPostalName ='eStoreChild2.com'
	--	,@szCountryCode ='USA'
	--	,@biParentStoreID = 7
	--	,@iSessionExpiration = 43400
	--	,@dtContractExpirationDate = '2013-12-31 19:07:31.807'

	select
   		@biStoreID = 7
		,@ieCommerceType = 1
		,@szStoreName = 'eStoreChildChild1.com'
		,@szPostalName ='eStoreChildChild1.com'
		,@szCountryCode ='USA'
		,@biParentStoreID = 4
		,@iSessionExpiration = 43400
		,@dtContractExpirationDate = '2013-12-31 19:07:31.807'



	exec [svc].[uspStoreUpdate]
		@biStoreID 
		,@ieCommerceType
		,@szStoreName
		,@szPostalName
		,@szAddress1
		,@szAddress2
		,@szCity
		,@szStateProvince
		,@szPostalCode
		,@szCountryCode
		,@biParentStoreID
		,@iSessionExpiration
		,@dtContractExpirationDate
		,@biUpdUser
   		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

select StoreNode.ToString(),* from tblStore
select * from dbo.tblSQLAudit

update tblStore set ParentStoreID = 1 where StoreID = 3
update tblStore set ParentStoreID = 3 where StoreID between 4 and 6
update tblStore set ParentStoreID = 5 where StoreID = 7

*/

CREATE PROCEDURE [svc].[uspStoreUpdate]
   	@biStoreID [bigint]
	,@ieCommerceType [int]
	,@szStoreName [nvarchar] (255)
	,@szPostalName [nvarchar] (255)
	,@szAddress1 [nvarchar] (255)
	,@szAddress2 [nvarchar] (255)
	,@szCity [nvarchar] (100)
	,@szStateProvince [nvarchar] (64)
	,@szPostalCode [nvarchar] (64)
	,@szCountryCode [varchar] (3) = 'USA'
	,@biParentStoreID [bigint] = 0
	,@iSessionExpiration [int] = 43200
	,@dtContractExpirationDate [datetime] = null
	,@biUpdUser [bigint] = null
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspStoreUpdate')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)
	,@szPostalName = coalesce(@szPostalName,@szStoreName)
	,@szAddress1 = coalesce(@szAddress1,'')
	,@szAddress2 = coalesce(@szAddress2,'')
	,@szCity = coalesce(@szCity,'')
	,@szStateProvince = coalesce(@szStateProvince,'')
	,@szPostalCode = coalesce(@szPostalCode,'')
	,@szCountryCode = coalesce(@szCountryCode,'USA')

	if coalesce(@iSessionExpiration,0) <= 0
	 set @iSessionExpiration = 43200
	 
	--if @dtContractExpirationDate is null
	-- set @dtContractExpirationDate = DATEADD(year,1,getdate())
	--if coalesce(@biParentStoreID,0)=0
	--	select @biParentStoreID= StoreID from dbo.tblStore (nolock) where StoreNode = hierarchyid::GetRoot()

declare @bTmpRevokeStatus [bit]
	,@biTmpParentStoreID [bigint]
	,@ParentStoreNode [hierarchyid] 
	,@TmpStoreNode [hierarchyid] 

declare @hiOld [hierarchyid]
	,@hiNew [hierarchyid]

-- Validate parameters
if	coalesce(@biStoreID,0)<=0
	set @iRetCode = -2
if	coalesce(@szStoreName,'')=''
	set @iRetCode = -3
else if	coalesce(@ieCommerceType,0)<=0
	set @iRetCode = -4
else if	not exists (select 1 from dbo.vwLookUp (nolock) 
	where LookUpName = 'eCommerceType' and LookUpSubType = @ieCommerceType)
	set @iRetCode = -5
else if @dtContractExpirationDate < GETDATE()
	set @iRetCode = -6
else if	coalesce(@biParentStoreID,0)<=0
	set @iRetCode = -7
else if	not exists (select 1 from dbo.tblStore (nolock) where StoreID = @biParentStoreID)
	select @iRetCode = -8
		,@szErrSubst = CAST(@biParentStoreID as [varchar] (30))

if @iRetCode = 0
 begin
 
	select @bTmpRevokeStatus = RevokeStatus
		,@biTmpParentStoreID = ParentStoreID
		,@hiOld = StoreNode
		from dbo.tblStore (nolock)
		where StoreID = @biStoreID
		
	if @@ROWCOUNT = 0
	 begin
	 -- store not found
		select @iRetCode = -9
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 end
	else if coalesce(@bTmpRevokeStatus,0)=1
	 begin
	 -- syore has revoked status
		select @iRetCode = -10
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 end
 end



if @iRetCode = 0
 begin
	 
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	  SAVE TRANSACTION uspStoreUpdate;
	else
	  BEGIN TRANSACTION;

	update dbo.tblStore set
		eCommerceType = @ieCommerceType
		,StoreName = @szStoreName
		,PostalName = @szPostalName
		,Address1 = @szAddress1
		,Address2 = @szAddress2
		,City = @szCity
		,StateProvince = @szStateProvince
		,PostalCode = @szPostalCode
		,CountryCode = @szCountryCode
		,SessionExpiration = @iSessionExpiration
		,ContractExpirationDate = @dtContractExpirationDate
		,DateUpdated = GETDATE()
		,UserUpdated = @biUpdUser
		,ParentStoreID = @biParentStoreID
		where StoreID = @biStoreID 
	 
	if @biTmpParentStoreID <> @biParentStoreID
	 begin
	
		SELECT @hiNew = StoreNode 
			FROM dbo.tblStore (nolock)
			WHERE StoreID = @biParentStoreID ;

		SELECT @hiNew = @hiNew.GetDescendant(max(StoreNode), NULL) 
			FROM dbo.tblStore (nolock) 
			WHERE StoreNode.GetAncestor(1)=@hiNew ;

		UPDATE dbo.tblStore  
			SET StoreNode = StoreNode.GetReparentedValue(@hiOld, @hiNew)
			WHERE StoreNode.IsDescendantOf(@hiOld) = 1 ; 
 
	 end

	-- commit transaction
	if @TranCounter = 0 -- no outer transaction
	 begin
	  COMMIT TRANSACTION;		
	  set @TranCounter = NULL
	 end 
 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspStoreUpdate;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspStoreIPListUpdate]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Insert dbo.tblStore record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/14/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreIPListID is null or zero
	@iRetCode = -3, Parameter @szIPAddress is null or empty
	@iRetCode = -4, Parameter @szDomainName is null or empty
	@iRetCode = -5, Parameter @iTimerValue1 is null, zero or negative
	@iRetCode = -6, Parameter @iTimerValue2 is null, zero or negative
	@iRetCode = -7, Record with @biParentStoreID not found in dbo.tblStoreIPList table
	@iRetCode = -8, Record in dbo.tblStoreIPList has "revoke" status
	@iRetCode = -9, Record with @biStoreID {0} not found in dbo.tblStore table
	@iRetCode = -10, Store has "revoked" status
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/14/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
   		@biStoreIPListID [bigint]
		,@szIPAddress [varchar] (64)
		,@szDomainName [nvarchar] (128)
		,@iTimerValue1 [int]
		,@iTimerValue2 [int]
		,@biUpdUser [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select
   		@biStoreIPListID = 1
		,@szIPAddress =''
		,@szDomainName = 'etronics.com'
		,@iTimerValue1 = 180
		,@iTimerValue2 = 300


	exec [svc].[uspStoreIPListUpdate]
   		@biStoreIPListID
		,@szIPAddress
		,@szDomainName
		,@iTimerValue1
		,@iTimerValue2
		,@biUpdUser
   		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspStoreIPListUpdate]
   	@biStoreIPListID [bigint]
	,@szIPAddress [varchar] (64)
	,@szDomainName [nvarchar] (128)
	,@iTimerValue1 [int]
	,@iTimerValue2 [int]
	,@biUpdUser [bigint] = null
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspStoreIPListUpdate')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)

declare @bRevokeStatus [bit]
	,@biStoreID [bigint]
	,@bStoreRevokeStatus [bit]


-- Validate parameters
if	coalesce(@biStoreIPListID,0)<=0
	set @iRetCode = -2
else if	coalesce(@szIPAddress,'')=''
	set @iRetCode = -3
else if	coalesce(@szDomainName,'')=''
	set @iRetCode = -4
else if	coalesce(@iTimerValue1,0)<=0
	set @iRetCode = -5
else if	coalesce(@iTimerValue2,0)<=0
	set @iRetCode = -6
else
 begin
	select @biStoreID = StoreID,
		@bRevokeStatus = RevokeStatus
		from dbo.tblStoreIPList (nolock) 
		where StoreIPListID = @biStoreIPListID
	
	if @@ROWCOUNT = 0
	 select @iRetCode = -7
	 	,@szErrSubst = CAST(@biStoreIPListID as [varchar] (30))
	else if coalesce(@bRevokeStatus,0)=1
	 select @iRetCode = -8
	 	,@szErrSubst = CAST(@biStoreIPListID as [varchar] (30))
	else
	 begin
	 
		select @bStoreRevokeStatus = RevokeStatus
			from dbo.tblStore (nolock) 
			where StoreID = @biStoreID
	 
		if @@ROWCOUNT = 0
		 select @iRetCode = -9
		 	,@szErrSubst = CAST(@biStoreID as [varchar] (30))
		else if coalesce(@bStoreRevokeStatus,0)=1
		 select @iRetCode = -10
		 	,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 
	 end 
 end

if @iRetCode = 0
 begin
	 
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	  SAVE TRANSACTION uspStoreIPListUpdate;
	else
	  BEGIN TRANSACTION;

	update dbo.tblStoreIPList set
		IPAddress = @szIPAddress
		,DomainName = @szDomainName
		,TimerValue1 = @iTimerValue1
		,TimerValue2 = @iTimerValue2
		,DateUpdated = GETDATE()
		,UserUpdated = @biUpdUser
		where StoreIPListID = @biStoreIPListID 

	-- commit transaction
	if @TranCounter = 0 -- no outer transaction
	 begin
	  COMMIT TRANSACTION;		
	  set @TranCounter = NULL
	 end 
 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspStoreIPListUpdate;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspStoreIPListChangeStatus]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Reset RevokeStatus of tblStoreIPList record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/14/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @biStoreIPListID is NULL or zero
	@iRetCode = -3, Store not found
	@iRetCode = -4, Store already has requested status
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/14/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------
	declare
		@biStoreIPListID [bigint]
		,@bRevokeStatus [bit]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biStoreIPListID = 1
		,@bRevokeStatus = 0

	exec [svc].[uspStoreIPListChangeStatus]
		@biStoreIPListID
		,@bRevokeStatus
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		
select * from tbluser where userId = 1
*/

CREATE PROCEDURE [svc].[uspStoreIPListChangeStatus]
	@biStoreIPListID [bigint]
	,@bRevokeStatus [bit] = 1
	,@biUpdUser [bigint] = null
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspStoreIPListChangeStatus')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

declare @bStoreRevokeStatus [bit],
	@bTmpRevokeStatus [bit]	,
	@dtTmpDateRevoked [datetime],
	@biStoreID [bigint]
	

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bRevokeStatus = coalesce(@bRevokeStatus,0)
	,@biUpdUser = coalesce(@biUpdUser,0)
	
-- Validate parameters
if coalesce(@biStoreIPListID,'')=0
-- Parameter is NULL or zero
	set @iRetCode = -2
 begin

	select 
		@dtTmpDateRevoked = sip.DateRevoked,
		@bTmpRevokeStatus = sip.RevokeStatus,
		@biStoreID = sip.StoreID
		from dbo.tblStoreIPList sip (nolock)
		where StoreIPListID = @biStoreIPListID
		
	if @@ROWCOUNT = 0
	 begin
-- no record for update found
		set @iRetCode = -3
	 end
	else if exists (select 1 from  dbo.tblStore (nolock) where StoreID = @biStoreID and RevokeStatus =1 )
	 begin
-- no record for update found
		select @iRetCode = -4,
			@szErrSubst='StoreID='+CAST(@biStoreID as [varchar] (30))					
	 end
	else if @bTmpRevokeStatus=@bRevokeStatus
	 begin
-- record already has requested status	 
		select @iRetCode = -5,
			@szErrSubst='RevokeStatus='+CAST(@bRevokeStatus as [varchar] (10))			
	 end
	else
	 begin
-- update Store

		-- do we have outer transaction?
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspStoreIPListChangeStatus;
		else
		  BEGIN TRANSACTION;

		update dbo.tblStoreIPList set [RevokeStatus] = @bRevokeStatus,
			DateRevoked = case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			DateUpdated = GETDATE(),
			UserUpdated = @biUpdUser
			where StoreIPListID = @biStoreIPListID

		insert into dbo.tblStatusLog (SourceTable,
			SourceTableID,
			OldStatus,
			OldDateRevoked,
			NewStatus,
			NewDateRevoked,
			DateCreated,
			UserCreated)
		values ('tblStoreIPList',
			@biStoreIPListID,
			@bTmpRevokeStatus,
			@dtTmpDateRevoked,
			@bRevokeStatus,
			case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			GETDATE(),
			@biUpdUser)
			
		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;
		  set @TranCounter = NULL
		 end 
	 end 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspStoreIPListChangeStatus;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspStoreInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspStoreInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Insert dbo.tblStore record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/12/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @szStoreName is null or empty
	@iRetCode = -3, Parameter @ieCommerceType is null or zero
	@iRetCode = -4, Wrong value of parameter @ieCommerceType

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/12/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@ieCommerceType [int]
		,@szStoreName [nvarchar] (255)
		,@szPostalName [nvarchar] (255)
		,@szAddress1 [nvarchar] (255)
		,@szAddress2 [nvarchar] (255)
		,@szCity [nvarchar] (100)
		,@szStateProvince [nvarchar] (64)
		,@szPostalCode [nvarchar] (64)
		,@szCountryCode [varchar] (3)
		,@biParentStoreID [bigint]
		,@iSessionExpiration [int]
		,@dtContractExpirationDate [datetime]
		,@biUpdUser [bigint]
   		,@biStoreID [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)



	--select
	--	@ieCommerceType = 1
	--	,@szStoreName = 'eStore.com'

	select
		@ieCommerceType = 1
		,@szStoreName = 'eStoreChildChild1.com'
		,@biParentStoreID = 5

	--select
	--	@ieCommerceType = 1
	--	,@szStoreName = 'eStoreChild2.com'
	--	,@biParentStoreID = 3 


	exec [svc].[uspStoreInsert]
		@ieCommerceType
		,@szStoreName
		,@szPostalName
		,@szAddress1
		,@szAddress2
		,@szCity
		,@szStateProvince
		,@szPostalCode
		,@szCountryCode
		,@biParentStoreID
		,@iSessionExpiration
		,@dtContractExpirationDate
		,@biUpdUser
   		,@biStoreID output
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

select StoreNode.ToString(),* from tblStore
select * from dbo.tblSQLAudit

*/

CREATE PROCEDURE [svc].[uspStoreInsert]
	@ieCommerceType [int]
	,@szStoreName [nvarchar] (255)
	,@szPostalName [nvarchar] (255)
	,@szAddress1 [nvarchar] (255)
	,@szAddress2 [nvarchar] (255)
	,@szCity [nvarchar] (100)
	,@szStateProvince [nvarchar] (64)
	,@szPostalCode [nvarchar] (64)
	,@szCountryCode [varchar] (3) = 'USA'
	,@biParentStoreID [bigint] = 0
	,@iSessionExpiration [int] = 43200
	,@dtContractExpirationDate [datetime] = null
	,@biUpdUser [bigint] = null
   	,@biStoreID [bigint]	output
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspStoreInsert')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)
	,@szPostalName = coalesce(@szPostalName,@szStoreName)
	,@szAddress1 = coalesce(@szAddress1,'')
	,@szAddress2 = coalesce(@szAddress2,'')
	,@szCity = coalesce(@szCity,'')
	,@szStateProvince = coalesce(@szStateProvince,'')
	,@szPostalCode = coalesce(@szPostalCode,'')
	,@szCountryCode = coalesce(@szCountryCode,'USA')
	,@biStoreID = 0

	if coalesce(@biParentStoreID,0)=0
		select @biParentStoreID= StoreID from dbo.tblStore (nolock) where StoreNode = hierarchyid::GetRoot()
	 
	if coalesce(@iSessionExpiration,0) <= 0
	 set @iSessionExpiration = 43200
	 
	if @dtContractExpirationDate is null
	 set @dtContractExpirationDate = DATEADD(year,1,getdate())

declare @bTmpRevokeStatus [bit]
	,@ParentStoreNode [hierarchyid] 
	,@TmpStoreNode [hierarchyid] 


-- Validate parameters
if	coalesce(@szStoreName,'')=''
	set @iRetCode = -2
else if	coalesce(@ieCommerceType,0)<=0
	set @iRetCode = -3
else if	not exists (select 1 from dbo.vwLookUp (nolock) 
	where LookUpName = 'eCommerceType' and LookUpSubType = @ieCommerceType)
	set @iRetCode = -4

if @iRetCode = 0
 begin

	select @ParentStoreNode = StoreNode 
		from dbo.tblStore (nolock) 
		where StoreID = @biParentStoreID 
	 
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	  SAVE TRANSACTION uspStoreInsert;
	else
	  BEGIN TRANSACTION;

      SELECT @TmpStoreNode = max(StoreNode) 
      FROM dbo.tblStore 
      WHERE StoreNode.GetAncestor(1) = @ParentStoreNode ;
 
	  insert into dbo.tblStore (
		eCommerceType
		,StoreName
		,PostalName
		,Address1
		,Address2
		,City
		,StateProvince
		,PostalCode
		,CountryCode
		,SessionExpiration
		,ContractExpirationDate
		,DateCreated
		,UserCreated
		,StoreNode
		,ParentStoreID)
	values (	
		@ieCommerceType
		,@szStoreName
		,@szPostalName
		,@szAddress1
		,@szAddress2
		,@szCity
		,@szStateProvince
		,@szPostalCode
		,@szCountryCode
		,@iSessionExpiration
		,@dtContractExpirationDate
		,getdate()
		,@biUpdUser
		,@ParentStoreNode.GetDescendant(@TmpStoreNode, NULL)
		,@biParentStoreID)

		set @biStoreID = SCOPE_IDENTITY()
		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;		
		  set @TranCounter = NULL
		 end 
		
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspStoreInsert;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

/****** Object:  StoredProcedure [svc].[uspStoreChangeStatus]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Reset store's RevokeStatus
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/13/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @biStoreID is NULL or zero
	@iRetCode = -3, Store not found
	@iRetCode = -4, Store already has requested status
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/13/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------
	declare
		@biStoreID [bigint]
		,@bRevokeStatus [bit]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biStoreID = 1
		,@bRevokeStatus = 0

	exec [svc].[uspStoreChangeStatus]
		@biStoreID
		,@bRevokeStatus
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		
select * from tbluser where userId = 1
*/

CREATE PROCEDURE [svc].[uspStoreChangeStatus]
	@biStoreID [bigint]
	,@bRevokeStatus [bit] = 1
	,@biUpdUser [bigint] = null
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspStoreChangeStatus')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

declare @bStoreRevokeStatus [bit],
	@bTmpRevokeStatus [bit]	,
	@dtTmpDateRevoked [datetime]

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bRevokeStatus = coalesce(@bRevokeStatus,0)
	,@biUpdUser = coalesce(@biUpdUser,0)
	
-- Validate parameters
if coalesce(@biStoreID,'')=0
-- Parameter is NULL or zero
	set @iRetCode = -2
 begin
	select 
		@dtTmpDateRevoked = s.DateRevoked,
		@bStoreRevokeStatus = s.RevokeStatus
		from dbo.tblStore s (nolock)
		where StoreID = @biStoreID
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		set @iRetCode = -3
	 end
	else if @bStoreRevokeStatus=@bRevokeStatus
	 begin
-- user has requested status	 
		select @iRetCode = -4,
			@szErrSubst='RevokeStatus='+CAST(@bRevokeStatus as [varchar] (10))			
	 end
	else
	 begin
-- update Store

		-- do we have outer transaction?
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspStoreChangeStatus;
		else
		  BEGIN TRANSACTION;

		update dbo.tblStore set [RevokeStatus] = @bRevokeStatus,
			DateRevoked = case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			DateUpdated = GETDATE(),
			UserUpdated = @biUpdUser
			where StoreID = @biStoreID

		insert into dbo.tblStatusLog (SourceTable,
			SourceTableID,
			OldStatus,
			OldDateRevoked,
			NewStatus,
			NewDateRevoked,
			DateCreated,
			UserCreated)
		values ('tblStore',
			@biStoreID,
			@bTmpRevokeStatus,
			@dtTmpDateRevoked,
			@bRevokeStatus,
			case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			GETDATE(),
			@biUpdUser)
			
		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;
		  set @TranCounter = NULL
		 end 
	 end 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspStoreChangeStatus;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[uspSendEmail]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date: 
-- Description:	Sends email based on recipient key
-- =============================================
CREATE PROCEDURE [dbo].[uspSendEmail] 
	@szSource [varchar](100) = NULL,
	@szEmailSubject [nvarchar](512),
	@szEmailDetails [nvarchar](max),
	@szRecipientListAddresses [nvarchar](max),
	@szSQLQueryString [nvarchar] (max),	/* query string to email */
	@iUserID [int] = 0,
	@iQResultWidth [int] = 125,			/* for query result */
	@szEmailFormat [varchar] (10) = 'Text', /* type of email body Text/HTML*/
	@iRetCode [int] OUTPUT,
	@bRecordOnly [bit] = 1
AS
/*
	@iRetCode :
		0 sucessful;
		-1 No Recipient List is empty
		-2 Database profile record not found
		-3 Database profile is empty
		-4 Failed to send message inside of call to sp_send_dbmail
*/
BEGIN
	SET NOCOUNT ON

	Declare @iMessageKey [int],
		 @szMsg [varchar] (max),
		 @iMailResult [int],
		 @szDBMailProfileName [nvarchar] (255),
		 @szCurrentDBName [nvarchar] (128),
		 @bAttachResult [bit],
		 @szTmpSource [varchar] (100)

	select @iRetCode = 0
	
	--Clean up parameters
	select	@szEmailSubject		= coalesce(@szEmailSubject,''),
			@szEmailDetails	= coalesce(@szEmailDetails, ''),
			@szSQLQueryString = coalesce(@szSQLQueryString,''),
			@szRecipientListAddresses = coalesce(@szRecipientListAddresses,''),
			@szTmpSource = 'uspSendEmail',
			@iUserID = coalesce(@iUserID,0),
			@bRecordOnly = coalesce(@bRecordOnly,0)
	
	--Log and fail if no addresses stored for the recipient key list
	if len(rtrim(@szRecipientListAddresses)) = 0
		begin
			--select @szMsg = 'No addresses stored for recipient list key [' + @pRecipientListKey
			--		+ '].  The intended message follows.'
			--		+ char(10) + char(13) + 'Subj: ' + rtrim(@szEmailSubject)
			--		+ char(10) + char(13) + 'Message: ' + rtrim(@szEmailDetails)

			--insert into dbo.tblEventLog (BatchKey, ProcessKey, EventMsg, EventDetails, EventSource)
			--values (@pBatchKey,@pProcessKey,'No addresses stored for recipient list key [' + @pRecipientListKey
			--		+ '].',@szMsg,@szTmpSource)

			Select @iRetCode = -1
			Return
		end

	--Retrieve DB mail profile name
	select @szDBMailProfileName = LookUpShortName
		from [dbo].[vwLookUp] (nolock)
		where LookUpName = 'DBMail' and LookupSubType = 1
	
	--Log and fail if no match to recipient key list
	if @@rowcount = 0
		begin
			--select @szMsg = 'No DBMail profile record found when '
			--		+ 'attempting to send the following message.'
			--		+ char(10) + char(13) + 'Subj: ' + rtrim(@szEmailSubject)
			--		+ char(10) + char(13) + 'Message: ' + rtrim(@szEmailDetails)

			--insert into dbo.tblEventLog (BatchKey, ProcessKey, EventMsg, EventDetails, EventSource)
			--values (@pBatchKey,@pProcessKey,'No tblSettings record found when '
			--		+ 'attempting to send the following message.',@szMsg,@szTmpSource)

			Select @iRetCode = -2
			Return
		end

	--Log and fail if no profile name is stored
	if len(rtrim(@szDBMailProfileName)) = 0
		begin
			--select @szMsg = 'DBMail profile was empty when '
			--		+ 'attempting to send the following message.'
			--		+ char(10) + char(13) + 'Subj: ' + rtrim(@szEmailSubject)
			--		+ char(10) + char(13) + 'Message: ' + rtrim(@szEmailDetails)

			--insert into dbo.tblEventLog (BatchKey, ProcessKey, EventMsg, EventDetails, EventSource)
			--values (@pBatchKey,@pProcessKey,'tblSettings.DBMailProfileName was empty when '
			--		+ 'attempting to send the following message.',@szMsg,@szTmpSource)

			Select @iRetCode = -3
			Return
		end

	--All validations passed
	select @bAttachResult = case when len(@szSQLQueryString) = 0 then 0 else 1 end;
	select @szCurrentDBName = DB_NAME(dbid) from master.dbo.sysprocesses WHERE spid = @@SPID;

	--Add entry to messages table
	insert into [dbo].[tblEmailLog](
		[Source],
		[EmailSubject],
		[EmailDetails],
		[RecipientList],
		[DateCreated],
		[UserCreated])	
	values (@szSource,
		@szEmailSubject,
		@szEmailDetails,
		@szRecipientListAddresses,
		GETDATE(),
		@iUserID)

	--Remember new key
	Select @iMessageKey = scope_identity()

	--Physically send the message
	if @bRecordOnly = 0
	 begin
		execute @iMailResult = msdb.dbo.sp_send_dbmail 
				@Profile_Name = @szDBMailProfileName, 
				@recipients = @szRecipientListAddresses, 
				@Subject = @szEmailSubject,
				@execute_query_database = @szCurrentDBName,
				@Body = @szEmailDetails,
				@body_format = @szEmailFormat,
				@query = @szSQLQueryString,
				@query_result_width = @iQResultWidth,
				@query_result_header = 1,
				@attach_query_result_as_file = @bAttachResult;

		if @iMailResult = 0
		begin
			Update dbo.tblEmailLog set [DateSend] = GetDate() where EmailLogID = @iMessageKey
			Select @iRetCode = 0
		end
		else
		begin
			Select @iRetCode = -4
			Return
		end

	 end
	else
	 begin
		Select @iRetCode = 0
	 end 
	--Only update the sent date if the message appears to have been sent

	Return
END
GO
/****** Object:  StoredProcedure [svc].[uspGetUsersStoreList]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Returns list of stores from dbo.tblStore table base 
	on value of @biUserID parameter
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/13/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biUserID is NULL or zero
	@iRetCode = -3, User {0} not found in dbo.tblUser table
	@iRetCode = -4, Store {0} not found in dbo.tblStore table	

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/13/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@biUserID [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biUserID = 1

	exec [svc].[uspGetUsersStoreList]
		@biUserID
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspGetUsersStoreList]
	@biUserID [bigint]
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspGetUsersStoreList')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''

declare @tbl table (StoreID [bigint] not null primary key,
	eCommerceType [int] not null,
	StoreName [nvarchar] (255) not null,
	RevokeStatus [bit] not null,
	DateRevoked [datetime] null,
	StoreNode [hierarchyid] null,
	ParentStoreID [bigint] null)

declare @CurrentStoreNode [hierarchyid]
	,@biStoreID [bigint] 

-- Validate parameters
if coalesce(@biUserID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else 	
 begin
	select @biStoreID = StoreID from dbo.tblUser (nolock) where UserID = @biUserID
	
	if @@ROWCOUNT = 0 
		select @iRetCode = -3
			,@szErrSubst = CAST(@biUserID as [varchar] (30))
	else 
	 begin

		select @CurrentStoreNode = StoreNode 
			from dbo.tblStore (nolock) 
			where StoreID = @biStoreID

		if @@ROWCOUNT = 0 
			select @iRetCode = -4
				,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 end
 end

if @iRetCode = 0
 begin

	insert into @tbl (
		StoreID,
		eCommerceType,
		StoreName,
		RevokeStatus,
		DateRevoked,
		StoreNode,
		ParentStoreID)
	select 
		StoreID,
		eCommerceType,
		StoreName,
		RevokeStatus,
		DateRevoked,
		StoreNode,
		ParentStoreID
		from dbo.tblStore (nolock) 
		where StoreNode.IsDescendantOf(@CurrentStoreNode) = 1 
	
	select t.StoreID,
		t.eCommerceType,
		vw.LookUpShortName as eCommerceName,
		t.StoreName,
		t.RevokeStatus,
		t.DateRevoked,
		t.StoreNode.ToString() as RNumber,
		t.ParentStoreID
	from @tbl t 
	left join dbo.vwLookUp vw (nolock) on vw.LookUpName = 'eCommerceType'
		and vw.LookUpSubType = t.eCommerceType
	order by RNumber	
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspGetUsersStoreList;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = 0

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspGetStoreUsersList]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Returns list of Users from dbo.tblUser table base 
	on value of @biStoreID parameter
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/13/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreID is NULL or zero
	@iRetCode = -3, Store {0} not found in dbo.tblStore table
	@iRetCode = -4, Store {0} has "revoked" status

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/13/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare @biStoreID [bigint]
		,@iPageIndex [int]
		,@iPageSize [int]    
		,@szSortColumn [varchar] (50)
		,@szSortDirection [varchar] (50)
		,@biResultCount [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biStoreID = 1
		,@iPageIndex = 1
		,@iPageSize = 10
		,@szSortColumn = 'UserEmail'
		,@szSortDirection = 'ASC'

	exec [svc].[uspGetStoreUsersList]
		@biStoreID
		,@iPageIndex
		,@iPageSize
		,@szSortColumn
		,@szSortDirection
		,@biResultCount output
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspGetStoreUsersList]
	@biStoreID [bigint]
	,@iPageIndex [int]
	,@iPageSize [int]    
	,@szSortColumn [varchar] (50) = null
	,@szSortDirection [varchar] (50) = null
	,@biResultCount [bigint] output
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspGetStoreUsersList')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''

declare @tbl table (UserID [bigint] not null,
	FirstName [nvarchar] (128) not null,
	LastName [nvarchar] (128) not null,
	UserEmail [nvarchar] (512) not null,
	DateLastLogin [datetime] null,
	EmailAlertRecipient [bit] not null,
	BillingReportRecipient [bit] not null,
	ResetPassword [bit] not null,
	RevokeStatus [bit] not null,
	DateRevoked [datetime] null,
	DateCreated [datetime] null,
	DateUpdated [datetime] null,
	SortColumn [nvarchar] (512) null,
	[RowNum] [bigint] not null primary key)	

declare @CurrentStoreNode [hierarchyid],
	@bTmpStoreStatus [bit]

-- Validate parameters
if coalesce(@biStoreID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else 	
 begin
	select @bTmpStoreStatus = RevokeStatus from dbo.tblStore (nolock) where StoreID = @biStoreID
	
	if @@ROWCOUNT = 0 
		select @iRetCode = -3
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	else if coalesce(@bTmpStoreStatus,0) = 1
		select @iRetCode = -4
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
 end

if @iRetCode = 0
 begin

	insert into @tbl (
		UserID,
		FirstName,
		LastName,
		UserEmail,
		DateLastLogin,
		EmailAlertRecipient,
		BillingReportRecipient,
		ResetPassword,
		RevokeStatus,
		DateRevoked,
		DateCreated,
		DateUpdated,
		[RowNum])
	select 
		UserID,
		FirstName,
		LastName,
		UserEmail,
		DateLastLogin,
		EmailAlertRecipient,
		BillingReportRecipient,
		ResetPassword,
		RevokeStatus,
		DateRevoked,
		DateCreated,
		DateUpdated,
		UserID
		from dbo.tblUser (nolock) 
		where StoreID = @biStoreID

	select @biResultCount = @@ROWCOUNT

	if coalesce(@szSortColumn,'')<>'' and coalesce(@szSortDirection,'')<>''
	 begin
		update @tbl set [SortColumn] =
		case 
			when @szSortColumn = 'UserEmail' then UserEmail
			when @szSortColumn = 'FirstName' then FirstName
			when @szSortColumn = 'LastName' then LastName
			when @szSortColumn = 'DateLastLogin' then convert([varchar] (30), [DateLastLogin],120)
			when @szSortColumn = 'DateCreated' then convert([varchar] (30), [DateCreated],120)
			when @szSortColumn = 'DateUpdated' then convert([varchar] (30), [DateUpdated],120)			
			when @szSortColumn = 'DateRevoked' then convert([varchar] (30), [DateRevoked],120)			
			when @szSortColumn = 'RevokeStatus' then cast([RevokeStatus] as [varchar] (10))
			when @szSortColumn = 'EmailAlertRecipient' then cast([EmailAlertRecipient] as [varchar] (10))
			when @szSortColumn = 'BillingReportRecipient' then cast([BillingReportRecipient] as [varchar] (10))
			when @szSortColumn = 'ResetPassword' then cast([ResetPassword] as [varchar] (10))			
			else cast(UserID as [varchar] (30))					
		end

		if @szSortDirection = 'ASC'
		 begin
			;with t0 as (
				select [UserID],[RowNum],ROW_NUMBER() OVER(ORDER BY SortColumn) as RNumber from @tbl
			)
			update t set [RowNum] = t0.RNumber
			from @tbl t
			inner join t0 on t0.[UserID] = t.[UserID] and t0.[RowNum]=t.[RowNum]
		 end
		else
		 begin

			;with t0 as (
				select [UserID],[RowNum],ROW_NUMBER() OVER(ORDER BY SortColumn desc) as RNumber from @tbl
			)
			update t set [RowNum] = t0.RNumber
			from @tbl t
			inner join t0 on t0.[UserID] = t.[UserID] and t0.[RowNum]=t.[RowNum]
		 end
	 end

	select UserID,
		FirstName,
		LastName,
		UserEmail,
		DateLastLogin,
		EmailAlertRecipient,
		BillingReportRecipient,
		ResetPassword,
		RevokeStatus,
		DateRevoked,
		DateCreated,
		DateUpdated,
		RowNum
	from @tbl
	WHERE RowNum between ((@iPageIndex - 1) * @iPageSize + 1) and @iPageIndex*@iPageSize   

 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspGetStoreUsersList;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = 0

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspGetStoreIPList]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Returns list of records from tblStoreIPList table base 
	on value of @biStoreID parameter
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/14/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreID is NULL or zero
	@iRetCode = -3, Store {0} not found in dbo.tblStore table
	@iRetCode = -4, Store {0} has "revoked" status

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/14/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare @biStoreID [bigint]
		,@iPageIndex [int]
		,@iPageSize [int]    
		,@szSortColumn [varchar] (50)
		,@szSortDirection [varchar] (50)
		,@biResultCount [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biStoreID = 1
		,@iPageIndex = 1
		,@iPageSize = 10
		,@szSortColumn = 'DomainName'
		,@szSortDirection = 'ASC'

	exec [svc].[uspGetStoreIPList]
		@biStoreID
		,@iPageIndex
		,@iPageSize
		,@szSortColumn
		,@szSortDirection
		,@biResultCount output
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspGetStoreIPList]
	@biStoreID [bigint]
	,@iPageIndex [int]
	,@iPageSize [int]    
	,@szSortColumn [varchar] (50) = null
	,@szSortDirection [varchar] (50) = null
	,@biResultCount [bigint] output
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspGetStoreIPList')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''

declare @tbl table (StoreIPListID [bigint] not null,
	StoreID [bigint] not null,
	IPAddress [varchar] (64) not null,
	DomainName [nvarchar] (128) not null,
	TimerValue1 [int] not null,
	TimerValue2 [int] not null,
	RevokeStatus [bit] not null,
	DateRevoked [datetime] null,
	DateCreated [datetime] null,
	DateUpdated [datetime] null,
	SortColumn [nvarchar] (512) null,
	[RowNum] [bigint] not null primary key)	

declare @bTmpStoreStatus [bit]

-- Validate parameters
if coalesce(@biStoreID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else 	
 begin
	select @bTmpStoreStatus = RevokeStatus from dbo.tblStore (nolock) where StoreID = @biStoreID
	
	if @@ROWCOUNT = 0 
		select @iRetCode = -3
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	else if coalesce(@bTmpStoreStatus,0) = 1
		select @iRetCode = -4
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
 end

if @iRetCode = 0
 begin

	insert into @tbl (
		StoreIPListID,
		StoreID,
		IPAddress,
		DomainName,
		TimerValue1,
		TimerValue2,
		RevokeStatus,
		DateRevoked,
		DateCreated,
		DateUpdated,
		[RowNum])
	select 
		StoreIPListID,
		StoreID,
		IPAddress,
		DomainName,
		TimerValue1,
		TimerValue2,
		RevokeStatus,
		DateRevoked,
		DateCreated,
		DateUpdated,
		StoreIPListID as RowNum
		from dbo.tblStoreIPList (nolock) 
		where StoreID = @biStoreID
		order by StoreIPListID

	select @biResultCount = @@ROWCOUNT

	if coalesce(@szSortColumn,'')<>'' and coalesce(@szSortDirection,'')<>''
	 begin
		update @tbl set [SortColumn] =
		case 
			when @szSortColumn = 'IPAddress' then IPAddress
			when @szSortColumn = 'DomainName' then DomainName
			when @szSortColumn = 'TimerValue1' then cast([TimerValue1] as [varchar] (10))
			when @szSortColumn = 'TimerValue2' then cast([TimerValue2] as [varchar] (10))
			when @szSortColumn = 'DateCreated' then convert([varchar] (30), [DateCreated],120)
			when @szSortColumn = 'DateUpdated' then convert([varchar] (30), [DateUpdated],120)			
			when @szSortColumn = 'DateRevoked' then convert([varchar] (30), [DateRevoked],120)			
			when @szSortColumn = 'RevokeStatus' then cast([RevokeStatus] as [varchar] (10))
			else cast(StoreIPListID as [varchar] (30))					
		end

		if @szSortDirection = 'ASC'
		 begin
			;with t0 as (
				select [StoreIPListID],[RowNum],ROW_NUMBER() OVER(ORDER BY SortColumn) as RNumber from @tbl
			)
			update t set [RowNum] = t0.RNumber
			from @tbl t
			inner join t0 on t0.[StoreIPListID] = t.[StoreIPListID] and t0.[RowNum]=t.[RowNum]
		 end
		else
		 begin

			;with t0 as (
				select [StoreIPListID],[RowNum],ROW_NUMBER() OVER(ORDER BY SortColumn desc) as RNumber from @tbl
			)
			update t set [RowNum] = t0.RNumber
			from @tbl t
			inner join t0 on t0.[StoreIPListID] = t.[StoreIPListID] and t0.[RowNum]=t.[RowNum]
		 end
	 end

	select 	StoreIPListID,
		IPAddress,
		DomainName,
		TimerValue1,
		TimerValue2,
		RevokeStatus,
		DateRevoked,
		DateCreated,
		DateUpdated,
		[RowNum]
	from @tbl
	WHERE RowNum between ((@iPageIndex - 1) * @iPageSize + 1) and @iPageIndex*@iPageSize   

 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspGetStoreIPList;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = 0

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspGetStoreInfo]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Returns Store data from dbo.tblStore table base 
	on value of @biStoreID parameter
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/15/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreID is NULL or zero
	@iRetCode = -3, Store {0} not found in dbo.tblStore table

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/15/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare @biStoreID [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biStoreID = 3

	exec [svc].[uspGetStoreInfo]
		@biStoreID
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspGetStoreInfo]
	@biStoreID [bigint]
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspGetStoreInfo')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''

declare @tbl table (StoreID [bigint] not null primary key,
	eCommerceType [int] not null,
	StoreName [nvarchar] (255) not null,
	PostalName [nvarchar] (255) not null,
	Address1 [nvarchar] (255) null,
	Address2 [nvarchar] (255) null,
	City [nvarchar] (100) not null,
	StateProvince [nvarchar] (64) not null,
	PostalCode [nvarchar] (64) not null,
	CountryCode [varchar] (3) not null,
	RevokeStatus [bit] not null,
	DateRevoked [datetime] null,
	ParentStoreID [bigint] null,
	SessionExpiration [int] null,
	ContractExpirationDate [datetime] not null,
	DateCreated [datetime] null,
	DateUpdated [datetime] null)	

-- Validate parameters
if coalesce(@biStoreID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else 	
 begin
	insert into @tbl (
		StoreID,
		eCommerceType,
		StoreName,
		PostalName,
		Address1,
		Address2,
		City,
		StateProvince,
		PostalCode,
		CountryCode,
		RevokeStatus,
		DateRevoked,
		ParentStoreID,
		SessionExpiration,
		ContractExpirationDate,
		DateCreated,
		DateUpdated)
	select 
		StoreID,
		eCommerceType,
		StoreName,
		PostalName,
		Address1,
		Address2,
		City,
		StateProvince,
		PostalCode,
		CountryCode,
		RevokeStatus,
		DateRevoked,
		ParentStoreID,
		SessionExpiration,
		ContractExpirationDate,
		DateCreated,
		DateUpdated
		from dbo.tblStore (nolock) 
		where StoreID = @biStoreID
	
	if @@ROWCOUNT = 0 
		select @iRetCode = -3
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
 end

if @iRetCode = 0
 begin
	select 
		t.StoreID,
		t.eCommerceType,
		coalesce(lk.LookUpShortName,'') as eCommerceName,
		t.StoreName,
		t.PostalName,
		t.Address1,
		t.Address2,
		t.City,
		t.StateProvince,
		t.PostalCode,
		t.CountryCode,
		t.RevokeStatus,
		t.DateRevoked,
		t.ParentStoreID,
		t.SessionExpiration,
		t.ContractExpirationDate,
		t.DateCreated,
		t.DateUpdated
	from @tbl t
	left join dbo.vwLookUp lk (nolock) on lk.LookUpName = 'eCommerceType'
		and lk.LookUpSubType = t.eCommerceType
	
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspGetStoreInfo;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = 0

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspGetLookUpData]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Returns set of records from dbo.vwLookUp view base 
	on parameter value
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/10/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @szLookUpName is NULL or empty
	@iRetCode = -3, No data found

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/10/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@szLookUpName [varchar] (64)
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @szLookUpName = 'MenuStatus'

	exec [svc].[uspGetLookUpData]
		@szLookUpName
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspGetLookUpData]
	@szLookUpName [varchar] (64)
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspGetLookUpData')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------


select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''

declare @tbl table (LookUpValue [varchar] (64) not null,
	LookUpSubType [int] not null)

-- Validate parameters
if coalesce(@szLookUpName,'')=''
-- Parameter is NULL or empty
	set @iRetCode = -2
else 	
 begin
	insert into @tbl (
		LookUpValue,
		LookUpSubType)
	select 
		LookUpShortName,
		LookUpSubType
		from dbo.vwLookUp (nolock) 
		where LookUpName = @szLookUpName
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		select @iRetCode = -3,
			@szErrSubst = @szLookUpName
	 end
 end

if @iRetCode = 0
 begin
		
	select * from @tbl order by LookUpSubType
		
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspGetLookUpData;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = 0

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspValidateLogin]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Validate user credentials
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/06/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @szUserEmail is NULL or empty
	@iRetCode = -3, parameter @szPassword is NULL or empty
	@iRetCode = -4, User not found
	@iRetCode = -5, User has revoke status
	@iRetCode = -6, Store has revoke status
	@iRetCode = -7, parameter @szPassword does not match, secure question has been return
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/06/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------
	declare
		@szUserEmail [nvarchar] (512)
		,@szPassword [nvarchar] (128)
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)
		,@biUserID [bigint]
		,@szSecureQuestion [nvarchar] (512)

	select
		@szUserEmail = 'admin@ppj.com'
		,@szPassword = 'AdminPPJ'

	exec [svc].[uspValidateLogin]
		@szUserEmail
		,@szPassword
		,@iRetCode output 
		,@szMsgTier1 output   
		,@szMsgTier2 output  
		,@biUserID output 
		,@szSecureQuestion output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg]   
		,@biUserID as [UserID] 
		,@szSecureQuestion as [SecureQuestion]  
*/

CREATE PROCEDURE [svc].[uspValidateLogin]
	@szUserEmail [nvarchar] (512)
	,@szPassword [nvarchar] (128)
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
	,@biUserID [bigint] output 
	,@szSecureQuestion [nvarchar] (512) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspValidateLogin')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

declare @bRevokeStatus [bit],
	@szTmpPassword [nvarchar] (128),
	@biTmpUserID [bigint],
	@szTmpSecureQuestion [nvarchar] (512),
	@bStoreRevokeStatus [bit]

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUserID = 0
	,@bRevokeStatus = 1
	,@szTmpPassword = ''
	,@biTmpUserID = 0
	,@szSecureQuestion = ''
	,@bStoreRevokeStatus= 1

-- Validate parameters
if coalesce(@szUserEmail,'')=''
-- Parameter is NULL or empty
	set @iRetCode = -2
else if coalesce(@szPassword,'')=''
	set @iRetCode = -3
else
 begin
	select @biTmpUserID = u.UserID, 
		@bRevokeStatus = u.RevokeStatus,
		@szTmpPassword = u.[Password],
		@szTmpSecureQuestion = u.SecureQuestion,
		@bStoreRevokeStatus = s.RevokeStatus
		from dbo.tblUser u (nolock) 
		left join dbo.tblStore s (nolock) on s.StoreID = u.StoreID
		where UserEmail = @szUserEmail
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		set @iRetCode = -4
	 end
	else if coalesce(@bRevokeStatus,0)=1
	 begin
-- user has Revoked status	 
		set @iRetCode = -5
	 end
	else if coalesce(@bStoreRevokeStatus,0)=1
	 begin
-- user has Revoked status	 
		set @iRetCode = -6
	 end
	else if @szTmpPassword <> @szPassword COLLATE SQL_Latin1_General_CP1_CS_AS
	 begin
-- password does not match	 
		select @iRetCode = -7,
				 @szSecureQuestion = @szTmpSecureQuestion
	 end
	else
	 begin
		select @biUserID = @biTmpUserID

		-- do we have outer transaction?
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspValidateLogin;
		else
		  BEGIN TRANSACTION;

		update dbo.tblUser set DateLastLogin = GETDATE()
			where UserID = @biUserID

		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		  COMMIT TRANSACTION;
	 end 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = 
ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = 
ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspValidateLogin;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biTmpUserID

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspUserUpdate]    Script Date: 08/17/2012 21:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Update dbo.tblUser record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/10/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biUserID is null or zero
	@iRetCode = -3, Parameter @biStoreID is null or zero
	@iRetCode = -4, Parameter @szFirstName is null or empty
	@iRetCode = -5, Parameter @szLastName is null or empty
	@iRetCode = -6, Parameter @szUserEmail is null or empty
	@iRetCode = -7, Store not found
	@iRetCode = -8, Store has "revoke" status
	@iRetCode = -9, User not found
	@iRetCode = -10, User has "revoke" status
	@iRetCode = -11, Secure answer does not match
	@iRetCode = -12, New user's email already in use

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/10/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@szLookUpName [varchar] (64)
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @szLookUpName = 'MenuStatus'

	exec [svc].[uspUserUpdate]
		@szLookUpName
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspUserUpdate]
	@biUserID [bigint]	
	,@biStoreID [bigint]
	,@szFirstName [nvarchar] (128)
	,@szLastName [nvarchar] (128)
	,@szUserEmail [nvarchar] (512)
	,@bEmailAlertRecipient [bit] = 0
	,@bBillingReportRecipient [bit] = 0
	,@biUpdUser [bigint] = null
	,@szAnswer [nvarchar] (512) = NULL	
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserUpdate')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bEmailAlertRecipient = coalesce(@bEmailAlertRecipient,0)
	,@bBillingReportRecipient = coalesce(@bBillingReportRecipient,0)
	,@biUpdUser = coalesce(@biUpdUser,@biUserID)
	,@szAnswer = coalesce(@szAnswer,'') 

declare @bTmpRevokeStatus [bit]
	,@szTmpUserEmail [nvarchar] (512)
	,@szTmpAnswer [nvarchar] (512)

-- Validate parameters
if coalesce(@biUserID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else if	coalesce(@biStoreID,0)=0
	set @iRetCode = -3
else if	coalesce(@szFirstName,'')=''
	set @iRetCode = -4
else if	coalesce(@szLastName,'')=''
	set @iRetCode = -5
else if	coalesce(@szUserEmail,'')=''
	set @iRetCode = -6
else
 begin
 
-- validate Store parameters	
	select @bTmpRevokeStatus = RevokeStatus 
		from dbo.tblStore s (nolock)
		where StoreID = @biStoreID
		
	if @@ROWCOUNT = 0	
	 begin
-- store not found
		select @iRetCode = -7,
			@szErrSubst = CAST(@biStoreID as [varchar] (10))
	 end
	else if coalesce(@bTmpRevokeStatus,0)=1
	 begin
-- store has "revoke" status
		select @iRetCode = -8,
			@szErrSubst = CAST(@biStoreID as [varchar] (10))
	 end
	else
	 begin
-- validate use info
		select @bTmpRevokeStatus = RevokeStatus
			,@szTmpUserEmail = UserEmail
			,@szTmpAnswer = SecureAnswer
			from dbo.tblUser (nolock)
			where UserID = @biUserID

		if @@ROWCOUNT = 0	
		 begin
-- user not found
			select @iRetCode = -9,
				@szErrSubst = CAST(@biUserID as [varchar] (10))
		 end
		else if coalesce(@bTmpRevokeStatus,0)=1
		 begin
-- user has "revoke" status
			select @iRetCode = -10,
				@szErrSubst = CAST(@biUserID as [varchar] (10))
		 end
		else if @szAnswer<>'' and @szAnswer<> @szTmpAnswer
		 begin
			select @iRetCode = -11
		 end
		else if @szTmpUserEmail <> @szUserEmail and 
			exists (select 1 from dbo.tblUser (nolock) where UserEmail = @szUserEmail)
		 begin
-- new email already in use
			select @iRetCode = -12,
				@szErrSubst = @szUserEmail
		 end	
	 end
 end

if @iRetCode = 0
 begin
 
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspUserUpdate;
		else
		  BEGIN TRANSACTION;

		update dbo.tblUser set 
			StoreID = @biStoreID,
			FirstName = @szFirstName,
			LastName=@szLastName,
			UserEmail=@szUserEmail,
			EmailAlertRecipient=@bEmailAlertRecipient,
			BillingReportRecipient=@bBillingReportRecipient,
			DateUpdated = GETDATE(),
			UserUpdated = @biUpdUser
			where UserID = @biUserID

		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;		
		  set @TranCounter = NULL
		 end 
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspUserUpdate;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUserID

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspUserMenuRecordUpdate]    Script Date: 08/17/2012 21:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Update status of dbo.tblUserMenu record 
	   Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/15/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biUserMenuID is NULL, zero or negative
	@iRetCode = -3, Parameter @iStatus is null, zero or negative
	@iRetCode = -4, Wrong value of parameter @iStatus {0}
	@iRetCode = -5, No record for update found in dbo.tblUserMenu with {0}
	@iRetCode = -6, Record in dbo.tblUserMenu with {0} has "revoke" status

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/15/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@biUserMenuID [bigint]
		,@iStatus [int]
		,@biUserUpd [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biUserMenuID = 1
		,@iStatus = 1
		,@biUserUpd = 1

	exec [svc].[uspUserMenuRecordUpdate]
		@biUserMenuID
		,@iStatus
		,@biUserUpd
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspUserMenuRecordUpdate]
	@biUserMenuID [bigint]
	,@iStatus [int]
	,@biUpdUser [bigint]
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserMenuRecordUpdate')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)

declare @bRevokeStatus [bit]

-- Validate parameters
if coalesce(@biUserMenuID,0)<=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else if coalesce(@iStatus,0)<=0
	set @iRetCode = -3
else if not exists (select 1 from dbo.vwLookUp lk (nolock) 
		where lk.LookUpName = 'MenuStatus'
		and lk.LookUpSubType = @iStatus)
	select @iRetCode = -4
		,@szErrSubst = cast(@iStatus as [varchar] (10))
else 	
 begin
	select @bRevokeStatus = RevokeStatus from dbo.tblUserMenu (nolock)
		where UserMenuID = @biUserMenuID
 
	if @@ROWCOUNT = 0
		select @iRetCode = -5
			,@szErrSubst = cast(@biUserMenuID as [varchar] (30))
	else if @bRevokeStatus = 1
		select @iRetCode = -6
			,@szErrSubst = cast(@biUserMenuID as [varchar] (30))
 end

if @iRetCode = 0
 begin

	 
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	  SAVE TRANSACTION uspUserMenuRecordUpdate;
	else
	  BEGIN TRANSACTION;

	update dbo.tblUserMenu set 
		[Status] = @iStatus
			,DateUpdated = GETDATE()
		,UserUpdated = @biUpdUser
		where UserMenuID = @biUserMenuID

	-- commit transaction
	if @TranCounter = 0 -- no outer transaction
	 begin
	  COMMIT TRANSACTION;		
	  set @TranCounter = NULL
	 end 
		
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspUserMenuRecordUpdate;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  Table [dbo].[tblBillingEmailLog]    Script Date: 08/17/2012 21:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblBillingEmailLog](
	[BillingEmailLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[BillingID] [bigint] NOT NULL,
	[UserID] [bigint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[UserCreated] [bigint] NOT NULL,
	[DateUpdated] [datetime] NULL,
	[UserUpdated] [bigint] NULL,
 CONSTRAINT [PK_tblBillingEmailLog] PRIMARY KEY CLUSTERED 
(
	[BillingEmailLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [svc].[uspUserGetMenuOptions]    Script Date: 08/17/2012 21:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Return user's info and user's menu options
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/08/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @biUserID is NULL or zero
	@iRetCode = -3, User not found

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/08/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@biUserID [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biUserID = 1

	exec [svc].[uspUserGetMenuOptions]
		@biUserID
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspUserGetMenuOptions]
	@biUserID [bigint]
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserGetMenuOptions')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------


select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''

declare @tblUser table (UserID [bigint] not null,
	FirstName [nvarchar] (128) not null,
	LastName [nvarchar] (128) not null,
	UserEmail [nvarchar] (512) not null,
	SecureQuestion [nvarchar] (512) not null,
	DateLastLogin [datetime] null,
	EmailAlertRecipient [bit] null,
	BillingReportRecipient [bit] null,
	ResetPassword [bit] not null,
	RevokeStatus [bit] null,
	DateRevoked [datetime] null,
	DateCreated [datetime] not null,
	DateUpdated [datetime] null,
	StoreID [bigint] not null default (0),
	StoreName [nvarchar] (512) not null default (''))

-- Validate parameters
if coalesce(@biUserID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else 	
 begin
	insert into @tblUser (
		UserID,
		FirstName,
		LastName,
		UserEmail,
		SecureQuestion,
		DateLastLogin,
		EmailAlertRecipient,
		BillingReportRecipient,
		ResetPassword,
		RevokeStatus,
		DateRevoked,
		DateCreated,
		DateUpdated,
		StoreID,
		StoreName)
	select 
		u.UserID,
		u.FirstName,
		u.LastName,
		u.UserEmail,
		u.SecureQuestion,
		u.DateLastLogin,
		u.EmailAlertRecipient,
		u.BillingReportRecipient,
		u.ResetPassword,
		u.RevokeStatus,
		u.DateRevoked,
		u.DateCreated,
		u.DateUpdated,
		s.StoreID,
		s.StoreName
		from dbo.tblUser u (nolock) 
		left join dbo.tblStore s (nolock) on s.StoreID = u.StoreID
		where UserID = @biUserID
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		select @iRetCode = -3,
			@szErrSubst = CAST(@biUserID as [varchar] (10))
	 end
 end

if @iRetCode = 0
 begin
	
	select * from @tblUser

	;with t0 as (
	select am.AppMenuID,
		 am.MenuName,
		 am.ParentID,
		 am.PageLocation,
		 um.UserMenuID,
		 um.[Status],
		 vl.LookUpShortName,
		 um.RevokeStatus,
		 um.DateRevoked,
		 um.DateCreated,
		 um.DateUpdated
		from dbo.tblUserMenu um (nolock) 
		inner join dbo.tblAppMenu am (nolock) on um.AppMenuID = am.AppMenuID
		inner join dbo.vwLookUp vl (nolock) on vl.LookUpName = 'MenuStatus'
			and vl.LookUpSubType = um.[Status]
		where um.UserID = @biUserID
--		and um.RevokeStatus = 0
		and am.RevokeStatus = 0
--		and vl.LookUpShortName <> 'Invisible'
	), tt as (
	select AppMenuID,
			MenuName,
			ParentID,
			PageLocation,
			UserMenuID,
		    [Status],
			LookUpShortName as UserRights,
		    RevokeStatus,
		    DateRevoked,
		    DateCreated,
		    DateUpdated,
--			ROW_NUMBER() OVER(ORDER BY AppMenuID) as RNumber
			cast('/'+CAST(t0.AppMenuID as [varchar] (10))+'/' as [varchar] (128)) as RNumber
			from t0 where ParentID =0
		union all 
		select t.AppMenuID,
			t.MenuName,
			t.ParentID,
			t.PageLocation,
			t.UserMenuID,
		    t.[Status],
			t.LookUpShortName as UserRights,
		    t.RevokeStatus,
		    t.DateRevoked,
		    t.DateCreated,
		    t.DateUpdated,
			cast(tt.RNumber+CAST(t.AppMenuID as [varchar] (10))+'/' as [varchar] (128)) RNumber
--			ROW_NUMBER() OVER(ORDER BY t.AppMenuID) as RNumber
			from t0 t 
			inner join tt on tt.AppMenuID=t.ParentID
			where t.ParentID <>0
		)
	select * from tt order by RNumber	
		
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspUserGetMenuOptions;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUserID

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspUserChangeStatus]    Script Date: 08/17/2012 21:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Reset user's RevokeStatus
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/08/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @biUserID is NULL or zero
	@iRetCode = -3, User not found
	@iRetCode = -4, Store has revoke status
	@iRetCode = -5, User already has requested status
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/08/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------
	declare
		@biUserID [bigint]
		,@bRevokeStatus [bit]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biUserID = 1
		,@bRevokeStatus = 0

	exec [svc].[uspUserChangeStatus]
		@biUserID
		,@bRevokeStatus
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		
select * from tbluser where userId = 1
*/

CREATE PROCEDURE [svc].[uspUserChangeStatus]
	@biUserID [bigint]
	,@bRevokeStatus [bit] = 1
	,@biUpdUser [bigint] = null
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserChangeStatus')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

declare @bStoreRevokeStatus [bit],
	@bTmpRevokeStatus [bit]	,
	@dtTmpDateRevoked [datetime]

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bRevokeStatus = coalesce(@bRevokeStatus,0)
	,@bStoreRevokeStatus= 1
	,@biUpdUser = coalesce(@biUpdUser,0)
	
-- Validate parameters
if coalesce(@biUserID,'')=0
-- Parameter is NULL or zero
	set @iRetCode = -2
 begin
	select 
		@bTmpRevokeStatus = u.RevokeStatus,
		@dtTmpDateRevoked = u.DateRevoked,
		@bStoreRevokeStatus = s.RevokeStatus
		from dbo.tblUser u (nolock) 
		left join dbo.tblStore s (nolock) on s.StoreID = u.StoreID
		where UserID = @biUserID
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		set @iRetCode = -3
	 end
	else if coalesce(@bStoreRevokeStatus,0)=1
	 begin
-- user has Revoked status	 
		set @iRetCode = -4
	 end
	else if @bTmpRevokeStatus=@bRevokeStatus
	 begin
-- user has requested status	 
		select @iRetCode = -5,
			@szErrSubst='RevokeStatus='+CAST(@bRevokeStatus as [varchar] (10))			
	 end
	else
	 begin
-- update User

		-- do we have outer transaction?
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspUserChangeStatus;
		else
		  BEGIN TRANSACTION;

		update dbo.tblUser set [RevokeStatus] = @bRevokeStatus,
			DateRevoked = case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			DateUpdated = GETDATE()
			where UserID = @biUserID

		insert into dbo.tblStatusLog (SourceTable,
			SourceTableID,
			OldStatus,
			OldDateRevoked,
			NewStatus,
			NewDateRevoked,
			DateCreated,
			UserCreated)
		values ('tblUser',
			@biUserID,
			@bTmpRevokeStatus,
			@dtTmpDateRevoked,
			@bRevokeStatus,
			case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			GETDATE(),
			@biUpdUser)
			
		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;
		  set @TranCounter = NULL
		 end 
	 end 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspUserChangeStatus;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspUserChangePassword]    Script Date: 08/17/2012 21:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Reset user's password on new or generated randomly value
	 and send email, if password was generated
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/08/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @biUserID is NULL or zero
	@iRetCode = -3, Some of Password parameters is NULL or empty
	@iRetCode = -4, User not found
	@iRetCode = -5, User has revoke status
	@iRetCode = -6, Store has revoke status
	@iRetCode = -7, parameter @szPasswordOld does not match
	@iRetCode = -8, Email template not found
	@iRetCode = -9, error in [dbo].[uspSendEmail]	
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/08/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------
select * from tblUser
	declare
		@biUserID [bigint]
		,@szPasswordOld [nvarchar] (128)
		,@szPassword [nvarchar] (128)
		,@szSecureQuestion [nvarchar] (512)
		,@szSecureAnswer [nvarchar] (512)
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biUserID = 1
		,@szPasswordOld = 'test'
		,@szPassword = 'test1'		
		,@szSecureQuestion ='test'
		,@szSecureAnswer ='test'

	exec [svc].[uspUserChangePassword]
		@biUserID
		,@szPasswordOld
		,@szPassword
		,@szSecureQuestion
		,@szSecureAnswer
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  


	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspUserChangePassword]
	@biUserID [bigint]
	,@szPasswordOld [nvarchar] (128) = NULL
	,@szPassword [nvarchar] (128) = NULL
	,@szSecureQuestion [nvarchar] (512) = NULL
	,@szSecureAnswer [nvarchar] (512) = NULL		
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserChangePassword')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

declare @bRevokeStatus [bit],
	@szTmpPassword [nvarchar] (128),
	@biTmpUserID [bigint],
	@szTmpSecureQuestion [nvarchar] (512),
	@bStoreRevokeStatus [bit],
	@bResetPassword [bit],
	@szUserEmail [nvarchar] (512)	

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bRevokeStatus = 1
	,@szTmpPassword = ''
	,@biTmpUserID = 0
--	,@szSecureQuestion = ''
	,@bStoreRevokeStatus= 1

-- Validate parameters
if coalesce(@biUserID,'')=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else if coalesce(@szPassword,'')<>'' and (coalesce(@szPasswordOld,'')='' or coalesce(@szSecureQuestion,'')='' or coalesce(@szSecureAnswer,'')='')
 begin
	set @iRetCode = -3
  end
else 	
 begin
	select 
		@bRevokeStatus = u.RevokeStatus,
		@szTmpPassword = u.[Password],
		@szTmpSecureQuestion = u.SecureQuestion,
		@bStoreRevokeStatus = s.RevokeStatus,
		@bResetPassword = ResetPassword,
		@szUserEmail = UserEmail
		from dbo.tblUser u (nolock) 
		left join dbo.tblStore s (nolock) on s.StoreID = u.StoreID
		where UserID = @biUserID
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		set @iRetCode = -4
	 end
	else if coalesce(@bRevokeStatus,0)=1
	 begin
-- user has Revoked status	 
		set @iRetCode = -5
	 end
	else if coalesce(@bStoreRevokeStatus,0)=1
	 begin
-- user has Revoked status	 
		set @iRetCode = -6
	 end
	else if (coalesce(@szPasswordOld,'')<>'' and @szTmpPassword <> @szPasswordOld COLLATE SQL_Latin1_General_CP1_CS_AS)
	 begin
-- password does not match	 
		select @iRetCode = -7
	 end
	else
	 begin
-- update password 
		if coalesce(@szPassword,'')=''
		 begin
-- generate password
			select @szPassword = dbo.fnGeneratePassword(8,0)
				,@szSecureQuestion = 'Randomly generated password'
				,@szSecureAnswer = @szPassword
				,@bResetPassword = 1
		 end
		else
		 begin
			set @bResetPassword = 0
		 end 

		-- do we have outer transaction?
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspUserChangePassword;
		else
		  BEGIN TRANSACTION;

		update dbo.tblUser set [Password] = @szPassword,
			SecureQuestion = @szSecureQuestion,
			SecureAnswer = @szSecureAnswer,
			ResetPassword = @bResetPassword,
			DateUpdated = GETDATE()
			where UserID = @biUserID

		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;
		  set @TranCounter = NULL
		 end 

		if @bResetPassword = 1
		 begin
-- send email
			declare @szEmailTemplate [varchar] (max),
				@szSource [varchar](100),
				@szEmailSubject [nvarchar](512),
				@szEmailDetails [nvarchar](max),
				@szRecipientListAddresses [nvarchar](max),
				@szSQLQueryString [nvarchar] (max),
				@iTmpUserID [int],
				@iQResultWidth [int],
				@szEmailFormat [varchar] (10),
				@iTmpRetCode [int],
				@bRecordOnly [bit],
				@szDateTime [varchar] (30)

			declare @szYear [varchar] (4),
				@szDate1 [varchar] (30),
				@szDate2 [varchar] (30),
				@iPos [int]
			
			select @szEmailTemplate = [Template],
				@szEmailSubject = [Subject],
				@szEmailFormat = [EmailFormat] 
				from dbo.tblEmailTemplate (nolock) 
				where TemplateName = 'ChangePassword'
				and RevokeStatus = 0

			if coalesce(@szEmailTemplate,'')=''
			 begin
	-- email template not found
				select @iRetCode = -8		 
			 end
			else
			 begin
					
				select @szYear = CAST(year(getDate()) as [varchar] (4)),
					@szDate1 = convert(varchar, getdate(), 100),
					@iPos = charindex(@szYear,@szDate1)+5,
					@szDate2 = CONVERT(varchar,getdate(),101)+' at '+
					ltrim(SUBSTRING(@szDate1,@iPos,len(@szDate1)-@iPos+1))		 
					
				select @szEmailTemplate = replace(replace(@szEmailTemplate,'[pwd]',@szTmpPassword),'[dt]',@szDate2)

				select @szEmailDetails = @szEmailTemplate,
					@szSource = @szProcessName,
					@szRecipientListAddresses = @szUserEmail

				exec [dbo].[uspSendEmail]
					@szSource,
					@szEmailSubject,
					@szEmailDetails,
					@szRecipientListAddresses,
					@szSQLQueryString,
					@iTmpUserID,
					@iQResultWidth,
					@szEmailFormat,
					@iTmpRetCode output
				
				if @iTmpRetCode <> 0
				 begin
					select @iRetCode = -9
				 end
			 end	
		 end
	 end 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspUserChangePassword;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biTmpUserID

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspValidateSecureAnswer]    Script Date: 08/17/2012 21:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Validate user credentials
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/06/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @szUserEmail is NULL or empty
	@iRetCode = -3, parameter @szSecureAnswer is NULL or empty
	@iRetCode = -4, User not found
	@iRetCode = -5, User has revoke status
	@iRetCode = -6, Store has revoke status
	@iRetCode = -7, parameter @szSecureAnswer does not match with secure answer
	@iRetCode = -8, Email template not found
	@iRetCode = -9, error in [dbo].[uspSendEmail]
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/06/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------
	declare
		@szUserEmail [nvarchar] (512)
		,@szPassword [nvarchar] (128)
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)
		,@biUserID [bigint]
		,@szSecureQuestion [nvarchar] (512)

	select
		@szUserEmail = 'admin@ppj.com'
		,@szPassword = 'AdminPPJ'

	exec [svc].[uspValidateLogin]
		@szUserEmail
		,@szPassword
		,@iRetCode output 
		,@szMsgTier1 output   
		,@szMsgTier2 output  
		,@biUserID output 
		,@szSecureQuestion output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   
		,@biUserID as [UserID] 
		,@szSecureQuestion as [SecureQuestion]  
*/

CREATE PROCEDURE [svc].[uspValidateSecureAnswer]
	@szUserEmail [nvarchar] (512)
	,@szSecureAnswer [nvarchar] (512)
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspValidateSecureAnswer')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

declare @bRevokeStatus [bit],
	@szTmpPassword [nvarchar] (128),
	@biTmpUserID [bigint],
	@szTmpSecureAnswer [nvarchar] (512),
	@bStoreRevokeStatus [bit]

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
--	,@biUserID = 0
	,@bRevokeStatus = 1
	,@szTmpPassword = ''
	,@biTmpUserID = 0
--	,@szSecureQuestion = ''
	,@bStoreRevokeStatus= 1

-- Validate parameters
if coalesce(@szUserEmail,'')=''
-- Parameter is NULL or empty
	set @iRetCode = -2
else if coalesce(@szSecureAnswer,'')=''
	set @iRetCode = -3
else
 begin
	select @biTmpUserID = u.UserID, 
		@bRevokeStatus = u.RevokeStatus,
		@szTmpPassword = u.[Password],
		@szTmpSecureAnswer = u.SecureAnswer,
		@bStoreRevokeStatus = s.RevokeStatus
		from dbo.tblUser u (nolock) 
		left join dbo.tblStore s (nolock) on s.StoreID = u.StoreID
		where UserEmail = @szUserEmail
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		set @iRetCode = -4
	 end
	else if coalesce(@bRevokeStatus,0)=1
	 begin
-- user has Revoked status	 
		set @iRetCode = -5
	 end
	else if coalesce(@bStoreRevokeStatus,0)=1
	 begin
-- user has Revoked status	 
		set @iRetCode = -6
	 end
	else if @szTmpSecureAnswer <> @szSecureAnswer -- COLLATE SQL_Latin1_General_CP1_CS_AS
	 begin
-- answer does not match	 
		select @iRetCode = -7
	 end
	else
	 begin
-- answer match, send email with existed password				
		declare @szEmailTemplate [varchar] (max),
			@szSource [varchar](100),
			@szEmailSubject [nvarchar](512),
			@szEmailDetails [nvarchar](max),
			@szRecipientListAddresses [nvarchar](max),
			@szSQLQueryString [nvarchar] (max),
			@iTmpUserID [int],
			@iQResultWidth [int],
			@szEmailFormat [varchar] (10),
			@iTmpRetCode [int],
			@bRecordOnly [bit],
			@szDateTime [varchar] (30)

		declare @szYear [varchar] (4),
			@szDate1 [varchar] (30),
			@szDate2 [varchar] (30),
			@iPos [int]
		
		select @szEmailTemplate = [Template],
			@szEmailSubject = [Subject],
			@szEmailFormat = [EmailFormat] 
			from dbo.tblEmailTemplate (nolock) 
			where TemplateName = 'ChangePassword'
			and RevokeStatus = 0

		if coalesce(@szEmailTemplate,'')=''
		 begin
-- email template not found
			select @iRetCode = -8		 
		 end
		else
		 begin
				
			select @szYear = CAST(year(getDate()) as [varchar] (4)),
				@szDate1 = convert(varchar, getdate(), 100),
				@iPos = charindex(@szYear,@szDate1)+5,
				@szDate2 = CONVERT(varchar,getdate(),101)+' at '+
				ltrim(SUBSTRING(@szDate1,@iPos,len(@szDate1)-@iPos+1))		 
				
			select @szEmailTemplate = replace(replace(@szEmailTemplate,'[pwd]',@szTmpPassword),'[dt]',@szDate2)

			select @szEmailDetails = @szEmailTemplate,
				@szSource = @szProcessName,
				@szRecipientListAddresses = @szUserEmail

			exec [dbo].[uspSendEmail]
				@szSource,
				@szEmailSubject,
				@szEmailDetails,
				@szRecipientListAddresses,
				@szSQLQueryString,
				@iTmpUserID,
				@iQResultWidth,
				@szEmailFormat,
				@iTmpRetCode output
			
			if @iTmpRetCode <> 0
			 begin
				select @iRetCode = -9
			 end
		 end	
	 end 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspValidateLogin;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biTmpUserID

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  StoredProcedure [svc].[uspUserInsert]    Script Date: 08/17/2012 21:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Insert dbo.tblUser record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/12/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreID is null or zero
	@iRetCode = -3, Parameter @szFirstName is null or empty
	@iRetCode = -4, Parameter @szLastName is null or empty
	@iRetCode = -5, Parameter @szUserEmail is null or empty
	@iRetCode = -6, Store not found
	@iRetCode = -7, Store has "revoke" status
	@iRetCode = -8, New user's email already in use
	@iRetCode = -9, Error in svc.uspUserChangePassword SP

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/12/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@biUserID [bigint]	
		,@biStoreID [bigint]
		,@szFirstName [nvarchar] (128)
		,@szLastName [nvarchar] (128)
		,@szUserEmail [nvarchar] (512)
		,@bEmailAlertRecipient [bit]
		,@bBillingReportRecipient [bit]
		,@biUpdUser [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biStoreID = 1
		,@szFirstName = 'Sergey'
		,@szLastName = 'Morozov'
		,@szUserEmail = 'smorozov@@ppj.com'

	exec [svc].[uspUserInsert]
		@biStoreID
		,@szFirstName
		,@szLastName
		,@szUserEmail
		,@bEmailAlertRecipient
		,@bBillingReportRecipient
		,@biUpdUser
		,@biUserID output
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

select * from tblUser

*/

CREATE PROCEDURE [svc].[uspUserInsert]
	@biStoreID [bigint]
	,@szFirstName [nvarchar] (128)
	,@szLastName [nvarchar] (128)
	,@szUserEmail [nvarchar] (512)
	,@bEmailAlertRecipient [bit] = 0
	,@bBillingReportRecipient [bit] = 0
	,@biUpdUser [bigint] = null
   	,@biUserID [bigint]	output
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserInsert')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bEmailAlertRecipient = coalesce(@bEmailAlertRecipient,0)
	,@bBillingReportRecipient = coalesce(@bBillingReportRecipient,0)
	,@biUpdUser = coalesce(@biUpdUser,0)

declare @bTmpRevokeStatus [bit]
	,@szTmpUserEmail [nvarchar] (512)
	,@szTmpAnswer [nvarchar] (512)

-- Validate parameters
if	coalesce(@biStoreID,0)=0
	set @iRetCode = -2
else if	coalesce(@szFirstName,'')=''
	set @iRetCode = -3
else if	coalesce(@szLastName,'')=''
	set @iRetCode = -4
else if	coalesce(@szUserEmail,'')=''
	set @iRetCode = -5
else if	exists (select 1 from dbo.tblUser (nolock) where UserEmail = @szUserEmail)
	set @iRetCode = -8
else
 begin
 
-- validate Store parameters	
	select @bTmpRevokeStatus = RevokeStatus 
		from dbo.tblStore s (nolock)
		where StoreID = @biStoreID
		
	if @@ROWCOUNT = 0	
	 begin
-- store not found
		select @iRetCode = -6,
			@szErrSubst = CAST(@biStoreID as [varchar] (10))
	 end
	else if coalesce(@bTmpRevokeStatus,0)=1
	 begin
-- store has "revoke" status
		select @iRetCode = -7,
			@szErrSubst = CAST(@biStoreID as [varchar] (10))
	 end
 end

if @iRetCode = 0
 begin
 
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspUserInsert;
		else
		  BEGIN TRANSACTION;

		insert into dbo.tblUser (
			StoreID,
			FirstName,
			LastName,
			UserEmail,
			EmailAlertRecipient,
			BillingReportRecipient,
			DateCreated,
			UserCreated,
			[Password],
			SecureQuestion,
			SecureAnswer)
		values (	
			@biStoreID,
			@szFirstName,
			@szLastName,
			@szUserEmail,
			@bEmailAlertRecipient,
			@bBillingReportRecipient,
			GETDATE(),
			@biUpdUser,
			'','','')

		set @biUserID = SCOPE_IDENTITY()
		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;		
		  set @TranCounter = NULL
		 end 

		-- generate password and set email to user
		declare @iTmpRetCode [int]
			,@szTmpMsgTier1 [nvarchar] (2048)
			,@szTmpMsgTier2 [nvarchar] (1024)
		
		exec [svc].[uspUserChangePassword] 
				@biUserID
				,NULL
				,NULL
				,NULL
				,NULL		
				,@iTmpRetCode output
				,@szTmpMsgTier1 output   
				,@szTmpMsgTier2 output 

		if @iTmpRetCode <> 0
		 begin
			set @iRetCode = -9
		 end
		else
		 begin
		 -- insert data into tblUserMenu and return record set
			if not exists (select 1 from dbo.tblUserMenu (nolock) where UserID = @biUserID)
			insert into dbo.tblUserMenu (
				UserID
				,AppMenuID
				,[Status]
				,[DateCreated]
				,[UserCreated]
				,[RevokeStatus]
				,[DateRevoked])
			select @biUserID
				,AppMenuID
				,DefaultStatus
				,GETDATE()
				,@biUpdUser
				,[RevokeStatus]
				,[DateRevoked]	
			from dbo.tblAppMenu	(nolock)
				order by AppMenuID
							
		-- return user's menu set for edit on UI
			;with t0 as (
			select um.UserMenuID,
				 am.MenuName,
				 am.ParentID,
				 um.[Status],
				 vl.LookUpShortName
				from dbo.tblUserMenu um (nolock) 
				inner join dbo.tblAppMenu am (nolock) on um.AppMenuID = am.AppMenuID
				inner join dbo.vwLookUp vl (nolock) on vl.LookUpName = 'MenuStatus'
					and vl.LookUpSubType = um.[Status]
				where um.UserID = @biUserID
				and um.RevokeStatus = 0
				and am.RevokeStatus = 0
--				and vl.LookUpShortName <> 'Invisible'
			)
			select UserMenuID,
				MenuName,
				LookUpShortName as UserRights,
				ParentID,
				[Status]
			from t0 order by UserMenuID
			
		 end
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspUserInsert;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO
/****** Object:  Default [DF__tblSessio__Order__2C3393D0]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblSessionList] ADD  DEFAULT ((0)) FOR [OrderSubmitFlag]
GO
/****** Object:  Default [DF__tblSessio__DateC__2A4B4B5E]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblSessionList] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblSessio__UserC__2B3F6F97]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblSessionList] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF_tblMsgOutput_DateCreated]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblMsgOutput] ADD  CONSTRAINT [DF_tblMsgOutput_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblMsgOutput_UserCreated]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblMsgOutput] ADD  CONSTRAINT [DF_tblMsgOutput_UserCreated]  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblLookUp__LookU__25869641]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblLookUp] ADD  DEFAULT ((0)) FOR [LookUpSubID]
GO
/****** Object:  Default [DF__tblLookUp__DateC__267ABA7A]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblLookUp] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblLookUp__UserC__276EDEB3]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblLookUp] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblEmailT__Email__7F2BE32F]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblEmailTemplate] ADD  DEFAULT ('text') FOR [EmailFormat]
GO
/****** Object:  Default [DF__tblEmailT__Revok__00200768]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblEmailTemplate] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblEmailT__DateC__01142BA1]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblEmailTemplate] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblEmailT__UserC__02084FDA]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblEmailTemplate] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblEmailL__DateC__6754599E]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblEmailLog] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblEmailL__UserC__68487DD7]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblEmailLog] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblStore__StoreG__1CF15040]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblStore] ADD  DEFAULT (newid()) FOR [StoreGUID]
GO
/****** Object:  Default [DF__tblStore__Revoke__1DE57479]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblStore] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblStore__Sessio__1ED998B2]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblStore] ADD  DEFAULT ((43200)) FOR [SessionExpiration]
GO
/****** Object:  Default [DF__tblStore__DateCr__1FCDBCEB]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblStore] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblStore__UserCr__20C1E124]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblStore] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblStatus__OldSt__3C34F16F]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblStatusLog] ADD  DEFAULT ((0)) FOR [OldStatus]
GO
/****** Object:  Default [DF__tblStatus__NewSt__3D2915A8]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblStatusLog] ADD  DEFAULT ((0)) FOR [NewStatus]
GO
/****** Object:  Default [DF__tblStatus__DateC__3E1D39E1]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblStatusLog] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblStatus__UserC__3F115E1A]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblStatusLog] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF_tblSQLAudit_DateCreated]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblSQLAudit] ADD  CONSTRAINT [DF_tblSQLAudit_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblSQLAudit_UserCreated]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblSQLAudit] ADD  CONSTRAINT [DF_tblSQLAudit_UserCreated]  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF_tblUIMessage_DateCreated]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblUIMessage] ADD  CONSTRAINT [DF_tblUIMessage_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblUIMessage_UserCreated]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblUIMessage] ADD  CONSTRAINT [DF_tblUIMessage_UserCreated]  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF_tblWEBAudit_DateCreated]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblWEBAudit] ADD  CONSTRAINT [DF_tblWEBAudit_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF_tblWEBAudit_UserCreated]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblWEBAudit] ADD  CONSTRAINT [DF_tblWEBAudit_UserCreated]  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblAppMen__Paren__6D0D32F4]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblAppMenu] ADD  DEFAULT ((0)) FOR [ParentID]
GO
/****** Object:  Default [DF__tblAppMen__Revok__6E01572D]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblAppMenu] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblAppMen__Defau__6EF57B66]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblAppMenu] ADD  DEFAULT ((1)) FOR [DefaultStatus]
GO
/****** Object:  Default [DF__tblAppMen__DateC__6FE99F9F]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblAppMenu] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblAppMen__UserC__70DDC3D8]    Script Date: 08/17/2012 21:14:37 ******/
ALTER TABLE [dbo].[tblAppMenu] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblBillin__Billi__403A8C7D]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT ((0)) FOR [BillingUnit]
GO
/****** Object:  Default [DF__tblBillin__Billi__412EB0B6]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT ((1)) FOR [BillingRate]
GO
/****** Object:  Default [DF__tblBillin__Bille__4222D4EF]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT (getdate()) FOR [BilledDate]
GO
/****** Object:  Default [DF__tblBilling__Mode__4316F928]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT ((0)) FOR [Mode]
GO
/****** Object:  Default [DF__tblBillin__DateC__440B1D61]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblBillin__UserC__44FF419A]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBilling] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblBSetti__Billi__49C3F6B7]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBSetting] ADD  DEFAULT ((0)) FOR [BillingUnit]
GO
/****** Object:  Default [DF__tblBSetti__Billi__4AB81AF0]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBSetting] ADD  DEFAULT ((1)) FOR [BillingRate]
GO
/****** Object:  Default [DF__tblBSetti__LastB__4BAC3F29]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBSetting] ADD  DEFAULT (getdate()) FOR [LastBilledDate]
GO
/****** Object:  Default [DF__tblBSetti__DateC__4CA06362]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBSetting] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblBSetti__UserC__4D94879B]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBSetting] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblWSLog__OrderT__2F10007B]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ((0)) FOR [OrderTotal]
GO
/****** Object:  Default [DF__tblWSLog__Activa__300424B4]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ('N') FOR [ActivateTimer]
GO
/****** Object:  Default [DF__tblWSLog__TimerV__30F848ED]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ((0)) FOR [TimerValue]
GO
/****** Object:  Default [DF__tblWSLog__PopUpF__31EC6D26]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ((0)) FOR [PopUpFlag]
GO
/****** Object:  Default [DF__tblWSLog__OrderS__32E0915F]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ((0)) FOR [OrderSubmitFlag]
GO
/****** Object:  Default [DF__tblWSLog__DateCr__33D4B598]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblWSLog__UserCr__34C8D9D1]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblWSLog] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblUserMe__Statu__1AD3FDA4]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUserMenu] ADD  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF__tblUserMe__Revok__19DFD96B]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUserMenu] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblUserMe__DateC__1BC821DD]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUserMenu] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblUserMe__UserC__1CBC4616]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUserMenu] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblUser__EmailAl__35BCFE0A]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT ((0)) FOR [EmailAlertRecipient]
GO
/****** Object:  Default [DF__tblUser__Billing__36B12243]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT ((0)) FOR [BillingReportRecipient]
GO
/****** Object:  Default [DF__tblUser__ResetPa__37A5467C]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT ((0)) FOR [ResetPassword]
GO
/****** Object:  Default [DF__tblUser__RevokeS__38996AB5]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblUser__DateCre__398D8EEE]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblUser__UserCre__3A81B327]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUser] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblStoreI__Timer__3B75D760]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblStoreIPList] ADD  DEFAULT ((300)) FOR [TimerValue1]
GO
/****** Object:  Default [DF__tblStoreI__Timer__3C69FB99]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblStoreIPList] ADD  DEFAULT ((300)) FOR [TimerValue2]
GO
/****** Object:  Default [DF__tblStoreI__Revok__3D5E1FD2]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblStoreIPList] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblStoreI__DateC__3E52440B]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblStoreIPList] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblStoreI__UserC__3F466844]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblStoreIPList] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblShoppi__DateC__5070F446]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblShoppingList] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblShoppi__UserC__5165187F]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblShoppingList] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblCoupon__Limit__45F365D3]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblCoupon] ADD  DEFAULT ((0)) FOR [LimitedNumber]
GO
/****** Object:  Default [DF__tblCoupon__Revok__46E78A0C]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblCoupon] ADD  DEFAULT ((0)) FOR [RevokeStatus]
GO
/****** Object:  Default [DF__tblCoupon__DateC__47DBAE45]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblCoupon] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblCoupon__UserC__48CFD27E]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblCoupon] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  Default [DF__tblBillin__DateC__4E88ABD4]    Script Date: 08/17/2012 21:14:43 ******/
ALTER TABLE [dbo].[tblBillingEmailLog] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO
/****** Object:  Default [DF__tblBillin__UserC__4F7CD00D]    Script Date: 08/17/2012 21:14:43 ******/
ALTER TABLE [dbo].[tblBillingEmailLog] ADD  DEFAULT ((0)) FOR [UserCreated]
GO
/****** Object:  ForeignKey [FK_tblBilling_StoreID]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBilling]  WITH CHECK ADD  CONSTRAINT [FK_tblBilling_StoreID] FOREIGN KEY([StoreID])
REFERENCES [dbo].[tblStore] ([StoreID])
GO
ALTER TABLE [dbo].[tblBilling] CHECK CONSTRAINT [FK_tblBilling_StoreID]
GO
/****** Object:  ForeignKey [FK_tblBSetting_StoreID]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblBSetting]  WITH CHECK ADD  CONSTRAINT [FK_tblBSetting_StoreID] FOREIGN KEY([StoreID])
REFERENCES [dbo].[tblStore] ([StoreID])
GO
ALTER TABLE [dbo].[tblBSetting] CHECK CONSTRAINT [FK_tblBSetting_StoreID]
GO
/****** Object:  ForeignKey [FK_tblWSLog_StoreID]    Script Date: 08/17/2012 21:14:38 ******/
ALTER TABLE [dbo].[tblWSLog]  WITH CHECK ADD  CONSTRAINT [FK_tblWSLog_StoreID] FOREIGN KEY([StoreID])
REFERENCES [dbo].[tblStore] ([StoreID])
GO
ALTER TABLE [dbo].[tblWSLog] CHECK CONSTRAINT [FK_tblWSLog_StoreID]
GO
/****** Object:  ForeignKey [FK_tblUserMenu_AppMenuID]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUserMenu]  WITH CHECK ADD  CONSTRAINT [FK_tblUserMenu_AppMenuID] FOREIGN KEY([AppMenuID])
REFERENCES [dbo].[tblAppMenu] ([AppMenuID])
GO
ALTER TABLE [dbo].[tblUserMenu] CHECK CONSTRAINT [FK_tblUserMenu_AppMenuID]
GO
/****** Object:  ForeignKey [FK_tblUser_StoreID]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblUser]  WITH CHECK ADD  CONSTRAINT [FK_tblUser_StoreID] FOREIGN KEY([StoreID])
REFERENCES [dbo].[tblStore] ([StoreID])
GO
ALTER TABLE [dbo].[tblUser] CHECK CONSTRAINT [FK_tblUser_StoreID]
GO
/****** Object:  ForeignKey [FK_tblStoreIPList_StoreID]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblStoreIPList]  WITH CHECK ADD  CONSTRAINT [FK_tblStoreIPList_StoreID] FOREIGN KEY([StoreID])
REFERENCES [dbo].[tblStore] ([StoreID])
GO
ALTER TABLE [dbo].[tblStoreIPList] CHECK CONSTRAINT [FK_tblStoreIPList_StoreID]
GO
/****** Object:  ForeignKey [FK_tblShoppingList_WSLogID]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblShoppingList]  WITH CHECK ADD  CONSTRAINT [FK_tblShoppingList_WSLogID] FOREIGN KEY([WSLogID])
REFERENCES [dbo].[tblWSLog] ([WSLogID])
GO
ALTER TABLE [dbo].[tblShoppingList] CHECK CONSTRAINT [FK_tblShoppingList_WSLogID]
GO
/****** Object:  ForeignKey [FK_tblCoupon_StoreIPListID]    Script Date: 08/17/2012 21:14:42 ******/
ALTER TABLE [dbo].[tblCoupon]  WITH CHECK ADD  CONSTRAINT [FK_tblCoupon_StoreIPListID] FOREIGN KEY([StoreIPListID])
REFERENCES [dbo].[tblStoreIPList] ([StoreIPListID])
GO
ALTER TABLE [dbo].[tblCoupon] CHECK CONSTRAINT [FK_tblCoupon_StoreIPListID]
GO
/****** Object:  ForeignKey [FK_tblBillingEmailLog_BillingID]    Script Date: 08/17/2012 21:14:43 ******/
ALTER TABLE [dbo].[tblBillingEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_tblBillingEmailLog_BillingID] FOREIGN KEY([BillingID])
REFERENCES [dbo].[tblBilling] ([BillingID])
GO
ALTER TABLE [dbo].[tblBillingEmailLog] CHECK CONSTRAINT [FK_tblBillingEmailLog_BillingID]
GO
/****** Object:  ForeignKey [FK_tblBillingEmailLog_UserID]    Script Date: 08/17/2012 21:14:43 ******/
ALTER TABLE [dbo].[tblBillingEmailLog]  WITH CHECK ADD  CONSTRAINT [FK_tblBillingEmailLog_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[tblUser] ([UserID])
GO
ALTER TABLE [dbo].[tblBillingEmailLog] CHECK CONSTRAINT [FK_tblBillingEmailLog_UserID]
GO


use master;
go

CREATE PROCEDURE dbo.uspSetSQLServerAuthenticationMode
(
       @MixedMode BIT
)
AS
 
SET NOCOUNT ON
 
DECLARE @InstanceName NVARCHAR(1000),
       @Key NVARCHAR(4000),
       @NewLoginMode INT,
       @OldLoginMode INT
 
EXEC master..xp_regread    N'HKEY_LOCAL_MACHINE',
                     N'Software\Microsoft\Microsoft SQL Server\Instance Names\SQL\',
                     N'MSSQLSERVER',
                     @InstanceName OUTPUT
 
IF @@ERROR <> 0 OR @InstanceName IS NULL
       BEGIN
              RAISERROR('Could not read SQL Server instance name.', 18, 1)
              RETURN -100
       END
 
SET    @Key = N'Software\Microsoft\Microsoft SQL Server\' + @InstanceName + N'\MSSQLServer\'
 
EXEC master..xp_regread    N'HKEY_LOCAL_MACHINE',
                     @Key,
                     N'LoginMode',
                     @OldLoginMode OUTPUT

IF @@ERROR <> 0
       BEGIN
              RAISERROR('Could not read login mode for SQL Server instance %s.', 18, 1, @InstanceName)
              RETURN -110
       END
 
IF @MixedMode IS NULL
       BEGIN
              RAISERROR('No change to authentication mode was made. Login mode is %d.', 10, 1, @OldLoginMode)
              RETURN -120
       END
 
IF @MixedMode = 1
       SET    @NewLoginMode = 2
ELSE
       SET    @NewLoginMode = 1
 
EXEC master..xp_regwrite   N'HKEY_LOCAL_MACHINE',
                           @Key,
                           N'LoginMode',
                           'REG_DWORD',
                           @NewLoginMode
 
IF @@ERROR <> 0
       BEGIN
              RAISERROR('Could not write login mode %d for SQL Server instance %s. Login mode is %d', 18, 1, @NewLoginMode, @InstanceName, @OldLoginMode)
              RETURN -130
       END
 
RAISERROR('Login mode is now %d for SQL Server instance %s. Login mode was %d before.', 10, 1, @NewLoginMode, @InstanceName, @OldLoginMode)
RETURN 0
Go

exec dbo.uspSetSQLServerAuthenticationMode 1
go

declare @szPassword nvarchar(32)
, @szLogin nvarchar(32)
, @szSQL nvarchar(1024)

set @szLogin = 'PopUpJoeWEBUser'
--set @szLogin = UPPER(@szLogin)
set @szPassword = 'p0PuPj0EwebuSER'

USE [master]

IF NOT EXISTS (SELECT 1 FROM master.sys.server_principals WHERE name = @szLogin)
begin
set @szSQL= 'CREATE LOGIN ' + @szLogin + ' WITH PASSWORD=N''' + @szPassword+''', DEFAULT_DATABASE=[ppjdb], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF'
EXECUTE sp_executesql @szSQL
end

USE [ppjdb]

IF NOT EXISTS (SELECT 1 FROM sys.database_principals WHERE name = @szLogin)
begin
set @szSQL= 'CREATE USER ' + @szLogin + ' FOR LOGIN ' + @szLogin
exec sp_executesql @szSQL
exec sp_addrolemember N'db_datareader', @szLogin
exec sp_addrolemember N'db_datawriter', @szLogin
exec sp_addrolemember N'db_executor', @szLogin
end
else
begin
exec sp_change_users_login 'Update_One', @szLogin, @szLogin
if not exists(SELECT 1 FROM sys.sysmembers (nolock)
	WHERE USER_NAME(groupuid) IN (N'db_datareader') and USER_NAME(memberuid) = @szLogin)
exec sp_addrolemember N'db_datareader', @szLogin
if not exists(SELECT 1 FROM sys.sysmembers (nolock)
	WHERE USER_NAME(groupuid) IN (N'db_datawriter') and USER_NAME(memberuid) = @szLogin)
exec sp_addrolemember N'db_datawriter', @szLogin
if not exists(SELECT 1 FROM sys.sysmembers (nolock)
	WHERE USER_NAME(groupuid) IN (N'db_executor') and USER_NAME(memberuid) = @szLogin)
exec sp_addrolemember N'db_executor', @szLogin
end

end

print 'Login: ' + @szLogin
print 'Password: ' + @szPassword

GO

