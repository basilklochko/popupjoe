
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspGetCouponList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspGetCouponList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Returns list of records from tblCoupon table base 
	on value of @biStoreID parameter
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/20/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreIPListID is NULL or zero
	@iRetCode = -3, No record found for @biStoreIPListIDStore {0}
	@iRetCode = -4, Store {0} has "revoked" status
	@iRetCode = -5, Store domain {0} has "revoked" status

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/20/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare @biStoreIPListID [bigint]
		,@iPageIndex [int]
		,@iPageSize [int]    
		,@szSortColumn [varchar] (50)
		,@szSortDirection [varchar] (50)
		,@biResultCount [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biStoreIPListID = 1
		,@iPageIndex = 1
		,@iPageSize = 10
		,@szSortColumn = 'CouponDescription'
		,@szSortDirection = 'ASC'

	exec [svc].[uspGetCouponList]
		@biStoreIPListID
		,@iPageIndex
		,@iPageSize
		,@szSortColumn
		,@szSortDirection
		,@biResultCount output
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspGetCouponList]
	@biStoreIPListID [bigint]
	,@iPageIndex [int]
	,@iPageSize [int]    
	,@szSortColumn [varchar] (50) = null
	,@szSortDirection [varchar] (50) = null
	,@biResultCount [bigint] output
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output 
    ,@bHideRevoked [bit] = 0  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspGetCouponList')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bHideRevoked = coalesce(@bHideRevoked,0)

declare @tbl table (CouponID [bigint] not null,
	StoreIPListID [bigint] not null,
	StoreID [bigint] not null,
	eCommCouponID [nvarchar] (64) not null,
	CouponDescription [nvarchar] (512) null,
	CouponValue [decimal] (8,3) null,
	MinOrderAmt [decimal] (7,2) not null,
	MaxOrderAmt [decimal] (7,2) not null,
	CouponType [int] not null,
	CouponTypeName [varchar] (64) not null,
	MsgToPopUp [nvarchar] (max) not null,
	ExpirationDate [datetime] null,
	LimitedNumber [bit] not null,
	Uses [int] null,
	RevokeStatus [bit] not null,
	DateRevoked [datetime] null,
	DateCreated [datetime] null,
	DateUpdated [datetime] null,
	SortColumn [nvarchar] (512) null,
	[RowNum] [bigint] not null primary key)	

declare @bTmpStoreStatus [bit]
	,@bTmpStoreIPStatus [bit]
	,@biStoreID [bigint]

-- Validate parameters
if coalesce(@biStoreIPListID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else 	
 begin
	select @bTmpStoreStatus = s.RevokeStatus 
		,@bTmpStoreIPStatus = sipl.RevokeStatus
		,@biStoreID = s.StoreID
		from dbo.tblStore s (nolock) 
		inner join dbo.tblStoreIPList sipl (nolock) on sipl.StoreID = s.StoreID
		where sipl.StoreIPListID = @biStoreIPListID
	
	if @@ROWCOUNT = 0 
		select @iRetCode = -3
			,@szErrSubst = CAST(@biStoreIPListID as [varchar] (30))
	else if coalesce(@bTmpStoreStatus,0) = 1
		select @iRetCode = -4
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	else if coalesce(@bTmpStoreIPStatus,0) = 1
		select @iRetCode = -5
			,@szErrSubst = CAST(@biStoreIPListID as [varchar] (30))
			
 end

if @iRetCode = 0
 begin

	insert into @tbl (
		CouponID,
		StoreIPListID,
		StoreID,
		eCommCouponID,
		CouponDescription,
		CouponValue,
		MinOrderAmt,
		MaxOrderAmt,
		CouponType,
		CouponTypeName,
		MsgToPopUp,
		ExpirationDate,
		LimitedNumber,
		Uses,
		RevokeStatus,
		DateRevoked,
		DateCreated,
		DateUpdated,
		[RowNum])
	select 
		c.CouponID,
		c.StoreIPListID,
		sipl.StoreID,
		c.eCommCouponID,
		c.CouponDescription,
		c.CouponValue,
		c.MinOrderAmt,
		c.MaxOrderAmt,
		c.CouponType,
		vl.LookUpShortName,
		c.MsgToPopUp,
		c.ExpirationDate,
		c.LimitedNumber,
		c.Uses,
		c.RevokeStatus,
		c.DateRevoked,
		c.DateCreated,
		c.DateUpdated,
		c.CouponID as RowNum
		from dbo.tblCoupon  c (nolock) 
		inner join dbo.tblStoreIPList sipl (nolock) 
			on c.StoreIPListID = sipl.StoreIPListID
			and sipl.StoreIPListID = @biStoreIPListID
		inner join dbo.vwLookUp vl (nolock) 
			on vl.LookUpSubType = c.CouponType
			and vl.LookUpName = 'CouponType'
		order by c.CouponID
		
--	select @biResultCount = @@ROWCOUNT

	if @bHideRevoked = 1
	 begin
		delete from @tbl where RevokeStatus = 1
	 end

	select @biResultCount = count(*) from @tbl

	if coalesce(@szSortColumn,'')<>'' and coalesce(@szSortDirection,'')<>''
	 begin
		update @tbl set [SortColumn] =
		case 
			when @szSortColumn = 'eCommCouponID' then eCommCouponID
			when @szSortColumn = 'CouponDescription' then CouponDescription
			when @szSortColumn = 'MsgToPopUp' then left(MsgToPopUp,512)
			when @szSortColumn = 'CouponTypeName' then CouponTypeName
			when @szSortColumn = 'CouponValue' then cast([CouponValue] as [varchar] (10))
			when @szSortColumn = 'MinOrderAmt' then cast([MinOrderAmt] as [varchar] (10))
			when @szSortColumn = 'MaxOrderAmt' then cast([MaxOrderAmt] as [varchar] (10))
			when @szSortColumn = 'CouponType' then cast([CouponType] as [varchar] (10))
			when @szSortColumn = 'LimitedNumber' then cast([LimitedNumber] as [varchar] (10))
			when @szSortColumn = 'Uses' then cast([Uses] as [varchar] (10))
			when @szSortColumn = 'ExpirationDate' then convert([varchar] (30), [ExpirationDate],120)
			when @szSortColumn = 'DateCreated' then convert([varchar] (30), [DateCreated],120)
			when @szSortColumn = 'DateUpdated' then convert([varchar] (30), [DateUpdated],120)			
			when @szSortColumn = 'DateRevoked' then convert([varchar] (30), [DateRevoked],120)			
			when @szSortColumn = 'RevokeStatus' then cast([RevokeStatus] as [varchar] (10))
			else cast(CouponID as [varchar] (30))					
		end

		if @szSortDirection = 'ASC'
		 begin
			;with t0 as (
				select [CouponID],[RowNum],ROW_NUMBER() OVER(ORDER BY SortColumn) as RNumber from @tbl
			)
			update t set [RowNum] = t0.RNumber
			from @tbl t
			inner join t0 on t0.[CouponID] = t.[CouponID]
		 end
		else
		 begin

			;with t0 as (
				select [CouponID],[RowNum],ROW_NUMBER() OVER(ORDER BY SortColumn desc) as RNumber from @tbl
			)
			update t set [RowNum] = t0.RNumber
			from @tbl t
			inner join t0 on t0.[CouponID] = t.[CouponID]
		 end
	 end

	select CouponID,
		StoreIPListID,
		StoreID,
		eCommCouponID,
		CouponDescription,
		CouponValue,
		MinOrderAmt,
		MaxOrderAmt,
		CouponType,
		CouponTypeName,
		MsgToPopUp,
		ExpirationDate,
		LimitedNumber,
		Uses,
		RevokeStatus,
		DateRevoked,
		DateCreated,
		DateUpdated,
		[RowNum]
	from @tbl
	WHERE RowNum between ((@iPageIndex - 1) * @iPageSize + 1) and @iPageIndex*@iPageSize   

 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspGetCouponList;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = 0

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspGetCouponList')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspGetCouponList',-1, 'SQL update error',207
union all
select 'uspGetCouponList',-2, 'Parameter @biStoreIPListID is NULL or zero',207
union all
select 'uspGetCouponList',-3, 'No record found for @biStoreIPListID {0}',207
union all
select 'uspGetCouponList',-4, 'StoreID {0} has "revoked" status',214
union all
select 'uspGetCouponList',-5, 'Store domain {0} has "revoked" status',214
go