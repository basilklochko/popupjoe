declare @szPassword nvarchar(32)
, @szLogin nvarchar(32)
, @szSQL nvarchar(1024)

set @szLogin = 'PopUpJoeWEBUser'
--set @szLogin = UPPER(@szLogin)
set @szPassword = 'p0PuPj0EwebuSER'

USE [master]

IF NOT EXISTS (SELECT 1 FROM master.sys.server_principals WHERE name = @szLogin)
begin
set @szSQL= 'CREATE LOGIN ' + @szLogin + ' WITH PASSWORD=N''' + @szPassword+''', DEFAULT_DATABASE=[ppjdb], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF'
EXECUTE sp_executesql @szSQL
end

USE [ppjdb]

IF NOT EXISTS (SELECT 1 FROM sys.database_principals WHERE name = @szLogin)
begin
set @szSQL= 'CREATE USER ' + @szLogin + ' FOR LOGIN ' + @szLogin
exec sp_executesql @szSQL
exec sp_addrolemember N'db_datareader', @szLogin
exec sp_addrolemember N'db_datawriter', @szLogin
exec sp_addrolemember N'db_executor', @szLogin
end
else
begin
exec sp_change_users_login 'Update_One', @szLogin, @szLogin
end

print 'Login: ' + @szLogin
print 'Password: ' + @szPassword

