
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspUserMenuRecordUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspUserMenuRecordUpdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Update status of dbo.tblUserMenu record 
	   Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/15/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biUserMenuID is NULL, zero or negative
	@iRetCode = -3, Parameter @iStatus is null, zero or negative
	@iRetCode = -4, Wrong value of parameter @iStatus {0}
	@iRetCode = -5, No record for update found in dbo.tblUserMenu with {0}
	@iRetCode = -6, Record in dbo.tblUserMenu with {0} has "revoke" status

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/15/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@biUserMenuID [bigint]
		,@iStatus [int]
		,@biUserUpd [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biUserMenuID = 1
		,@iStatus = 1
		,@biUserUpd = 1

	exec [svc].[uspUserMenuRecordUpdate]
		@biUserMenuID
		,@iStatus
		,@biUserUpd
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspUserMenuRecordUpdate]
	@biUserMenuID [bigint]
	,@iStatus [int]
	,@biUpdUser [bigint]
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserMenuRecordUpdate')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)

declare @bRevokeStatus [bit]

-- Validate parameters
if coalesce(@biUserMenuID,0)<=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else if coalesce(@iStatus,0)<=0
	set @iRetCode = -3
else if not exists (select 1 from dbo.vwLookUp lk (nolock) 
		where lk.LookUpName = 'MenuStatus'
		and lk.LookUpSubType = @iStatus)
	select @iRetCode = -4
		,@szErrSubst = cast(@iStatus as [varchar] (10))
else 	
 begin
	select @bRevokeStatus = RevokeStatus from dbo.tblUserMenu (nolock)
		where UserMenuID = @biUserMenuID
 
	if @@ROWCOUNT = 0
		select @iRetCode = -5
			,@szErrSubst = cast(@biUserMenuID as [varchar] (30))
	else if @bRevokeStatus = 1
		select @iRetCode = -6
			,@szErrSubst = cast(@biUserMenuID as [varchar] (30))
 end

if @iRetCode = 0
 begin

	 
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	  SAVE TRANSACTION uspUserMenuRecordUpdate;
	else
	  BEGIN TRANSACTION;

	update dbo.tblUserMenu set 
		[Status] = @iStatus
			,DateUpdated = GETDATE()
		,UserUpdated = @biUpdUser
		where UserMenuID = @biUserMenuID

	-- commit transaction
	if @TranCounter = 0 -- no outer transaction
	 begin
	  COMMIT TRANSACTION;		
	  set @TranCounter = NULL
	 end 
		
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspUserMenuRecordUpdate;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspUserMenuRecordUpdate')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspUserMenuRecordUpdate',-1, 'SQL update error',205
union all
select 'uspUserMenuRecordUpdate',-2, 'Parameter @biUserMenuID is NULL, zero or negative',206
union all
select 'uspUserMenuRecordUpdate',-3, 'Parameter @iStatus is null, zero or negative',206
union all
select 'uspUserMenuRecordUpdate',-4, 'Wrong value of parameter @iStatus {0}',206
union all
select 'uspUserMenuRecordUpdate',-5, 'No record for update found in dbo.tblUserMenu with {0}',206
union all
select 'uspUserMenuRecordUpdate',-6, 'Record in dbo.tblUserMenu with {0} has "revoke" status',216
go

