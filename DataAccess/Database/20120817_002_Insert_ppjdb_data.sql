
use ppjdb;
go

insert into [dbo].[tblLookUp](
	[LookUpShortName], 
	[LookUpSubType])
select 'DBMail',1
union all
select 'CouponType',2
union all
select 'BillingPeriod',3
union all
select 'BillingType',4
union all
select 'eCommerceType',5
union all
select 'MenuStatus',6

insert into [dbo].[tblLookUp](
	[LookUpShortName], 
	[LookUpSubType],
	[LookUpSubID])
select 'ppj',1,1
union all
select 'Fixed Amount',1,2
union all
select '% of OrderTotal',2,2
union all
select 'Free Shipping',3,2
union all
select 'Quarterly',1,3
union all
select 'Monthly',2,3
union all
select 'Bi-Weekly',3,3
union all
select 'Weekly',4,3
union all
select 'Fixed Amount per BullingPeriod',1,4
union all
select 'Fixed Amount per PopUp',2,4
union all
select 'Fixed Amount per Submit',3,4
union all
select '% OrderTotal on PopUp',4,4
union all
select '% OrderTotal on Submit',5,4
union all
select 'Magento',1,5
union all
select 'Invisible',1,6
union all
select 'Read-only',2,6
union all
select 'Editable',3,6
union all
select 'Full control',4,6
GO

insert into dbo.tblAppMenu (MenuName,ParentID,PageLocation,DefaultStatus)
select '$|img src=''~/Images/glyphicons/png/glyphicons_041_charts_white.png'' width=''15px'' /|$ Dashboard',0,'Home/Index',2
union all
select '$|img src=''~/Images/glyphicons/png/glyphicons_202_shopping_cart_white.png'' width=''15px'' /|$ Stores',0,'',2
union all
select '$|img src=''~/Images/glyphicons/png/glyphicons_202_shopping_cart.png'' width=''15px'' /|$ Store Management',2,'Store/StoreManagement',2
union all
select '$|img src=''~/Images/glyphicons/png/glyphicons_280_settings.png'' width=''15px'' /|$ Domain Management',2,'Store/DomainManagement',2
union all
select '$|img src=''~/Images/glyphicons/png/glyphicons_037_credit.png'' width=''15px'' /|$ Billing Settings',2,'Store/BillingSettings',1
union all
select '$|img src=''~/Images/glyphicons/png/glyphicons_066_tags.png'' width=''15px'' /|$ Coupon Settings',2,'Store/CouponSettings',2
union all
select '$|img src=''~/Images/glyphicons/png/glyphicons_043_group.png'' width=''15px'' /|$ User Management',2,'Account/UserManagement',3
union all
select '$|img src=''~/Images/glyphicons/png/glyphicons_029_notes_2.png'' width=''15px'' /|$ Invoices',4,'Store/Invoices',1
union all
select '$|img src=''~/Images/glyphicons/png/glyphicons_040_stats_white.png'' width=''15px'' /|$ PopUpJoe Statistics',0,'Report/PopUpJoeStatistics',2
GO

insert into [dbo].[tblEmailTemplate] ([TemplateName],[Subject],[EmailFormat],[Template])
values ('ChangePassword','Password has been changed','html',
'<html>
<head>
<title>CBM - Customer Billing Module--Development Environment</title>
<style type="text/css">
a:link, a:visited { color: #3b3a33; text-decoration: underline; }
a:active, a:hover { color: #3b3a33; text-decoration: none; }
body { background-color: #ffffff; color: #3b3a33; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; margin: 4px 4px 4px 4px; }
td { color: #3a5333; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 12px; padding: 10px 10px 10px 10px; border: 1px solid #3A5333;}
td.green { background-color: #3a5333; color: #ffffff; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 12px; padding: 10px 10px 10px 10px; border: 1px solid #3A5333;}
</style>
</head>

<body>
  <table width="400" cellspacing="0" cellpadding="0" border="0">
    <tr>
    <td class="green">PopUpJoe - application--Development Environment notification</td>
    </tr>
    <tr>
    <td><b>Account update</b><br><br>Your personal information on PopUpJoe - application--Development Environment was changed on [dt].
    <br><br>Your username is your email address and <br>Your new password is: <b>[pwd]</b>
    <br><br>Please access PopUpJoe - application--Development Environment at <a href="http://198.101.233.96">http://www.ppj.com</a><br><br>
    <i>This is an automated email. Please do not reply.</i></td>
    </tr>
  </table>
</body>
</html>')
GO

insert into [dbo].[tblStore](
	[eCommerceType],
	[StoreName],
	[PostalName],
	[Address1],
	[Address2],
	[City],
	[StateProvince],
	[PostalCode],
	[CountryCode],
	[ParentStoreID],
	[StoreNode],
	[SessionExpiration],
	[ContractExpirationDate]
)
values (
	0,
	'PopUpJoe',
	'PopUpJoe LLC',
	'216 Maspeth Avenue',
	NULL,
	'Brooklyn',
	'NY',
	'11211',
	'USA',
	0,
	cast('/' as hierarchyid),
	0,
	'2070-01-01'
)
go

declare
@ieCommerceType [int]
,@szStoreName [nvarchar] (255)
,@szPostalName [nvarchar] (255)
,@szAddress1 [nvarchar] (255)
,@szAddress2 [nvarchar] (255)
,@szCity [nvarchar] (100)
,@szStateProvince [nvarchar] (64)
,@szPostalCode [nvarchar] (64)
,@szCountryCode [varchar] (3)
,@biParentStoreID [bigint]
,@iSessionExpiration [int]
,@dtContractExpirationDate [datetime]
,@biUpdUser [bigint]
   	,@biStoreID [bigint]
,@iRetCode [int]
,@szMsgTier1 [nvarchar] (2048)
,@szMsgTier2 [nvarchar] (1024)

select
@ieCommerceType = 1
,@szStoreName = 'eStore1.com'
,@biParentStoreID = 1


exec [svc].[uspStoreInsert]
@ieCommerceType
,@szStoreName
,@szPostalName
,@szAddress1
,@szAddress2
,@szCity
,@szStateProvince
,@szPostalCode
,@szCountryCode
,@biParentStoreID
,@iSessionExpiration
,@dtContractExpirationDate
,@biUpdUser
   	,@biStoreID output
,@iRetCode output
,@szMsgTier1 output   
,@szMsgTier2 output  

select
@ieCommerceType = 1
,@szStoreName = 'eStore2.com'
,@biParentStoreID = 1
exec [svc].[uspStoreInsert]
@ieCommerceType
,@szStoreName
,@szPostalName
,@szAddress1
,@szAddress2
,@szCity
,@szStateProvince
,@szPostalCode
,@szCountryCode
,@biParentStoreID
,@iSessionExpiration
,@dtContractExpirationDate
,@biUpdUser
   	,@biStoreID output
,@iRetCode output
,@szMsgTier1 output   
,@szMsgTier2 output  


select
@ieCommerceType = 1
,@szStoreName = 'eStore3.com'
,@biParentStoreID = 1
exec [svc].[uspStoreInsert]
@ieCommerceType
,@szStoreName
,@szPostalName
,@szAddress1
,@szAddress2
,@szCity
,@szStateProvince
,@szPostalCode
,@szCountryCode
,@biParentStoreID
,@iSessionExpiration
,@dtContractExpirationDate
,@biUpdUser
   	,@biStoreID output
,@iRetCode output
,@szMsgTier1 output   
,@szMsgTier2 output  

select
@ieCommerceType = 1
,@szStoreName = 'eStore2Child1.com'
,@biParentStoreID = 3
exec [svc].[uspStoreInsert]
@ieCommerceType
,@szStoreName
,@szPostalName
,@szAddress1
,@szAddress2
,@szCity
,@szStateProvince
,@szPostalCode
,@szCountryCode
,@biParentStoreID
,@iSessionExpiration
,@dtContractExpirationDate
,@biUpdUser
   	,@biStoreID output
,@iRetCode output
,@szMsgTier1 output   
,@szMsgTier2 output  

select
@ieCommerceType = 1
,@szStoreName = 'eStore2Child2.com'
,@biParentStoreID = 3
exec [svc].[uspStoreInsert]
@ieCommerceType
,@szStoreName
,@szPostalName
,@szAddress1
,@szAddress2
,@szCity
,@szStateProvince
,@szPostalCode
,@szCountryCode
,@biParentStoreID
,@iSessionExpiration
,@dtContractExpirationDate
,@biUpdUser
   	,@biStoreID output
,@iRetCode output
,@szMsgTier1 output   
,@szMsgTier2 output  
GO

if not exists (select 1 from [dbo].[tblUser] (nolock) where UserEmail = 'admin@ppj.com')
insert into [dbo].[tblUser](
	[StoreID],
	[FirstName],
	[LastName],
	[UserEmail],
	[Password],
	[SecureQuestion],
	[SecureAnswer])
values (	
	1,
	'ppj',
	'admin',
	'admin@ppj.com',
	'test',
	'test',
	'test')
go

declare @biTmpStoreID [bigint]
select @biTmpStoreID = storeid from dbo.tblStore where storename ='eStore1.com'
if coalesce(@biTmpStoreID,0)<>0
 begin
	insert into [dbo].[tblUser](
		[StoreID],
		[FirstName],
		[LastName],
		[UserEmail],
		[Password],
		[SecureQuestion],
		[SecureAnswer])
	values (	
		@biTmpStoreID,
		'eStore1',
		'admin',
		'admin@eStore1.com',
		'test',
		'test',
		'test')
 end

select @biTmpStoreID = storeid from dbo.tblStore where storename ='eStore2.com'
if coalesce(@biTmpStoreID,0)<>0
 begin
	insert into [dbo].[tblUser](
		[StoreID],
		[FirstName],
		[LastName],
		[UserEmail],
		[Password],
		[SecureQuestion],
		[SecureAnswer])
	values (	
		@biTmpStoreID,
		'eStore2',
		'admin',
		'admin@eStore2.com',
		'test',
		'test',
		'test')
 end

select @biTmpStoreID = storeid from dbo.tblStore where storename ='eStore3.com'
if coalesce(@biTmpStoreID,0)<>0
 begin
	insert into [dbo].[tblUser](
		[StoreID],
		[FirstName],
		[LastName],
		[UserEmail],
		[Password],
		[SecureQuestion],
		[SecureAnswer])
	values (	
		@biTmpStoreID,
		'eStore3',
		'admin',
		'admin@eStore3.com',
		'test',
		'test',
		'test')
 end

select @biTmpStoreID = storeid from dbo.tblStore where storename ='eStore2Child1.com'
if coalesce(@biTmpStoreID,0)<>0
 begin
	insert into [dbo].[tblUser](
		[StoreID],
		[FirstName],
		[LastName],
		[UserEmail],
		[Password],
		[SecureQuestion],
		[SecureAnswer])
	values (	
		@biTmpStoreID,
		'eStore2Child1',
		'admin',
		'admin@eStore2Child1.com',
		'test',
		'test',
		'test')
 end

select @biTmpStoreID = storeid from dbo.tblStore where storename ='eStore2Child2.com'
if coalesce(@biTmpStoreID,0)<>0
 begin
	insert into [dbo].[tblUser](
		[StoreID],
		[FirstName],
		[LastName],
		[UserEmail],
		[Password],
		[SecureQuestion],
		[SecureAnswer])
	values (	
		@biTmpStoreID,
		'eStore2',
		'admin',
		'admin@eStore2Child2.com',
		'test',
		'test',
		'test')
 end
GO

-- at the very end, when all users are in DB
insert into dbo.tblUserMenu (UserID,AppMenuID,[Status])
select u.UserID,m.AppMenuID,m.DefaultStatus
from dbo.tblUser u (nolock), dbo.tblAppMenu m (nolock)
order by u.UserID,m.AppMenuID
GO


INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(201,'Login failed please retype credentials and repeat again.','Login failed please retype credentials and repeat again.','Aug  8 2012  9:10:30:727AM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(202,'Login failed please contact with PPJ administrator.','Login failed please contact with PPJ administrator.','Aug  8 2012  9:10:30:727AM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(203,'Login failed please contact with PPJ administrator.','Login failed please contact with PPJ administrator.','Aug  8 2012  2:33:00:063PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(204,'Update status failed please contact with PPJ administrator.','Update status failed please contact with PPJ administrator.','Aug  9 2012  2:11:05:017PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(211,'First name is empty.','First name is empty.','Aug 10 2012  1:40:46:527PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(212,'Last name is empty.','Last name is empty.','Aug 10 2012  1:40:46:537PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(213,'Email is empty.','Email is empty.','Aug 10 2012  1:40:46:560PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(214,'Store has "revoke" status.','Store has "revoke" status.','Aug 10 2012  1:40:46:563PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(215,'User has "revoke" status.','User has "revoke" status.','Aug 10 2012  1:40:46:567PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(216,'Answer does not match with recorded value.','Answer does not match with recorded value.','Aug 10 2012  1:40:46:570PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(217,'New email value {0} already in use.','New email value {0} already in use.','Aug 10 2012  1:40:46:573PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(205,'Error happened during insert, please contact with PPJ administrator.','Error happened during insert, please contact with PPJ administrator.','Aug 10 2012  1:45:10:683PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(206,'Error happened during update, please contact with PPJ administrator.','Error happened during update, please contact with PPJ administrator.','Aug 10 2012  1:48:51:997PM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(218,'Contract has been expired for this store.','Contract has been expired for this store.','Aug 13 2012  9:07:24:027AM',0,NULL,NULL)
INSERT INTO [tblUIMessage] ([UIErrCode],[MsgTier2],[MsgTier3],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES(207,'Error happened in SQL SP, please contact with PPJ administrator.','Error happened in SQL SP, please contact with PPJ administrator.','Aug 13 2012 11:57:23:183AM',0,NULL,NULL)
GO

INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateLogin',-1,'SQL update error',201,'Aug  8 2012  9:10:30:733AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateLogin',-2,'parameter @szUserEmail is NULL or empty',201,'Aug  8 2012  9:10:30:733AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateLogin',-3,'parameter @szPassword is NULL or empty',201,'Aug  8 2012  9:10:30:733AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateLogin',-4,'User not found',201,'Aug  8 2012  9:10:30:733AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateLogin',-5,'User has revoke status',202,'Aug  8 2012  9:10:30:733AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateLogin',-6,'Store has revoke status',202,'Aug  8 2012  9:10:30:733AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateLogin',-7,'parameter @szPassword does not match, secure question has been return',203,'Aug  8 2012  9:10:30:733AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateSecureAnswer',-1,'SQL update error',202,'Aug  8 2012  9:11:07:167AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateSecureAnswer',-2,'parameter @szUserEmail is NULL or empty',202,'Aug  8 2012  9:11:07:167AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateSecureAnswer',-3,'parameter @szSecureAnswer is NULL or empty',202,'Aug  8 2012  9:11:07:167AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateSecureAnswer',-4,'User not found',202,'Aug  8 2012  9:11:07:167AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateSecureAnswer',-5,'User has revoke status',202,'Aug  8 2012  9:11:07:167AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateSecureAnswer',-6,'Store has revoke status',202,'Aug  8 2012  9:11:07:167AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateSecureAnswer',-7,'parameter @szSecureAnswer does not match with secure answer',202,'Aug  8 2012  9:11:07:167AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateSecureAnswer',-8,'Email template not found',202,'Aug  8 2012  9:11:07:167AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspValidateSecureAnswer',-9,'error in [dbo].[uspSendEmail]',202,'Aug  8 2012  9:11:07:167AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangePassword',-1,'SQL update error',202,'Aug  9 2012  2:11:04:960PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangePassword',-2,'parameter @biUserID is NULL or zero',202,'Aug  9 2012  2:11:04:960PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangePassword',-3,'parameter @szPasswordOld is NULL or empty',202,'Aug  9 2012  2:11:04:960PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangePassword',-4,'User not found',202,'Aug  9 2012  2:11:04:960PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangePassword',-5,'User has revoke status',202,'Aug  9 2012  2:11:04:960PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangePassword',-6,'Store has revoke status',202,'Aug  9 2012  2:11:04:960PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangePassword',-7,'parameter @szPasswordOld does not match',202,'Aug  9 2012  2:11:04:960PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangePassword',-8,'Email template not found',202,'Aug  9 2012  2:11:04:960PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangePassword',-9,'error in [dbo].[uspSendEmail]',202,'Aug  9 2012  2:11:04:960PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserGetMenuOptions',-1,'SQL update error',202,'Aug  9 2012  2:11:05:050PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserGetMenuOptions',-2,'parameter @biUserID is NULL or zero',202,'Aug  9 2012  2:11:05:050PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserGetMenuOptions',-3,'User not found',202,'Aug  9 2012  2:11:05:050PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetLookUpData',-1,'SQL update error',202,'Aug 10 2012 12:02:06:803PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetLookUpData',-2,'Parameter @szLookUpName is NULL or empty',202,'Aug 10 2012 12:02:06:803PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetLookUpData',-3,'No data found',202,'Aug 10 2012 12:02:06:803PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-1,'SQL update error',206,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-2,'Parameter @biUserID is null or zero',206,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-3,'Parameter @biStoreID is null or zero',206,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-4,'Parameter @szFirstName is null or empty',211,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-5,'Parameter @szLastName is null or empty',212,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-6,'Parameter @szUserEmail is null or empty',213,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-7,'Store not found',206,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-8,'Store has "revoke" status',214,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-9,'User not found',206,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-10,'User has "revoke" status',215,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-11,'Secure answer does not match',216,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserUpdate',-12,'New user''s email already in use',217,'Aug 10 2012  1:49:19:773PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserInsert',-1,'SQL update error',205,'Aug 13 2012  9:07:11:043AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserInsert',-2,'Parameter @biStoreID is null or zero',205,'Aug 13 2012  9:07:11:043AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserInsert',-3,'Parameter @szFirstName is null or empty',211,'Aug 13 2012  9:07:11:043AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserInsert',-4,'Parameter @szLastName is null or empty',212,'Aug 13 2012  9:07:11:043AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserInsert',-5,'Parameter @szUserEmail is null or empty',213,'Aug 13 2012  9:07:11:043AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserInsert',-6,'Store not found',205,'Aug 13 2012  9:07:11:043AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserInsert',-7,'Store has "revoke" status',214,'Aug 13 2012  9:07:11:043AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserInsert',-8,'New user''s email already in use',217,'Aug 13 2012  9:07:11:043AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserInsert',-9,'Error in svc.uspUserChangePassword SP',205,'Aug 13 2012  9:07:11:043AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreInsert',-1,'SQL update error',205,'Aug 13 2012  9:07:17:073AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreInsert',-2,'Parameter @szStoreName is null or empty',205,'Aug 13 2012  9:07:17:073AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreInsert',-3,'Parameter @ieCommerceType is null or zero',205,'Aug 13 2012  9:07:17:073AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreInsert',-4,'Wrong value of parameter @ieCommerceType',205,'Aug 13 2012  9:07:17:073AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreUpdate',-1,'SQL update error',205,'Aug 13 2012  9:07:24:033AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreUpdate',-2,'Parameter @biStoreID is null or zero',206,'Aug 13 2012  9:07:24:033AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreUpdate',-3,'Parameter @szStoreName is null or empty',206,'Aug 13 2012  9:07:24:033AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreUpdate',-4,'Parameter @ieCommerceType is null or zero',206,'Aug 13 2012  9:07:24:033AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreUpdate',-5,'Wrong value of parameter @ieCommerceType',206,'Aug 13 2012  9:07:24:033AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreUpdate',-6,'Parameter @dtContractExpirationDate is expired',218,'Aug 13 2012  9:07:24:033AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreUpdate',-7,'Parameter @biParentStoreID is null or zero',206,'Aug 13 2012  9:07:24:033AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreUpdate',-8,'Record not found for @biParentStoreID',206,'Aug 13 2012  9:07:24:033AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreUpdate',-9,'Record not found for @bStoreID',206,'Aug 13 2012  9:07:24:033AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreUpdate',-10,'Store has "revoked" status',214,'Aug 13 2012  9:07:24:033AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetUsersStoreList',-1,'SQL update error',207,'Aug 13 2012 11:57:23:187AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetUsersStoreList',-2,'Parameter @biUserID is NULL or zero',207,'Aug 13 2012 11:57:23:187AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetUsersStoreList',-3,'User {0} not found in dbo.tblUser table',207,'Aug 13 2012 11:57:23:187AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetUsersStoreList',-4,'StoreID {0} not found in tblStore table',207,'Aug 13 2012 11:57:23:187AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangeStatus',-1,'SQL update error',207,'Aug 13 2012  4:11:20:843PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangeStatus',-2,'Parameter @biUserID is NULL or zero',207,'Aug 13 2012  4:11:20:843PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangeStatus',-3,'User {0} not found in dbo.tblUser table',207,'Aug 13 2012  4:11:20:843PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangeStatus',-4,'StoreID {0} has "revoked" status',214,'Aug 13 2012  4:11:20:843PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserChangeStatus',-5,'User already has requested status',206,'Aug 13 2012  4:11:20:843PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreUsersList',-1,'SQL update error',207,'Aug 13 2012  4:59:03:783PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreUsersList',-2,'Parameter @biStoreID is NULL or zero',207,'Aug 13 2012  4:59:03:783PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreUsersList',-3,'Store {0} not found in dbo.tblStore table',207,'Aug 13 2012  4:59:03:783PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreUsersList',-4,'StoreID {0} has "revoked" status',214,'Aug 13 2012  4:59:03:783PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreChangeStatus',-1,'SQL update error',207,'Aug 14 2012 12:16:11:633PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreChangeStatus',-2,'Parameter @biStoreID is NULL or zero',207,'Aug 14 2012 12:16:11:633PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreChangeStatus',-3,'Store {0} not found in dbo.tblStore table',207,'Aug 14 2012 12:16:11:633PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreChangeStatus',-4,'Store already has requested status',206,'Aug 14 2012 12:16:11:633PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListChangeStatus',-1,'SQL update error',207,'Aug 14 2012 12:39:13:500PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListChangeStatus',-2,'Parameter @biStoreIPListID is NULL or zero',207,'Aug 14 2012 12:39:13:500PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListChangeStatus',-3,'No record for update found in dbo.tblStoreIPList table',207,'Aug 14 2012 12:39:13:500PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListChangeStatus',-4,'Store {0} has "revoked" status',214,'Aug 14 2012 12:39:13:500PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListChangeStatus',-5,'Record already has requested status',206,'Aug 14 2012 12:39:13:500PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreIPList',-1,'SQL update error',207,'Aug 14 2012 12:56:59:173PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreIPList',-2,'Parameter @biStoreID is NULL or zero',207,'Aug 14 2012 12:56:59:173PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreIPList',-3,'Store {0} not found in dbo.tblStore table',207,'Aug 14 2012 12:56:59:173PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreIPList',-4,'StoreID {0} has "revoked" status',214,'Aug 14 2012 12:56:59:173PM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserMenuRecordUpdate',-1,'SQL update error',205,'Aug 16 2012 11:30:36:883AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserMenuRecordUpdate',-2,'Parameter @biUserMenuID is NULL, zero or negative',206,'Aug 16 2012 11:30:36:883AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserMenuRecordUpdate',-3,'Parameter @iStatus is null, zero or negative',206,'Aug 16 2012 11:30:36:883AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserMenuRecordUpdate',-4,'Wrong value of parameter @iStatus {0}',206,'Aug 16 2012 11:30:36:883AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserMenuRecordUpdate',-5,'No record for update found in dbo.tblUserMenu with {0}',206,'Aug 16 2012 11:30:36:883AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspUserMenuRecordUpdate',-6,'Record in dbo.tblUserMenu with {0} has "revoke" status',216,'Aug 16 2012 11:30:36:883AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreInfo',-1,'SQL update error',207,'Aug 16 2012 11:30:44:277AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreInfo',-2,'Parameter @biStoreID is NULL or zero',207,'Aug 16 2012 11:30:44:277AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetStoreInfo',-3,'No record found in dbo.tblStore for StoreID {0}',207,'Aug 16 2012 11:30:44:277AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetUserInfo',-1,'SQL update error',207,'Aug 16 2012 11:30:49:513AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetUserInfo',-2,'Parameter @biUserID is NULL or zero',207,'Aug 16 2012 11:30:49:513AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetUserInfo',-3,'No record found in dbo.tblUser for UserID {0}',207,'Aug 16 2012 11:30:49:513AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetUserInfo',-4,'User {0} has "revoked" status',215,'Aug 16 2012 11:30:49:513AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspGetUserInfo',-5,'StoreID {0} has "revoked" status',214,'Aug 16 2012 11:30:49:513AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListUpdate',-1,'SQL update error',205,'Aug 16 2012 11:35:14:027AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListUpdate',-2,'Parameter @biStoreIPListID is null or zero',206,'Aug 16 2012 11:35:14:027AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListUpdate',-3,'Parameter @szIPAddress is null or empty',206,'Aug 16 2012 11:35:14:027AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListUpdate',-4,'Parameter @szDomainName is null or empty',206,'Aug 16 2012 11:35:14:027AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListUpdate',-5,'Parameter @iTimrValue1 is null, zero or negative',206,'Aug 16 2012 11:35:14:027AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListUpdate',-6,'Parameter @iTimrValue2 is null, zero or negative',218,'Aug 16 2012 11:35:14:027AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListUpdate',-7,'Record not found for @biStoreIPListID',206,'Aug 16 2012 11:35:14:027AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListUpdate',-8,'Record has "revoke" status',206,'Aug 16 2012 11:35:14:027AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListUpdate',-9,'Record with @bStoreID {0} not found in dbo.tblStore table',206,'Aug 16 2012 11:35:14:027AM',0,NULL,NULL)
INSERT INTO [tblMsgOutput] ([SPName],[RetCode],[MsgTier1],[UIErrCode],[DateCreated],[UserCreated],[DateUpdated],[UserUpdated])VALUES('uspStoreIPListUpdate',-10,'Store has "revoked" status',214,'Aug 16 2012 11:35:14:027AM',0,NULL,NULL)
GO

