
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspStoreUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspStoreUpdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Insert dbo.tblStore record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/12/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreID is null or zero
	@iRetCode = -3, Parameter @szStoreName is null or empty
	@iRetCode = -4, Parameter @ieCommerceType is null or zero
	@iRetCode = -5, Wrong value of parameter @ieCommerceType
	@iRetCode = -6, Parameter @dtContractExpirationDate is expired
	@iRetCode = -7, Parameter @biParentStoreID is null or zero
	@iRetCode = -8, Record not found for @biParentStoreID
	@iRetCode = -9, Record not found for @biStoreID
	@iRetCode = -10, Store has "revoked" status
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/12/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
   		@biStoreID [bigint]
		,@ieCommerceType [int]
		,@szStoreName [nvarchar] (255)
		,@szPostalName [nvarchar] (255)
		,@szAddress1 [nvarchar] (255)
		,@szAddress2 [nvarchar] (255)
		,@szCity [nvarchar] (100)
		,@szStateProvince [nvarchar] (64)
		,@szPostalCode [nvarchar] (64)
		,@szCountryCode [varchar] (3)
		,@biParentStoreID [bigint]
		,@iSessionExpiration [int]
		,@dtContractExpirationDate [datetime]
		,@biUpdUser [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	--select
 --  		@biStoreID = 7
	--	,@ieCommerceType = 1
	--	,@szStoreName = 'eStoreChildChild1.com'
	--	,@szPostalName ='eStoreChildChild1.com'
	--	,@szCountryCode ='USA'
	--	,@biParentStoreID = 3
	--	,@iSessionExpiration = 43400
	--	,@dtContractExpirationDate = '2013-12-31 19:07:31.807'


	--select
 --  		@biStoreID = 5
	--	,@ieCommerceType = 1
	--	,@szStoreName = 'eStoreChild2.com'
	--	,@szPostalName ='eStoreChild2.com'
	--	,@szCountryCode ='USA'
	--	,@biParentStoreID = 7
	--	,@iSessionExpiration = 43400
	--	,@dtContractExpirationDate = '2013-12-31 19:07:31.807'

	select
   		@biStoreID = 7
		,@ieCommerceType = 1
		,@szStoreName = 'eStoreChildChild1.com'
		,@szPostalName ='eStoreChildChild1.com'
		,@szCountryCode ='USA'
		,@biParentStoreID = 4
		,@iSessionExpiration = 43400
		,@dtContractExpirationDate = '2013-12-31 19:07:31.807'



	exec [svc].[uspStoreUpdate]
		@biStoreID 
		,@ieCommerceType
		,@szStoreName
		,@szPostalName
		,@szAddress1
		,@szAddress2
		,@szCity
		,@szStateProvince
		,@szPostalCode
		,@szCountryCode
		,@biParentStoreID
		,@iSessionExpiration
		,@dtContractExpirationDate
		,@biUpdUser
   		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspStoreUpdate]
   	@biStoreID [bigint]
	,@ieCommerceType [int]
	,@szStoreName [nvarchar] (255)
	,@szPostalName [nvarchar] (255) = NULL
	,@szAddress1 [nvarchar] (255) = NULL
	,@szAddress2 [nvarchar] (255) = NULL
	,@szCity [nvarchar] (100) = NULL
	,@szStateProvince [nvarchar] (64) = NULL
	,@szPostalCode [nvarchar] (64) = NULL
	,@szCountryCode [varchar] (3) = 'USA'
	,@biParentStoreID [bigint] = 0
	,@iSessionExpiration [int] = 43200
	,@dtContractExpirationDate [datetime] = NULL
	,@biUpdUser [bigint] = null
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspStoreUpdate')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)
	,@szPostalName = coalesce(@szPostalName,@szStoreName)
	,@szAddress1 = coalesce(@szAddress1,'')
	,@szAddress2 = coalesce(@szAddress2,'')
	,@szCity = coalesce(@szCity,'')
	,@szStateProvince = coalesce(@szStateProvince,'')
	,@szPostalCode = coalesce(@szPostalCode,'')
	,@szCountryCode = coalesce(@szCountryCode,'USA')

	if coalesce(@iSessionExpiration,0) <= 0
	 set @iSessionExpiration = 43200
	 
	--if @dtContractExpirationDate is null
	-- set @dtContractExpirationDate = DATEADD(year,1,getdate())
	--if coalesce(@biParentStoreID,0)=0
	--	select @biParentStoreID= StoreID from dbo.tblStore (nolock) where StoreNode = hierarchyid::GetRoot()

declare @bTmpRevokeStatus [bit]
	,@biTmpParentStoreID [bigint]
	,@ParentStoreNode [hierarchyid] 
	,@TmpStoreNode [hierarchyid] 

declare @hiOld [hierarchyid]
	,@hiNew [hierarchyid]

-- Validate parameters
if	coalesce(@biStoreID,0)<=0
	set @iRetCode = -2
if	coalesce(@szStoreName,'')=''
	set @iRetCode = -3
else if	coalesce(@ieCommerceType,0)<=0
	set @iRetCode = -4
else if	not exists (select 1 from dbo.vwLookUp (nolock) 
	where LookUpName = 'eCommerceType' and LookUpSubType = @ieCommerceType)
	set @iRetCode = -5
else if @dtContractExpirationDate < GETDATE()
	set @iRetCode = -6
else if	coalesce(@biParentStoreID,0)<=0
	set @iRetCode = -7
else if	not exists (select 1 from dbo.tblStore (nolock) where StoreID = @biParentStoreID)
	select @iRetCode = -8
		,@szErrSubst = CAST(@biParentStoreID as [varchar] (30))

if @iRetCode = 0
 begin
 
	select @bTmpRevokeStatus = RevokeStatus
		,@biTmpParentStoreID = ParentStoreID
		,@hiOld = StoreNode
		from dbo.tblStore (nolock)
		where StoreID = @biStoreID
		
	if @@ROWCOUNT = 0
	 begin
	 -- store not found
		select @iRetCode = -9
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 end
	else if coalesce(@bTmpRevokeStatus,0)=1
	 begin
	 -- syore has revoked status
		select @iRetCode = -10
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 end
 end



if @iRetCode = 0
 begin
	 
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	  SAVE TRANSACTION uspStoreUpdate;
	else
	  BEGIN TRANSACTION;

	update dbo.tblStore set
		eCommerceType = @ieCommerceType
		,StoreName = @szStoreName
		,PostalName = @szPostalName
		,Address1 = @szAddress1
		,Address2 = @szAddress2
		,City = @szCity
		,StateProvince = @szStateProvince
		,PostalCode = @szPostalCode
		,CountryCode = @szCountryCode
		,SessionExpiration = @iSessionExpiration
		,ContractExpirationDate = @dtContractExpirationDate
		,DateUpdated = GETDATE()
		,UserUpdated = @biUpdUser
		,ParentStoreID = @biParentStoreID
		where StoreID = @biStoreID 
	 
	if @biTmpParentStoreID <> @biParentStoreID
	 begin
	
		SELECT @hiNew = StoreNode 
			FROM dbo.tblStore (nolock)
			WHERE StoreID = @biParentStoreID ;

		SELECT @hiNew = @hiNew.GetDescendant(max(StoreNode), NULL) 
			FROM dbo.tblStore (nolock) 
			WHERE StoreNode.GetAncestor(1)=@hiNew ;

		UPDATE dbo.tblStore  
			SET StoreNode = StoreNode.GetReparentedValue(@hiOld, @hiNew)
			WHERE StoreNode.IsDescendantOf(@hiOld) = 1 ; 
 
	 end

	-- commit transaction
	if @TranCounter = 0 -- no outer transaction
	 begin
	  COMMIT TRANSACTION;		
	  set @TranCounter = NULL
	 end 
 
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspStoreUpdate;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 218)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 218,
	'Contract has been expired for this store.',
	'Contract has been expired for this store.'
go

-- delete from dbo.tblMsgOutput where SPName = 'uspStoreUpdate'
if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspStoreUpdate')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspStoreUpdate',-1, 'SQL update error',205
union all
select 'uspStoreUpdate',-2, 'Parameter @biStoreID is null or zero',206
union all
select 'uspStoreUpdate',-3, 'Parameter @szStoreName is null or empty',206
union all
select 'uspStoreUpdate',-4, 'Parameter @ieCommerceType is null or zero',206
union all
select 'uspStoreUpdate',-5, 'Wrong value of parameter @ieCommerceType',206
union all
select 'uspStoreUpdate',-6, 'Parameter @dtContractExpirationDate is expired',218
union all
select 'uspStoreUpdate',-7, 'Parameter @biParentStoreID is null or zero',206
union all
select 'uspStoreUpdate',-8, 'Record not found for @biParentStoreID',206
union all
select 'uspStoreUpdate',-9, 'Record not found for @bStoreID',206
union all
select 'uspStoreUpdate',-10, 'Store has "revoked" status',214
go

--select * from dbo.tblUIMessage