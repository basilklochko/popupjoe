
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspUserUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspUserUpdate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Update dbo.tblUser record
	SP has been developed with idea for using from self edit page
	and manage users page
	
	Note:
	For running from self-edit page, parameter @szAnswer is matadory.
	
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/12/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biUserID is null or zero
	@iRetCode = -3, Parameter @biStoreID is null or zero
	@iRetCode = -4, Parameter @szFirstName is null or empty
	@iRetCode = -5, Parameter @szLastName is null or empty
	@iRetCode = -6, Parameter @szUserEmail is null or empty
	@iRetCode = -7, Store not found
	@iRetCode = -8, Store has "revoke" status
	@iRetCode = -9, User not found
	@iRetCode = -10, User has "revoke" status
	@iRetCode = -11, Secure answer does not match
	@iRetCode = -12, New user's email already in use

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/12/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@biUserID [bigint]	
		,@biStoreID [bigint]
		,@szFirstName [nvarchar] (128)
		,@szLastName [nvarchar] (128)
		,@szUserEmail [nvarchar] (512)
		,@bEmailAlertRecipient [bit]
		,@bBillingReportRecipient [bit]
		,@biUpdUser [bigint]
		,@szAnswer [nvarchar] (512)		
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biUserID = 1
		,@biStoreID = 1
		,@szFirstName = 'admin'
		,@szLastName = 'ppjadmin'
		,@szUserEmail = 'admin@ppj.com'

	exec [svc].[uspUserUpdate]
		@biUserID
		,@biStoreID
		,@szFirstName
		,@szLastName
		,@szUserEmail
		,@bEmailAlertRecipient
		,@bBillingReportRecipient
		,@biUpdUser
		,@szAnswer
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

select * from tblUser
*/

CREATE PROCEDURE [svc].[uspUserUpdate]
	@biUserID [bigint]	
	,@biStoreID [bigint]
	,@szFirstName [nvarchar] (128)
	,@szLastName [nvarchar] (128)
	,@szUserEmail [nvarchar] (512)
	,@bEmailAlertRecipient [bit] = 0
	,@bBillingReportRecipient [bit] = 0
	,@biUpdUser [bigint] = null
	,@szAnswer [nvarchar] (512) = NULL	
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspUserUpdate')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bEmailAlertRecipient = coalesce(@bEmailAlertRecipient,0)
	,@bBillingReportRecipient = coalesce(@bBillingReportRecipient,0)
	,@biUpdUser = coalesce(@biUpdUser,@biUserID)
	,@szAnswer = coalesce(@szAnswer,'') 

declare @bTmpRevokeStatus [bit]
	,@szTmpUserEmail [nvarchar] (512)
	,@szTmpAnswer [nvarchar] (512)

-- Validate parameters
if coalesce(@biUserID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else if	coalesce(@biStoreID,0)=0
	set @iRetCode = -3
else if	coalesce(@szFirstName,'')=''
	set @iRetCode = -4
else if	coalesce(@szLastName,'')=''
	set @iRetCode = -5
else if	coalesce(@szUserEmail,'')=''
	set @iRetCode = -6
else
 begin
 
-- validate Store parameters	
	select @bTmpRevokeStatus = RevokeStatus 
		from dbo.tblStore s (nolock)
		where StoreID = @biStoreID
		
	if @@ROWCOUNT = 0	
	 begin
-- store not found
		select @iRetCode = -7,
			@szErrSubst = CAST(@biStoreID as [varchar] (10))
	 end
	else if coalesce(@bTmpRevokeStatus,0)=1
	 begin
-- store has "revoke" status
		select @iRetCode = -8,
			@szErrSubst = CAST(@biStoreID as [varchar] (10))
	 end
	else
	 begin
-- validate use info
		select @bTmpRevokeStatus = RevokeStatus
			,@szTmpUserEmail = UserEmail
			,@szTmpAnswer = SecureAnswer
			from dbo.tblUser (nolock)
			where UserID = @biUserID

		if @@ROWCOUNT = 0	
		 begin
-- user not found
			select @iRetCode = -9,
				@szErrSubst = CAST(@biUserID as [varchar] (10))
		 end
		else if coalesce(@bTmpRevokeStatus,0)=1
		 begin
-- user has "revoke" status
			select @iRetCode = -10,
				@szErrSubst = CAST(@biUserID as [varchar] (10))
		 end
		else if @szAnswer<>'' and @szAnswer<> @szTmpAnswer
		 begin
			select @iRetCode = -11
		 end
		else if @szTmpUserEmail <> @szUserEmail and 
			exists (select 1 from dbo.tblUser (nolock) where UserEmail = @szUserEmail)
		 begin
-- new email already in use
			select @iRetCode = -12,
				@szErrSubst = @szUserEmail
		 end	
	 end
 end

if @iRetCode = 0
 begin
 
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspUserUpdate;
		else
		  BEGIN TRANSACTION;

		update dbo.tblUser set 
			StoreID = @biStoreID,
			FirstName = @szFirstName,
			LastName=@szLastName,
			UserEmail=@szUserEmail,
			EmailAlertRecipient=@bEmailAlertRecipient,
			BillingReportRecipient=@bBillingReportRecipient,
			DateUpdated = GETDATE(),
			UserUpdated = @biUpdUser
			where UserID = @biUserID

		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;		
		  set @TranCounter = NULL
		 end 
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspUserUpdate;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUserID

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 206)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 206,
	'Error happened during update, please contact with PPJ administrator.',
	'Error happened during update, please contact with PPJ administrator.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 205)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 205,
	'Error happened during insert, please contact with PPJ administrator.',
	'Error happened during insert, please contact with PPJ administrator.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 211)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 211,
	'First name is empty.',
	'First name is empty.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 212)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 212,
	'Last name is empty.',
	'Last name is empty.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 213)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 213,
	'Email is empty.',
	'Email is empty.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 214)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 214,
	'Store has "revoke" status.',
	'Store has "revoke" status.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 215)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 215,
	'User has "revoke" status.',
	'User has "revoke" status.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 216)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 216,
	'Answer does not match with recorded value.',
	'Answer does not match with recorded value.'
go
if not exists (select 1 from dbo.tblUIMessage (nolock) where UIErrCode = 217)
insert into dbo.tblUIMessage (UIErrCode,MsgTier2,MsgTier3)
select 217,
	'New email value {0} already in use.',
	'New email value {0} already in use.'
go

if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspUserUpdate')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspUserUpdate',-1, 'SQL update error',206
union all
select 'uspUserUpdate',-2, 'Parameter @biUserID is null or zero',206
union all
select 'uspUserUpdate',-3, 'Parameter @biStoreID is null or zero',206
union all
select 'uspUserUpdate',-4, 'Parameter @szFirstName is null or empty',211
union all
select 'uspUserUpdate',-5, 'Parameter @szLastName is null or empty',212
union all
select 'uspUserUpdate',-6, 'Parameter @szUserEmail is null or empty',213
union all
select 'uspUserUpdate',-7, 'Store not found',206
union all
select 'uspUserUpdate',-8, 'Store has "revoke" status',214
union all
select 'uspUserUpdate',-9, 'User not found',206
union all
select 'uspUserUpdate',-10, 'User has "revoke" status',215
union all
select 'uspUserUpdate',-11, 'Secure answer does not match',216
union all
select 'uspUserUpdate',-12, 'New user''s email already in use',217
go

