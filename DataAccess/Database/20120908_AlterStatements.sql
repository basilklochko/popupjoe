use ppjdb;
go

alter table dbo.tblSessionList drop column StoreIPListID 
go

alter table dbo.tblWSLog drop column PopUpJoeSessionID 
go
alter table dbo.tblWSLog add SessionID [bigint] not null,
	CustomerIPAddress [varchar] (64) null,
	CustomerLoginName [nvarchar] (128) null,
	FirstTimeVisitor [bit] not null default (0),
	FirstTimeBuyer [bit] not null default (0),
	LoggedIn [bit] not null default (0)
go

ALTER TABLE [dbo].[tblWSLog]  WITH CHECK ADD  CONSTRAINT [FK_tblWSLog_SessionID] FOREIGN KEY([SessionID])
REFERENCES [dbo].[tblSessionList] ([SessionID])
GO
ALTER TABLE [dbo].[tblWSLog] CHECK CONSTRAINT [FK_tblWSLog_SessionID]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblWSLog_StoreID]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblWSLog]'))
ALTER TABLE [dbo].[tblWSLog] DROP CONSTRAINT [FK_tblWSLog_StoreID]
GO

