

USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspBSettingChangeStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspBSettingChangeStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Reset BSetting's RevokeStatus
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/25/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, parameter @biBSettingID is NULL or zero
	@iRetCode = -3, Record with @biBSettingID={0} not nound in dbo.tblBSetting
	@iRetCode = -4, Record already has requested status
	@iRetCode = -5, Could not find Store for parameter @biStoreID {0}
	@iRetCode = -6, Store with StoreID={0} has "revoke" status
	
  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/25/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------
	declare
		@biBSettingID [bigint]
		,@bRevokeStatus [bit]
		,@biUpdUser [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biBSettingID = 1
		,@bRevokeStatus = 0

	exec [svc].[uspBSettingChangeStatus]
		@biBSettingID
		,@bRevokeStatus
		,@biUpdUser
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspBSettingChangeStatus]
	@biBSettingID [bigint]
	,@bRevokeStatus [bit] = 1
	,@biUpdUser [bigint] = null
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspBSettingChangeStatus')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

declare @bStoreRevokeStatus [bit],
	@bStoreIPListRevokeStatus [bit],
	@bTmpRevokeStatus [bit]	,
	@dtTmpDateRevoked [datetime],
	@biStoreIPListID [bigint],
	@biStoreID [bigint]	
	

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@bRevokeStatus = coalesce(@bRevokeStatus,1)
	,@biUpdUser = coalesce(@biUpdUser,0)
	
-- Validate parameters
if coalesce(@biBSettingID,'')=0
-- Parameter is NULL or zero
	set @iRetCode = -2
 begin
 
 
	select 
		@dtTmpDateRevoked = b.DateRevoked,
		@bTmpRevokeStatus = b.RevokeStatus,
		@biStoreID = b.StoreID
		from dbo.tblBSetting b (nolock)
		where BSettingID = @biBSettingID
		
	if @@ROWCOUNT = 0
	 begin
-- no user found
		set @iRetCode = -3
	 end
	else if @bTmpRevokeStatus=@bRevokeStatus
	 begin
-- record has requested status	 
		select @iRetCode = -4,
			@szErrSubst='RevokeStatus='+CAST(@bRevokeStatus as [varchar] (10))			
	 end
	else
	 begin
-- validate Revoke status on Store and StoreIPList levels

		select @bStoreRevokeStatus = s.RevokeStatus 
			from dbo.tblStore s (nolock) 
			where s.StoreID = @biStoreID

		if @@ROWCOUNT = 0 
			select @iRetCode = -5
				,@szErrSubst = CAST(@biBSettingID as [varchar] (30))
		else if coalesce(@bStoreRevokeStatus,0) = 1
			select @iRetCode = -6
				,@szErrSubst = CAST(@biStoreID as [varchar] (30))

	 end
 end

if @iRetCode = 0
 begin
-- update Coupon Status

		-- do we have outer transaction?
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspBSettingChangeStatus;
		else
		  BEGIN TRANSACTION;

		update dbo.tblBSetting set [RevokeStatus] = @bRevokeStatus,
			DateRevoked = case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			DateUpdated = GETDATE(),
			UserUpdated = @biUpdUser
			where BSettingID = @biBSettingID

		insert into dbo.tblStatusLog (SourceTable,
			SourceTableID,
			OldStatus,
			OldDateRevoked,
			NewStatus,
			NewDateRevoked,
			DateCreated,
			UserCreated)
		values ('tblBSetting',
			@biBSettingID,
			@bTmpRevokeStatus,
			@dtTmpDateRevoked,
			@bRevokeStatus,
			case when @bRevokeStatus = 1 then GETDATE() else NULL end,
			GETDATE(),
			@biUpdUser)
			
		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;
		  set @TranCounter = NULL
		 end 
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	begin
	IF @TranCounter = 0 -- Transaction started in procedure.
	ROLLBACK TRANSACTION;
	ELSE
	IF XACT_STATE() <> -1 -- roll back to the savepoint
	ROLLBACK TRANSACTION uspBSettingChangeStatus;
	end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

if exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspBSettingChangeStatus')
delete from dbo.tblMsgOutput where SPName = 'uspBSettingChangeStatus'
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspBSettingChangeStatus',-1, 'SQL update error',207
union all
select 'uspBSettingChangeStatus',-2, 'Parameter @biBSettingID is NULL or zero',207
union all
select 'uspBSettingChangeStatus',-3, 'Billing setting with @biBSettingID={0} not found in dbo.tblBSetting table',207
union all
select 'uspBSettingChangeStatus',-4, 'Billing setting already has requested status',206
union all
select 'uspBSettingChangeStatus',-5, 'Could not find Store for parameter @biStoreID',207
union all
select 'uspBSettingChangeStatus',-6, 'Store with StoreID={0} has "revoke" status',207
go
