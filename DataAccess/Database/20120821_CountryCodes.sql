
alter table dbo.tblLookUp add col1 [nvarchar] (128) null,
col2 [nvarchar] (128) null,
col3 [nvarchar] (128) null
go

insert into [dbo].[tblLookUp](
	[LookUpShortName], 
	[LookUpSubType])
select 'CountryCode',7
union all
select 'StateCode',8
go

insert into [dbo].[tblLookUp](
	[LookUpShortName], 
	[LookUpSubType],
	[LookUpSubID])
select 'USA',1,7
union all
select 'CAN',2,7
go

insert into [dbo].[tblLookUp](
	[LookUpShortName], 
	[LookUpDescription],
	[LookUpSubType],
	[LookUpSubID],
	[Col1])
select 'AL','Alabama',1,8,'USA'union allselect 'AK','Alaska',2,8,'USA'union allselect 'AZ','Arizona',3,8,'USA'union allselect 'AR','Arkansas',4,8,'USA'union allselect 'CA','California',5,8,'USA'union allselect 'CO','Colorado',6,8,'USA'union allselect 'CT','Connecticut',7,8,'USA'union allselect 'DE','Delaware',8,8,'USA'union allselect 'FL','Florida',9,8,'USA'union allselect 'GA','Georgia',10,8,'USA'union allselect 'HI','Hawaii',11,8,'USA'union allselect 'ID','Idaho',12,8,'USA'union allselect 'IL','Illinois',13,8,'USA'union allselect 'IN','Indiana',14,8,'USA'union allselect 'IA','Iowa',15,8,'USA'union allselect 'KS','Kansas',16,8,'USA'union allselect 'KY','Kentucky',17,8,'USA'union allselect 'LA','Louisiana',18,8,'USA'union allselect 'ME','Maine',19,8,'USA'union allselect 'MD','Maryland',20,8,'USA'union allselect 'MA','Massachusetts',21,8,'USA'union allselect 'MI','Michigan',22,8,'USA'union allselect 'MN','Minnesota',23,8,'USA'union allselect 'MS','Mississippi',24,8,'USA'union allselect 'MO','Missouri',25,8,'USA'union allselect 'MT','Montana',26,8,'USA'union allselect 'NE','Nebraska',27,8,'USA'union allselect 'NV','Nevada',28,8,'USA'union allselect 'NH','New Hampshire',29,8,'USA'union allselect 'NJ','New Jersey',30,8,'USA'union allselect 'NM','New Mexico',31,8,'USA'union allselect 'NY','New York',32,8,'USA'union allselect 'NC','North Carolina',33,8,'USA'union allselect 'ND','North Dakota',34,8,'USA'union allselect 'OH','Ohio',35,8,'USA'union allselect 'OK','Oklahoma',36,8,'USA'union allselect 'OR','Oregon',37,8,'USA'union allselect 'PA','Pennsylvania',38,8,'USA'union allselect 'RI','Rhode Island',39,8,'USA'union allselect 'SC','South Carolina',40,8,'USA'union allselect 'SD','South Dakota',41,8,'USA'union allselect 'TN','Tennessee',42,8,'USA'union allselect 'TX','Texas',43,8,'USA'union allselect 'UT','Utah',44,8,'USA'union allselect 'VT','Vermont',45,8,'USA'union allselect 'VA','Virginia',46,8,'USA'union allselect 'WA','Washington',47,8,'USA'union allselect 'WV','West Virginia',48,8,'USA'union allselect 'WI','Wisconsin',49,8,'USA'union allselect 'WY','Wyoming',50,8,'USA'go
insert into [dbo].[tblLookUp](
	[LookUpShortName], 
	[LookUpDescription],
	[LookUpSubType],
	[LookUpSubID],
	[Col1])
select 'AB','Alberta',1,8,'CAN'
union all
select 'BC','British Columbia',2,8,'CAN'
union all
select 'LB','Labrador',3,8,'CAN'
union all
select 'MB','Manitoba',4,8,'CAN'
union all
select 'NB','New Brunswick',5,8,'CAN'
union all
select 'NF','Newfoundland',6,8,'CAN'
union all
select 'NS','Nova Scotia',7,8,'CAN'
union all
select 'NU','Nunavut',8,8,'CAN'
union all
select 'NW','North West Terr.',9,8,'CAN'	 	
union all
select 'ON','Ontario',10,8,'CAN'
union all
select 'PE','Prince Edward Is.',11,8,'CAN'	 	
union all
select 'QC','Quebec',12,8,'CAN'
union all
select 'SK','Saskatchewen',13,8,'CAN'	 	
union all
select 'YU','Yukon',14,8,'CAN'