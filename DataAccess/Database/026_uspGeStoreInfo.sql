
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspGetStoreInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspGetStoreInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Returns Store data from dbo.tblStore table base 
	on value of @biStoreID parameter
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/15/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreID is NULL or zero
	@iRetCode = -3, Store {0} not found in dbo.tblStore table

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/15/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare @biStoreID [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select @biStoreID = 3

	exec [svc].[uspGetStoreInfo]
		@biStoreID
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspGetStoreInfo]
	@biStoreID [bigint]
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspGetStoreInfo')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''

declare @tbl table (StoreID [bigint] not null primary key,
	eCommerceType [int] not null,
	StoreName [nvarchar] (255) not null,
	PostalName [nvarchar] (255) not null,
	Address1 [nvarchar] (255) null,
	Address2 [nvarchar] (255) null,
	City [nvarchar] (100) not null,
	StateProvince [nvarchar] (64) not null,
	PostalCode [nvarchar] (64) not null,
	CountryCode [varchar] (3) not null,
	RevokeStatus [bit] not null,
	DateRevoked [datetime] null,
	ParentStoreID [bigint] null,
	SessionExpiration [int] null,
	ContractExpirationDate [datetime] not null,
	DateCreated [datetime] null,
	DateUpdated [datetime] null)	

-- Validate parameters
if coalesce(@biStoreID,0)=0
-- Parameter is NULL or zero
	set @iRetCode = -2
else 	
 begin
	insert into @tbl (
		StoreID,
		eCommerceType,
		StoreName,
		PostalName,
		Address1,
		Address2,
		City,
		StateProvince,
		PostalCode,
		CountryCode,
		RevokeStatus,
		DateRevoked,
		ParentStoreID,
		SessionExpiration,
		ContractExpirationDate,
		DateCreated,
		DateUpdated)
	select 
		StoreID,
		eCommerceType,
		StoreName,
		PostalName,
		Address1,
		Address2,
		City,
		StateProvince,
		PostalCode,
		CountryCode,
		RevokeStatus,
		DateRevoked,
		ParentStoreID,
		SessionExpiration,
		ContractExpirationDate,
		DateCreated,
		DateUpdated
		from dbo.tblStore (nolock) 
		where StoreID = @biStoreID
	
	if @@ROWCOUNT = 0 
		select @iRetCode = -3
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
 end

if @iRetCode = 0
 begin
	select 
		t.StoreID,
		t.eCommerceType,
		coalesce(lk.LookUpShortName,'') as eCommerceName,
		t.StoreName,
		t.PostalName,
		t.Address1,
		t.Address2,
		t.City,
		t.StateProvince,
		t.PostalCode,
		t.CountryCode,
		t.RevokeStatus,
		t.DateRevoked,
		t.ParentStoreID,
		t.SessionExpiration,
		t.ContractExpirationDate,
		t.DateCreated,
		t.DateUpdated
	from @tbl t
	left join dbo.vwLookUp lk (nolock) on lk.LookUpName = 'eCommerceType'
		and lk.LookUpSubType = t.eCommerceType
	
 end
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspGetStoreInfo;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = 0

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspGetStoreInfo')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspGetStoreInfo',-1, 'SQL update error',207
union all
select 'uspGetStoreInfo',-2, 'Parameter @biStoreID is NULL or zero',207
union all
select 'uspGetStoreInfo',-3, 'No record found in dbo.tblStore for StoreID {0}',207
go