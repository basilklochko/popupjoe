
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspBSettingInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspBSettingInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Insert dbo.tblBSetting record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/25/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreID is null or zero
	@iRetCode = -3, Parameter @biBillingPeriod is null, zero/negative or not found in BillingPeriod LookUp table
	@iRetCode = -4, Parameter @iBillingType is null, zero/negative or not found in BillingType LookUp table
	@iRetCode = -5, Parameter @iBillingUnit is null, zero/negative
	@iRetCode = -6, Parameter @nBillingRate is null, zero/negative
	@iRetCode = -7, Parameter @dtBillingDate is null	
	@iRetCode = -8, Parameter @biStoreID {0} is not in tblStore table
	@iRetCode = -9, Store with @biStoreID={0} has "revoke" status

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/24/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@biStoreID [bigint]
		,@biBillingPeriod [bigint] 
		,@iBillingType [int]
		,@iBillingUnit [int]
		,@nBillingRate [decimal] (7,4)
		,@dtBillingDate [datetime]
		,@biUpdUser [bigint]
   		,@biBSettingID [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select
		@biStoreID = 1
		,@biBillingPeriod = 1
		,@iBillingType = 2
		,@iBillingUnit = 1000
		,@nBillingRate =50.00
		,@dtBillingDate = '2012-01-01'

	exec [svc].[uspBSettingInsert]
		@biStoreID
		,@biBillingPeriod
		,@iBillingType
		,@iBillingUnit
		,@nBillingRate
		,@dtBillingDate
		,@biUpdUser
   		,@biBSettingID output
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select
		@biBSettingID as [BSettingID]
		,@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/


CREATE PROCEDURE [svc].[uspBSettingInsert]
	@biStoreID [bigint]
	,@biBillingPeriod [bigint] 
	,@iBillingType [int]
	,@iBillingUnit [int]
	,@nBillingRate [decimal] (7,4)
	,@dtBillingDate [datetime]
	,@biUpdUser [bigint] = null
   	,@biBSettingID [bigint]	output
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspBSettingInsert')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)
	,@biBillingPeriod = coalesce(@biBillingPeriod,0)
	,@iBillingType = coalesce(@iBillingType,0)
	,@iBillingUnit = coalesce(@iBillingUnit,0)
	,@nBillingRate = coalesce(@nBillingRate,0)
   	,@biBSettingID = 0

declare @bTmpRevokeStatus [bit]

-- Validate parameters

if	coalesce(@biStoreID,0)=0
	set @iRetCode = -2
else if @biBillingPeriod <= 0 or not exists (select 1 	
			from dbo.vwLookUp vl (nolock) 
			where vl.LookUpSubType = @biBillingPeriod
			and vl.LookUpName = 'BillingPeriod')
	select @iRetCode = -3
		,@szErrSubst = CAST(@biBillingPeriod as [varchar] (10))		
else if @iBillingType <= 0 or not exists (select 1 	
			from dbo.vwLookUp vl (nolock) 
			where vl.LookUpSubType = @iBillingType
			and vl.LookUpName = 'BillingType')
	select @iRetCode = -4
		,@szErrSubst = CAST(@iBillingType as [varchar] (10))	
else if @iBillingUnit <= 0
	set @iRetCode = -5
else if @nBillingRate <= 0
	set @iRetCode = -6
else if @dtBillingDate is null
	set @iRetCode = -7
else
 begin

	select @bTmpRevokeStatus = RevokeStatus 
		from dbo.tblStore (nolock) 
		where StoreID = @biStoreID 
		
	if @@ROWCOUNT = 0
	 begin
		select @iRetCode = -8
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 end
	else if coalesce(@bTmpRevokeStatus,0) = 1
	 begin
		select @iRetCode = -9
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 end 
 end

if @iRetCode = 0
 begin

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	  SAVE TRANSACTION uspBSettingInsert;
	else
	  BEGIN TRANSACTION;
	
	insert into dbo.tblBSetting (
		StoreID,
		BillingPeriod,
		BillingType,
		BillingUnit,
		BillingRate,
		BillingDate,
		RevokeStatus,
		DateCreated,
		UserCreated)
	values (
		@biStoreID,
		@biBillingPeriod,
		@iBillingType,
		@iBillingUnit,
		@nBillingRate,
		@dtBillingDate,
		0,
		getdate(),
		@biUpdUser)

	set @biBSettingID = SCOPE_IDENTITY()

	-- commit transaction
	if @TranCounter = 0 -- no outer transaction
	 begin
	  COMMIT TRANSACTION;		
	  set @TranCounter = NULL
	 end 

 end 	
else
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspBSettingInsert;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

if exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspBSettingInsert')
delete from dbo.tblMsgOutput where SPName = 'uspBSettingInsert'

if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspBSettingInsert')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspBSettingInsert',-1, 'SQL update error',205
union all
select 'uspBSettingInsert',-2, 'Parameter @biStoreID is null or zero',205
union all
select 'uspBSettingInsert',-3, 'Parameter @biBillingPeriod is null, zero/negative or not found in BillingPeriod LookUp table',205
union all
select 'uspBSettingInsert',-4, 'Parameter @iBillingType is null, zero/negative or not found in BillingType LookUp table',205
union all
select 'uspBSettingInsert',-5, 'Parameter @iBillingUnit is null, zero/negative',205
union all
select 'uspBSettingInsert',-6, 'Parameter @nBillingRate is null, zero/negative',205
union all
select 'uspBSettingInsert',-7, 'Parameter @dtBillingDate is null',205
union all
select 'uspBSettingInsert',-8, 'Parameter @biStoreID {0} is not in tblStore table',205
union all
select 'uspBSettingInsert',-9, 'Store with @biStoreID={0} has "revoke" status',205
go

