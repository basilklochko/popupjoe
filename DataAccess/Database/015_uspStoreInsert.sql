
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspStoreInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspStoreInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Insert dbo.tblStore record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/12/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @szStoreName is null or empty
	@iRetCode = -3, Parameter @ieCommerceType is null or zero
	@iRetCode = -4, Wrong value of parameter @ieCommerceType

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/12/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@ieCommerceType [int]
		,@szStoreName [nvarchar] (255)
		,@szPostalName [nvarchar] (255)
		,@szAddress1 [nvarchar] (255)
		,@szAddress2 [nvarchar] (255)
		,@szCity [nvarchar] (100)
		,@szStateProvince [nvarchar] (64)
		,@szPostalCode [nvarchar] (64)
		,@szCountryCode [varchar] (3)
		,@biParentStoreID [bigint]
		,@iSessionExpiration [int]
		,@dtContractExpirationDate [datetime]
		,@biUpdUser [bigint]
   		,@biStoreID [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)



	--select
	--	@ieCommerceType = 1
	--	,@szStoreName = 'eStore.com'

	select
		@ieCommerceType = 1
		,@szStoreName = 'eStoreChildChild1.com'
		,@biParentStoreID = 5

	--select
	--	@ieCommerceType = 1
	--	,@szStoreName = 'eStoreChild2.com'
	--	,@biParentStoreID = 3 


	exec [svc].[uspStoreInsert]
		@ieCommerceType
		,@szStoreName
		,@szPostalName
		,@szAddress1
		,@szAddress2
		,@szCity
		,@szStateProvince
		,@szPostalCode
		,@szCountryCode
		,@biParentStoreID
		,@iSessionExpiration
		,@dtContractExpirationDate
		,@biUpdUser
   		,@biStoreID output
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

select StoreNode.ToString(),* from tblStore
select * from dbo.tblSQLAudit

*/

CREATE PROCEDURE [svc].[uspStoreInsert]
	@ieCommerceType [int]
	,@szStoreName [nvarchar] (255)
	,@szPostalName [nvarchar] (255)
	,@szAddress1 [nvarchar] (255)
	,@szAddress2 [nvarchar] (255)
	,@szCity [nvarchar] (100)
	,@szStateProvince [nvarchar] (64)
	,@szPostalCode [nvarchar] (64)
	,@szCountryCode [varchar] (3) = 'USA'
	,@biParentStoreID [bigint] = 0
	,@iSessionExpiration [int] = 43200
	,@dtContractExpirationDate [datetime] = null
	,@biUpdUser [bigint] = null
   	,@biStoreID [bigint]	output
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspStoreInsert')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)
	,@szPostalName = coalesce(@szPostalName,@szStoreName)
	,@szAddress1 = coalesce(@szAddress1,'')
	,@szAddress2 = coalesce(@szAddress2,'')
	,@szCity = coalesce(@szCity,'')
	,@szStateProvince = coalesce(@szStateProvince,'')
	,@szPostalCode = coalesce(@szPostalCode,'')
	,@szCountryCode = coalesce(@szCountryCode,'USA')
	,@biStoreID = 0

	if coalesce(@biParentStoreID,0)=0
		select @biParentStoreID= StoreID from dbo.tblStore (nolock) where StoreNode = hierarchyid::GetRoot()
	 
	if coalesce(@iSessionExpiration,0) <= 0
	 set @iSessionExpiration = 43200
	 
	if @dtContractExpirationDate is null
	 set @dtContractExpirationDate = DATEADD(year,1,getdate())

declare @bTmpRevokeStatus [bit]
	,@ParentStoreNode [hierarchyid] 
	,@TmpStoreNode [hierarchyid] 


-- Validate parameters
if	coalesce(@szStoreName,'')=''
	set @iRetCode = -2
else if	coalesce(@ieCommerceType,0)<=0
	set @iRetCode = -3
else if	not exists (select 1 from dbo.vwLookUp (nolock) 
	where LookUpName = 'eCommerceType' and LookUpSubType = @ieCommerceType)
	set @iRetCode = -4

if @iRetCode = 0
 begin

	select @ParentStoreNode = StoreNode 
		from dbo.tblStore (nolock) 
		where StoreID = @biParentStoreID 
	 
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
	SET @TranCounter = @@TRANCOUNT;
	IF @TranCounter > 0
	  SAVE TRANSACTION uspStoreInsert;
	else
	  BEGIN TRANSACTION;

      SELECT @TmpStoreNode = max(StoreNode) 
      FROM dbo.tblStore 
      WHERE StoreNode.GetAncestor(1) = @ParentStoreNode ;
 
	  insert into dbo.tblStore (
		eCommerceType
		,StoreName
		,PostalName
		,Address1
		,Address2
		,City
		,StateProvince
		,PostalCode
		,CountryCode
		,SessionExpiration
		,ContractExpirationDate
		,DateCreated
		,UserCreated
		,StoreNode
		,ParentStoreID)
	values (	
		@ieCommerceType
		,@szStoreName
		,@szPostalName
		,@szAddress1
		,@szAddress2
		,@szCity
		,@szStateProvince
		,@szPostalCode
		,@szCountryCode
		,@iSessionExpiration
		,@dtContractExpirationDate
		,getdate()
		,@biUpdUser
		,@ParentStoreNode.GetDescendant(@TmpStoreNode, NULL)
		,@biParentStoreID)

		set @biStoreID = SCOPE_IDENTITY()
		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;		
		  set @TranCounter = NULL
		 end 
		
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspStoreInsert;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

-- delete from dbo.tblMsgOutput where SPName = 'uspStoreInsert'
if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspStoreInsert')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspStoreInsert',-1, 'SQL update error',205
union all
select 'uspStoreInsert',-2, 'Parameter @szStoreName is null or empty',205
union all
select 'uspStoreInsert',-3, 'Parameter @ieCommerceType is null or zero',205
union all
select 'uspStoreInsert',-4, 'Wrong value of parameter @ieCommerceType',205
go
