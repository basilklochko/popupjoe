
USE [ppjdb]
GO

alter table dbo.tblStore alter column ParentStoreID [bigint] null
go

IF NOT EXISTS (select 1 from Information_SCHEMA.columns (nolock)
	where Table_Schema = 'dbo' 
	and Table_name='tblStore' 
	and column_name='StoreLevelID')
 begin
  ALTER TABLE [dbo].[tblStore] ADD [StoreLevelID] as StoreNode.GetLevel()
 end
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblStore]') AND name = N'UI_tblStore_StoreGUID')
DROP INDEX [UI_tblStore_StoreGUID] ON [dbo].[tblStore] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE INDEX UI_tblStore_StoreGUID ON dbo.tblStore(StoreGUID) ;
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[tblStore]') AND name = N'UI_tblStore_StoreLevelID_StoreNode')
DROP INDEX [UI_tblStore_StoreLevelID_StoreNode] ON [dbo].[tblStore] WITH ( ONLINE = OFF )
GO
CREATE UNIQUE INDEX UI_tblStore_StoreLevelID_StoreNode ON dbo.tblStore(StoreLevelID, StoreNode) ;
GO


