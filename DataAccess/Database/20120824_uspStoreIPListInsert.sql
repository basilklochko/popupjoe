
USE [ppjdb]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[svc].[uspStoreIPListInsert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [svc].[uspStoreIPListInsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
  -----------------------------------------------------------------------
  File Name		:
  Description	:	Insert dbo.tblStore record
  Author        :   Sergey Morozov
  Copyright     :   (c) 2012 PPJ
  Incept		:	08/24/2012
  -----------------------------------------------------------------------
  Description/Purpose:

  Output values:
	@iRetCode =  0, sucess,
	@iRetCode = -1, SQL update error, detail info recorded into dbo.tblSQLAudit table
	@iRetCode = -2, Parameter @biStoreID is null or zero
	@iRetCode = -3, Parameters @szIPAddress and @szDomainName are null or empty
	@iRetCode = -4, Parameter @iTimerValue1 is null or zero
	@iRetCode = -5, Parameter @iTimerValue2 is null or zero
	@iRetCode = -6, Parameter @biStoreID {0} is not in tblStore table
	@iRetCode = -7, Store with @biStoreID={0} has "revoke" status

  -----------------------------------------------------------------------
  Change Log:
	Author			Date         Change
  -----------------------------------------------------------------------
  Sergiy Morozov 08/24/2012		development
  
  -----------------------------------------------------------------------
  Sample Script
  -----------------------------------------------------------------------

	declare
		@biStoreID [bigint]
		,@szIPAddress [varchar] (64)
		,@szDomainName [nvarchar] (128)
		,@iTimerValue1 [int]
		,@iTimerValue2 [int]
		,@biUpdUser [bigint]
   		,@biStoreIPListID [bigint]
		,@iRetCode [int]
		,@szMsgTier1 [nvarchar] (2048)
		,@szMsgTier2 [nvarchar] (1024)

	select
		@biStoreID = 4
		,@szDomainName = 'eStore.com'
		,@iTimerValue1 = 300
		,@iTimerValue2 = 900

	exec [svc].[uspStoreIPListInsert]
		@biStoreID
		,@szIPAddress
		,@szDomainName
		,@iTimerValue1
		,@iTimerValue2
		,@biUpdUser
   		,@biStoreIPListID output
		,@iRetCode output
		,@szMsgTier1 output   
		,@szMsgTier2 output  

	select 	
		@iRetCode as [RetCode] 
		,@szMsgTier1 as [RetMsg1]   
		,@szMsgTier2 as [RetMsg2]   		

*/

CREATE PROCEDURE [svc].[uspStoreIPListInsert]
	@biStoreID [bigint]
	,@szIPAddress [varchar] (64) = NULL 
	,@szDomainName [nvarchar] (128) = NULL
	,@iTimerValue1 [int] = 0
	,@iTimerValue2 [int] = 0
	,@biUpdUser [bigint] = null
   	,@biStoreIPListID [bigint]	output
    ,@iRetCode [int] output
    ,@szMsgTier1 [nvarchar] (2048) output   
    ,@szMsgTier2 [nvarchar] (1024) output  
AS
BEGIN

SET NOCOUNT ON;

DECLARE @iIsProcessComplete int
, @szProcessName VARCHAR(50)
, @ErrorMessage nvarchar(2048) 
, @ErrorNumber int
, @ErrorSeverity int
, @ErrorState int
, @ErrorProcedure nvarchar(2048)
, @ErrorLine int
, @TranCounter int
, @RowCount int
, @Msg nvarchar(4000)
, @iJobSetID int
, @szInsertedBy varchar(16)
, @szErrSubst [varchar] (100)

-- Get current stored proc name
SELECT @szProcessName = ISNULL(OBJECT_NAME(@@PROCID), 'uspStoreIPListInsert')

BEGIN TRY
-----------------------------------
--BEGIN WORK
-----------------------------------

select @iRetCode = 0
	,@szMsgTier1 =''
	,@szMsgTier2=''
	,@szErrSubst = ''
	,@biUpdUser = coalesce(@biUpdUser,0)
	,@szIPAddress = coalesce(@szIPAddress,'')
	,@szDomainName = coalesce(@szDomainName,'')
	,@iTimerValue1 = coalesce(@iTimerValue1,0)
	,@iTimerValue2 = coalesce(@iTimerValue2,0)
   	,@biStoreIPListID = 0

declare @bTmpRevokeStatus [bit]

-- Validate parameters

if	coalesce(@biStoreID,0)=0
	set @iRetCode = -2
else if @szIPAddress = '' and @szDomainName = ''
	set @iRetCode = -3
else if @iTimerValue1 = 0
	set @iRetCode = -4
else if @iTimerValue2 = 0
	set @iRetCode = -5
else
 begin

	select @bTmpRevokeStatus = RevokeStatus 
		from dbo.tblStore (nolock) 
		where StoreID = @biStoreID 
		
	if @@ROWCOUNT = 0
	 begin
		select @iRetCode = -6
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 end
	else if coalesce(@bTmpRevokeStatus,0) = 1
	 begin
		select @iRetCode = -7
			,@szErrSubst = CAST(@biStoreID as [varchar] (30))
	 end 
	else
	 begin
		SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
		SET @TranCounter = @@TRANCOUNT;
		IF @TranCounter > 0
		  SAVE TRANSACTION uspStoreIPListInsert;
		else
		  BEGIN TRANSACTION;
		
		insert into dbo.tblStoreIPList (
			StoreID,
			IPAddress,
			DomainName,
			TimerValue1,
			TimerValue2,
			RevokeStatus,
			DateCreated,
			UserCreated)
		values (
			@biStoreID,
			@szIPAddress,
			@szDomainName,
			@iTimerValue1,
			@iTimerValue2,
			0,
			getdate(),
			@biUpdUser)

		set @biStoreIPListID = SCOPE_IDENTITY()

		-- commit transaction
		if @TranCounter = 0 -- no outer transaction
		 begin
		  COMMIT TRANSACTION;		
		  set @TranCounter = NULL
		 end 
	 end 	
 end

if @iRetCode <> 0
 begin
	select @szMsgTier1=replace(t.MsgTier1,'{0}',''''+@szErrSubst+''''),
		@szMsgTier2=replace(u.MsgTier2,'{0}',''''+@szErrSubst+''''),
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode
 end

 RETURN 0
END TRY
BEGIN CATCH
-- If failed rollback
-- get extended error information
	select @ErrorNumber  = ERROR_NUMBER(), @ErrorSeverity  = ERROR_SEVERITY(), @ErrorState = ERROR_STATE()
	, @ErrorProcedure  = ERROR_PROCEDURE(), @ErrorLine  = ERROR_LINE(), @ErrorMessage  = ERROR_MESSAGE()
	set @Msg = '; ErrorNumber = ' + cast(@ErrorNumber as varchar(4))
	+ ', ErrorSeverity  = '       + cast(@ErrorSeverity as varchar(4))
	+ ', ErrorState = '                 + cast(@ErrorState as varchar(4))
	+ ', ErrorProcedure = '       + cast(@ErrorProcedure as varchar(128))
	+ ', ErrorLine = '                  + cast(@ErrorLine as varchar(4))
	+ ', ErrorMessage = '         + cast(@ErrorMessage as varchar(2048))

	IF @TranCounter is not null
	 begin
		IF @TranCounter = 0 -- Transaction started in procedure.
		 ROLLBACK TRANSACTION;
		ELSE IF XACT_STATE() <> -1 -- roll back to the savepoint
		 ROLLBACK TRANSACTION uspStoreIPListInsert;
	 end

	-- log error
	EXECUTE  dbo.uspRecordSQLAudit
	@biProcessID = -1
	,@iErrorNumber = @ErrorNumber
	,@iErrorSeverity = @ErrorSeverity
	,@iErrorState = @ErrorState
	,@szErrorProcedure = @ErrorProcedure
	,@iErrorLine = @ErrorLine
	,@szErrorMessage = @ErrorMessage
	,@szParameters = ''
	,@iInserted = @biUpdUser

	select @iRetCode = -1 

	select @szMsgTier1=@ErrorMessage,
		@szMsgTier2=u.MsgTier2,
		@iRetCode= case when u.[UIErrCode] is null then @iRetCode else t.[UIErrCode] end
		from [dbo].[tblMsgOutput] t (nolock)
		left join [dbo].[tblUIMessage] u (nolock) on u.[UIErrCode] = t.[UIErrCode]
		where t.SPName = @szProcessName
		and t.RetCode = @iRetCode

RETURN -1
END CATCH

END
GO

-- delete from dbo.tblMsgOutput where SPName = 'uspStoreIPListInsert'
if not exists (select 1 from dbo.tblMsgOutput (nolock) where SPName = 'uspStoreIPListInsert')
insert into dbo.tblMsgOutput (SPName,RetCode,MsgTier1,UIErrCode)
select 'uspStoreIPListInsert',-1, 'SQL update error',205
union all
select 'uspStoreIPListInsert',-2, 'Parameter @biStoreID is null or zero',205
union all
select 'uspStoreIPListInsert',-3, 'Parameters @szIPAddress and @szDomainName are null or empty',205
union all
select 'uspStoreIPListInsert',-4, 'Parameter @iTimerValue1 is null or zero',205
union all
select 'uspStoreIPListInsert',-5, 'Parameter @iTimerValue2 is null or zero',205
union all
select 'uspStoreIPListInsert',-6, 'Parameter @biStoreID {0} is not in tblStore table',205
union all
select 'uspStoreIPListInsert',-7, 'Store with @biStoreID={0} has "revoke" status',205
go

